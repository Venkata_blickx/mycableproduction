package com.blickx.id.generator;

import java.util.List;

import org.apache.log4j.Logger;
import org.hibernate.Query;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;

public class EmployeeIdGenerator {

	@Autowired
	SessionFactory sfactory;
	
	private Logger logger = Logger.getLogger(EmployeeIdGenerator.class);
	
	public String getNextSid(String client_id){
		
	String employee_id = client_id + "0000E";
		
		try{
			Session session=sfactory.openSession();
			Query q=session.createQuery("from Employee emp where emp.client_id=?").setString(0, client_id);
			int size=q.list().size();
			if(size!=0){
				Query query=session.createQuery("select max(employee_id) from Employee emp where emp.client_id=?").setString(0, client_id);
				List list=query.list();
				System.out.println(list.size());
				Object o=list.get(0);
				String id="";
	            id=o.toString();
				String p2=id.substring(13);
				int x=Integer.parseInt(p2);
				x=x+1;
				if(x<=9){employee_id=employee_id+"00"+x;}
				else if(x<=99){employee_id=employee_id+"0"+x;}
				else if(x<=999){employee_id=employee_id+x;}
			}else{
				employee_id = employee_id + "001";
			}
		}catch (Exception e) {
			
			logger.error("From ID generator"+e);
			
		}
		return employee_id;
	}
}
