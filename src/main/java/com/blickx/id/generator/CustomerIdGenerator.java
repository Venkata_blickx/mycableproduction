package com.blickx.id.generator;

import java.util.List;

import org.apache.log4j.Logger;
import org.hibernate.Query;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;

public class CustomerIdGenerator {

	@Autowired
	SessionFactory sfactory;
	
	private Logger logger = Logger.getLogger(CustomerIdGenerator.class);
	
	public String getNextSid(String client_id){
		
	String customer_id = client_id;
		
		try{
			Session session=sfactory.openSession();
			Query q=session.createQuery("select customer_id from Customer cus where client_id = ?").setString(0, client_id);
			int size=q.list().size();
			if(size!=0){
				Query query=session.createQuery("select max(customer_id) from Customer cus where cus.client_id = ?").setParameter(0, client_id);
				List list=query.list();
				System.out.println(list.size());
				Object o=list.get(0);
				String id="";
	            id=o.toString();
				String p2=id.substring(8);
				//System.out.println("p2"+p2);
				int x=Integer.parseInt(p2);
				x=x+1;
				if(x<=9){customer_id=client_id+"0000000"+x;}
				else if(x<=99){customer_id=client_id+"000000"+x;}
				else if(x<=999){customer_id=client_id+"00000"+x;}
				else if(x<=9999){customer_id=client_id+"0000"+x;}
			}else{
				customer_id = customer_id + "00000001";
			}
		}catch (Exception e) {
			
			logger.error("From ID generator"+e);
			
		}
		return customer_id;
		
		
		
	}
	
}
