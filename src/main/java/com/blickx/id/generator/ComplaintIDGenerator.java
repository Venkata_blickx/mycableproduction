package com.blickx.id.generator;

import java.math.BigInteger;
import java.util.Calendar;
import java.util.List;

import org.apache.log4j.Logger;
import org.hibernate.Query;
import org.hibernate.Session;

public class ComplaintIDGenerator {

	public String getNextSid(Session session, String client_id){
		
		Calendar now = Calendar.getInstance();
		
		int year = now.get(Calendar.YEAR);
		BigInteger month = (BigInteger) session.createSQLQuery("select month(current_date)").uniqueResult();
		String s = Integer.toString(year).substring(2);
		String s1 = "";
		if (month.intValue() < 4) {
			s1 = Integer.toString(year - 1).substring(2) + "-"
					+ s;
		} else {
			s1 = Integer.parseInt(Integer.toString(year).substring(2))
					+ "-" + Integer.toString((Integer.parseInt(s) + 1));
		}
		
		
		String comp_id = client_id + "C" + s1 +"/";
		
		Logger logger = Logger.getLogger(ComplaintIDGenerator.class);
			
			try{
				// session=sfactory.openSession();
				Query q = session.createQuery("from ComplaintEntity com where com.client_id = ?").setString(0, client_id);
				int size = q.list().size();
				
				if(size!=0 ){
					Query query=session.createSQLQuery("select com.complaint_id from complaint com where com.client_id = ? order by com.row_id desc limit 1")
							.setParameter(0, client_id);
					//List list = query.list();
					String recipt_no = (String) query.uniqueResult();
					System.out.println(recipt_no.substring(0,15));
					if(comp_id.equals(recipt_no.substring(0,15))){
					
						
						int id_no = Integer.parseInt(recipt_no.substring(15));
						comp_id = comp_id + (id_no+1);
					}else{
						comp_id = comp_id + "1";
					}	
					
					
					
				}else{
					comp_id = comp_id + "1";
				}
				}catch (Exception e) {
				
				logger.error("From ID generator"+e);
				
			}
			System.out.println(comp_id);
			return comp_id;
		
	/*String complaint_id = client_id + "C";
	
	Logger logger = Logger.getLogger(ComplaintIDGenerator.class);
		
		try{
			// session=sfactory.openSession();
			
			
			
			Query q=session.createQuery("from ComplaintEntity com where com.client_id = ?").setString(0, client_id);
			int size=q.list().size();
			if(size!=0){
				Query query=session.createQuery("select max(complaint_id) from ComplaintEntity com where com.client_id = ?").setParameter(0, client_id);
				List list=query.list();
				System.out.println(list.size());
				logger.info("**************");
				Object o=list.get(0);
				String id="";
	            id=o.toString();
	            
	            String p2 = id.length()==16?id.substring(9):id;
				//String p2=id.substring(9);
				
				//System.out.println("p2"+p2);
				int x=Integer.parseInt(p2);
				x=x+1;
				if(x<=9){complaint_id=complaint_id+"000000"+x;}
				else if(x<=99){complaint_id=complaint_id+"00000"+x;}
				else if(x<=999){complaint_id=complaint_id+"0000"+x;}
				else if(x<=9999){complaint_id=complaint_id+"000"+x;}
				else if(x<=99999){complaint_id=complaint_id+"00"+x;}
				else if(x<=999999){complaint_id=complaint_id+"0"+x;}
			}else{
				complaint_id = complaint_id + "0000001";
			}
		}catch (Exception e) {
			
			logger.error("From ID generator"+e);
			
		}
		return complaint_id;*/
	}
	
}
