package com.blickx.id.generator;

import java.math.BigInteger;
import java.util.Calendar;
import java.util.List;

import org.apache.log4j.Logger;
import org.hibernate.Query;
import org.hibernate.Session;

public class BillIdGenerator {

	public String getNextSid(Session session, String client_id){
		
		Calendar now = Calendar.getInstance();
		
		int year = now.get(Calendar.YEAR);
		BigInteger month = (BigInteger) session.createSQLQuery("select month(current_date)").uniqueResult();
		String s = Integer.toString(year).substring(2);
		String s1 = "";
		if (month.intValue() < 4) {
			s1 = Integer.toString(year - 1).substring(2) + "-"
					+ s;
		} else {
			s1 = Integer.parseInt(Integer.toString(year).substring(2))
					+ "-" + Integer.toString((Integer.parseInt(s) + 1));
		}
		
		
		String bill_id = s1 +"/";
		
		Logger logger = Logger.getLogger(BillIdGenerator.class);
			
			try{
				// session=sfactory.openSession();
				Query q = session.createQuery("from Billing bil where bil.client_id = ? and bil.paid_amount>0").setString(0, client_id);
				int size = q.list().size();
				
				if(size!=0 ){
					Query query=session.createSQLQuery("SELECT ifnull(MAX(CONVERT(SUBSTRING_INDEX(bill_recipt_no, '/', -1), unsigned INTEGER)), 0) "+ 
					"FROM billing where paid_amount > 0 and client_id = ?").setParameter(0, client_id);
					/*SELECT ifnull(MAX(CONVERT(SUBSTRING_INDEX(bill_recipt_no, '/', -1), unsigned INTEGER)), 0) 
					FROM billing where paid_amount > 0 and client_id = "MYA00015"*/
					Query query2=session.createSQLQuery("select bill_recipt_no from billing where client_id = ? and paid_amount>0 order by date desc limit 1").setParameter(0, client_id);
					//List list = query.list();
					String recipt_no = (String) query2.uniqueResult();
					BigInteger id_no = (BigInteger) query.uniqueResult();
					//System.out.println(recipt_no.substring(0,6));
					if(bill_id.equals(recipt_no.substring(0,6))){
					
						
						//int id_no = Integer.parseInt(recipt_no.substring(6));
						bill_id = bill_id + (id_no.intValue()+1);
					}else{
						bill_id = bill_id + "1";
					}	
					
					
					
				}else{
					bill_id = bill_id + "1";
				}
				}catch (Exception e) {
				
				logger.error("From ID generator " + e);
				
			}
			logger.info("bill_id " + bill_id);
			return bill_id;
	}
}
