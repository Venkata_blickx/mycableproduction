package com.blickx.copyitems.to.objects;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Map;

import com.blickx.to.PaymentTxEntityDTO;

public class GetPaymentTxDTO {

	public static PaymentTxEntityDTO getPaymentTxDTO(Map<String, String> input) throws ParseException {
		
		PaymentTxEntityDTO payEntity = new PaymentTxEntityDTO();
		
		SimpleDateFormat format = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
		
		payEntity.setResponseCode(Integer.parseInt(input.get("ResponseCode")));
		payEntity.setResponseMsg(input.get("ResponseMessage"));
		payEntity.setCreatedDate(format.parse(input.get("DateCreated")));
		payEntity.setPaymentId(input.get("PaymentID"));
		payEntity.setMerchantRefNo(input.get("MerchantRefNo"));
		payEntity.setClient_id(input.get("Description").substring(0, 8));
		payEntity.setAmount(input.get("Amount"));
		payEntity.setMode(input.get("Mode"));
		payEntity.setBillingName(input.get("BillingName"));
		payEntity.setBillingAddress(input.get("BillingAddress"));
		payEntity.setBillingCity(input.get("BillingCity"));
		payEntity.setBillingState(input.get("BillingState"));
		payEntity.setBillingPostalCode(input.get("BillingPostalCode"));
		payEntity.setBillingCountry(input.get("BillingCountry"));
		payEntity.setBillingPhone(input.get("BillingPhone"));
		payEntity.setBillingEmail(input.get("BillingEmail"));
		payEntity.setDeliveryName(input.get("DeliveryName"));
		payEntity.setDeliveryAddress(input.get("DeliveryAddress"));
		payEntity.setDeliveryCity(input.get("DeliveryCity"));
		payEntity.setDeliveryState(input.get("DeliveryState"));
		payEntity.setDeliveryCountry(input.get("DeliveryCountry"));
		payEntity.setDeliveryPostalCode(input.get("DeliveryPostalCode"));
		payEntity.setDeliveryPhone(input.get("DeliveryPhone"));
		payEntity.setDescription(input.get("Description"));
		payEntity.setCustomer_id(input.get("Description"));
		payEntity.setFlagged(input.get("IsFlagged"));
		payEntity.setTransactionid(input.get("TransactionID"));
		payEntity.setPaymentMethod(input.get("PaymentMethod"));
		payEntity.setRequestId(input.get("RequestID"));
		payEntity.setSecureHash(input.get("SecureHash"));
		
		return payEntity;
		
	}
}
