package com.blickx.copyitems.to.objects;

import java.util.Map;

import com.blickx.domain.OnlinePaymentDetailsEntity;

public class OnlineTxCopyProerptiesToEntity {

	public static OnlinePaymentDetailsEntity copyPropertiesToEntity(Map<String, String> input){
		
		OnlinePaymentDetailsEntity onlinePaymentDetailsTo = new OnlinePaymentDetailsEntity();
		
		onlinePaymentDetailsTo.setClient_id(input.get("client_id"));
		
		return onlinePaymentDetailsTo;
		
	}
}
