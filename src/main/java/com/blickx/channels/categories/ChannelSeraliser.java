package com.blickx.channels.categories;

import java.util.List;

import com.blickx.to.ChannelDisplay;

public class ChannelSeraliser {
	String category;
	List<ChannelDisplay> channels;
	public String getCategory() {
		return category;
	}
	public void setCategory(String category) {
		this.category = category;
	}
	public List<ChannelDisplay> getChannels() {
		return channels;
	}
	public void setChannels(List<ChannelDisplay> channels) {
		this.channels = channels;
	}
	public ChannelSeraliser(String category, List<ChannelDisplay> channels) {
		super();
		this.category = category;
		this.channels = channels;
	}	
	
	public ChannelSeraliser(){
		
	}
}
