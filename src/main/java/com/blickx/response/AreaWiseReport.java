package com.blickx.response;

import java.math.BigDecimal;
import java.math.BigInteger;

public class AreaWiseReport {

	
	private BigInteger setupboxCount;
	private String c_id;
	private String customer_id;
	private String customer_name;
	private String areacode;
	private String contact_number;
	private String status;
	private String paid_status;
	private String line1;
	private String line2;
	private String vc_number;
	private BigDecimal package_amount;
	private BigDecimal addon_amount;
	private BigDecimal discount;
	private BigDecimal balance;
	private BigDecimal total;
	
	public String getVc_number() {
		return vc_number;
	}
	public void setVc_number(String vc_number) {
		this.vc_number = vc_number;
	}
	public BigInteger getSetupboxCount() {
		return setupboxCount;
	}
	public void setSetupboxCount(BigInteger setupboxCount) {
		this.setupboxCount = setupboxCount;
	}
	public String getC_id() {
		return c_id;
	}
	public void setC_id(String c_id) {
		this.c_id = c_id;
	}
	public String getCustomer_id() {
		return customer_id;
	}
	public void setCustomer_id(String customer_id) {
		this.customer_id = customer_id;
	}
	public String getCustomer_name() {
		return customer_name;
	}
	public void setCustomer_name(String customer_name) {
		this.customer_name = customer_name;
	}
	public String getAreacode() {
		return areacode;
	}
	public void setAreacode(String areacode) {
		this.areacode = areacode;
	}
	public String getContact_number() {
		return contact_number;
	}
	public void setContact_number(String contact_number) {
		this.contact_number = contact_number;
	}
	public String getStatus() {
		return status;
	}
	public void setStatus(String status) {
		this.status = status;
	}
	public String getPaid_status() {
		return paid_status;
	}
	public void setPaid_status(String paid_status) {
		this.paid_status = paid_status;
	}
	public String getLine1() {
		return line1;
	}
	public void setLine1(String line1) {
		this.line1 = line1;
	}
	public String getLine2() {
		return line2;
	}
	public void setLine2(String line2) {
		this.line2 = line2;
	}
	public BigDecimal getPackage_amount() {
		return package_amount;
	}
	public void setPackage_amount(BigDecimal package_amount) {
		this.package_amount = package_amount;
	}
	public BigDecimal getAddon_amount() {
		return addon_amount;
	}
	public void setAddon_amount(BigDecimal addon_amount) {
		this.addon_amount = addon_amount;
	}
	public BigDecimal getDiscount() {
		return discount;
	}
	public void setDiscount(BigDecimal discount) {
		this.discount = discount;
	}
	public BigDecimal getBalance() {
		return balance;
	}
	public void setBalance(BigDecimal balance) {
		this.balance = balance;
	}
	public BigDecimal getTotal() {
		return total;
	}
	public void setTotal(BigDecimal total) {
		this.total = total;
	}
	@Override
	public String toString() {
		return "AreaWiseReport [setupboxCount=" + setupboxCount + ", c_id="
				+ c_id + ", customer_id=" + customer_id + ", customer_name="
				+ customer_name + ", areacode=" + areacode
				+ ", contact_number=" + contact_number + ", status=" + status
				+ ", paid_status=" + paid_status + ", line1=" + line1
				+ ", line2=" + line2 + ", vc_number=" + vc_number
				+ ", package_amount=" + package_amount + ", addon_amount="
				+ addon_amount + ", discount=" + discount + ", balance="
				+ balance + ", total=" + total + "]";
	}
	
	
}
