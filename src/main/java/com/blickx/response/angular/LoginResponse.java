package com.blickx.response.angular;

import java.util.Map;

public class LoginResponse {

	private String username;
	
	private String firstName;
	
	private String lastName;
	
	private String clientName;
	
	private boolean success;
	
	private String clientId;
	
	private String message;
	
	private String longitude;
	
	private String latitude;
	
	private boolean isBalanceUpdateAllowed;
	
	private boolean smsActivate;
	
	private boolean isBroadbandUser;
	
	private String employee_id;
	
	
	
	public String getEmployee_id() {
		return employee_id;
	}

	public void setEmployee_id(String employee_id) {
		this.employee_id = employee_id;
	}

	private Map<String, Boolean> resourceToAccess;
	
	public Map<String, Boolean> getResourceToAccess() {
		return resourceToAccess;
	}

	public void setResourceToAccess(Map<String, Boolean> resourceToAccess) {
		this.resourceToAccess = resourceToAccess;
	}

	public boolean isBroadbandUser() {
		return isBroadbandUser;
	}

	public void setBroadbandUser(boolean isBroadbandUser) {
		this.isBroadbandUser = isBroadbandUser;
	}

	public boolean BalanceUpdateAllowed() {
		return isBalanceUpdateAllowed;
	}

	public void setBalanceUpdateAllowed(boolean isBalanceUpdateAllowed) {
		this.isBalanceUpdateAllowed = isBalanceUpdateAllowed;
	}



	public boolean getSmsActivate() {
		return smsActivate;
	}

	public void setSmsActivate(boolean smsActivate) {
		this.smsActivate = smsActivate;
	}

	public Map<String, Object> getCurrentLocation() {
		return currentLocation;
	}

	public void setCurrentLocation(Map<String, Object> currentLocation) {
		this.currentLocation = currentLocation;
	}

	private boolean areacode_activated;
	
	private Map<String, Object> currentLocation;

	public boolean getAreacode_activated() {
		return areacode_activated;
	}
	
	public boolean getIsBroadbandUser(){
		return isBroadbandUser;
	}

	public void setAreacode_activated(boolean areacode_activated) {
		this.areacode_activated = areacode_activated;
	}

	public boolean getBalanceUpdateAllowed() {
		return isBalanceUpdateAllowed;
	}

	public void setIsBalanceUpdateAllowed(boolean isBalanceUpdateAllowed) {
		this.isBalanceUpdateAllowed = isBalanceUpdateAllowed;
	}

	public String getLongitude() {
		return longitude;
	}

	public void setLongitude(String longitude) {
		this.longitude = longitude;
	}

	public String getLatitude() {
		return latitude;
	}

	public void setLatitude(String latitude) {
		this.latitude = latitude;
	}

	public String getMessage() {
		return message;
	}

	public void setMessage(String message) {
		this.message = message;
	}

	public String getUsername() {
		return username;
	}

	public void setUsername(String username) {
		this.username = username;
	}

	public String getFirstName() {
		return firstName;
	}

	public void setFirstName(String firstName) {
		this.firstName = firstName;
	}

	public String getLastName() {
		return lastName;
	}

	public void setLastName(String lastName) {
		this.lastName = lastName;
	}

	public String getClientName() {
		return clientName;
	}

	public void setClientName(String clientName) {
		this.clientName = clientName;
	}

	public boolean isSuccess() {
		return success;
	}

	public void setSuccess(boolean success) {
		this.success = success;
	}

	public String getClientId() {
		return clientId;
	}

	public void setClientId(String clientId) {
		this.clientId = clientId;
	}

	
	@Override
	public String toString() {
		return "LoginResponse [username=" + username + ", firstName="
				+ firstName + ", lastName=" + lastName + ", clientName="
				+ clientName + ", success=" + success + ", clientId="
				+ clientId + ", message=" + message + ", longitude="
				+ longitude + ", latitude=" + latitude
				+ ", isBalanceUpdateAllowed=" + isBalanceUpdateAllowed
				+ ", smsActivate=" + smsActivate + ", isBroadbandUser="
				+ isBroadbandUser + ", employee_id=" + employee_id
				+ ", resourceToAccess=" + resourceToAccess
				+ ", areacode_activated=" + areacode_activated
				+ ", currentLocation=" + currentLocation + "]";
	}

	public LoginResponse(){
		
	}
}
