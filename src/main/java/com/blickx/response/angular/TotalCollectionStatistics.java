package com.blickx.response.angular;

public class TotalCollectionStatistics {

	public int getTotal_Current_Day_Collection_Amount() {
		return Total_Current_Day_Collection_Amount;
	}
	public void setTotal_Current_Day_Collection_Amount(
			int total_Current_Day_Collection_Amount) {
		Total_Current_Day_Collection_Amount = total_Current_Day_Collection_Amount;
	}
	public int getTotal_Current_Week_Collection_Amount() {
		return Total_Current_Week_Collection_Amount;
	}
	public void setTotal_Current_Week_Collection_Amount(
			int total_Current_Week_Collection_Amount) {
		Total_Current_Week_Collection_Amount = total_Current_Week_Collection_Amount;
	}
	public int getTotal_Current_Month_Collection_Amount() {
		return Total_Current_Month_Collection_Amount;
	}
	public void setTotal_Current_Month_Collection_Amount(
			int total_Current_Month_Collection_Amount) {
		Total_Current_Month_Collection_Amount = total_Current_Month_Collection_Amount;
	}
	@Override
	public String toString() {
		return "TotalCollectionStatistics [Total_Current_Day_Collection_Amount="
				+ Total_Current_Day_Collection_Amount
				+ ", Total_Current_Week_Collection_Amount="
				+ Total_Current_Week_Collection_Amount
				+ ", Total_Current_Month_Collection_Amount="
				+ Total_Current_Month_Collection_Amount + "]";
	}

	private int Total_Current_Day_Collection_Amount;
	private int Total_Current_Week_Collection_Amount;
	private int Total_Current_Month_Collection_Amount;
	
	
}
