package com.blickx.response;

import java.math.BigDecimal;

import com.blickx.domain.AddOnsEntity;

public class PackagesAndAddonsOfCustomer {

	
	private String box_number;
	
	private String assigned_package;
	
	private BigDecimal package_amount;
	
	private String addons;
	
	private BigDecimal discount;
	
	private BigDecimal package_payable;
	
	private java.util.List<AddOnsEntity> addonList;

	public BigDecimal getDiscount() {
		return discount;
	}

	public void setDiscount(BigDecimal discount) {
		this.discount = discount;
	}

	public BigDecimal getPackage_payable() {
		return package_payable;
	}

	public void setPackage_payable(BigDecimal package_payable) {
		this.package_payable = package_payable;
	}

	public String getBox_number() {
		return box_number;
	}

	public void setBox_number(String box_number) {
		this.box_number = box_number;
	}

	public String getAssigned_package() {
		return assigned_package;
	}

	public void setAssigned_package(String assigned_package) {
		this.assigned_package = assigned_package;
	}

	public BigDecimal getPackage_amount() {
		return package_amount;
	}

	public void setPackage_amount(BigDecimal package_amount) {
		this.package_amount = package_amount;
	}

	public String getAddons() {
		return addons;
	}

	public void setAddons(String addons) {
		this.addons = addons;
	}

	public java.util.List<AddOnsEntity> getAddonList() {
		return addonList;
	}

	public void setAddonList(java.util.List<AddOnsEntity> addonList) {
		this.addonList = addonList;
	}

	@Override
	public String toString() {
		return "PackagesAndAddonsOfCustomer [box_number=" + box_number
				+ ", assigned_package=" + assigned_package
				+ ", package_amount=" + package_amount + ", addons=" + addons
				+ ", discount=" + discount + ", package_payable="
				+ package_payable + ", addonList=" + addonList + "]";
	}

	
}
