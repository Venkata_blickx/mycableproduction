package com.blickx.response;

import java.util.List;
import java.util.Map;

import com.blickx.domain.Authentication;
import com.blickx.to.CustomerBillTo;

public class CustomerBillToOnlinePayment {

	CustomerBillTo customerBill;
	private String responseCode;
	private String errorMsg;
	
	List<PackagesAndAddonsOfCustomer> planDetails;
	
	Map<String, String> clientInformations;


	public String getResponseCode() {
		return responseCode;
	}

	public void setResponseCode(String responseCode) {
		this.responseCode = responseCode;
	}

	public String getErrorMsg() {
		return errorMsg;
	}

	public void setErrorMsg(String errorMsg) {
		this.errorMsg = errorMsg;
	}

	public Map<String, String> getClientInformations() {
		return clientInformations;
	}

	public void setClientInformations(Map<String, String> clientInformations) {
		this.clientInformations = clientInformations;
	}

	public CustomerBillTo getCustomerBill() {
		return customerBill;
	}

	public void setCustomerBill(CustomerBillTo customerBill) {
		this.customerBill = customerBill;
	}

	public List<PackagesAndAddonsOfCustomer> getPlanDetails() {
		return planDetails;
	}

	public void setPlanDetails(List<PackagesAndAddonsOfCustomer> planDetails) {
		this.planDetails = planDetails;
	}

	@Override
	public String toString() {
		return "CustomerBillToOnlinePayment [customerBill=" + customerBill
				+ ", responseCode=" + responseCode + ", errorMsg=" + errorMsg
				+ ", planDetails=" + planDetails + ", clientInformations="
				+ clientInformations + "]";
	}
	
	
	
}
