package com.blickx.response;

import java.math.BigInteger;

public class ResourcesToAccess {

	private String resource;
	
	private BigInteger access;

	public String getResource() {
		return resource;
	}

	public void setResource(String resource) {
		this.resource = resource;
	}

	public BigInteger getAccess() {
		return access;
	}

	public void setAccess(BigInteger access) {
		this.access = access;
	}

	@Override
	public String toString() {
		return "ResourcesToAccess [resource=" + resource + ", access=" + access
				+ "]";
	}
	
	public ResourcesToAccess() {
		
	}
	
}
