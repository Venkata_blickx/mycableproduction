package com.blickx.response;

import java.math.BigDecimal;
import java.util.List;

public class AreaWiseReportWrap {

	private BigDecimal total;
	
	private List<AreaWiseReport> report;

	public BigDecimal getTotal() {
		return total;
	}

	public void setTotal(BigDecimal total) {
		this.total = total;
	}

	public List<AreaWiseReport> getReport() {
		return report;
	}

	public void setReport(List<AreaWiseReport> report) {
		this.report = report;
	}

	@Override
	public String toString() {
		return "AreaWiseReportWrap [total=" + total + ", report=" + report
				+ "]";
	}
	
	public AreaWiseReportWrap() {
		
	}
}
