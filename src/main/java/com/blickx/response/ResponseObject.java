package com.blickx.response;

public class ResponseObject {

	@Override
	public String toString() {
		return "ResponseObject [isDataValid=" + isDataValid + ", responseMsg="
				+ responseMsg + "]";
	}

	public boolean isDataValid() {
		return isDataValid;
	}

	public void setDataValid(boolean isDataValid) {
		this.isDataValid = isDataValid;
	}

	public String getResponseMsg() {
		return responseMsg;
	}

	public void setResponseMsg(String responseMsg) {
		this.responseMsg = responseMsg;
	}

	private boolean isDataValid;
	
	private String responseMsg;
	
	public ResponseObject(){
		
	}
}
