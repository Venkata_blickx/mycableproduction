package com.blickx.output;

import java.math.BigDecimal;
import java.math.BigInteger;
import java.util.Date;

import com.blickx.domain.DateSerializerWithTimeStamp;
import com.fasterxml.jackson.databind.annotation.JsonSerialize;

public class PaidCustomerTo {

	private String customer_id;
	
	private String c_id;
	
	private String customer_name;
	
	private String line1;
	
	private String line2;
	
	private String vc_number;
	
	private BigDecimal paid_amount;
	
	private String bill_recipt_no;
	
	private String contact_number;
	
	private BigDecimal balance_amount;
	
	private BigInteger setTopBoxesCount;
	
	private Date date;

	@Override
	public String toString() {
		return "PaidCustomerTo [customer_id=" + customer_id + ", c_id=" + c_id
				+ ", customer_name=" + customer_name + ", paid_amount="
				+ paid_amount + ", bill_recipt_no=" + bill_recipt_no
				+ ", contact_number=" + contact_number + ", balance_amount="
				+ balance_amount + ", setTopBoxesCount=" + setTopBoxesCount
				+ ", date=" + date + "]";
	}

	public String getCustomer_id() {
		return customer_id;
	}

	public void setCustomer_id(String customer_id) {
		this.customer_id = customer_id;
	}

	public String getC_id() {
		return c_id;
	}

	public String getLine1() {
		return line1;
	}

	public void setLine1(String line1) {
		this.line1 = line1;
	}

	public String getLine2() {
		return line2;
	}

	public void setLine2(String line2) {
		this.line2 = line2;
	}

	public String getVc_number() {
		return vc_number;
	}

	public void setVc_number(String vc_number) {
		this.vc_number = vc_number;
	}

	public void setC_id(String c_id) {
		this.c_id = c_id;
	}

	public String getCustomer_name() {
		return customer_name;
	}

	public void setCustomer_name(String customer_name) {
		this.customer_name = customer_name;
	}

	public BigDecimal getPaid_amount() {
		return paid_amount;
	}

	public void setPaid_amount(BigDecimal paid_amount) {
		this.paid_amount = paid_amount;
	}

	public String getBill_recipt_no() {
		return bill_recipt_no;
	}

	public void setBill_recipt_no(String bill_recipt_no) {
		this.bill_recipt_no = bill_recipt_no;
	}

	public String getContact_number() {
		return contact_number;
	}

	public void setContact_number(String contact_number) {
		this.contact_number = contact_number;
	}

	public BigDecimal getBalance_amount() {
		return balance_amount;
	}

	public void setBalance_amount(BigDecimal balance_amount) {
		this.balance_amount = balance_amount;
	}

	public BigInteger getSetTopBoxesCount() {
		return setTopBoxesCount;
	}

	public void setSetTopBoxesCount(BigInteger setTopBoxesCount) {
		this.setTopBoxesCount = setTopBoxesCount;
	}

	@JsonSerialize(using = DateSerializerWithTimeStamp.class)
	public Date getDate() {
		return date;
	}

	public void setDate(Date date) {
		this.date = date;
	}
	
	
}
