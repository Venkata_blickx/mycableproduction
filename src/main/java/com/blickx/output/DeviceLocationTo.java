package com.blickx.output;

import java.math.BigDecimal;
import java.util.List;

public class DeviceLocationTo {

	@Override
	public String toString() {
		return "DeviceLocationTo [deviceName=" + deviceName + ", deviceStatus="
				+ deviceStatus + ", deviceOwner=" + deviceOwner
				+ ", deviceOwnerID=" + deviceOwnerID + ", deviceLocation="
				+ deviceLocation + ", lastTransactionMadeBefore="
				+ lastTransactionMadeBefore + ", currentDayCollection="
				+ currentDayCollection + ", currentWeekCollection="
				+ currentWeekCollection + ", currentMonthCollection="
				+ currentMonthCollection + "]";
	}
	public String getDeviceName() {
		return deviceName;
	}
	public void setDeviceName(String deviceName) {
		this.deviceName = deviceName;
	}
	public boolean getDeviceStatus() {
		return deviceStatus;
	}
	public void setDeviceStatus(boolean deviceStatus) {
		this.deviceStatus = deviceStatus;
	}
	public String getDeviceOwner() {
		return deviceOwner;
	}
	public void setDeviceOwner(String deviceOwner) {
		this.deviceOwner = deviceOwner;
	}
	public String getDeviceOwnerID() {
		return deviceOwnerID;
	}
	public void setDeviceOwnerID(String deviceOwnerID) {
		this.deviceOwnerID = deviceOwnerID;
	}
	public List<Double> getDeviceLocation() {
		return deviceLocation;
	}
	public void setDeviceLocation(List<Double> deviceLocation) {
		this.deviceLocation = deviceLocation;
	}
	
	public String getLastTransactionMadeBefore() {
		return lastTransactionMadeBefore;
	}
	public void setLastTransactionMadeBefore(String lastTransactionMadeBefore) {
		this.lastTransactionMadeBefore = lastTransactionMadeBefore;
	}

	private String deviceName;//"Samsung"
	private boolean deviceStatus;//"active"
	private String deviceOwner;//"Deepika"
	private String deviceOwnerID;//"121122"
	private List<Double> deviceLocation;
	private String lastTransactionMadeBefore;
	private BigDecimal currentDayCollection;
	private BigDecimal currentWeekCollection;
	private BigDecimal currentMonthCollection;
	public BigDecimal getCurrentDayCollection() {
		return currentDayCollection;
	}
	public void setCurrentDayCollection(BigDecimal currentDayCollection) {
		this.currentDayCollection = currentDayCollection;
	}
	public BigDecimal getCurrentWeekCollection() {
		return currentWeekCollection;
	}
	public void setCurrentWeekCollection(BigDecimal currentWeekCollection) {
		this.currentWeekCollection = currentWeekCollection;
	}
	public BigDecimal getCurrentMonthCollection() {
		return currentMonthCollection;
	}
	public void setCurrentMonthCollection(BigDecimal currentMonthCollection) {
		this.currentMonthCollection = currentMonthCollection;
	}
	
	
	
}
