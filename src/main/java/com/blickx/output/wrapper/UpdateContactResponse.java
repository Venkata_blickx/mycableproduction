package com.blickx.output.wrapper;

public class UpdateContactResponse {

	@Override
	public String toString() {
		return "UpdateContactResponse [customerId=" + customerId
				+ ", newContactNumber=" + newContactNumber + ", status="
				+ status + "]";
	}

	public String getCustomerId() {
		return customerId;
	}

	public void setCustomerId(String customerId) {
		this.customerId = customerId;
	}

	public String getNewContactNumber() {
		return newContactNumber;
	}

	public void setNewContactNumber(String newContactNumber) {
		this.newContactNumber = newContactNumber;
	}

	public int getStatus() {
		return status;
	}

	public void setStatus(int status) {
		this.status = status;
	}

	private String customerId;
	
	private String newContactNumber;
	
	private int status;
	
	public UpdateContactResponse(){
		
	}
}
