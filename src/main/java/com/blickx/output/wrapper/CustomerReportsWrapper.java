package com.blickx.output.wrapper;

import java.math.BigDecimal;
import java.util.List;

import com.blickx.to.CustomerReports;

public class CustomerReportsWrapper {

	private BigDecimal paidTotal;
	
	private List<CustomerReports> report;

	public BigDecimal getPaidTotal() {
		return paidTotal;
	}

	public void setPaidTotal(BigDecimal paidTotal) {
		this.paidTotal = paidTotal;
	}

	public List<CustomerReports> getReport() {
		return report;
	}

	public void setReport(List<CustomerReports> report) {
		this.report = report;
	}

	@Override
	public String toString() {
		return "CustomerReportsWrapper [paidTotal=" + paidTotal + ", report="
				+ report + "]";
	}
	
	public CustomerReportsWrapper() {
		
	}
}
