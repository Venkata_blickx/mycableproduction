package com.blickx.output.wrapper;

import java.util.List;

public class UpdateContactResponseTO {

	public UpdateContactResponseTO() {
		
	}

	@Override
	public String toString() {
		return "UpdateContactResponseTO [updateContactResponse="
				+ updateContactResponse + "]";
	}

	public List<UpdateContactResponse> getUpdateContactResponse() {
		return updateContactResponse;
	}

	public void setUpdateContactResponse(
			List<UpdateContactResponse> updateContactResponse) {
		this.updateContactResponse = updateContactResponse;
	}

	private List<UpdateContactResponse> updateContactResponse;
	
	
}
