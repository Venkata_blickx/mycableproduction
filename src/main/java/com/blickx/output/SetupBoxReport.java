package com.blickx.output;

import java.math.BigDecimal;

public class SetupBoxReport {

	private String c_id;
	
	private String customer_id;
	
	private String customer_name;
	
	private String line1;
	
	private String line2;
	
	private String box_number;
	
	private String vc_number;
	
	private String package_name;

	private BigDecimal package_charge;
	
	private String areacode;
	
	public BigDecimal getPackage_charge() {
		return package_charge;
	}

	public void setPackage_charge(BigDecimal package_charge) {
		this.package_charge = package_charge;
	}

	private BigDecimal package_amount;
	
	private BigDecimal discount;
	
	private String status;
	
	private String paid_status;
	
	public String getAreacode() {
		return areacode;
	}

	public void setAreacode(String areacode) {
		this.areacode = areacode;
	}

	public String getCustomer_id() {
		return customer_id;
	}

	public void setCustomer_id(String customer_id) {
		this.customer_id = customer_id;
	}

	public String getPaid_status() {
		return paid_status;
	}

	public void setPaid_status(String paid_status) {
		this.paid_status = paid_status;
	}

	private BigDecimal addon_monthly_amount;
	
	

	public String getC_id() {
		return c_id;
	}

	public void setC_id(String c_id) {
		this.c_id = c_id;
	}

	public String getCustomer_name() {
		return customer_name;
	}

	public void setCustomer_name(String customer_name) {
		this.customer_name = customer_name;
	}

	public String getBox_number() {
		return box_number;
	}

	public void setBox_number(String box_number) {
		this.box_number = box_number;
	}

	public String getVc_number() {
		return vc_number;
	}

	public void setVc_number(String vc_number) {
		this.vc_number = vc_number;
	}

	public String getPackage_name() {
		return package_name;
	}

	public void setPackage_name(String package_name) {
		this.package_name = package_name;
	}

	public BigDecimal getPackage_amount() {
		return package_amount;
	}

	public void setPackage_amount(BigDecimal package_amount) {
		this.package_amount = package_amount;
	}

	public String getLine1() {
		return line1;
	}

	public void setLine1(String line1) {
		this.line1 = line1;
	}

	public String getLine2() {
		return line2;
	}

	public void setLine2(String line2) {
		this.line2 = line2;
	}

	public BigDecimal getDiscount() {
		return discount;
	}

	public void setDiscount(BigDecimal discount) {
		this.discount = discount;
	}

	public String getStatus() {
		return status;
	}

	public void setStatus(String status) {
		this.status = status;
	}

	public BigDecimal getAddon_monthly_amount() {
		return addon_monthly_amount;
	}

	public void setAddon_monthly_amount(BigDecimal addon_monthly_amount) {
		this.addon_monthly_amount = addon_monthly_amount;
	}

	@Override
	public String toString() {
		return "SetupBoxReport [c_id=" + c_id + ", customer_id=" + customer_id
				+ ", customer_name=" + customer_name + ", line1=" + line1
				+ ", line2=" + line2 + ", box_number=" + box_number
				+ ", vc_number=" + vc_number + ", package_name=" + package_name
				+ ", package_charge=" + package_charge + ", areacode="
				+ areacode + ", package_amount=" + package_amount
				+ ", discount=" + discount + ", status=" + status
				+ ", paid_status=" + paid_status + ", addon_monthly_amount="
				+ addon_monthly_amount + "]";
	}
	
	
}
