package com.blickx.output;

import java.math.BigInteger;

public class DeviceLacationRespons {

	
	@Override
	public String toString() {
		return "DeviceLacationRespons [deviceName=" + deviceName
				+ ", deviceStatus=" + deviceStatus + ", deviceOwner="
				+ deviceOwner + ", deviceOwnerID=" + deviceOwnerID
				+ ", location_longitude=" + location_longitude
				+ ", location_lattitude=" + location_lattitude + ", timeDiff="
				+ timeDiff + "]";
	}
	public String getDeviceName() {
		return deviceName;
	}
	public void setDeviceName(String deviceName) {
		this.deviceName = deviceName;
	}
	public boolean isDeviceStatus() {
		return deviceStatus;
	}
	public void setDeviceStatus(boolean deviceStatus) {
		this.deviceStatus = deviceStatus;
	}
	public String getDeviceOwner() {
		return deviceOwner;
	}
	public void setDeviceOwner(String deviceOwner) {
		this.deviceOwner = deviceOwner;
	}
	public String getDeviceOwnerID() {
		return deviceOwnerID;
	}
	public void setDeviceOwnerID(String deviceOwnerID) {
		this.deviceOwnerID = deviceOwnerID;
	}
	public String getLocation_longitude() {
		return location_longitude;
	}
	public void setLocation_longitude(String location_longitude) {
		this.location_longitude = location_longitude;
	}
	public String getLocation_lattitude() {
		return location_lattitude;
	}
	public void setLocation_lattitude(String location_lattitude) {
		this.location_lattitude = location_lattitude;
	}
	public BigInteger getTimeDiff() {
		return timeDiff;
	}
	public void setTimeDiff(BigInteger timeDiff) {
		this.timeDiff = timeDiff;
	}
	private String deviceName;//"Samsung"
	private boolean deviceStatus;//"active"
	private String deviceOwner;//"Deepika"
	private String deviceOwnerID;//"121122"
	private String location_longitude;
	private String location_lattitude;
	private BigInteger timeDiff;

}
