package com.blickx.output;

import java.util.Date;

import javax.persistence.Column;

import com.blickx.domain.DateSerializerWithTimeStamp;
import com.blickx.utill.CustomJsonDateDeserializer;
import com.blickx.utill.CustomerDateSerializer2;
import com.fasterxml.jackson.databind.annotation.JsonDeserialize;
import com.fasterxml.jackson.databind.annotation.JsonSerialize;

public class EditCustomerCommentTo {

private int customerCommentsId;
	
	private String user;
	
	private String customer_id;
	
	private Date created_timestamp;
	
	private int priority;
	
	private String comment;

	public String getComment() {
		return comment;
	}

	public void setComment(String comment) {
		this.comment = comment;
	}

	public int getCustomerCommentsId() {
		return customerCommentsId;
	}

	public void setCustomerCommentsId(int customerCommentsId) {
		this.customerCommentsId = customerCommentsId;
	}

	public String getUser() {
		return user;
	}

	public void setUser(String user) {
		this.user = user;
	}

	public String getCustomer_id() {
		return customer_id;
	}

	public void setCustomer_id(String customer_id) {
		this.customer_id = customer_id;
	}

	@JsonSerialize(using = DateSerializerWithTimeStamp.class)
	public Date getCreated_timestamp() {
		return created_timestamp;
	}

	@JsonDeserialize(using = CustomerDateSerializer2.class)
	public void setCreated_timestamp(Date created_timestamp) {
		this.created_timestamp = created_timestamp;
	}

	public int isPriority() {
		return priority;
	}

	public void setPriority(int priority) {
		this.priority = priority;
	}

	@Override
	public String toString() {
		return "EditCustomerCommentTo [customerCommentsId="
				+ customerCommentsId + ", user=" + user + ", customer_id="
				+ customer_id + ", created_timestamp=" + created_timestamp
				+ ", priority=" + priority + ", comment=" + comment + "]";
	}
	
	public EditCustomerCommentTo(){
		
	}
}
