package com.blickx.output;

import java.util.Date;

import com.blickx.utill.DateSerializerForComplaintsList;
import com.blickx.utill.DateSerializerTimeStamp;
import com.fasterxml.jackson.databind.annotation.JsonSerialize;

public class GetComplaintList {

	private String customer_id;
	private String c_id;
	private String customer_name;
	private String contact_number;
	private String email;
	private String line1;
	private String line2;
	private String complaint_id;
	private String details;
	private String status;
	private String assigned_to;
	private String closed_by;
	private String comment;
	private String source;
	private Date created_date;
	private Date assigned_date;
	private Date closed_date;
	public String getCustomer_id() {
		return customer_id;
	}
	public String getC_id() {
		return c_id;
	}
	public void setC_id(String c_id) {
		this.c_id = c_id;
	}
	public void setCustomer_id(String customer_id) {
		this.customer_id = customer_id;
	}
	public String getCustomer_name() {
		return customer_name;
	}
	public void setCustomer_name(String customer_name) {
		this.customer_name = customer_name;
	}
	public String getContact_number() {
		return contact_number;
	}
	public void setContact_number(String contact_number) {
		this.contact_number = contact_number;
	}
	public String getEmail() {
		return email;
	}
	public void setEmail(String email) {
		this.email = email;
	}
	public String getLine1() {
		return line1;
	}
	public void setLine1(String line1) {
		this.line1 = line1;
	}
	public String getLine2() {
		return line2;
	}
	public void setLine2(String line2) {
		this.line2 = line2;
	}
	public String getComplaint_id() {
		return complaint_id;
	}
	public void setComplaint_id(String complaint_id) {
		this.complaint_id = complaint_id;
	}
	public String getDetails() {
		return details;
	}
	public void setDetails(String details) {
		this.details = details;
	}
	public String getStatus() {
		return status;
	}
	public void setStatus(String status) {
		this.status = status;
	}
	public String getAssigned_to() {
		return assigned_to;
	}
	public void setAssigned_to(String assigned_to) {
		this.assigned_to = assigned_to;
	}
	public String getClosed_by() {
		return closed_by;
	}
	public void setClosed_by(String closed_by) {
		this.closed_by = closed_by;
	}
	public String getComment() {
		return comment;
	}
	public void setComment(String comment) {
		this.comment = comment;
	}
	public String getSource() {
		return source;
	}
	public void setSource(String source) {
		this.source = source;
	}
	@JsonSerialize(using = DateSerializerTimeStamp.class)
	public Date getCreated_date() {
		return created_date;
	}
	public void setCreated_date(Date created_date) {
		this.created_date = created_date;
	}
	@JsonSerialize(using = DateSerializerForComplaintsList.class)
	public Date getAssigned_date() {
		return assigned_date;
	}
	public void setAssigned_date(Date assigned_date) {
		this.assigned_date = assigned_date;
	}
	@JsonSerialize(using = DateSerializerTimeStamp.class)
	public Date getClosed_date() {
		return closed_date;
	}
	
	public void setClosed_date(Date closed_date) {
		this.closed_date = closed_date;
	}
	
	public GetComplaintList(){
		
	}
	@Override
	public String toString() {
		return "GetComplaintList [customer_id=" + customer_id + ", c_id="
				+ c_id + ", customer_name=" + customer_name
				+ ", contact_number=" + contact_number + ", email=" + email
				+ ", line1=" + line1 + ", line2=" + line2 + ", complaint_id="
				+ complaint_id + ", details=" + details + ", status=" + status
				+ ", assigned_to=" + assigned_to + ", closed_by=" + closed_by
				+ ", comments=" + comment + ", source=" + source
				+ ", created_date=" + created_date + ", assigned_date="
				+ assigned_date + ", closed_date=" + closed_date + "]";
	}
	
}
