package com.blickx.settings.to;

import java.util.Date;

import javax.persistence.Column;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;

import org.springframework.format.annotation.DateTimeFormat;

public class AddonsEntityTo {

	private int id;
	
	private String customer_id;
	
	private String box_number;
	
	private String addon;
	
	private Date expiry_date;
	
	private Date start_date;
	
	private int package_amt;

	public int getId() {
		return id;
	}

	public int getPackage_amt() {
		return package_amt;
	}

	public void setPackage_amt(int package_amt) {
		this.package_amt = package_amt;
	}

	public void setId(int id) {
		this.id = id;
	}

	public String getCustomer_id() {
		return customer_id;
	}

	public void setCustomer_id(String customer_id) {
		this.customer_id = customer_id;
	}

	public String getBox_number() {
		return box_number;
	}

	public void setBox_number(String box_number) {
		this.box_number = box_number;
	}

	public String getAddon() {
		return addon;
	}

	public void setAddon(String addon) {
		this.addon = addon;
	}

	public Date getExpiry_date() {
		return expiry_date;
	}

	public void setExpiry_date(Date expiry_date) {
		this.expiry_date = expiry_date;
	}

	public Date getStart_date() {
		return start_date;
	}

	public void setStart_date(Date start_date) {
		this.start_date = start_date;
	}
	
	public AddonsEntityTo(){
		
	}

	@Override
	public String toString() {
		return "AddonEntityTo [id=" + id + ", customer_id=" + customer_id
				+ ", box_number=" + box_number + ", addon=" + addon
				+ ", expiry_date=" + expiry_date + ", start_date=" + start_date
				+ "]";
	}
	
	
}
