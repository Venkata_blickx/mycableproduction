package com.blickx.settings.to;

import java.math.BigInteger;

public class GetDeActiveAndActiveCustomersAndSetupBoxesCount {
	
	private BigInteger activeCustomersCount;
	private BigInteger deActiveCustomersCount;
	private BigInteger activeSetupBoxesCount;
	private BigInteger deActiveSetupBoxesCount;
	public BigInteger getActiveCustomersCount() {
		return activeCustomersCount;
	}
	public void setActiveCustomersCount(BigInteger activeCustomersCount) {
		this.activeCustomersCount = activeCustomersCount;
	}
	public BigInteger getDeActiveCustomersCount() {
		return deActiveCustomersCount;
	}
	public void setDeActiveCustomersCount(BigInteger deActiveCustomersCount) {
		this.deActiveCustomersCount = deActiveCustomersCount;
	}
	public BigInteger getActiveSetupBoxesCount() {
		return activeSetupBoxesCount;
	}
	public void setActiveSetupBoxesCount(BigInteger activeSetupBoxesCount) {
		this.activeSetupBoxesCount = activeSetupBoxesCount;
	}
	public BigInteger getDeActiveSetupBoxesCount() {
		return deActiveSetupBoxesCount;
	}
	public void setDeActiveSetupBoxesCount(BigInteger deActiveSetupBoxesCount) {
		this.deActiveSetupBoxesCount = deActiveSetupBoxesCount;
	}
	
	public GetDeActiveAndActiveCustomersAndSetupBoxesCount(){
		
	}
	 

}
