package com.blickx.settings.to;

import java.math.BigDecimal;

public class GetBalanceTo {

	private String customer_id;
	
	private BigDecimal balance_amount;

	public String getCustomer_id() {
		return customer_id;
	}

	public void setCustomer_id(String customer_id) {
		this.customer_id = customer_id;
	}

	public BigDecimal getBalance_amount() {
		return balance_amount;
	}

	public void setBalance_amount(BigDecimal balance_amount) {
		this.balance_amount = balance_amount;
	}
	
	public GetBalanceTo(){
		
	}

	@Override
	public String toString() {
		return "GetBalanceTo [customer_id=" + customer_id + ", balance_amount="
				+ balance_amount + "]";
	}
	
	
}
