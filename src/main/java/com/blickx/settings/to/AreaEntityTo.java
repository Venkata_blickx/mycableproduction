package com.blickx.settings.to;

import java.util.Date;


public class AreaEntityTo {
	private int id;
	
	private String areaCode;
	
	private String areaDescription;
	
	private String clientId;
	
	private Date createdTimeStamp;
	
	private Date lastUpdatedTimestamp;
	
	private String employeesGotAssigned;
	
	public AreaEntityTo() {
	}
	
	@Override
	public String toString() {
		return "AreaEntityTo [id=" + id + ", areaCode=" + areaCode
				+ ", areaDescription=" + areaDescription + ", clientId="
				+ clientId + ", createdTimeStamp=" + createdTimeStamp
				+ ", lastUpdatedTimestamp=" + lastUpdatedTimestamp
				+ ", employeesGotAssigned=" + employeesGotAssigned + "]";
	}

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public String getAreaCode() {
		return areaCode;
	}

	public void setAreaCode(String areaCode) {
		this.areaCode = areaCode;
	}

	public String getAreaDescription() {
		return areaDescription;
	}

	public void setAreaDescription(String areaDescription) {
		this.areaDescription = areaDescription;
	}

	public String getClientId() {
		return clientId;
	}

	public void setClientId(String clientId) {
		this.clientId = clientId;
	}

	public Date getCreatedTimeStamp() {
		return createdTimeStamp;
	}

	public void setCreatedTimeStamp(Date createdTimeStamp) {
		this.createdTimeStamp = createdTimeStamp;
	}

	public Date getLastUpdatedTimestamp() {
		return lastUpdatedTimestamp;
	}

	public void setLastUpdatedTimestamp(Date lastUpdatedTimestamp) {
		this.lastUpdatedTimestamp = lastUpdatedTimestamp;
	}

	public String getEmployeesGotAssigned() {
		return employeesGotAssigned;
	}

	public void setEmployeesGotAssigned(String employeesGotAssigned) {
		this.employeesGotAssigned = employeesGotAssigned;
	}
}
