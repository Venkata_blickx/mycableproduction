package com.blickx.daoImpl;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

import org.apache.log4j.Logger;
import org.hibernate.HibernateException;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.Transaction;
import org.hibernate.transform.Transformers;
import org.springframework.beans.factory.annotation.Autowired;

import com.blickx.dao.DashBoardDao;
import com.blickx.settings.to.GetDeActiveAndActiveCustomersAndSetupBoxesCount;
import com.blickx.to.BillCollectorsGraphTO;
import com.blickx.to.CollectionStatistics;
import com.blickx.to.ConsolidatePaymentStatus;
import com.blickx.to.CustomerToMobileTo;
import com.blickx.to.DailyCollectionReport;
import com.blickx.to.DashBoardCollectionDetailsTo;
import com.blickx.to.NoOfPayedCustomerTo;
import com.blickx.to.PackageDetailsForDashBoardTo;
import com.blickx.to.SmartCardIssuedCustomerTo;

public class DashBoardDaoImpl implements DashBoardDao {

	private Logger logger = Logger.getLogger(DashBoardDaoImpl.class);
	
	@Autowired
	SessionFactory sfactory;

	public List<DailyCollectionReport> getDailyCollection(String client_id){

		List<DailyCollectionReport> resultset = new ArrayList<DailyCollectionReport>();
		
		Session session = sfactory.openSession();
		Transaction tx = null;
		try {
			tx = session.beginTransaction();
			
			logger.info("Calling GetCollectionReport SP");
			
			@SuppressWarnings({ "rawtypes", "unchecked" })
			List<DailyCollectionReport> resultWithAliasedBean  = (List) session.createSQLQuery(
				"CALL GetCollectionReport(:client_id)")
				.setString("client_id", client_id)
				.setResultTransformer(Transformers.aliasToBean(DailyCollectionReport.class))
				.list();
			
			resultset = resultWithAliasedBean;
			
			tx.commit();
		} catch (HibernateException e) {
			if (tx != null)
				tx.rollback();
			e.printStackTrace();
			logger.error("Exception! " + e);
		} finally {
			session.close();
		}
		return resultset;
	}

	@Override
	public List<BillCollectorsGraphTO> getBillCollectorsGraph(String client_id) {
		List<BillCollectorsGraphTO> resultset = new ArrayList<BillCollectorsGraphTO>();
		
		Session session = sfactory.openSession();
		Transaction tx = null;
		try {
			tx = session.beginTransaction();
			
			logger.info("Calling DashboardBillCollectorsGraph SP");
			
			@SuppressWarnings({ "rawtypes", "unchecked" })
			List<BillCollectorsGraphTO> resultWithAliasedBean  = (List) session.createSQLQuery(
			"CALL DashboardBillCollectorsGraph(:client_id)")
			.setString("client_id", client_id)
			.setResultTransformer(Transformers.aliasToBean(BillCollectorsGraphTO.class))
			.list();
			
			for (int i = 0; i < resultWithAliasedBean.size(); i++) {
				resultset.add(resultWithAliasedBean.get(i));
				}
			
			tx.commit();
		} catch (HibernateException e) {
			if (tx != null)
				tx.rollback();
			e.printStackTrace();
			logger.error("Exception! " + e);
		} finally {
			session.close();
		}
		return resultset;
	}

	@Override
	public List<PackageDetailsForDashBoardTo> getPackageDetails(String client_id) {
		
		List<PackageDetailsForDashBoardTo> resultset = new ArrayList<PackageDetailsForDashBoardTo>();
		
		Session session = sfactory.openSession();
		Transaction tx = null;
		try {
			tx = session.beginTransaction();
			
			logger.info("Calling DashboardPackageDetails SP");
			
			@SuppressWarnings({ "rawtypes", "unchecked" })
			List<PackageDetailsForDashBoardTo> resultWithAliasedBean  = (List) session.createSQLQuery(
			"CALL DashboardPackageDetails(:client_id)")
			.setString("client_id", client_id)
			.setResultTransformer(Transformers.aliasToBean(PackageDetailsForDashBoardTo.class))
			.list();
			
			resultset = resultWithAliasedBean;
			
			tx.commit();
		} catch (HibernateException e) {
			if (tx != null)
				tx.rollback();
			e.printStackTrace();
			logger.error("Exception! " + e);
		} finally {
			session.close();
		}
		return resultset;
	}

	@Override
	public NoOfPayedCustomerTo getNoOfCustomersPayed() {
		NoOfPayedCustomerTo resultset = new NoOfPayedCustomerTo();
		
		Session session = sfactory.openSession();
		Transaction tx = null;
		try {
			tx = session.beginTransaction();
			
			logger.info("Calling DashboardCustomerPaymentDetails SP");
			
			@SuppressWarnings({ "rawtypes", "unchecked" })
			List<NoOfPayedCustomerTo> resultWithAliasedBean  = (List) session.createSQLQuery(
			"CALL DashboardCustomerPaymentDetails")
			.setResultTransformer(Transformers.aliasToBean(NoOfPayedCustomerTo.class))
			.list();
			
			resultset = resultWithAliasedBean.get(0);
			
			tx.commit();
		} catch (HibernateException e) {
			if (tx != null)
				tx.rollback();
			e.printStackTrace();
			logger.error("Exception! " + e);
		} finally {
			session.close();
		}
		return resultset;
	}

	@Override
	public DashBoardCollectionDetailsTo getCollectionsForDashBoard(String client_id) {
		DashBoardCollectionDetailsTo resultset = new DashBoardCollectionDetailsTo();
		
		Session session = sfactory.openSession();
		Transaction tx = null;
		try {
			tx = session.beginTransaction();
			
			logger.info("Calling DashBoardCollectionDetails SP");
			
			@SuppressWarnings({ "rawtypes", "unchecked" })
			List<DashBoardCollectionDetailsTo> resultWithAliasedBean  = (List) session.createSQLQuery(
			"CALL DashBoardCollectionDetails(:client_id)")
			.setString("client_id", client_id)
			.setResultTransformer(Transformers.aliasToBean(DashBoardCollectionDetailsTo.class))
			.list();
			
			resultset = resultWithAliasedBean.get(0);
			if(resultset.getDay_collection() == null)resultset.setDay_collection(new BigDecimal(0));
			if(resultset.getWeek_collection() == null)resultset.setWeek_collection(new BigDecimal(0));
			if(resultset.getMonth_collection() == null)resultset.setMonth_collection(new BigDecimal(0));
			tx.commit();
		} catch (HibernateException e) {
			if (tx != null)
				tx.rollback();
			e.printStackTrace();
			logger.error("Exception! " + e);
		} finally {
			session.close();
		}
		return resultset;
	}

	@Override
	public SmartCardIssuedCustomerTo getSmartCardIssuedCustomers(String client_id) {
		SmartCardIssuedCustomerTo resultset = new SmartCardIssuedCustomerTo();
		
		Session session = sfactory.openSession();
		Transaction tx = null;
		try {
			tx = session.beginTransaction();
			
			logger.info("Calling DashBoardSmartCardIssuedCusts SP");
			
			@SuppressWarnings({ "rawtypes", "unchecked" })
			List<SmartCardIssuedCustomerTo> resultWithAliasedBean  = (List) session.createSQLQuery(
			"CALL DashBoardSmartCardIssuedCusts(:client_id)")
			.setString("client_id", client_id)
			.setResultTransformer(Transformers.aliasToBean(SmartCardIssuedCustomerTo.class))
			.list();
			
			resultset = resultWithAliasedBean.get(0);
			
			tx.commit();
		} catch (HibernateException e) {
			if (tx != null)
				tx.rollback();
			e.printStackTrace();
			logger.error("Exception! " + e);
		} finally {
			session.close();
		}
		return resultset;
	}

	@Override
	public GetDeActiveAndActiveCustomersAndSetupBoxesCount getDeActivateAndActiveCustomersSetupBoxCount(String client_id) {
		GetDeActiveAndActiveCustomersAndSetupBoxesCount resultset = new GetDeActiveAndActiveCustomersAndSetupBoxesCount();
		
		Session session = sfactory.openSession();
		Transaction tx = null;
		try {
			tx = session.beginTransaction();
			
			logger.info("Calling DashBoard_Active_DeActive_Count_Of_CustomersAndSetupBoxes SP");
			
			@SuppressWarnings({ "rawtypes", "unchecked" })
			List<GetDeActiveAndActiveCustomersAndSetupBoxesCount> resultWithAliasedBean  = (List) session.createSQLQuery(
			"CALL DashBoard_Active_DeActive_Count_Of_CustomersAndSetupBoxes(:client_id)")
			.setString("client_id", client_id)
			.setResultTransformer(Transformers.aliasToBean(GetDeActiveAndActiveCustomersAndSetupBoxesCount.class))
			.list();
			
			resultset = resultWithAliasedBean.get(0);
			
			tx.commit();
		} catch (HibernateException e) {
			if (tx != null)
				tx.rollback();
			e.printStackTrace();
			logger.error("Exception! " + e);
		} finally {
			session.close();
		}
		return resultset;
	}
	
	public ConsolidatePaymentStatus getConsolidatedPaymentStatus(String client_id) {
		ConsolidatePaymentStatus result = new ConsolidatePaymentStatus();
		Session session = sfactory.openSession();
		Transaction tx = null;
		try{
			tx = session.beginTransaction();
			logger.info("getConsolidatedPaymentStatus For dashboard");
			result = (ConsolidatePaymentStatus) session.createSQLQuery("call GetConsolidatedPaymentStatus(?)")
												.setParameter(0, client_id)
												.setResultTransformer(Transformers.aliasToBean(ConsolidatePaymentStatus.class))
												.list().get(0);
			//logger.info(result.get(0).toString());
			System.out.println(result);
			logger.info("getConsolidatedPaymentStatus Ending");
			
			tx.commit();
			
		}catch(HibernateException e){
			if(tx != null)tx.rollback();
			logger.error(e);
			return result;
		}catch(Exception e){
			if(tx != null)tx.rollback();
			logger.error(e);
			return result;
		}finally{
			session.close();
		}
		return result;
	}

	@Override
	public List<CollectionStatistics> getCollectionStatistics(String client_id) {
		
		List<CollectionStatistics> result = new ArrayList<CollectionStatistics>();
		Session session = sfactory.openSession();
		Transaction tx = null;
		try{
			tx = session.beginTransaction();
			logger.info("getCollection Statistics For dashboard");
			result = session.createSQLQuery("call collection_statistics(?)")
												.setParameter(0, client_id)
												.setResultTransformer(Transformers.aliasToBean(CollectionStatistics.class))
												.list();
			//logger.info(result.get(0).toString());
			System.out.println(result);
			logger.info("getCollection Statistics Ending");
			
			tx.commit();
			
		}catch(HibernateException e){
			if(tx != null)tx.rollback();
			logger.error(e);
			return result;
		}catch(Exception e){
			if(tx != null)tx.rollback();
			logger.error(e);
			return result;
		}finally{
			session.close();
		}
		return result;

	}

	

}
