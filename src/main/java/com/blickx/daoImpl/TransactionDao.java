package com.blickx.daoImpl;

import com.blickx.to.ClientInformationEntityDTO;
import com.blickx.to.PaymentTxEntityDTO;
import com.blickx.tx.mailing.DetailsToSend;

public interface TransactionDao {

	void saveOnlineTxDetails(PaymentTxEntityDTO paymentTxEntityDTO);

	void updateTxAfterEBSResponse(PaymentTxEntityDTO paymentTxEntityDTO);

	boolean checkTxEligible(PaymentTxEntityDTO payDto);

	void updateEmailIfNotExists(PaymentTxEntityDTO payDto);

	DetailsToSend getDetailsToSendEmail(PaymentTxEntityDTO payDTO);

	
}
