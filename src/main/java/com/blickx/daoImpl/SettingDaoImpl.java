package com.blickx.daoImpl;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import org.apache.log4j.Logger;
import org.hibernate.HibernateException;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.Transaction;
import org.hibernate.transform.Transformers;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;

import com.blickx.dao.SettingsDao;
import com.blickx.domain.Authentication;
import com.blickx.domain.ClientInformationEntity;
import com.blickx.domain.EditCustomerCommentEntity;
import com.blickx.domain.SMSGateWayEntity;
import com.blickx.output.EditCustomerCommentTo;
import com.blickx.settings.to.GetBalanceTo;
import com.blickx.sms.gateway.send.SendSMSRunnable;
import com.blickx.to.input.DetailsToSend;
import com.blickx.utill.CommonUtils;
import com.blickx.utill.MsgFormats;


public class SettingDaoImpl  implements SettingsDao {

	@Autowired
	SessionFactory sfactory;
	
	private Logger logger = Logger.getLogger(SettingDaoImpl.class);
	
	@Override
	public int updateBalance(String customer_id, BigDecimal newBalance, String reason) {
		
		logger.info("updateBalance Start"+"\t"+customer_id +"\t"+newBalance+"\t"+new Date());
		
		int resultset = 0;
		Session session = sfactory.openSession();
		Transaction tx = null;
		try {
			tx = session.beginTransaction();
			
			@SuppressWarnings({ })
			int resultWithAliasedBean  =  (int) session.createSQLQuery(
			"call update_balance(:cus_id, :balance, :ireason) ")
			.setString("cus_id", customer_id)
			.setBigDecimal("balance", newBalance)
			.setString("ireason", reason).executeUpdate();
			
			logger.info(resultWithAliasedBean);
			tx.commit();
		} catch (HibernateException e) {
			if (tx != null)
				tx.rollback();
			
			logger.error("Exception! " + e);
			return 0;
		} catch (Exception e) {
			if (tx != null)
				tx.rollback();
			
			logger.error("Exception! " + e);
			return 0;
		} finally {
			session.close();logger.info("UpdateBalanceAmount End"+"\t"+resultset +"\t"+new Date());
		}
		return 1;
		
	}

	@Override
	public GetBalanceTo getBalanceAmount(String customer_id) {
		GetBalanceTo resultset = new GetBalanceTo();
		
		logger.info("getBalanceAmount Start"+"\t"+customer_id +"\t"+new Date());
		
		Session session = sfactory.openSession();
		Transaction tx = null;
		try {
			tx = session.beginTransaction();
			
			@SuppressWarnings({ "rawtypes", "unchecked" })
			List<GetBalanceTo> resultWithAliasedBean  = (List) session.createSQLQuery(
			"select b.customer_id,b.balance_amount from billing b where customer_id = ? ORDER BY date DESC LIMIT 1")
			.setString(0, customer_id)
			.setResultTransformer(Transformers.aliasToBean(GetBalanceTo.class))
			.list();
			
			resultset = resultWithAliasedBean.get(0);
			
			tx.commit();
		} catch (HibernateException e) {
			if (tx != null)
				tx.rollback();
			
			logger.error("Exception! " + e);
			return resultset;
		} catch (Exception e) {
			if (tx != null)
				tx.rollback();
			
			logger.error("Exception! " + e);
			return resultset;
		} finally {
			session.close();
			logger.info("getBalanceAmount End"+"\t"+resultset +"\t"+new Date());
		}
		
		return resultset;	
	}

	@Override
	public BigDecimal getServiceCharge(String client_id) {
		
		BigDecimal resultset = null;
		Session session = sfactory.openSession();
		Transaction tx = null;
		try {
			tx = session.beginTransaction();
			
			BigDecimal resultWithAliasedBean  =  (BigDecimal) session.createSQLQuery(
					"SELECT service_charge from tax where client_id = ? order by service_charge desc limit 1")
					.setString(0, client_id)
					.uniqueResult();
			
			resultset = resultWithAliasedBean;
			
			logger.info(resultWithAliasedBean);
			tx.commit();
		} catch (HibernateException e) {
			if (tx != null)
				tx.rollback();
			
			logger.error("Exception! " + e);
			return resultset;
		} catch (Exception e) {
			if (tx != null)
				tx.rollback();
			
			logger.error("Exception! " + e);
			return resultset;
		} finally {
			session.close();logger.info("UpdateBalanceAmount End"+"\t"+resultset +"\t"+new Date());
		}
		return resultset;
	}

	@Override
	public List<String> getVc_numbers(String customer_id) {
		List<String> resultset = new ArrayList<String>();
		
		logger.info("getBalanceAmount Start"+"\t"+customer_id +"\t"+new Date());
		
		Session session = sfactory.openSession();
		Transaction tx = null;
		try {
			tx = session.beginTransaction();
			
			@SuppressWarnings({ "rawtypes", "unchecked" })
			List<String> resultWithAliasedBean  = (List) session.createSQLQuery(
			"select distinct vc_number from setupbox where customer_id=?")
			.setString(0, customer_id)
			.list();
			
			resultset = resultWithAliasedBean;
			
			tx.commit();
		} catch (HibernateException e) {
			if (tx != null)
				tx.rollback();
			
			logger.error("Exception! " + e);
			return resultset;
		} catch (Exception e) {
			if (tx != null)
				tx.rollback();
			
			logger.error("Exception! " + e);
			return resultset;
		} finally {
			session.close();logger.info("getBalanceAmount End"+"\t"+resultset +"\t"+new Date());
		}
		
		return resultset;
	}

	@Override
	public void addClientInfoForBlueToothPrint(
			ClientInformationEntity clientInfo) {
		// TODO Auto-generated method stub
		
		
		logger.info("addClientInfoForBlueToothPrint Start" + "\t" + clientInfo);
		
		Session session = sfactory.openSession();
		Transaction tx = null;
		try {
			tx = session.beginTransaction();
			
			//if(session.createQuery("from ClientInformationEntity cie where cie.clientId = ? and cie.printer_name = ?")))
			
			session.saveOrUpdate(clientInfo);
			
			tx.commit();
		} catch (HibernateException e) {
			logger.error("Exception! " + e);
			throw e;
		} catch (Exception e) {
			logger.error("Exception! " + e);
			throw e;
		} finally {
			
			session.close();logger.info("addClientInfoForBlueToothPrint End");
		}
		
		
	}

	@Override
	public ClientInformationEntity getClientInfoForBlueToothPrint(
			ClientInformationEntity clientInformationForPrint, String client_id) {
		// TODO Auto-generated method stub
		
		logger.info("getClientInfoForBlueToothPrint Start" + "\t" + clientInformationForPrint +": \n" + client_id);
		ClientInformationEntity response = new ClientInformationEntity();
		Session session = sfactory.openSession();
		Transaction tx = null;
		try {
			
			response = (ClientInformationEntity) session.createQuery("from ClientInformationEntity cie where cie.client_id = ? and cie.printer_name = ?")
				.setString(0, client_id)
				.setString(1, clientInformationForPrint.getPrinter_name())
				.list().get(0);
			
		} catch (HibernateException e) {
			logger.error("Exception! " + e);
			throw e;
		} catch (Exception e) {
			logger.error("Exception! " + e);
			return response;
		} finally {
			
			session.close();logger.info("addClientInfoForBlueToothPrint End");
		}
		
		return response;

	}

	public String saveOrUpdateComments(List<EditCustomerCommentTo> comments,
			String client_id) {
		
		Session session = sfactory.openSession();
		Transaction tx = null;
		try {
			tx = session.beginTransaction();
			for(EditCustomerCommentTo commentTo: comments){
				
				EditCustomerCommentEntity commentEntity = new EditCustomerCommentEntity();
				BeanUtils.copyProperties(commentTo, commentEntity);
				session.saveOrUpdate(commentEntity);
				
			}
		
			tx.commit();
		} catch (HibernateException e) {
			if(tx != null)tx.rollback();
			logger.error("SaveOrUpdate Comments Exception! " + e);
			return "Exception";
		} catch (Exception e) {
			if(tx != null)tx.rollback();
			logger.error("SaveOrUpdate Comments Exception! " + e);
			return "Exception";
		} finally {
			
			session.close();
			logger.info("SaveOrUpdate Comments DaoImpl End");
		}
		
		return "Success";
		
		
	}

	@SuppressWarnings("unchecked")
	@Override
	public String sendWishestoAll(String msg, String client_id) {
		Session session = sfactory.openSession();
		Transaction tx = null;
		try {
			tx = session.beginTransaction();
			logger.info("sendWishes Starting Dao Impl");
			
			List<Authentication> listOfAuths = session.createQuery("from Authentication a where a.client_id = ?").setString(0, client_id).list();
			
			if(listOfAuths.get(0).isMsg_alert()){
				
				SMSGateWayEntity smsGateWayEntity = (SMSGateWayEntity) session.load(SMSGateWayEntity.class, 1);
				
				SMSGateWayEntity smsGateWay = new SMSGateWayEntity();
				BeanUtils.copyProperties(smsGateWayEntity, smsGateWay);
			
				String contact = (String) session.createSQLQuery("select group_concat(c.contact_number) from customer c where c.client_id = ? and c.status = 1").setString(0, client_id).uniqueResult();
				int i = 0;
					if(CommonUtils.exists(contact)){
						logger.info("contact number " + contact + " is valid");
						DetailsToSend details = new DetailsToSend("", "", msg);
						details.setContact_number(contact);
						
						
						SendSMSRunnable sendSMSThread = new SendSMSRunnable(details, smsGateWay);
						Thread t = new Thread(sendSMSThread);
						t.start();
						i++;
						
					}
					
					
				
				logger.info("No.of Customers count of sent msgs: " + i);
				session.createSQLQuery("update authentication set msg_count = (msg_count+?) where client_id = ?").setInteger(0, contact.split(",").length).setString(1, listOfAuths.get(0).getClient_id()).executeUpdate();
			} else {
				return "Sorry! you don't have this facility, please contact server administrator to enable this feature";
			}
			
		
			tx.commit();
		} catch (HibernateException e) {
			if(tx != null)tx.rollback();
			logger.error("sendWishes Comments Exception! " + e);
			return "Exception";
		} catch (Exception e) {
			if(tx != null)tx.rollback();
			logger.error("sendWishes Comments Exception! " + e);
			return "Exception";
		} finally {
			
			session.close();
			logger.info("sendWishes Comments DaoImpl End");
		}
		
		return "SMS will reach all of your customers very soon.";
	}

	@Override
	public String getUpdateApkVersion() {
		// TODO Auto-generated method stub
		String updateApkVersion = "";
		Session session = sfactory.openSession();
		Transaction tx = null;
		try {
			tx = session.beginTransaction();
			logger.info("sendWishes Starting Dao Impl");
			
			updateApkVersion = (String) session.createSQLQuery("select updated_apk_version from updated_apk_version").list().get(0);
					
			tx.commit();
		} catch (HibernateException e) {
			if(tx != null)tx.rollback();
			logger.error("getUpdateApkVersionDaoImpl Exception! " + e);
			return "Exception";
		} catch (Exception e) {
			if(tx != null)tx.rollback();
			logger.error("getUpdateApkVersionDaoImpl  Exception! " + e);
			return "Exception";
		} finally {
			
			session.close();
			logger.info("getUpdateApkVersion DaoImpl End");
		}
		
		return updateApkVersion;
	}

	@Override
	public String paymentReminder(String msg, String client_id) {
		Session session = sfactory.openSession();
		Transaction tx = null;
		try {
			tx = session.beginTransaction();
			logger.info("sendWishes Starting Dao Impl");
			
			List<Authentication> listOfAuths = session.createQuery("from Authentication a where a.client_id = ?").setString(0, client_id).list();
			
			if(listOfAuths.get(0).isMsg_alert()){
				
				SMSGateWayEntity smsGateWayEntity = (SMSGateWayEntity) session.load(SMSGateWayEntity.class, 1);
				
				SMSGateWayEntity smsGateWay = new SMSGateWayEntity();
				BeanUtils.copyProperties(smsGateWayEntity, smsGateWay);
			
				String listOfContacts = (String) session.createSQLQuery("select group_concat(distinct contact_number) from customer c "
						+ "join bill_generation b on c.customer_id = b.customer_id and month(b.date) = case when month(current_date()) = 12 then 1 else (month(current_date()) -1) end "
						+ "where c.client_id = ? and b.paid_status = 0 and b.package_monthly_amount > 0 and c.status = 1").setString(0, client_id).uniqueResult();
				int i = 0;
					if(CommonUtils.exists(listOfContacts)){
						logger.info("contact number " + listOfContacts + " is valid");
						DetailsToSend details = new DetailsToSend("", "", msg);
						details.setContact_number(listOfContacts);
						
						
						SendSMSRunnable sendSMSThread = new SendSMSRunnable(details, smsGateWay);
						Thread t = new Thread(sendSMSThread);
						t.start();
						i++;
						
					}
				
				logger.info("No.of Customers count of sent msgs: " + i);
				session.createSQLQuery("update authentication set msg_count = (msg_count+?) where client_id = ?").setInteger(0, listOfContacts.split(",").length).setString(1, listOfAuths.get(0).getClient_id()).executeUpdate();
			} else {
				return "Sorry! you don't have this facility, please contact server administrator to enable this feature";
			}
			
		
			tx.commit();
		} catch (HibernateException e) {
			if(tx != null)tx.rollback();
			logger.error("sendWishes Comments Exception! " + e);
			return "Exception";
		} catch (Exception e) {
			if(tx != null)tx.rollback();
			logger.error("sendWishes Comments Exception! " + e);
			return "Exception";
		} finally {
			
			session.close();
			logger.info("sendWishes Comments DaoImpl End");
		}
		
		return "SMS will reach all of your customers very soon.";	}

}
