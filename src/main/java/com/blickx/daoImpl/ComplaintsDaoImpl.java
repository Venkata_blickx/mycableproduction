package com.blickx.daoImpl;


import java.math.BigInteger;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;
import java.util.Map;

import org.apache.log4j.Logger;
import org.hibernate.HibernateException;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.Transaction;
import org.hibernate.transform.Transformers;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;

import com.blickx.controller.ComplaintsController;
import com.blickx.dao.ComplaintsDao;
import com.blickx.dao.UserDao;
import com.blickx.domain.Authentication;
import com.blickx.domain.ComplaintEntity;
import com.blickx.domain.DeviceEntity;
import com.blickx.domain.SMSGateWayEntity;
import com.blickx.id.generator.ComplaintIDGenerator;
import com.blickx.searchEntities.AssignComplaintSearch;
import com.blickx.searchEntities.CloseComplaint;
import com.blickx.searchEntities.GetComplaint;
import com.blickx.searchEntities.SearchByDateEntity;
import com.blickx.sms.gateway.send.SendSMS;
import com.blickx.sms.gateway.send.SendSMSRunnable;
import com.blickx.to.ComplaintRatingObject;
import com.blickx.to.ComplaintRatingTo;
import com.blickx.to.ComplaintStatusTo;
import com.blickx.to.ComplaintSummaryTo;
import com.blickx.to.GetComplaintTo;
import com.blickx.to.GetOpenClosedComplaintTo;
import com.blickx.to.input.ComplaintInputTo;
import com.blickx.to.input.DetailsToSend;
import com.blickx.utill.CommonUtils;
import com.blickx.utill.SendSMSThroughBSMS;
//import com.blickx.utill.MsgFormats;
//import com.blickx.mycable.beme.dto.ComplaintBemeInputDTO;
import com.blickx.output.GetComplaintList;

public class ComplaintsDaoImpl implements ComplaintsDao{

	private Logger logger = Logger.getLogger(ComplaintsDaoImpl.class);
	
	@Autowired
	SessionFactory sfactory;
	
	@Autowired
	ComplaintIDGenerator idGenerator;
	
	@Autowired
	UserDao udao;
	
	@Override
	public String addComplaint(ComplaintEntity complaint) {
		
		Session session=sfactory.openSession();
		Transaction tx = null;
		
		try{
			tx = session.beginTransaction();
			complaint.setComplaint_id(idGenerator.getNextSid(session, complaint.getClient_id()));
			complaint.setDate(new Date());
			session.save(complaint);
			tx.commit();
		}catch(Exception e){
			if(tx != null)tx.rollback();
			logger.error(e);
			return "Not Added";
		}finally{
			session.close();
		}
		return "Complaint Registered";
	}

	@SuppressWarnings("unchecked")
	@Override
	public ComplaintEntity getComplaint(GetComplaint getComplaint) {
		List<GetComplaintTo> resultset = new ArrayList<GetComplaintTo>();
		
		ComplaintEntity result = new ComplaintEntity();
		Session session = sfactory.openSession();
		Transaction tx = null;
		try {
			tx = session.beginTransaction();
			
			logger.info("Calling GetComplaint");
			/*if(CommonUtils.exists(getComplaint.getC_id())){
				getComplaint.setCutomer_id((String) session.createSQLQuery("select customer_id from customer where c_id = ? and client_id = ?")
						.setString(0, getComplaint.getC_id()).setString(1, getComplaint.getClient_id()).uniqueResult());
			}
			List<GetComplaintTo> resultWithAliasedBean  =  session.createSQLQuery(
					"CALL GetComplaint(:cid, :complaint_no, :client_id)")
					.setString("cid", getComplaint.getC_id())
					.setString("complaint_no", getComplaint.getComplaint_no())
					.setString("client_id", getComplaint.getClient_id())
					.setResultTransformer(Transformers.aliasToBean(GetComplaintTo.class))
					.list();
			*/
			
			ComplaintEntity complaint  = (ComplaintEntity) session.load(ComplaintEntity.class, getComplaint.getComplaint_no());
			System.out.println(complaint);
			
			BeanUtils.copyProperties(complaint, result);
			//resultset =  resultWithAliasedBean;
			
			tx.commit();
		} catch (HibernateException e) {
			if (tx != null)
				tx.rollback();
			e.printStackTrace();
			logger.error("Exception! " + e);
		} finally {
			session.close();
		}
		return result;
	}

	@Override
	public List<GetOpenClosedComplaintTo> getOpenOrClosedComplaints(String client_id, String type) {
		List<GetOpenClosedComplaintTo> resultset = new ArrayList<GetOpenClosedComplaintTo>();
		
		Session session = sfactory.openSession();
		Transaction tx = null;
		try {
			tx = session.beginTransaction();
			
			logger.info("Calling GetOpenClosedComplaint SP");
			
			@SuppressWarnings({ "rawtypes", "unchecked" })
			List<GetOpenClosedComplaintTo> resultWithAliasedBean  = (List) session.createSQLQuery(
			"CALL GetOpenClosedComplaint(:client_id, :type)")
				.setString("client_id", client_id)
				.setString("type", type)
				.setResultTransformer(Transformers.aliasToBean(GetOpenClosedComplaintTo.class))
				.list();
			
			resultset = resultWithAliasedBean;
			
			tx.commit();
		} catch (HibernateException e) {
			if (tx != null)
				tx.rollback();
			e.printStackTrace();
			logger.error("Exception! " + e);
		} finally {
			session.close();
		}
		return resultset;
	}

	@Override
	public List<GetOpenClosedComplaintTo> getOpenOrClosedComplaintsByDate(SearchByDateEntity dates) {
		List<GetOpenClosedComplaintTo> resultset = new ArrayList<GetOpenClosedComplaintTo>();
		
		Session session = sfactory.openSession();
		Transaction tx = null;
		try {
			tx = session.beginTransaction();
			
			logger.info("Calling GetOpenClosedComplaintByDate SP");
			logger.info(dates.getFrom());
			@SuppressWarnings({ "rawtypes", "unchecked" })
			List<GetOpenClosedComplaintTo> resultWithAliasedBean  = (List) session.createSQLQuery(
			"CALL GetComplaintByDate(:client_id, :fromdate, :todate)")
				.setString("client_id", dates.getClient_id())
				.setDate("fromdate", dates.getFrom())
				.setDate("todate", dates.getTo())
				.setResultTransformer(Transformers.aliasToBean(GetOpenClosedComplaintTo.class))
				.list();
			
			resultset = resultWithAliasedBean;
			
			tx.commit();
		} catch (HibernateException e) {
			if (tx != null)
				tx.rollback();
			e.printStackTrace();
			logger.error("Exception! " + e);
		} finally {
			session.close();
		}
		return resultset;
	}

	@Override
	public void updateComplaint(ComplaintEntity complaint) throws HibernateException {
		Session session = sfactory.openSession();
		Transaction tx;
		try{
			
			tx = session.beginTransaction();
			ComplaintEntity updatedComplaint = (ComplaintEntity) session.load(ComplaintEntity.class, complaint.getComplaint_id());
			updatedComplaint.setStatus(complaint.getStatus());
			updatedComplaint.setComment(complaint.getComment());
			updatedComplaint.setClosed_date(complaint.getClosed_date());
			session.update(updatedComplaint);
			tx.commit();
		}catch(HibernateException e){
			logger.error("from DaoImpl"+e);
			throw new HibernateException("Hibernate Exception");
		}finally{
			session.close();
		}
	}

	@SuppressWarnings("unchecked")
	@Override
	public ComplaintStatusTo getComplaintStatus(String client_id) {
		ComplaintStatusTo resultset = new ComplaintStatusTo();
		
		Session session = sfactory.openSession();
		Transaction tx = null;
		try {
			tx = session.beginTransaction();
			
			logger.info("Calling GetComplain_status_Details SP");
			List<ComplaintStatusTo> resultWithAliasedBean = (List<ComplaintStatusTo>) session.createSQLQuery(
					"select count(CASE WHEN c.status = 0 THEN 1 END) AS 'open', "+
							"count(CASE WHEN c.status = 2 THEN 1 END) AS 'assign', "+ 
							"count(CASE WHEN c.status = 3 THEN 1 END) AS 'resolve' "+ 
							"from complaint c"
					+ " where c.client_id = ?;")
					.setString(0, client_id)
					.setResultTransformer(Transformers.aliasToBean(ComplaintStatusTo.class)).list();
			//ComplaintStatusTo resultWithAliasedBean  =  (ComplaintStatusTo) list.get(0);
			
			resultset = resultWithAliasedBean.get(0);
			
			tx.commit();
		} catch (HibernateException e) {
			if (tx != null)
				tx.rollback();
			logger.error("Exception! " + e);
		} finally {
			session.close();
		}
		return resultset;

	}

	@SuppressWarnings("unchecked")
	public List<ComplaintSummaryTo> getComplaintSummaryOpen(String client_id) {
		List<ComplaintSummaryTo> resultset = new ArrayList<ComplaintSummaryTo>();
		
		Session session = sfactory.openSession();
		Transaction tx = null;
		try {
			tx = session.beginTransaction();
			
			logger.info("Calling GetComplain_status_Details SP");
			List<ComplaintSummaryTo> resultWithAliasedBean = (List<ComplaintSummaryTo>) session.createSQLQuery(
					"call get_complaints(:client_id)")
						.setString("client_id", client_id)
						.setResultTransformer(Transformers.aliasToBean(ComplaintSummaryTo.class)).list();
			//ComplaintStatusTo resultWithAliasedBean  =  (ComplaintStatusTo) list.get(0);
			
			resultset = resultWithAliasedBean;
			
			tx.commit();
		} catch (HibernateException e) {
			if (tx != null)
				tx.rollback();
			logger.error("Exception! " + e);
		} finally {
			session.close();
		}
		return resultset;

	}

	@Override
	public int makeAssign(AssignComplaintSearch assign) {

		Session session = sfactory.openSession();
		Transaction tx = null;
		try{
			tx = session.beginTransaction();
			logger.info("AssigneComplaint from DaoIMPl INput: " + assign);
			/*String employee_id = (String) session.createSQLQuery("select employee_id from employee where employee_name='"+assign.getEmployee_name().trim()+"';")
					.uniqueResult();*/
			
			ComplaintEntity comp = (ComplaintEntity) session.load(ComplaintEntity.class, assign.getComplaint_id());
			comp.setAssigned_to(assign.getEmployee_name());
			comp.setAssigned_date(new Date());
			comp.setStatus("2");
			session.update(comp);
			tx.commit();
		}catch(HibernateException e){
			if(tx != null)tx.rollback();
			logger.error(e);
			return 0;
		}finally{
			session.close();
		}
		return 1;
	}

	@Override
	public int closeComplaint(CloseComplaint closeComplaint) {
		Session session = sfactory.openSession();
		String customer_id =  "";
		Transaction tx = null;
		try{
			tx = session.beginTransaction();
			
			/*String employee_id = (String) session.createSQLQuery("select employee_id from employee where employee_name='"+closeComplaint.getEmployee_name().trim()+"';")
					.uniqueResult();*/
			logger.info("CloseCOmplaint Dao input:" + closeComplaint);
			ComplaintEntity comp = (ComplaintEntity) session.load(ComplaintEntity.class, closeComplaint.getComplaint_id());
				comp.setClosed_date(new Date());
				comp.setClosed_by(closeComplaint.getClosed_by());
				comp.setStatus("1");
				comp.setComment(closeComplaint.getComment());
				
				customer_id = comp.getCustomer_id();
				
				List<Authentication> listOfAuths = session.createQuery("from Authentication a where a.client_id = ?").setString(0, customer_id.substring(0, 8)).list();
				
				if(listOfAuths.get(0).isMsg_alert()){
					// 1) templateID, 2) paidAmount, 3) Cable Tv, 4) bill_recipt_no, 5) Regards(client name)
					String clientContactNumber = listOfAuths.get(0).getClientContactNumber();
					if(!CommonUtils.exists(clientContactNumber))clientContactNumber = "";
					
					DetailsToSend details = new DetailsToSend("43219", comp.getComplaint_id() + "", " Cable Tv ", listOfAuths.get(0).getClient_name(), clientContactNumber, "");
					
					
					//DetailsToSend details = new DetailsToSend(customer_id, client_id, MsgFormats.getPayedSMS(paid, bill_id, clientCredetnials.getClient_name()));
					details.setContact_number((String) session.createQuery("select c.contact_number from Customer c where c.customer_id = ?").setString(0, customer_id).uniqueResult());
					SMSGateWayEntity smsGateWayEntity = (SMSGateWayEntity) session.load(SMSGateWayEntity.class, 2);
					
					SMSGateWayEntity smsGateWay = new SMSGateWayEntity();
					BeanUtils.copyProperties(smsGateWayEntity, smsGateWay);
					
					/*SendSMSRunnable sendSMSThread = new SendSMSRunnable(session, details, smsGateWay);
					Thread t = new Thread(sendSMSThread);
					t.start();*/
					
					SendSMSThroughBSMS sendSms = new SendSMSThroughBSMS(listOfAuths.get(0).getClient_id(),"104",details.getContact_number(),comp.getComplaint_id(),
							"MYCABLE",listOfAuths.get(0).getSmsRegardsMsg(),clientContactNumber);
							Thread sendSmsThread = new Thread(sendSms);
							sendSmsThread.start();
					
					
					logger.info("if(clientCredetials.isMsg_alert() is ended.........."); 
					
					//System.out.println(SendSMS.sendSMSToCustomer(session, details));
						session.createSQLQuery("update authentication set msg_count = msg_count+1 where client_id = ?").setParameter(0, listOfAuths.get(0).getClient_id()).executeUpdate();
					
					
					
				}
				
			session.update(comp);
			tx.commit();
		}catch(HibernateException e){
			if(tx != null)tx.rollback();
			logger.error(e);
			return 0;
		}finally{
			session.close();
		}
		return 1;
	}

	@Override
	public ComplaintRatingObject getComplaintRating(String client_id) {
		
		ComplaintRatingObject result = new  ComplaintRatingObject();
		List<ComplaintRatingTo> resultset = new ArrayList<ComplaintRatingTo>();
		
		Session session = sfactory.openSession();
		Transaction tx = null;
		try {
			tx = session.beginTransaction();
			
			logger.info("Calling GetComplain_status_Details SP");
			@SuppressWarnings("unchecked")
			List<ComplaintRatingTo> resultWithAliasedBean = (List<ComplaintRatingTo>) session.createSQLQuery(
					"call GetComplaintSummaryRating(:client_id)").setString("client_id", client_id).setResultTransformer(Transformers.aliasToBean(ComplaintRatingTo.class)).list();
			//ComplaintStatusTo resultWithAliasedBean  =  (ComplaintStatusTo) list.get(0);
			
			BigInteger count = (BigInteger) session.createSQLQuery("select count(*) from complaint where client_id = ?").setString(0, client_id).uniqueResult();
			
			result.setComplaintRatings(resultWithAliasedBean);
			result.setTotalComplaints(count.intValue());
			
			resultset = resultWithAliasedBean;
			
			tx.commit();
		} catch (HibernateException e) {
			if (tx != null)
				tx.rollback();
			logger.error("Exception! " + e);
		} finally {
			session.close();
		}
		return result;

	}
	

	@Override
	public List<String> getC_ids(String client_id) {
		List<String> resultset = new ArrayList<String>();
		
		Session session = sfactory.openSession();
		Transaction tx = null;
		try {
			tx = session.beginTransaction();
			
			@SuppressWarnings("unchecked")
			List<String> resultWithAliasedBean = (List<String>) session.createSQLQuery("SELECT distinct c.c_id FROM customer c where c.client_id = ?")
					.setParameter(0, client_id)
					.list();
			//ComplaintStatusTo resultWithAliasedBean  =  (ComplaintStatusTo) list.get(0);
			
			resultset = resultWithAliasedBean;
			
			tx.commit();
		} catch (HibernateException e) {
			if (tx != null)
				tx.rollback();
			logger.error("Exception! " + e);
		} finally {
			session.close();
		}
		return resultset;
	}

	@Override
	public String addComplaintDesktop(ComplaintInputTo complaint) {
		Session session=sfactory.openSession();
		Transaction tx = null;
		String res = "";
		
		try{
			tx = session.beginTransaction();
			
			ComplaintEntity comp =  new ComplaintEntity();
			BeanUtils.copyProperties(complaint, comp);
			
			
			String complaint_id = idGenerator.getNextSid(session, comp.getClient_id());
			String customer_id = (String) session.createSQLQuery("select customer_id from customer where c_id=? and client_id = ?")
					.setString(0, complaint.getC_id()).setString(1, comp.getClient_id()).uniqueResult();
				comp.setCustomer_id(customer_id);
				comp.setDate(new Date());
				comp.setStatus("0");
				comp.setComplaint_id(complaint_id);
			session.save(comp);
			
			res = complaint_id;
			
			Authentication clientCredetnials = (Authentication) session.createQuery("from Authentication a where a.client_id = ?")
					.setParameter(0, complaint.getClient_id()).list().get(0);
			
			if(clientCredetnials.isMsg_alert()){
				// 1) templateID, 2) paidAmount, 3) Cable Tv, 4) bill_recipt_no, 5) Regards(client name)
				String clientContactNumber = clientCredetnials.getClientContactNumber();
				if(!CommonUtils.exists(clientContactNumber))clientContactNumber = "";
				
				DetailsToSend details = new DetailsToSend("43218", complaint_id, clientCredetnials.getClient_name(), clientContactNumber, "", "");
				
				
				//DetailsToSend details = new DetailsToSend(customer_id, client_id, MsgFormats.getPayedSMS(paid, bill_id, clientCredetnials.getClient_name()));
				details.setContact_number((String) session.createQuery("select c.contact_number from Customer c where c.customer_id = ?").setString(0, customer_id).uniqueResult());
				SMSGateWayEntity smsGateWayEntity = (SMSGateWayEntity) session.load(SMSGateWayEntity.class, 2);
				
				SMSGateWayEntity smsGateWay = new SMSGateWayEntity();
				BeanUtils.copyProperties(smsGateWayEntity, smsGateWay);
				
				/*SendSMSRunnable sendSMSThread = new SendSMSRunnable(session, details, smsGateWay);
				Thread t = new Thread(sendSMSThread);
				t.start();*/
				
				SendSMSThroughBSMS sendSms = new SendSMSThroughBSMS(clientCredetnials.getClient_id(),"103",details.getContact_number(),complaint_id
						,clientCredetnials.getSmsRegardsMsg(),clientContactNumber);
					Thread sendSmsThread = new Thread(sendSms);
					sendSmsThread.start();
				
				logger.info("if(clientCredetials.isMsg_alert() is ended.........."); 
				
				//System.out.println(SendSMS.sendSMSToCustomer(session, details));
							
				
			}
			
			tx.commit();
		}catch(Exception e){
			if(tx != null)tx.rollback();
			logger.error(e);
			return res;
		}finally{
			session.close();
		}
		return res;
	}

	@Override
	public List<GetComplaintList> GetComplaintList(
			String client_id, Map<String, String> input) {
		
		List<GetComplaintList> result = new ArrayList<GetComplaintList>();
		
		StringBuffer query = new StringBuffer("");
			query.append("select c.customer_id, c.c_id, c.customer_name, c.contact_number, c.email, c.line1, c.line2, ");
			query.append("co.complaint_id, co.details, co.date as created_date, ");
			query.append("(select employee_name from employee where employee_id = co.assigned_to) as assigned_to, ");
			query.append("(select employee_name from employee where employee_id = co.closed_by) as closed_by, ");
			query.append("case when co.status = 0 then 'Open' when co.status = 2 then 'Open' when co.status = 3 then 'Resolved' when co.status = 1 then 'Closed' end as status, ");
			query.append("ifnull(co.assigned_date, date(co.date)) as assigned_date, co.closed_date, co.source, co.comment ");
			query.append("from customer c, complaint co ");
			query.append("where c.customer_id = co.customer_id and co.client_id = :clientId ");
			query.append("and co.status in (2,3,1) and (co.assigned_to = :employee_id ) order by co.date desc");
				Session session = sfactory.openSession();
				Transaction tx = null;
				try {
					tx = session.beginTransaction();
					logger.info("ime_number " + input.get("ime_number"));
					DeviceEntity device = (DeviceEntity) session.createQuery("from DeviceEntity d where d.client_id = ? and d.ime_number = ?" )
							.setString(0, client_id)
							.setString(1, input.get("ime_number")).list().get(0);
					
					logger.info("ime_number " + input.get("ime_number"));
					logger.info(device);
					logger.info("employee_id " + device.getEmployee_id());
					@SuppressWarnings("unchecked")
					List<GetComplaintList> resultWithAliasedBean =  session.createSQLQuery(query.toString())
								.setParameter("clientId", client_id)
								.setParameter("employee_id", device.getEmployee_id())
								.setResultTransformer(Transformers.aliasToBean(GetComplaintList.class))
							.list();
					
					result = resultWithAliasedBean;
					
					tx.commit();
				} catch (HibernateException e) {
					if (tx != null)
						tx.rollback();
					logger.error("Exception! " + e);
				} finally {
					session.close();
				}	
				
				
		return result;
		
	}

	@Override
	public List<com.blickx.output.GetComplaintList> GetUpdatedComplaintList(
			String client_id, Map<String, String> input) {
		
		List<GetComplaintList> result = new ArrayList<GetComplaintList>();
		
		
		
		Calendar cal = Calendar.getInstance();
		
		
		try {
			cal.setTime(new SimpleDateFormat("yyyy-MM-dd HH:mm:ss").parse(input.get("last_sync_timing")));
		} catch (ParseException e1) {
			// TODO Auto-generated catch block
			logger.error(e1);
			e1.printStackTrace();
		}
		//cal.add(Calendar.MINUTE, 1);
		Date oneHourBack = cal.getTime();
		logger.info("Given timestamp: " + input.get("last_sync_timing"));
		logger.info("minute back timestamp: " + oneHourBack);
		String lastSyncTime = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss").format(oneHourBack);
		
		StringBuffer query = new StringBuffer("");
				query.append("select c.customer_id, c.c_id, c.customer_name, c.contact_number, c.email, c.line1, c.line2, ");
				query.append("co.complaint_id, co.details, co.date as created_date, ");
				query.append("(select employee_name from employee where employee_id = co.assigned_to) as assigned_to, ");
				query.append("(select employee_name from employee where employee_id = co.closed_by) as closed_by, ");
				query.append("case when co.status = 0 then 'Open' when co.status = 2 then 'Open' when co.status = 3 then 'Resolved'  when co.status = 1 then 'Closed' end as status, ");
				query.append("ifnull(co.assigned_date, date(co.date)) as assigned_date, co.closed_date, co.source, co.comment ");
				query.append("from customer c, complaint co ");
				//query.append("where c.customer_id = co.customer_id and co.client_id = :clientId and (co.date > \'" + lastSyncTime +"\' or co.assigned_date > \'" + lastSyncTime +"\' or co.closed_date > \'" + lastSyncTime +"\') ");
				query.append("where c.customer_id = co.customer_id and co.client_id = :clientId and (co.assigned_date > \'" + lastSyncTime +"\') ");	
				query.append("and co.status in (2,3,1) and (co.assigned_to = :employee_id ) order by co.date desc");
				Session session = sfactory.openSession();
				Transaction tx = null;
				try {
					
					
					
					
/*					String employee_id = (String) session.createSQLQuery("select distinct employee_id from device where ime_number = ?")
							.setString(0, input.get("ime_number"))
							.uniqueResult();*/
					DeviceEntity device = (DeviceEntity) session.createQuery("from DeviceEntity d where d.client_id = ? and d.ime_number = ?" )
							.setString(0, client_id)
							.setString(1, input.get("ime_number")).list().get(0);
					
					logger.info("ime_number " + input.get("ime_number"));
					logger.info(device);
					logger.info("employee_id " + device.getEmployee_id());
					@SuppressWarnings("unchecked")
					List<GetComplaintList> resultWithAliasedBean =  session.createSQLQuery(query.toString())
								.setParameter("clientId", client_id)
								.setParameter("employee_id", device.getEmployee_id())
								.setResultTransformer(Transformers.aliasToBean(GetComplaintList.class))
							.list();
					//ComplaintStatusTo resultWithAliasedBean  =  (ComplaintStatusTo) list.get(0);
					
					result = resultWithAliasedBean;
					
					
				} catch (HibernateException e) {
					if (tx != null)
						tx.rollback();
					logger.error("Exception! " + e);
				} finally {
					session.close();
				}	
				
				
		return result;
		

	}

	@Override
	public Integer resolveComplaint(CloseComplaint closeComplaint) {
		Session session = sfactory.openSession();
		String customer_id =  "";
		Transaction tx = null;
		try{
			tx = session.beginTransaction();
			
			/*String employee_id = (String) session.createSQLQuery("select employee_id from employee where employee_name='"+closeComplaint.getEmployee_name().trim()+"';")
					.uniqueResult();*/
			logger.info("CloseCOmplaint Dao input:" + closeComplaint);
			ComplaintEntity comp = (ComplaintEntity) session.load(ComplaintEntity.class, closeComplaint.getComplaint_id());
				comp.setClosed_date(closeComplaint.getClosed_date());
				comp.setClosed_by(closeComplaint.getClosed_by());
				comp.setStatus("3");
				comp.setComment(closeComplaint.getComment());
				
				
			session.update(comp);
			tx.commit();
		}catch(HibernateException e){
			if(tx != null)tx.rollback();
			logger.error(e);
			return 0;
		}finally{
			session.close();
		}
		return 1;
	}

	/*@Override
	public String addComplaintBeeMe(ComplaintBemeInputDTO complaint) {
		Session session=sfactory.openSession();
		Transaction tx = null;
		String res = "";
		
		try{
			tx = session.beginTransaction();
			
			ComplaintEntity comp =  new ComplaintEntity();
			BeanUtils.copyProperties(complaint, comp);
			
			
			String complaint_id = idGenerator.getNextSid(session, comp.getClient_id());
			String customer_id = (String) session.createSQLQuery("select customer_id from customer where c_id=? and client_id = ?")
					.setString(0, complaint.getC_id()).setString(1, comp.getClient_id()).uniqueResult();
				comp.setCustomer_id(customer_id);
				comp.setStatus("0");
				comp.setComplaint_id(complaint_id);
			session.save(comp);
			
			res = complaint_id;
			
			Authentication clientCredetnials = (Authentication) session.createQuery("from Authentication a where a.client_id = ?")
					.setParameter(0, complaint.getClient_id()).list().get(0);
			
			if(clientCredetnials.isMsg_alert()){
				// 1) templateID, 2) paidAmount, 3) Cable Tv, 4) bill_recipt_no, 5) Regards(client name)
				String clientContactNumber = clientCredetnials.getClientContactNumber();
				if(!CommonUtils.exists(clientContactNumber))clientContactNumber = "";
				
				DetailsToSend details = new DetailsToSend("43218", complaint_id, clientCredetnials.getClient_name(), clientContactNumber, "", "");
				if(CommonUtils.exists(clientCredetnials.getSmsRegardsMsg()))details.setF3(clientCredetnials.getSmsRegardsMsg());
				
				
				//DetailsToSend details = new DetailsToSend(customer_id, client_id, MsgFormats.getPayedSMS(paid, bill_id, clientCredetnials.getClient_name()));
				details.setContact_number((String) session.createQuery("select c.contact_number from Customer c where c.customer_id = ?").setString(0, customer_id).uniqueResult());
				SMSGateWayEntity smsGateWayEntity = (SMSGateWayEntity) session.load(SMSGateWayEntity.class, 2);
				
				SMSGateWayEntity smsGateWay = new SMSGateWayEntity();
				BeanUtils.copyProperties(smsGateWayEntity, smsGateWay);
				
				SendSMSRunnable sendSMSThread = new SendSMSRunnable(session, details, smsGateWay);
				Thread t = new Thread(sendSMSThread);
				t.start();
				logger.info("if(clientCredetials.isMsg_alert() is ended.........."); 
				
				//System.out.println(SendSMS.sendSMSToCustomer(session, details));
							
				
			}
			
			tx.commit();
		}catch(Exception e){
			if(tx != null)tx.rollback();
			logger.error(e);
			return res;
		}finally{
			session.close();
		}
		return res;

	}
	*/

}
