package com.blickx.daoImpl;

import java.util.ArrayList;
import java.util.List;

import org.apache.log4j.Logger;
import org.hibernate.HibernateException;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.Transaction;
import org.hibernate.transform.Transformers;
import org.springframework.beans.factory.annotation.Autowired;

import com.blickx.dao.PDAReprotDao;
import com.blickx.to.PDAReportDto;
import com.blickx.to.PackageReportsForAndroidTo;

public class PDAReportDaoImpl  implements PDAReprotDao{
	
	
	
	@Autowired
	 SessionFactory sfacFactory;

	private Logger logger = Logger.getLogger(PDAReportDaoImpl.class);
	
	@Override
	public List<PDAReportDto> getPDAReqport(String client_id) {
		List<PDAReportDto> pdaReport = new ArrayList<PDAReportDto>();
		Session session = null;
		Transaction tx = null;
		try{
			
			session = sfacFactory.openSession();
			tx = session.beginTransaction();
			
			logger.info("Calling PDAReport SP");

			@SuppressWarnings({ "rawtypes", "unchecked" })
			List<PDAReportDto> resultWithPdaAliase = (List) session
					.createSQLQuery("CALL pdaReport(:clientId)")
					.setString("clientId", client_id)
					.setResultTransformer(
							Transformers
									.aliasToBean(PDAReportDto.class))
					.list();

			pdaReport = resultWithPdaAliase;
			tx.commit();
		}catch(HibernateException e)
		{
			e.printStackTrace();
			tx.rollback();
		}finally{
			if(session!=null)
				session.close();
		}
		return pdaReport;
	}
	

}
