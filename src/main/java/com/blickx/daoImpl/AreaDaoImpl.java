package com.blickx.daoImpl;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import org.apache.log4j.Logger;
import org.hibernate.HibernateException;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.Transaction;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;

import com.blickx.dao.AreadDao;
import com.blickx.domain.AreaEntity;
import com.blickx.exceptions.AreaAlreadyAssignedToCustomException;
import com.blickx.exceptions.AreaAlreadyAssignedToEmployeeException;
import com.blickx.settings.to.AreaEntityTo;

public class AreaDaoImpl implements AreadDao {

	private Logger logger = Logger.getLogger(AreaDaoImpl.class);
	
	@Autowired
	SessionFactory sfactory;
	
	public void addAreaDetails(AreaEntity area) {

		logger.info("addAreaDetails Starting ....");
		Session session = null;
		Transaction tx = null;
		try {
			session = sfactory.openSession();
			tx = session.beginTransaction();
			
			Date date = new Date();
			area.setCreatedTimeStamp(date);
			area.setLastUpdatedTimestamp(date);
			session.save(area);
			
			tx.commit();
		} catch(HibernateException e) {
			if(tx != null)tx.rollback();
			logger.error(e);
			throw e;
		} catch(Exception e) {
			if(tx != null)tx.rollback();
			logger.error(e);
			throw e;
		} finally{
			
			if(session != null)session.close();
			logger.info("addAreaDetails Ending ....");	
		}
	}

	@Override
	public AreaEntity getAreaDetailsById(AreaEntity area) {

		logger.info("getAreaDetails Starting ....");
		AreaEntity areaTo = new AreaEntity();
		AreaEntity areaEntity = null;
		Session session = null;
		try {
			session = sfactory.openSession();
			
			areaEntity = (AreaEntity) session.load(AreaEntity.class, area.getId());
			logger.info(areaEntity);
			
			BeanUtils.copyProperties(areaEntity, areaTo);
			
		} catch(HibernateException e) {
			logger.error(e);
			throw e;
		} catch(Exception e) {
			logger.error(e);
			throw e;
		} finally {
			if(session != null)session.close();
			logger.info("getAreaDetails Ending ....");
		}
		logger.info(areaTo);
		return areaTo;
	}

	@Override
	public void updateAreaDetails(AreaEntity area) {

		logger.info("updateAreaDetails Starting ....");
		Session session = null;
		Transaction tx = null;
		try{
			session = sfactory.openSession();
			tx = session.beginTransaction();
			AreaEntity loadedArea = (AreaEntity) session.load(AreaEntity.class, area.getId());
			Date date = new Date();
			loadedArea.setLastUpdatedTimestamp(date);
			loadedArea.setAreaCode(area.getAreaCode());
			loadedArea.setAreaDescription(area.getAreaDescription());
			session.saveOrUpdate(loadedArea);
			
			tx.commit();
		}catch(HibernateException e){
			if(tx != null)tx.rollback();
			logger.error(e);
			throw e;
		}catch(Exception e){
			if(tx != null)tx.rollback();
			logger.error(e);
			throw e;
		}finally{
			if(session != null)session.close();
			logger.info("updateAreaDetails Ending ....");
		}
		
	}

	@Override
	public void deleteAreaDetails(AreaEntityTo areaTo) throws AreaAlreadyAssignedToCustomException, AreaAlreadyAssignedToEmployeeException {
		// TODO Auto-generated method stub

		logger.info("deleteAreaDetails Starting ....");
		Session session = null;
		Transaction tx = null;
		try{
			session = sfactory.openSession();
			tx = session.beginTransaction();
			
			
			if(session.createSQLQuery("select * from area_customer_map where area_id = ?").setInteger(0, new Integer(areaTo.getId())).list().size() > 0){
				throw new AreaAlreadyAssignedToCustomException("Area already assigned some Employee! could not be deleted");
			} if(session.createSQLQuery("select * from area_employee_map where area_id = ?").setInteger(0, new Integer(areaTo.getId())).list().size() > 0){
				throw new AreaAlreadyAssignedToEmployeeException("Area already assigned some Employee! could not be deleted");
			} else {
				AreaEntity area = (AreaEntity) session.load(AreaEntity.class, areaTo.getId());
				session.delete(area);
			}
			
			
			
			tx.commit();
		}catch(AreaAlreadyAssignedToCustomException e){
			if(tx != null)tx.rollback();
			logger.error(e);
			throw e;
		}catch(AreaAlreadyAssignedToEmployeeException e){
			if(tx != null)tx.rollback();
			logger.error(e);
			throw e;
		}catch(HibernateException e){
			if(tx != null)tx.rollback();
			logger.error(e);
			throw e;
		}catch(Exception e){
			if(tx != null)tx.rollback();
			logger.error(e);
			throw e;
		}finally{
			if(session != null)session.close();
			logger.info("deleteAreaDetails Starting ....");
		}
		
	}

	@SuppressWarnings("unchecked")
	@Override
	public List<AreaEntityTo> getAllAreaDetails(String client_id) {

		logger.info("getAllAreaDetails Starting ....");
		List<AreaEntity> response = new ArrayList<AreaEntity>();
		List<AreaEntityTo> responseTo = new ArrayList<AreaEntityTo>();
		Session session = null;
		try{
			session = sfactory.openSession();
			
			response = session.createQuery("from AreaEntity ae where ae.clientId = ?").setString(0, client_id).list();
			for(AreaEntity area: response){
				AreaEntityTo areaTo = new AreaEntityTo();
				BeanUtils.copyProperties(area, areaTo);
				String employeesGotAssigned = (String) session.createSQLQuery("select group_concat(distinct e.employee_name) from employee e, area_employee_map ae " 
																+"where e.employee_id = ae.employee_id "
																+"and ae.end_date is null and ae.area_id = ?").setInteger(0,area.getId()).uniqueResult();
				areaTo.setEmployeesGotAssigned(employeesGotAssigned);
				responseTo.add(areaTo);
			}
			
			
		} catch(HibernateException e) {
			logger.error(e);
			throw e;
		} catch(Exception e) {
			logger.error(e);
			throw e;
		} finally {
			if(session != null)session.close();
			logger.info("getAllAreaDetails Ending ....");
		}
		return responseTo;
	}

}
