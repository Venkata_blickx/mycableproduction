package com.blickx.daoImpl;

import java.util.Date;
import java.util.List;

import org.apache.log4j.Logger;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.Transaction;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;

import com.blickx.dao.CustomerDao;
import com.blickx.dao.CustomerDaoImpl;
import com.blickx.domain.ClientInformationEntity;
import com.blickx.domain.Customer;
import com.blickx.domain.PaymentTxEntity;
import com.blickx.to.ClientInformationEntityDTO;
import com.blickx.to.PaymentTxEntityDTO;
import com.blickx.tx.mailing.DetailsToSend;
import com.blickx.utill.CommonUtils;

public class TransactionDAOImpl implements TransactionDao {
	
	private static Logger logger = Logger.getLogger(TransactionDAOImpl.class);

	@Autowired
	SessionFactory sfactory;
	
	@Autowired
	CustomerDao cdao;

	
	@Override
	public void saveOnlineTxDetails(PaymentTxEntityDTO paymentTxEntityDTO) {
		// TODO Auto-generated method stub
		Session session = sfactory.openSession();
		Transaction tx = null;
				try{
					tx = session.beginTransaction();
					
					PaymentTxEntity paymentTxEntity = new PaymentTxEntity();
					BeanUtils.copyProperties(paymentTxEntityDTO, paymentTxEntity);
					logger.info("Insert Tx after going to  pay Option : " + paymentTxEntity);
					session.save(paymentTxEntity);
					
					tx.commit();
				}catch(Exception e){
					if(tx != null)tx.rollback();
					logger.error(e);
					throw e;
				}finally{
					session.close();
				}
	}

	@Override
	public void updateTxAfterEBSResponse(PaymentTxEntityDTO paymentTxEntityDTO) {
		// TODO Auto-generated method stub
		Session session = sfactory.openSession();
		Transaction tx = null;
				try{
					tx = session.beginTransaction();
					
					@SuppressWarnings("unchecked")
					List<PaymentTxEntity> paymentTxEntitys =  session.createQuery("from PaymentTxEntity pe where pe.merchantRefNo = ? and customer_id = ? and responseCode = ?")
							.setString(0, paymentTxEntityDTO.getMerchantRefNo())
							.setString(1, paymentTxEntityDTO.getDescription())
							.setInteger(2, 5)// responseCode : 5 is our status code wen it's inserting first time when click on pay
							.list();
					if(paymentTxEntitys.size() == 1) {
						PaymentTxEntity paymentTxEntity = paymentTxEntitys.get(0);
							
							logger.info("updateTxAfterEBSResponse Entry existed, So Updating the tx entry " + paymentTxEntity);
							int id = paymentTxEntity.getId();
							BeanUtils.copyProperties(paymentTxEntityDTO, paymentTxEntity);
							paymentTxEntity.setId(id);
							paymentTxEntity.setUpdatedTimestamp(new Date());
							paymentTxEntity.setCustomer_id(paymentTxEntityDTO.getDescription());
							logger.info("updateTxAfterEBSResponse before Update Object : " + paymentTxEntity);
							session.update(paymentTxEntity);
							
							
					} /*else {
						logger.info("updateTxAfterEBSResponse Entry Not existed, So Inserting the tx entry " + paymentTxEntityDTO);
						PaymentTxEntity payEntity = new PaymentTxEntity();
						BeanUtils.copyProperties(paymentTxEntityDTO, payEntity);
						session.save(payEntity);
					}*/
						
					tx.commit();
				}catch(Exception e){
					if(tx != null)tx.rollback();
					logger.error(e);
					throw e;
				}finally{
					session.close();
				}
	}

	@Override
	public boolean checkTxEligible(PaymentTxEntityDTO payDto) {
		Session session = sfactory.openSession();
				try{
					
					@SuppressWarnings("unchecked")
					List<PaymentTxEntity> resultList = session.createQuery("from PaymentTxEntity pe where pe.merchantRefNo = ? and customer_id = ? and responseCode = ?")
					.setString(0, payDto.getMerchantRefNo())
					.setString(1, payDto.getDescription())
					.setInteger(2, 5)
					.list();
					if(resultList.size() > 0) {
						
						logger.info("checkTxEligible : TransactionID : " + resultList.get(0).getRequestId());
						if(CommonUtils.exists(resultList.get(0).getRequestId()))
							return false;
						
					} else {
						return false;
					}
					
					
				}catch(Exception e){
					logger.error("checkTxEligible DAoIMPL Exceptions : " + e);
					return false;
				}finally{
					session.close();
				}
		return true;
	}

	@Override
	public void updateEmailIfNotExists(PaymentTxEntityDTO payDto) {
		// TODO Auto-generated method stub
		Session session = sfactory.openSession();
		Transaction tx = null;
				try{
					tx = session.beginTransaction();
					
					Customer customer = (Customer) session.load(Customer.class, payDto.getCustomer_id());
					if(CommonUtils.exists(customer.getEmail())){
						if(!customer.getEmail().equalsIgnoreCase(payDto.getBillingEmail())) {
							customer.setEmail(payDto.getBillingEmail());
							logger.info("updateEmailIfNotExists YES : Old Email : " + customer.getEmail() + "\n New Email : " + payDto.getBillingEmail());
							session.update(customer);
						}
					} else {
						customer.setEmail(payDto.getBillingEmail());
						logger.info("updateEmailIfNotExists YES : Old Email : " + customer.getEmail() + "\n New Email : " + payDto.getBillingEmail());
						session.update(customer);
					}
					
					tx.commit();
				}catch(Exception e){
					if(tx != null)tx.rollback();
					logger.error(e);
					throw e;
				}finally{
					session.close();
				}
			
	}

	@Override
	public DetailsToSend getDetailsToSendEmail(PaymentTxEntityDTO payDTO) {
		// TODO Auto-generated method stub
		DetailsToSend details = new DetailsToSend();
		Session session = sfactory.openSession();
		Transaction tx = null;
				try{
					tx = session.beginTransaction();
					
					details.setAmmountPaid(payDTO.getAmount());
					details.setCustomerName(payDTO.getBillingName());
					details.setEmail(payDTO.getBillingEmail());
					details.setContactNumber(payDTO.getBillingPhone());
					details.setReferenceNumber(payDTO.getMerchantRefNo());
					details.setOrderNumber(payDTO.getMerchantRefNo());
					details.setTransactionId(payDTO.getTransactionid());
					details.setMyID(payDTO.getCustomer_id());
					details.setDate(payDTO.getCreatedDate());
					
						ClientInformationEntityDTO clientInfoDTO = new ClientInformationEntityDTO();
						@SuppressWarnings("unchecked")
						List<ClientInformationEntity> clientDetails =  session.createQuery("from ClientInformationEntity c where c.client_id = ? and c.printer_name = ?")
								.setString(0, payDTO.getCustomer_id().substring(0, 8))
								.setString(1, "Coinel").list();
						ClientInformationEntity clientInfo = clientDetails.get(0);
						BeanUtils.copyProperties(clientInfo, clientInfoDTO);
					
					details.setClientDetails(clientInfoDTO);
					
					tx.commit();
				}catch(Exception e){
					if(tx != null)tx.rollback();
					logger.error(e);
					throw e;
				}finally{
					session.close();
				}
		return details;
		
	}


}
