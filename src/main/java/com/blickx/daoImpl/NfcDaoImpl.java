package com.blickx.daoImpl;

import org.apache.log4j.Logger;
import org.hibernate.HibernateException;
import org.hibernate.SQLQuery;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.Transaction;
import org.springframework.beans.factory.annotation.Autowired;

import com.blickx.dao.NfcDao;
import com.blickx.domain.NFCEntity;

public class NfcDaoImpl implements NfcDao{
	
	@Autowired
	SessionFactory sfactory;
	
	private Logger logger = Logger.getLogger(NfcDaoImpl.class);

	@Override
	public int addNfc(NFCEntity nfcEntity) {
		
		int res = 0;
		Session session = sfactory.openSession();
		Transaction tx = null;
		try{
			tx = session.beginTransaction();
			String hql = "insert into nfc(customer_id,card_number) values(?,?)";
			SQLQuery query = session.createSQLQuery(hql);
			query.setString(0, nfcEntity.getCustomer_id());
			query.setString(1, nfcEntity.getCard_number());
			
			res = query.executeUpdate();
			if(res != 0)res = 1;
			
			tx.commit();
		}catch(HibernateException e){
			if(tx!=null)tx.rollback();
			logger.error(e);
		}finally{
			session.close();
		}
		return res;
	}

	
}
