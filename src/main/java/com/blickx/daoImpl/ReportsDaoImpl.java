package com.blickx.daoImpl;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.apache.log4j.Logger;
import org.hibernate.HibernateException;
import org.hibernate.SQLQuery;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.Transaction;
import org.hibernate.transform.Transformers;
import org.springframework.beans.factory.annotation.Autowired;

import com.blickx.dao.ReportsDao;
import com.blickx.output.SetupBoxReport;
import com.blickx.output.wrapper.CustomerReportsWrapper;
import com.blickx.report.queries.GetReportQuery;
import com.blickx.response.AreaWiseReport;
import com.blickx.response.AreaWiseReportWrap;
import com.blickx.searchEntities.GetEmployeeReportsSearchEntity;
import com.blickx.to.CustomerBillReportByCidAndDateTo;
import com.blickx.to.CustomerReports;
import com.blickx.to.DailyCollectionByEmployee;
import com.blickx.to.GetCollectionOfEmployeeTo;
import com.blickx.to.GetCustomerBillReportDate;
import com.blickx.to.GetEmployeeReportsTo;
import com.blickx.to.GetLastSixTxsTo;
import com.blickx.to.input.LastSixMonthsTxsInputTo;
import com.blickx.to.input.SetupboxReportInput;
import com.blickx.utill.CommonUtils;
import com.blickx.wrapper.EmployeeReportWrapper;
import com.blickx.wrapper.UnpaidReportWrapper;

public class ReportsDaoImpl implements ReportsDao{
	
	private Logger logger = Logger.getLogger(ReportsDaoImpl.class);
	
	@Autowired
	SessionFactory sfactory;

	// Dash Board Or Collection Page DAO operations

	@Override
	public List<GetCollectionOfEmployeeTo> getCollectionOfEmployee(String client_id, String date) {
		
		List<GetCollectionOfEmployeeTo> resultset = new ArrayList<GetCollectionOfEmployeeTo>();
		
		Session session = sfactory.openSession();
		Transaction tx = null;
		try {
			tx = session.beginTransaction();
			
			logger.info("Calling GetCollectionReport");
			
			@SuppressWarnings({ "rawtypes", "unchecked" })
			List<GetCollectionOfEmployeeTo> resultWithAliasedBean  = (List) session.createSQLQuery(
			"CALL GetCollection(:client_id, :date)")
				.setString("client_id", client_id)
				.setString("date", date)
				.setResultTransformer(Transformers.aliasToBean(GetCollectionOfEmployeeTo.class))
				.list();
			
			resultset = resultWithAliasedBean;
			
			tx.commit();
		} catch (HibernateException e) {
			if (tx != null)
				tx.rollback();
			e.printStackTrace();
			logger.error("Exception! " + e);
		} finally {
			session.close();
		}
		return resultset;
	}

	
	@Override
	public List<DailyCollectionByEmployee> getCollectionByEmployee(String client_id) {
		
		List<DailyCollectionByEmployee> resultset = new ArrayList<DailyCollectionByEmployee>();
		
		Session session = sfactory.openSession();
		Transaction tx = null;
		try {
			tx = session.beginTransaction();
			
			logger.info("Calling DashboardDailyCollectionSummary");
			
			@SuppressWarnings({ "rawtypes", "unchecked" })
			List<DailyCollectionByEmployee> resultWithAliasedBean  = (List) session.createSQLQuery(
			"CALL DashboardDailyCollectionSummary(:client_id)")
			.setString("client_id", client_id)
			.setResultTransformer(Transformers.aliasToBean(DailyCollectionByEmployee.class))
			.list();
			
			resultset = resultWithAliasedBean;
			
			tx.commit();
		} catch (HibernateException e) {
			if (tx != null)
				tx.rollback();
			e.printStackTrace();
			logger.error("Exception! " + e);
		} finally {
			session.close();
		}
		return resultset;
	}
	
	// Reports Page Dao Operations

	@SuppressWarnings({ "unchecked", "rawtypes" })
	@Override
	public CustomerReportsWrapper getCustomerReports(GetCustomerBillReportDate customer) {
		CustomerReportsWrapper resultset = new CustomerReportsWrapper();
		
		Session session = sfactory.openSession();
		Transaction tx = null;
		try {
			tx = session.beginTransaction();
			
			logger.info("Calling GetCustomerReport");
			
			if(!customer.getCustomer_id().startsWith("MYA0"))
				customer.setC_id(customer.getCustomer_id());
			
			if(CommonUtils.exists(customer.getC_id())){
				customer.setCustomer_id((String) session.createQuery("select c.customer_id from Customer c where c_id=? and client_id =?")
						.setString(0, customer.getC_id())
						.setString(1, customer.getClient_id()).uniqueResult());
			}
			
			SQLQuery query = (SQLQuery) session.createSQLQuery(
				"CALL GetCustomerBillReport(:client_id, :customer_id)")
					.setResultTransformer(Transformers.aliasToBean(CustomerReports.class));
			query.setString("client_id", customer.getClient_id());
			query.setString("customer_id", customer.getCustomer_id());
			
			@SuppressWarnings("unchecked")
			List<CustomerReports> resultWithAliasedBean  =  query.list();
			logger.info(resultWithAliasedBean.size());
			
			BigDecimal PaidSum =  (BigDecimal) session.createSQLQuery(GetReportQuery.getPaidSumOfCustomerDateRangeReportQuery(customer)).uniqueResult();
			
			
			resultset.setPaidTotal(PaidSum.setScale(2, BigDecimal.ROUND_HALF_UP));
			resultset.setReport(resultWithAliasedBean);
			
			tx.commit();
		} catch (HibernateException e) {
			if (tx != null)
				tx.rollback();
			e.printStackTrace();
			logger.error("Exception! " + e);
		} finally {
			session.close();
		}
		return resultset;
	}

	@SuppressWarnings({ "unchecked", "null" })
	@Override
	public CustomerReportsWrapper getCustomerReportsWithDateRange(GetCustomerBillReportDate getCustomerBillReportDate) {
		CustomerReportsWrapper resultset = new CustomerReportsWrapper();
		
		Session session = sfactory.openSession();
		Transaction tx = null;
		try {
			tx = session.beginTransaction();
			if(CommonUtils.exists(getCustomerBillReportDate.getCustomer_id())){
			if(!getCustomerBillReportDate.getCustomer_id().startsWith("MYA0"))
				getCustomerBillReportDate.setC_id(getCustomerBillReportDate.getCustomer_id());
			}
			if(CommonUtils.exists(getCustomerBillReportDate.getC_id())){
				getCustomerBillReportDate.setCustomer_id((String) session.createQuery("select c.customer_id from Customer c where c_id=? and client_id =?")
						.setString(0, getCustomerBillReportDate.getC_id())
						.setString(1, getCustomerBillReportDate.getClient_id()).uniqueResult());
			}
			
			logger.info("Calling GetCustomerBillReportDate SP");
			logger.info(getCustomerBillReportDate.getFrom());
			@SuppressWarnings({ "rawtypes", "unchecked" })
			List<CustomerReports> resultWithAliasedBean  =  session.createSQLQuery(
					"CALL GetCustomerBillReportDate(:client_id, :fromdate, :todate, :customer_id)")
					.setResultTransformer(Transformers.aliasToBean(CustomerReports.class))
					.setString("client_id", getCustomerBillReportDate.getClient_id())
					.setString("fromdate", getCustomerBillReportDate.getFrom())
					.setString("todate", getCustomerBillReportDate.getTo())
					.setString("customer_id", getCustomerBillReportDate.getCustomer_id())
					.list();
			
			BigDecimal PaidSum =  (BigDecimal) session.createSQLQuery(GetReportQuery.getPaidSumOfCustomerDateRangeReportQuery(getCustomerBillReportDate)).uniqueResult();
			
			resultset.setPaidTotal(PaidSum.setScale(2, BigDecimal.ROUND_HALF_UP));
			
			//resultset.addAll(resultWithAliasedBean);
			resultset.setReport(resultWithAliasedBean);
			logger.info(resultset);
			
			
			/*for(int i=0; i<resultset.size(); i++) {
				CustomerReports stock = (CustomerReports)resultset.get(i);
			    logger.info(stock.getAdd_on());
			}*/
			
			tx.commit();
		} catch (HibernateException e) {
			if (tx != null)
				tx.rollback();
			e.printStackTrace();
			logger.error("Exception! " + e);
		} catch (Exception e) {
			if (tx != null)
				tx.rollback();
			e.printStackTrace();
			logger.error("Exception! " + e);
		} finally {
			session.close();
		}
		return resultset;		
	}
	
	

	@Override
	public UnpaidReportWrapper getCustomerBillReportDate(
			GetCustomerBillReportDate getCustomerBillReportDate) {
		UnpaidReportWrapper resultset = new UnpaidReportWrapper();
		Session session = sfactory.openSession();
		Transaction tx = null;
		try {
			tx = session.beginTransaction();
			
			logger.info("Calling GetCustomerBillReportDateByCid");
			
			@SuppressWarnings({ "rawtypes", "unchecked" })
			List<CustomerBillReportByCidAndDateTo> resultWithAliasedBean  = (List) session.createSQLQuery(
			"CALL GetCustomerBillDetailsByCid( :cid, :client_id, :istatus)")
				.setResultTransformer(Transformers.aliasToBean(CustomerBillReportByCidAndDateTo.class))
				.setString("cid", getCustomerBillReportDate.getC_id())
				.setString("client_id", getCustomerBillReportDate.getClient_id() )
				.setBoolean("istatus", getCustomerBillReportDate.getStatus())
				.list();
			logger.info(resultWithAliasedBean.size());
			BigDecimal totalAmount = BigDecimal.ZERO;
			if(resultWithAliasedBean.size() > 0)                                                             //CHANGED CODE STARTS
			{
					for(CustomerBillReportByCidAndDateTo cb : resultWithAliasedBean)
					{
						totalAmount = totalAmount.add(cb.getTotal());
					}
			}
			/*if(resultWithAliasedBean.size() > 0)
			{
				 totalAmount = (BigDecimal) session.createSQLQuery(GetReportQuery.getUnpaidSumQuery())
							.setString(0, getCustomerBillReportDate.getClient_id())
							.setString(1, getCustomerBillReportDate.getC_id())
							.setBoolean(2, getCustomerBillReportDate.getStatus()).uniqueResult();
				
			}*/                                                                                              // CHANGED CODE ENDS
			resultset.setTotalSum(totalAmount.setScale(2, BigDecimal.ROUND_HALF_UP));
			resultset.setReport(resultWithAliasedBean);
			
			tx.commit();
		} catch (HibernateException e) {
			e.printStackTrace();
			logger.info(e);
			if (tx != null)
				tx.rollback();
			e.printStackTrace();
			logger.error("Exception! " + e);
		} finally {
			session.close();
		}
		return resultset;
	}

	
	@Override
	public EmployeeReportWrapper getBillReportsOfEmployee(
			GetEmployeeReportsSearchEntity searchDetails) {
		
		EmployeeReportWrapper resultset = new EmployeeReportWrapper();
		
		Session session = sfactory.openSession();
		Transaction tx = null;
		try {
			tx = session.beginTransaction();
			
			logger.info("Calling GetEmployeeBillReport SP");
			
			@SuppressWarnings({ "rawtypes", "unchecked" })
			List<GetEmployeeReportsTo> resultWithAliasedBean  = (List) session.createSQLQuery(
				"CALL GetEmployeeBillReport(:client_id, :employee_id)")
				.setResultTransformer(Transformers.aliasToBean(GetEmployeeReportsTo.class))
				.setString("client_id", searchDetails.getClient_id())
				.setString("employee_id", searchDetails.getEmployee_id())
				.list();
			
			BigDecimal totalSum = (BigDecimal) session.createSQLQuery(GetReportQuery.getPaidSumOfCustomerForEmployeeReport(searchDetails)).uniqueResult();
			
			resultset.setTotalSum(totalSum.setScale(2, BigDecimal.ROUND_HALF_UP));
			resultset.setReport(resultWithAliasedBean);
			
			tx.commit();
		} catch (HibernateException e) {
			if (tx != null)
				tx.rollback();
			e.printStackTrace();
			logger.error("Exception! " + e);
		} finally {
			session.close();
		}
		return resultset;
	}

	@Override
	public EmployeeReportWrapper getBillReportsOfEmployeeByDate(
			GetEmployeeReportsSearchEntity searchDetails) {
		
		EmployeeReportWrapper resultset = new EmployeeReportWrapper();
		
		Session session = sfactory.openSession();
		Transaction tx = null;
		try {
			tx = session.beginTransaction();
			
			logger.info("Calling GetEmployeeBillReportDate SP");
			
			@SuppressWarnings({ "rawtypes", "unchecked" })
			List<GetEmployeeReportsTo> resultWithAliasedBean  = (List) session.createSQLQuery(
					"CALL GetEmployeeBilReportDate(:client_id, :employee_id, :fromdate, :todate)")
						.setResultTransformer(Transformers.aliasToBean(GetEmployeeReportsTo.class))
						.setString("client_id", searchDetails.getClient_id())
						.setString("employee_id", searchDetails.getEmployee_id())
						.setString("fromdate", searchDetails.getFrom())
						.setString("todate", searchDetails.getTo())
						.list();
			
			BigDecimal totalSum = (BigDecimal) session.createSQLQuery(GetReportQuery.getPaidSumOfCustomerForEmployeeReport(searchDetails)).uniqueResult();
			
			resultset.setTotalSum(totalSum.setScale(2, BigDecimal.ROUND_HALF_UP));
			resultset.setReport(resultWithAliasedBean);
			
			tx.commit();
		} catch (HibernateException e) {
			if (tx != null)
				tx.rollback();
			e.printStackTrace();
			logger.error("Exception! " + e);
		} finally {
			session.close();
		}
		return resultset;
	}

	@Override
	public Map<String, Object> getEmployeeCurrentDayCollections(
			String employee_id) {

		Map<String, Object> resultset = new HashMap<String, Object>();
		
		Session session = sfactory.openSession();
		Transaction tx = null;
		try {
			tx = session.beginTransaction();
			
			logger.info("Calling GetEmployeeBillReportDate SP");
			
			BigDecimal collection  =  (BigDecimal) session.createSQLQuery(
			"select sum(b.paid_amount) as todaycollection from billing b "
			+ "where employee_id = '"+employee_id+"' and b.date like concat(CURRENT_DATE,'%');")
			.uniqueResult();
			
			if(collection == null)collection = new BigDecimal(0);
			resultset.put("Today Collection", collection);
			
			tx.commit();
		} catch (HibernateException e) {
			if (tx != null)
				tx.rollback();
			resultset.put("Exception", e);
			logger.error("Exception! " + e);
		} finally {
			session.close();
		}
		return resultset;
	}

	
	@Override
	public List<GetLastSixTxsTo> getLastSixTxs(LastSixMonthsTxsInputTo input) {
		List<GetLastSixTxsTo> resultset = new ArrayList<GetLastSixTxsTo>();
		
		Session session = sfactory.openSession();
		Transaction tx = null;
		try {
			tx = session.beginTransaction();
			if(CommonUtils.exists(input.getC_id())){
			input.setCustomer_id((String) session.createSQLQuery("select customer_id from customer where c_id=? and client_id=?")
					.setString(0, input.getC_id())
					.setString(1, input.getClient_id()).uniqueResult());
			
			}
			logger.info("Calling GetLastSixTxsDesktop SP");
			
			@SuppressWarnings({ "rawtypes", "unchecked" })
			List<GetLastSixTxsTo> resultWithAliasedBean  = (List) session.createSQLQuery(
					"CALL GetLastSixTxsDesktop(:cus_id)")
				.setResultTransformer(Transformers.aliasToBean(GetLastSixTxsTo.class))
				.setString("cus_id", input.getCustomer_id())
				.list();
			
			resultset = resultWithAliasedBean;
			
			tx.commit();
		} catch (HibernateException e) {
			if (tx != null)
				tx.rollback();
			e.printStackTrace();
			logger.error("Exception! " + e);
		} finally {
			session.close();
		}
		return resultset;
	}

	@Override
	public List<SetupBoxReport> getSetupboxReport(SetupboxReportInput input) {
		List<SetupBoxReport> resultset = new ArrayList<SetupBoxReport>();
		
		Session session = sfactory.openSession();
		Transaction tx = null;
		try {
			tx = session.beginTransaction();
			
			logger.info("Calling GetLastSixTxsDesktop SP");
			
			@SuppressWarnings({ "rawtypes", "unchecked" })
			List<SetupBoxReport> resultWithAliasedBean  = (List) session.createSQLQuery(
					"CALL setupboxReport(:iclientId, :fetchType, :searchEntry, :istatus)")
				.setResultTransformer(Transformers.aliasToBean(SetupBoxReport.class))
				.setString("iclientId", input.getClient_id())
				.setString("fetchType", input.getFetchType())
				.setString("searchEntry", input.getSearchEntry())
				.setBoolean("istatus", input.getStatus())
				.list();
			
			resultset = resultWithAliasedBean;
			
			tx.commit();
		} catch (HibernateException e) {
			if (tx != null)
				tx.rollback();
			e.printStackTrace();
			logger.error("Exception! " + e);
		} finally {
			session.close();
		}
		return resultset;

	}

	@Override
	public AreaWiseReportWrap areaWiseReport(SetupboxReportInput input) {
		AreaWiseReportWrap resultset = new AreaWiseReportWrap();
		
		Session session = sfactory.openSession();
		Transaction tx = null;
		try {
			tx = session.beginTransaction();
			
			logger.info("Calling AreaWiseReport SP");
			
			
			String passSelectedAreaValue;
			StringBuffer selectedAreasWithcomma = new StringBuffer("");
			Double total = new Double(0);
			if (input.getSelectedAreas().size() > 0) {
				List<AreaWiseReport> allResults = new ArrayList<AreaWiseReport>();
				for (Map<String, Integer> res : input.getSelectedAreas()) {
					try {
						
						logger.info("selected areas : " + res.get("id"));
					@SuppressWarnings({ "rawtypes", "unchecked" })
					List<AreaWiseReport> resultWithAliasedBean  = (List) session.createSQLQuery(
							"CALL areaWiseReport(?, ?, ?, ?)")
						.setResultTransformer(Transformers.aliasToBean(AreaWiseReport.class))
						.setParameter(0, input.getClient_id())
						.setParameter(1, res.get("id"))
						.setParameter(2, input.getPaidType())
						.setBoolean(3, input.getStatus())
						.list();
					
					allResults.addAll(resultWithAliasedBean);
					
					
					for(AreaWiseReport report:resultWithAliasedBean){
						total = total + report.getTotal().doubleValue();
					}
					
					
					}catch (HibernateException e){
						logger.error("Inner Loop Exception" + e);
						
					}
					
					/*selectedAreasWithcomma.append(res.get("id"));
					selectedAreasWithcomma.append("A");*/
				}
				resultset.setTotal(new BigDecimal(total).setScale(2, BigDecimal.ROUND_HALF_UP));
				resultset.setReport(allResults);
				//passSelectedAreaValue = selectedAreasWithcomma.toString().substring(0, selectedAreasWithcomma.length() - 1);
				
			} else {
				passSelectedAreaValue = "All";
				
				@SuppressWarnings("unchecked")
				List<AreaWiseReport> resultWithAliasedBean  = (List) session.createSQLQuery(
						"CALL areaWiseReport(?, ?, ?, ?)")
					.setResultTransformer(Transformers.aliasToBean(AreaWiseReport.class))
					.setParameter(0, input.getClient_id())
					.setParameter(1, passSelectedAreaValue)
					.setParameter(2, input.getPaidType())
					.setBoolean(3, input.getStatus())
					.list();
				
				
				
				for(AreaWiseReport report:resultWithAliasedBean){
					total = total + report.getTotal().doubleValue();
				}
				
				resultset.setTotal(new BigDecimal(total));
				resultset.setReport(resultWithAliasedBean);
				
			}
			//logger.info(passSelectedAreaValue);
			
			/*@SuppressWarnings({ "rawtypes", "unchecked" })
			List<AreaWiseReport> resultWithAliasedBean  = (List) session.createSQLQuery(
					"CALL areaWiseReport(?, ?, ?)")
				.setResultTransformer(Transformers.aliasToBean(AreaWiseReport.class))
				.setParameter(0, input.getClient_id())
				.setParameter(1, passSelectedAreaValue)
				.setParameter(2, input.getPaidType())
				.list();
			
			resultset = resultWithAliasedBean;*/
			
			tx.commit();
		} catch (HibernateException e) {
			if (tx != null)
				tx.rollback();
			e.printStackTrace();
			logger.error("Exception! " + e);
		} finally {
			session.close();
		}
		return resultset;

	}
}
