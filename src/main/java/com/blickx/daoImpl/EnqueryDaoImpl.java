package com.blickx.daoImpl;

import org.apache.log4j.Logger;

import java.math.BigDecimal;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashSet;
import java.util.List;
import java.util.Set;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import org.glassfish.jersey.server.internal.scanning.PackageNamesScanner;
import org.hibernate.HibernateException;
import org.hibernate.SQLQuery;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.Transaction;
import org.hibernate.exception.ConstraintViolationException;
import org.hibernate.exception.DataException;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;

import com.blickx.controller.CustomerEnquery;
import com.blickx.dao.CustomerDaoImpl;
import com.blickx.dao.EnqueryDao;
import com.blickx.domain.AddOnsEntity;
import com.blickx.domain.AreaCustomerMapEntity;
import com.blickx.domain.Authentication;
import com.blickx.domain.Channels;
import com.blickx.domain.ComplaintEntity;
import com.blickx.domain.Customer;
import com.blickx.domain.PackageEntity;
import com.blickx.domain.SMSGateWayEntity;
import com.blickx.domain.SetupBox;
import com.blickx.exceptions.BoxNumberDupliactedException;
import com.blickx.exceptions.CIdDupliactedException;
import com.blickx.exceptions.ContactNumberDupliactedException;
import com.blickx.exceptions.CustomerIdNotThereException;
import com.blickx.exceptions.ErrorCodes;
import com.blickx.exceptions.SetupBoxNotThereException;
import com.blickx.exceptions.VcNumberDuplicatedException;
import com.blickx.id.generator.ComplaintIDGenerator;
import com.blickx.id.generator.CustomerIdGenerator;
import com.blickx.mycable.customerAddElements.Items;
import com.blickx.to.AutoCId;
import com.blickx.to.DeActiveEnquiryDto;
import com.blickx.to.DeActiveEnquiryResponse;
import com.blickx.to.SetupBoxTo;
import com.blickx.to.input.ComplaintInputTo;
import com.blickx.to.input.DetailsToSend;
import com.blickx.to.input.GetCustomer;
import com.blickx.utill.CommonUtils;
import com.blickx.utill.SendSMSThroughBSMS;

public class EnqueryDaoImpl implements EnqueryDao{
	
	
	
	
	
	@Autowired
	SessionFactory sfactory;

	@Autowired
	CustomerIdGenerator idGenerator;
	
	
	@Autowired
	ComplaintIDGenerator coidGenerator;
	
	
	private static Logger logger = Logger.getLogger(EnqueryDaoImpl.class);

	@Override
	public String enqueryAddCustomer(String client_id, GetCustomer customerDetails) {
		Session session = sfactory.openSession();
		Transaction tx = null;
		String customer_id = null;
		try {

			tx = session.beginTransaction();
			customer_id = idGenerator.getNextSid(client_id);
			logger.info(customer_id);
			
			Customer customer = new Customer();
			BeanUtils.copyProperties(customerDetails, customer);

			String cId = customerDetails.getCustomer_name().substring(0, 2)+customer_id.substring(customer_id.length()-4, customer_id.length());
			//customer.setC_id(cId);
			customer.setCustomer_id(customer_id);
			customer.setClient_id(client_id);
			customer.setStatus(true);
			
			isContactNumberExistsForAddCustomer(client_id, session, customer);
			isCustomerCIDExistedforAddCustomer(client_id, session, customer);
			
			Set<SetupBox> setupBoxes = new HashSet<SetupBox>();
			for(SetupBoxTo boxTo : customerDetails.getSetupBox())
		  	{ 
				isSetupBoxNumberExistedforAddSetupBox(session, boxTo);
				if(CommonUtils.exists(boxTo.getVc_number())){
					isVcNumberExistedforAddSetupBox(session, boxTo);
				}
		  		SetupBox setupBox = new SetupBox(); 
		  		BeanUtils.copyProperties(boxTo,setupBox);
		  		setupBox.setStatus(true);
		  		setupBox.setPackage_id(Integer.parseInt(boxTo.getPackage_id()));
		  		setupBox.setCustomer(customer);
		  		setupBoxes.add(setupBox);
		  	}
			customer.setSetupBoxes(setupBoxes);
			
			session.save(customer);
			
			if(CommonUtils.exists(customer.getAreacode())){
				AreaCustomerMapEntity areaCustomerMap = new AreaCustomerMapEntity();
				Date currentDate = new Date();
				areaCustomerMap.setAreaId(Integer.parseInt(customer.getAreacode()));
				areaCustomerMap.setStartDate(currentDate);
				areaCustomerMap.setCreatedTimeStamp(currentDate);
				areaCustomerMap.setLastUpdatedTimestamp(currentDate);
				areaCustomerMap.setCustomerId(customer.getCustomer_id());
				session.save(areaCustomerMap);
			}
			
			
			
			try{
			  	for(SetupBoxTo boxTo : customerDetails.getSetupBox())
			  	{ 
			  		SetupBox setupBox = new SetupBox(); 
			  		BeanUtils.copyProperties(boxTo,setupBox);
					//setupBox.setCustomer_id(customer_id);
					setupBox.setClient_id(client_id);
					setupBox.setStatus(true);
					
					addSetupBox(setupBox, session, tx, customer_id);
					//setUpBoxs.add(setupBox); 
				}
			  	
			  	session.createSQLQuery("CALL update_addon_amount_as_balance_in_billing(:cus_id)")
			  	.setString("cus_id", customer_id).executeUpdate();
			  	
			  	
			  	String employeeId = (String)session.createSQLQuery("select employee_id from employee where client_id = :clientId and employee_name =:empName limit 1")
			  			.setString("clientId", customer.getClient_id())
			  			.setString("empName", "Admin")
			  			.uniqueResult();
			  	
			  	ComplaintInputTo complaint = new ComplaintInputTo();
			  	complaint.setDetails("New Customer Request");
			  	complaint.setEmployee_id(employeeId);
			  	complaint.setSource("Web");
			  	complaint.setClient_id(customer.getClient_id());
					
					ComplaintEntity comp =  new ComplaintEntity();
					BeanUtils.copyProperties(complaint, comp);
					
					
					String complaint_id = coidGenerator.getNextSid(session, comp.getClient_id());
						comp.setCustomer_id(customer.getCustomer_id());
						comp.setDate(new Date());
						comp.setStatus("0");
						comp.setComplaint_id(complaint_id);
					session.save(comp);
					
					
					Authentication clientCredetnials = (Authentication) session.createQuery("from Authentication a where a.client_id = ?")
							.setParameter(0, complaint.getClient_id()).list().get(0);
					
					if(clientCredetnials.isMsg_alert()){
						// 1) templateID, 2) paidAmount, 3) Cable Tv, 4) bill_recipt_no, 5) Regards(client name)
						String clientContactNumber = clientCredetnials.getClientContactNumber();
						if(!CommonUtils.exists(clientContactNumber))clientContactNumber = "";
						
						DetailsToSend details = new DetailsToSend("43218", complaint_id, clientCredetnials.getClient_name(), clientContactNumber, "", "");
						
						
						//DetailsToSend details = new DetailsToSend(customer_id, client_id, MsgFormats.getPayedSMS(paid, bill_id, clientCredetnials.getClient_name()));
						details.setContact_number((String) session.createQuery("select c.contact_number from Customer c where c.customer_id = ?").setString(0, customer_id).uniqueResult());
						SMSGateWayEntity smsGateWayEntity = (SMSGateWayEntity) session.load(SMSGateWayEntity.class, 2);
						
						SMSGateWayEntity smsGateWay = new SMSGateWayEntity();
						BeanUtils.copyProperties(smsGateWayEntity, smsGateWay);
						
						/*SendSMSRunnable sendSMSThread = new SendSMSRunnable(session, details, smsGateWay);
						Thread t = new Thread(sendSMSThread);
						t.start();*/
						
						SendSMSThroughBSMS sendSms = new SendSMSThroughBSMS(clientCredetnials.getClient_id(),"103",details.getContact_number(),complaint_id
								,clientCredetnials.getSmsRegardsMsg(),clientContactNumber);
							Thread sendSmsThread = new Thread(sendSms);
							sendSmsThread.start();
						
						logger.info("if(clientCredetials.isMsg_alert() is ended.........."); 
						
						//System.out.println(SendSMS.sendSMSToCustomer(session, details));
					}
			  	
			}catch(ConstraintViolationException e){
				if(tx != null)tx.rollback();
				logger.error(e);
				return "MCI1001 Internal Server Error: Please contact your site adminstrator!";
			}catch(HibernateException e){
				if(tx != null)tx.rollback();
				logger.error(e);
				return "MCI1000 Internal Server Error: Please contact your site adminstrator!";
			}catch(Exception e){
				if(tx != null)tx.rollback();
				logger.error(e);
				return "MCI100 Internal Server Error: Please contact your site adminstrator!";
			}
			// customer.setSetupBoxes(setUpBoxs);
			
			//SQLQuery query = session.createSQLQuery("call Get_addon_amount_based_on_expiry(:boxNumber)");
			//totalAmount = (BigDecimal)query.uniqueResult();
			//BigDecimal totalAmount = (BigDecimal)query.setParameter("customer_id", customer_id).uniqueResult();
			//logger.info("Total Addon Amount for the Customer"+totalAmount);
			
			tx.commit();
		} catch (CIdDupliactedException e) {
			if (tx != null)
				tx.rollback();
			logger.error(e);
			return e.toString();
		}catch (ContactNumberDupliactedException e) {
			if (tx != null)
				tx.rollback();
			logger.error(e);
			return e.toString();
		}catch (ConstraintViolationException e) {
			if (tx != null)
				tx.rollback();
			logger.error(e);
			return "MCI1001 Internal Server Error: Please contact your site adminstrator!";
		}catch(BoxNumberDupliactedException msg){
			if(tx != null)tx.rollback();
			logger.error("boxNumberDuplicatedException catch block"+msg.toString());
			return msg.toString();
		}catch(VcNumberDuplicatedException msg){
			if(tx != null)tx.rollback();
			logger.error("VCNumberDuplicatedException catch block"+msg.toString());
			return msg.toString();
		} catch (DataException e) {
			if (tx != null)
				tx.rollback();
			logger.error(e);
			return "MCI1002 Internal Server Error: Please contact your site adminstrator!";
		} catch (HibernateException e) {
			if (tx != null)
				tx.rollback();
			logger.error(e);
			return "MCI1000 Internal Server Error: Please contact your site adminstrator!";
		} catch (Exception e) {
			if (tx != null)
				tx.rollback();
			logger.error(e);
			return "MCI100 Internal Server Error: Please contact your site adminstrator!";
		} finally {
			session.close();
		}
		return "Customer added successfully!";
	}


	private void isCustomerCIDExistedforAddCustomer(String client_id, Session session, Customer customer)
			throws CIdDupliactedException {
		if(session.createSQLQuery("select * from customer where c_id = '"+customer.getC_id()+"' and client_id = '"+client_id+"'").list().size()>0)throw new CIdDupliactedException(ErrorCodes.getcIdDuplicateMsg(customer.getC_id()));
	}

	private void isContactNumberExistsForAddCustomer(String client_id, Session session, Customer customer)
			throws ContactNumberDupliactedException {
		if(session.createSQLQuery("select * from customer where contact_number = '"+customer.getContact_number()+"' and client_id = '"+client_id+"'").list().size()>0)throw new ContactNumberDupliactedException(ErrorCodes.getContactNumberDuplicateMsg(customer.getContact_number()));
	}

	private void isVcNumberExistedforAddSetupBox(Session session, SetupBoxTo boxTo) throws VcNumberDuplicatedException {
		if(session.createSQLQuery("select * from setupbox where vc_number = '"+boxTo.getVc_number()+"'").list().size()>0)throw new VcNumberDuplicatedException(ErrorCodes.getVcNumberDuplicateMsg(boxTo.getVc_number()));
	}

	private void isSetupBoxNumberExistedforAddSetupBox(Session session, SetupBoxTo boxTo) throws BoxNumberDupliactedException {
		if(session.createSQLQuery("select * from setupbox where box_number = '"+boxTo.getBox_number()+"'").list().size()>0)throw new BoxNumberDupliactedException(ErrorCodes.getBoxNumberDuplicateMsg(boxTo.getBox_number()));
	}

	public static void addSetupBox(SetupBox setupBox, Session session, Transaction tx, String customer_id)
			throws HibernateException,ConstraintViolationException, BoxNumberDupliactedException, VcNumberDuplicatedException, ParseException {

		try {
			
			
				/*if(session.createSQLQuery("select * from setupbox where box_number = '"+setupBox.getBox_number()+"' and client_id ='"+setupBox.getClient_id()+"'").list().size()>0)throw new BoxNumberDupliactedException(ErrorCodes.BOX_NUMBER_DUPLICATE_CODE);
				if(session.createSQLQuery("select * from setupbox where vc_number = '"+setupBox.getVc_number()+"' and client_id ='"+setupBox.getClient_id()+"'").list().size()>0)throw new VcNumberDuplicatedException(ErrorCodes.VC_NUMBER_DUPLICATE_CODE);
			*/
			
			if (CommonUtils.exists(setupBox.getAddonNames())) {
				logger.info(setupBox.getAddonNames());
				String[] add_ons = setupBox.getAddonNames().split(",");
				StringBuffer addOnSearch = new StringBuffer();
				BigDecimal totalAmount;
				
				List<String> channels = new ArrayList<String>();
				List<String> expiry_dates = new ArrayList<String>();
				
				for(int x=0;x<add_ons.length;x++){
					if(x%2==0)
					channels.add(add_ons[x].trim());
				}
				for(int x=0;x<add_ons.length;x++){
					if(x%2==1)
					expiry_dates.add(add_ons[x].trim());
				}
				
				List<AddOnsEntity> addons = new ArrayList<AddOnsEntity>();
				SimpleDateFormat formatter = new SimpleDateFormat("yyyy-MM-dd");
				
				for(int x=0;x<channels.size();x++){
					AddOnsEntity addon = new AddOnsEntity();
					addon.setStart_date(setupBox.getStart_date());
					addon.setBox_number(setupBox.getBox_number());
					addon.setCustomer_id(customer_id);
					logger.info(expiry_dates.size()+"******hi******"+channels.size());
					addon.setAddon(channels.get(x));
					logger.info("************"+formatter.parse(expiry_dates.get(x)));
					
					addon.setExpiry_date(formatter.parse(expiry_dates.get(x)));
					addons.add(addon);
					
				}
				
				List<Integer> addedAddonIds = new ArrayList<Integer>();
				
				for(AddOnsEntity addon:addons){
					logger.info("Start of SEssion.save()************************************8");
					Integer recentId = (Integer) session.save(addon);	// Adding addonEntity to table
					addedAddonIds.add(recentId);
				}
				
				for(Integer ids:addedAddonIds){
					logger.info(ids);
				}
				
				session.flush();
				session.clear();
				
				StringBuffer addons_string = new StringBuffer("");
				for(String s: channels){
					addons_string.append(s);
					addons_string.append(",");
				}
				
				for (String s : channels) {
					addOnSearch.append("\'" + s.trim() + "\',");
				}
				
				logger.info(addOnSearch.toString().substring(0,
						addOnSearch.length() - 1));
				String add_on_string = addOnSearch.toString().substring(0, addOnSearch.length() - 1);
				logger.info(add_on_string.substring(1));
				
				StringBuffer  addOnnSearch = new StringBuffer("(");
				
				for(String s:channels){
					addOnnSearch.append("\'"+s+"\',");
				}
				logger.info(addOnnSearch);
				logger.info(addOnnSearch.toString().substring(0,addOnSearch.length()));
				@SuppressWarnings("unused")
				String addOnQuery = "SELECT SUM(channel_amount) from channels WHERE channel_name in " +addOnnSearch.toString().substring(0,addOnSearch.length())+");";
				
				StringBuffer addonIds = new StringBuffer("");
				for(Integer id:addedAddonIds){
					addonIds.append(id);
					addonIds.append(",");
				}
				
				StringBuffer queryToGetAddonSpotAmount = new StringBuffer();
				queryToGetAddonSpotAmount.append("SELECT sum((12 * (YEAR(expiry_date) - YEAR(start_date)) + (MONTH(expiry_date) - MONTH(start_date)))  *(select sum(c.channel_amount) from channels c where channel_name = addon and client_id = '"+setupBox.getClient_id()+"')) as addonAmount ");
				queryToGetAddonSpotAmount.append("FROM addons ");
				queryToGetAddonSpotAmount.append("where id in (");
				queryToGetAddonSpotAmount.append(addonIds.toString().substring(0, addonIds.length()-1)+");");

				
				BigDecimal addonAmount = (BigDecimal) session.createSQLQuery(queryToGetAddonSpotAmount.toString())
						.uniqueResult();
				
				
				
				
				
				SQLQuery query = session.createSQLQuery("call Get_addon_amount_based_on_expiry(:boxNumber)");
				//totalAmount = (BigDecimal)query.uniqueResult();
				totalAmount = (BigDecimal)query.setParameter("boxNumber", setupBox.getBox_number()).uniqueResult();
				logger.info("Addon Amount : " +totalAmount);
				logger.info(totalAmount);
				
				
				
				// String string = add_on_string.substring(1).toString();
				// logger.info(add_on_string);
				// String assigned_packageID=(Byte.toString((Byte)
				// session.createSQLQuery("select package_id from packages where package_name='"+setupBox.getAssigned_package()+"';").uniqueResult()));

				session.createSQLQuery(
						"call Add_Setupbox(:cus_id, :boxnumber, :start_date, :vc_number, :nds_number, :package, :addon_amount, :add_on_string, :addons_string, :discount, :clientid)")
						.setString("cus_id", customer_id)
						.setString("boxnumber", setupBox.getBox_number())
						.setDate("start_date", setupBox.getStart_date())
						.setString("vc_number", setupBox.getVc_number())
						.setString("nds_number", setupBox.getNds_number())
						.setString("package", setupBox.getAssigned_package())
						.setBigDecimal("addon_amount", addonAmount)
						.setString("add_on_string", add_on_string)
						.setString("addons_string", addons_string.toString().substring(0,addons_string.length()-1))
						.setBigDecimal("discount", new BigDecimal(setupBox.getDiscount()))
						.setString("clientid", setupBox.getClient_id())
						.executeUpdate();
				
				session.createSQLQuery(
						"call update_balance_for_addons(:cus_id, :balance) ")
						.setString("cus_id", customer_id)
						.setBigDecimal("balance", addonAmount).executeUpdate();
				
			}else{
				session.createSQLQuery(
						"call Add_Setupbox(:cus_id, :boxnumber, :start_date, :vc_number, :nds_number, :package, :addon_amount, :add_on_string, :addons_string, :discount, :clientid) ")
						.setString("cus_id", customer_id)
						.setString("boxnumber", setupBox.getBox_number())
						.setDate("start_date", setupBox.getStart_date())
						.setString("vc_number", setupBox.getVc_number())
						.setString("nds_number", setupBox.getNds_number())
						.setString("package", setupBox.getAssigned_package())
						.setBigDecimal("addon_amount", new BigDecimal(0))
						.setString("add_on_string", null)
						.setString("addons_string", null)
						.setBigDecimal("discount", new BigDecimal(setupBox.getDiscount()))
						.setString("clientid", setupBox.getClient_id())
						.executeUpdate();
			}
			
			
		} catch (ConstraintViolationException e) {
			logger.error(e);
			throw e;
		}catch (HibernateException e) {
			logger.error(e);
			throw e;
		}catch (Exception e) {
			// TODO: handle exception
			logger.error(e);
			throw e;
		}
	}


	@SuppressWarnings("unchecked")
	@Override
	public PackageEntity getPackageDetails(String client_id) {
		
		Session session = sfactory.openSession();
		
		List<PackageEntity> pc = null;
		try{
			pc = (List<PackageEntity>)session.createQuery("from PackageEntity where client_id=:clientId")
					.setString("clientId", client_id)
					.setMaxResults(1)
					.list();
		}catch(Exception e)
		{
			e.printStackTrace();
		}finally{
			if(session!=null)
				session.close();
		}
		return pc.get(0);
	}


	@SuppressWarnings("unchecked")
	@Override
	public Items getIteamDetails(String client_id) {
		
		Items it = new Items();
		Session session = sfactory.openSession();
		List<Channels> lc = null;
		try{
				lc = (List<Channels>)session.createQuery("from Channels where client_id=:clientId")
						.setString("clientId", client_id)
						.setMaxResults(1)
						.list();
				it.set_data(0);
				it.set_id(lc.get(0).getChannel_id()+"");
				it.set_name(lc.get(0).getChannel_name());
				it.set_price(lc.get(0).getChannel_amount());
				it.set_quantity(1);
				
				
		}catch(Exception e)
		{
			e.printStackTrace();
		}finally{
			if(session!=null)
				session.close();
		}
		
		return it;
	}


	@Override
	public DeActiveEnquiryResponse addDeActiveEnquiry(String clientId,
			DeActiveEnquiryDto deactiveEnquiry) {
		
		DeActiveEnquiryResponse der = new DeActiveEnquiryResponse();
		int updatedStatus,updatedStatus1 = 0;
		Session session = sfactory.openSession();
		Transaction tx = null;

			try{

				tx = session.beginTransaction();
				try{
						updatedStatus = (int)session.createSQLQuery("update customer set status=:stat, line2=:addTwo  where customer_id=:customerId")
								.setBoolean("stat", false)
								.setString("addTwo","Rejected")
								.setString("customerId", deactiveEnquiry.getCustomerId())
								.executeUpdate();
						
				isCustomerUpdatedStatus(updatedStatus);
				
				updatedStatus1 = (int)session.createSQLQuery("update setupbox set status=:stat where customer_id =:cusId")
						.setBoolean("stat", false)
						.setString("cusId", deactiveEnquiry.getCustomerId())
						.executeUpdate();
				isSetupBoxUpdatedStatus(updatedStatus1);

				}catch(CustomerIdNotThereException msg){
					der.setMessage("Customer is Not There");
					return der;
				}catch(SetupBoxNotThereException msg)
				{
					der.setMessage("SetUpBox is Not There");
					return der;
				}
				
				
				
				if(updatedStatus==1 && updatedStatus1==1)
					der.setMessage("Successfuly Enquiry is DeActivated");
				else
					der.setMessage("Successfuly Enquiry is Not DeActivated");
				
				tx.commit();
			}catch(HibernateException e)
			{
				tx.rollback();
				e.printStackTrace();
				logger.info(e);
				der.setMessage("Successfully  Enquiry is Not  DeActivated");
			}catch (Exception e) {
				tx.rollback();
				e.printStackTrace();
				logger.info(e);
				der.setMessage("Successfully  Enquiry is Not  DeActivated");
			}
			finally{
				if(session!=null)
					session.close();
			}
		return der;
	}


	private void isSetupBoxUpdatedStatus(int updatedStatus1) throws SetupBoxNotThereException {
		if(updatedStatus1==0)
			throw new SetupBoxNotThereException("SetupBox is Not There");
		
	}


	private void isCustomerUpdatedStatus(int updatedStatus) throws CustomerIdNotThereException {
		if(updatedStatus==0)
			throw new CustomerIdNotThereException("Customer is not there");
	}


	@Override
	public AutoCId getAutoCIdNumber(String clientId) {
		
		AutoCId auto = new AutoCId();
		
		String prevCIdNum = null;
		
		Session session = null;
		try{
			
			session = sfactory.openSession();
			prevCIdNum = (String)session.createSQLQuery("select max(c_id) from customer where client_id =:cliId and c_id not like '%PDA%';")
						.setString("cliId", clientId)
						.uniqueResult();
			
			if(prevCIdNum==null)
				auto.setAutoCid("AA"+clientId.substring(3));
			else
				auto.setAutoCid(getAutoCIdNum(prevCIdNum));
			
			
		}catch(HibernateException e)
		{
			e.printStackTrace();
		}catch (Exception e) {
			e.printStackTrace();
		}finally{
			if(session!=null)
				session.close();
		}
		
		
		return auto;
	}


	private String getAutoCIdNum(String prevCIdNum) {
		
		String alpha = prevCIdNum;
	 	Pattern pattern = Pattern.compile("[^a-z A-Z]");
	    Matcher matcher = pattern.matcher(alpha);
		String number = matcher.replaceAll("");
	//	System.out.println(number);
		String number2 = alpha.replaceAll("[^0-9]+","");
		int number1 = 0;
		if(number2.length()>0){
		number1 = Integer.parseInt(number2);
		}
		//System.out.println(number1+1);
		int numLength = number2.length();	
		String nextNum = getNextAutoSyncNumber(numLength,number1);
		String nextCIdNum = number+nextNum;
		
		return nextCIdNum;
	}


	private String getNextAutoSyncNumber(int numLength,int number1) {
		
		String nextNum;
		
		switch(numLength)
		{
			case 9 : 			nextNum = "00000000"+(number1+1);
										break;
				
		   case 8 : 			nextNum = "0000000"+(number1+1);
									break;
				
			case 7 :  			nextNum = "000000"+(number1+1);
									break;
				
			case 6 :  			nextNum = "00000"+(number1+1);
									break;
				
			case 5 :  			nextNum = "0000"+(number1+1);
									break;
			
			case 4 :  			nextNum = "000"+(number1+1);
									break;
				
			case 3 :  			nextNum = "00"+(number1+1);
									break;
				
			case 2 :  			nextNum = "0"+(number1+1);
									break;	
				
			default : 			nextNum = ""+(number1+1);
				
	}
		return nextNum;
}



}
