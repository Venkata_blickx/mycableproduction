package com.blickx.daoImpl;

import java.util.ArrayList;
import java.util.List;

import org.apache.log4j.Logger;
import org.hibernate.HibernateException;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.Transaction;
import org.springframework.beans.factory.annotation.Autowired;

import com.blickx.dao.UtillDAO;

public class UtilDaoImple implements UtillDAO {

	@Autowired
	SessionFactory sfactory;
	
	Logger logger = Logger.getLogger(UtilDaoImple.class);
	
	@SuppressWarnings("unchecked")
	@Override
	public List<String> getSubscriptionsIDs(String clientId) {
		
		List<String> result = new ArrayList<String>();
		Session session = sfactory.openSession();
		Transaction tx = null;
		
		try {
			tx = session.beginTransaction();
			logger.info("getAllAccountIds ClientId :  " + clientId);
			
			result = session.createSQLQuery("select unifysubsno from subscription where customerid like \'" + clientId + "%\'").list();
			
			logger.info("there was no error it seems ");
			
			tx.commit();
		} catch(HibernateException e){
			if(tx != null)tx.rollback();
			logger.error(e);
			throw e;
		} catch(Exception e){
			if(tx != null)tx.rollback();
			logger.error(e);
			throw e;
		} finally{
			session.close();
		}
		
		return result;
		
	}

	@Override
	public Object getStatus(String clientId, String mobile) {
		
		Session session = sfactory.openSession();
		Transaction tx = null;
		
		try {
			tx = session.beginTransaction();
			logger.info("getAllAccountIds ClientId :  " + clientId + "\t" + mobile);
			
			@SuppressWarnings("unchecked")
			List<String> list = session.createQuery("select c.customer_id from Customer c where c.contact_number = :mobile ")
				.setString("mobile", mobile).list();
			logger.info("UtillDaoInmple.. Fethced customers with the give contact_number Size : " + list.size());
			if(list.size() > 1) return new Integer(1);
			if(list.size() == 0) return new Integer(2);
			
			logger.info("there was no error it seems ");
			
			tx.commit();
		} catch(HibernateException e){
			if(tx != null)tx.rollback();
			logger.error(e);
			throw e;
		} catch(Exception e){
			if(tx != null)tx.rollback();
			logger.error(e);
			throw e;
		} finally{
			session.close();
		}
		
		return new Integer(0);
	}

}
