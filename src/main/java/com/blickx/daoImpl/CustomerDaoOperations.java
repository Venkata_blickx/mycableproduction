package com.blickx.daoImpl;


import org.apache.log4j.Logger;
import org.hibernate.HibernateException;
import org.hibernate.SQLQuery;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;

import com.blickx.dao.CustomerDaoImpl;
import com.blickx.to.CustomerUpdateDTO;
import com.blickx.utill.CommonUtils;

public class CustomerDaoOperations {
	
	@Autowired
	static SessionFactory sfactory;
	
	private static Logger logger = Logger.getLogger(CustomerDaoOperations.class);

	public static void updateAdd_OnDetails(CustomerUpdateDTO customer){
		
			Session session=null;
				try{
				session = sfactory.openSession();
				
				
				if(CommonUtils.exists(customer.getAdd_on())){
				String[] add_ons = customer.getAdd_on().split(",");
				StringBuffer  addOnString = new StringBuffer("(");
				
				for(String s:add_ons){
					addOnString.append("\'"+s+"\',");
				}
				
				logger.info(addOnString);
				logger.info(addOnString.toString().substring(0,addOnString.length()-1));
				//String addOnQuery = "SELECT SUM(channel_amount) from channels WHERE channel_name in "+addOnSearch.toString().substring(0,addOnSearch.length()-1)+");";
				
				session.createSQLQuery("call UpdateAddon(:cus_id, :add_on_string)")
						.setString("cus_id", customer.getCustomer_id())
						.setString("add_on_string", addOnString.toString().substring(0,addOnString.length()-1))
						.executeUpdate();
				
				
				}
				
			}catch(HibernateException e){
				
			}
				
				
				
		
				
	}
	
	public static void updatePackageAmount(CustomerUpdateDTO cdto){
		
	}
	
}
