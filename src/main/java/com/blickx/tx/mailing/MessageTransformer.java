/*package com.blickx.tx.mailing;

import org.apache.velocity.Template;
import org.apache.velocity.app.VelocityEngine;

public final class MessageTransformer extends AbstractMessageTransformer
{

    private VelocityEngine velocityEngine;

    private String templateName;

    private Template template;


   //This part is for getting the value from property file by declaring setter and getter for fileName and  subscriberName

    private String logo;

    public String getLogo() {
        return logo;
    }


    public void setLogo(String logo) {
        this.logo = logo;
    }

   //This part is for getting template for email from classpath configured in mule flow
    public VelocityMessageTransformer()
    {
        registerSourceType(Object.class);
        setReturnDataType(new SimpleDataType<String>(String.class));
    }

    public void setVelocityEngine(final VelocityEngine velocityEngine)
    {
        this.velocityEngine = velocityEngine;
    }

    public void setTemplateName(final String templateName)
    {
        this.templateName = templateName;
    }

    @Override
    public void initialise() throws InitialisationException
    {
        try
        {
            template = velocityEngine.getTemplate(templateName);
        }
        catch (final Exception e)
        {
            throw new InitialisationException(e, this);
        }
    }

    @Override
    public Object transformMessage(final MuleMessage message, final String outputEncoding)
        throws TransformerException
    {


        try
        {
            final StringWriter result = new StringWriter();
FileDataSource myFile = new FileDataSource (new File (logo)); // It contains path of image file


            message.setOutboundProperty("logo", myFile);
            // -------------------------------------------------------

            final Map<String, Object> context = new HashMap<String, Object>();
            context.put("message", message);
            context.put("payload", message.getPayload());

            context.put("logo", message.getOutboundProperty("logo"));
            template.merge(new VelocityContext(context), result); //Merging all the attributes
            System.out.println("MAIL WITH TEMPLATE SEND SUCCESSFULLY !!!");
            System.out.println( result.toString() );
            return result.toString();

        }
        catch (final Exception e)
        {
            throw new TransformerException(
                MessageFactory.createStaticMessage("Can not transform message with template: " + template), e);
        }
    }
}*/