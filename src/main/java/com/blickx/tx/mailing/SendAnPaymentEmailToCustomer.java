package com.blickx.tx.mailing;

import java.math.BigDecimal;
import java.text.SimpleDateFormat;
import java.util.HashMap;
import java.util.Map;
import java.util.Properties;

import javax.ws.rs.client.Client;
import javax.ws.rs.client.ClientBuilder;
import javax.ws.rs.client.Entity;
import javax.ws.rs.client.WebTarget;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

import org.apache.velocity.app.VelocityEngine;
import org.glassfish.jersey.client.authentication.HttpAuthenticationFeature;
import org.glassfish.jersey.media.multipart.FormDataMultiPart;
import org.glassfish.jersey.media.multipart.MultiPartFeature;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.ui.velocity.VelocityEngineUtils;

public class SendAnPaymentEmailToCustomer {

	@Autowired
	VelocityEngine velocityEngine;
	
	public String prepareString(DetailsToSend details) {
		Map<String, Object> model = new HashMap<String, Object>();
        
       // model.put("orderNumber", details.getOrderNumber() );
        model.put("myID", details.getMyID() );
        model.put("customerName", details.getCustomerName() );
        model.put("date", new SimpleDateFormat("yyyy-MM-dd HH:mm:ss").format(details.getDate()));
        model.put("contactNumber", details.getContactNumber() );
        model.put("email", details.getEmail() );
        model.put("totaNeedToPay", new BigDecimal(details.getTotalNeedtoPay()).setScale(2, BigDecimal.ROUND_HALF_UP));
        model.put("paidAmount", new BigDecimal(details.getAmmountPaid()).setScale(2, BigDecimal.ROUND_HALF_UP));
        model.put("referenceNumber", details.getReferenceNumber());
        
        model.put("clientName", details.getClientDetails().getAddress_line1());
        model.put("clientAddressline1", details.getClientDetails().getAddress_line2());
        model.put("clientAddressline2", details.getClientDetails().getAddress_line3());
        model.put("clientAddressline3", details.getClientDetails().getAddress_line4());
        model.put("clientAddressline4", details.getClientDetails().getAddress_line5());
        
        model.put("transactionID", details.getTransactionId() );
        
        
        VelocityEngine ve = new VelocityEngine();
        
        Properties props = new Properties();
        props.put("resource.loader", "class");
        props.put("class.resource.loader.class", "org.apache.velocity.runtime.resource.loader.ClasspathResourceLoader");
        ve.init(props);
        
        
        
        String billPriview = VelocityEngineUtils.mergeTemplateIntoString(
        		ve,
                     "/billpriview.vm",
                     model);
        
        return billPriview;
	}
	
	
	public void sendAnEmail (DetailsToSend details) {
		
		try{
				
		    
			 Client client = ClientBuilder.newBuilder().register(MultiPartFeature.class).build();
		        HttpAuthenticationFeature feature = HttpAuthenticationFeature.universalBuilder()
		                  .credentialsForBasic("api", "key-7d61ac76ab514af1126d65203b555c1f")
		                  .build();
		         client.register(feature);

		        WebTarget target = null;
		        try {
		            target = client
		                    .target("https://api.mailgun.net/v2/mail.mycable.in")
		                    .path("messages");
		        } catch (IllegalArgumentException | NullPointerException e) {
		            /*LOG_TO_CONSOLE.fatal(e, e);
		            LOG_TO_FILE.fatal(e, e);*/
		        	e.printStackTrace();
		        }

		       javax.ws.rs.client.Invocation.Builder builder = target.request(MediaType.MULTIPART_FORM_DATA);

		       FormDataMultiPart form = new FormDataMultiPart();
		       form.field("from", "MyCable noreply@mycable.in");
		       form.field("to", details.getEmail());
		       form.field("subject", "MyCable Payment Receipt - Transaction Number: " + details.getReferenceNumber());
		       //form.field("text", "Here you have the image.");

		       form.field("html", prepareString(details));
		       //File jpgFile = new File(/*System.getProperty("user.dir") +*/ "E:/sri/srii/Projects/NewLive/MyCableWebApp/src/main/resources/img2.jpg");
		       //if(jpgFile.exists() && !jpgFile.isDirectory()) { System.out.println("File exists !!!"); }
		       // form.bodyPart(new FileDataBodyPart("inline", jpgFile, MediaType.APPLICATION_OCTET_STREAM_TYPE)); 
		       Response response = builder.post(Entity.entity(form, MediaType.MULTIPART_FORM_DATA_TYPE));
		       
		       System.out.println(response.getEntity());
		       System.out.println(response.getStatus());

			
			
			
			
		       
		      /* client.addFilter(new HTTPBasicAuthFilter("api",
		                       "key-7d61ac76ab514af1126d65203b555c1f"));
		       WebResource webResource =
		               client.resource("https://api.mailgun.net/v3/mail.mycable.in" +
		                               "/messages");
		       FormDataMultiPart form = new FormDataMultiPart();
		       form.field("from", "mailgun@mail.mycable.in");
		       form.field("to", details.getEmail());
		       form.field("subject", "Hello");
		       form.field("text", "Testing some Mailgun awesomness!");
		       form.field("html", prepareString(details));
		       File pngFile = new File("/resources/img2.png");
		       form.bodyPart(new FileDataBodyPart("inline",pngFile,
		                       MediaType.APPLICATION_OCTET_STREAM_TYPE));
		       ClientResponse res =  webResource.type(MediaType.MULTIPART_FORM_DATA_TYPE).
		               post(ClientResponse.class, form);
			
	    String output = res.getEntity(String.class);
	    System.out.println(output);*/
	    
		}
	    catch (Exception e) {
			e.printStackTrace();// TODO: handle exception
		}
		
		
		
	}
	
	
}
