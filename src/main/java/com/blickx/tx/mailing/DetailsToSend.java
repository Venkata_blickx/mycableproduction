package com.blickx.tx.mailing;

import java.util.Date;

import com.blickx.to.ClientInformationEntityDTO;

public class DetailsToSend {

	private String referenceNumber;
	
	private String myID;
	
	private String orderNumber;
	
	private String transactionId;
	
	private String customerName;
	
	private String email;
	
	private String contactNumber;
	
	private Date date;
	
	private String totalNeedtoPay;
	
	private String ammountPaid;
	
	private String paidDate;
	
	public String getMyID() {
		return myID;
	}

	public void setMyID(String myID) {
		this.myID = myID;
	}

	private ClientInformationEntityDTO clientDetails;
	
	public String getTransactionId() {
		return transactionId;
	}

	public void setTransactionId(String transactionId) {
		this.transactionId = transactionId;
	}

	public String getCustomerName() {
		return customerName;
	}

	public void setCustomerName(String customerName) {
		this.customerName = customerName;
	}

	public String getContactNumber() {
		return contactNumber;
	}

	public void setContactNumber(String contactNumber) {
		this.contactNumber = contactNumber;
	}

	public Date getDate() {
		return date;
	}

	public void setDate(Date date) {
		this.date = date;
	}

	public String getTotalNeedtoPay() {
		return totalNeedtoPay;
	}

	public void setTotalNeedtoPay(String totalNeedtoPay) {
		this.totalNeedtoPay = totalNeedtoPay;
	}

	public ClientInformationEntityDTO getClientDetails() {
		return clientDetails;
	}

	public void setClientDetails(ClientInformationEntityDTO clientInformationEntityDTO) {
		this.clientDetails = clientInformationEntityDTO;
	}

	public String getOrderNumber() {
		return orderNumber;
	}

	public void setOrderNumber(String orderNumber) {
		this.orderNumber = orderNumber;
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public String getAmmountPaid() {
		return ammountPaid;
	}

	public void setAmmountPaid(String ammountPaid) {
		this.ammountPaid = ammountPaid;
	}

	public String getPaidDate() {
		return paidDate;
	}

	public void setPaidDate(String paidDate) {
		this.paidDate = paidDate;
	}

	public String getReferenceNumber() {
		return referenceNumber;
	}

	public void setReferenceNumber(String referenceNumber) {
		this.referenceNumber = referenceNumber;
	}

	@Override
	public String toString() {
		return "DetailsToSend [referenceNumber=" + referenceNumber + ", myID="
				+ myID + ", orderNumber=" + orderNumber + ", transactionId="
				+ transactionId + ", customerName=" + customerName + ", email="
				+ email + ", contactNumber=" + contactNumber + ", date=" + date
				+ ", totalNeedtoPay=" + totalNeedtoPay + ", ammountPaid="
				+ ammountPaid + ", paidDate=" + paidDate + ", clientDetails="
				+ clientDetails + "]";
	}
	
	
}
