package com.blickx.tx.mailing;

public class SendMailRunnable implements Runnable {
	
	DetailsToSend details;
	
	public SendMailRunnable(DetailsToSend details) {
		this.details = details;
	}

	@Override
	public void run() {
		// TODO Auto-generated method stub
		new SendAnPaymentEmailToCustomer().sendAnEmail(details);
	}

}
