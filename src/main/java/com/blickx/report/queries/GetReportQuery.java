package com.blickx.report.queries;

import com.blickx.searchEntities.GetEmployeeReportsSearchEntity;
import com.blickx.to.GetCustomerBillReportDate;
import com.blickx.utill.CommonUtils;

public class GetReportQuery {

	public static String getUnpaidSumQuery() {
		// TODO Auto-generated method stub
		StringBuffer query = new StringBuffer("");
		
		query.append("SELECT  ");
		query.append("    sum((bill.package_amount + (SELECT  ");
		query.append("            balance_amount ");
		query.append("        FROM ");
		query.append("            billing b ");
		query.append("        WHERE ");
		query.append("            b.customer_id = bill.customer_id ");
		query.append("        ORDER BY date DESC ");
		query.append("        LIMIT 1) - s.discount)) AS total ");
		query.append("FROM ");
		query.append("    (SELECT  ");
		query.append("        c.customer_id, ");
		query.append("            c.customer_name, ");
		query.append("            c.contact_number, ");
		query.append("            c.c_id, ");
		query.append("            c.areacode, ");
		query.append("            c.status, ");
		query.append("            c.client_id, ");
		query.append("            SUM(bill_gen.package_monthly_amount) package_amount, ");
		query.append("            SUM(bill_gen.addon_monthly_amount) addon_amount ");
		query.append("    FROM ");
		query.append("        bill_generation bill_gen, customer c ");
		query.append("    WHERE ");
		query.append("        paid_status = 0 ");
		query.append("            AND c.customer_id = bill_gen.customer_id ");
		query.append("            AND c.client_id = ?  AND c.permenent_status = 0  ");
		query.append("    GROUP BY customer_id , customer_name , contact_number , c_id , areacode , client_id , status) bill ");
		query.append("        JOIN ");
		query.append("    (SELECT  ");
		query.append("        customer_id, ");
		query.append("            SUM(discount) discount, ");
		query.append("            GROUP_CONCAT(vc_number) AS vc_number ");
		query.append("    FROM ");
		query.append("        setupbox where pda_status = 0 ");
		query.append("    GROUP BY customer_id) s ON bill.customer_id = s.customer_id ");
		query.append("WHERE ");
		query.append("    c_id LIKE CONCAT('%', ?, '%') and bill.status = ?; ");
		query.append(" ");
		
		return query.toString();
	}

	public static String getPaidSumOfCustomerDateRangeReportQuery(GetCustomerBillReportDate report) {
		// TODO Auto-generated method stub
		StringBuffer query = new StringBuffer();
		
		query.append("select ifnull(sum(b.paid_amount), 0) ");
		query.append("from customer c join billing b on c.customer_id = b.customer_id ");
		query.append("left join employee e on e.employee_id = b.employee_id ");
		query.append("where c.client_id = \'" + report.getClient_id() + "\' AND c.permenent_status = 0  ");
		if(CommonUtils.exists(report.getFrom()) && CommonUtils.exists(report.getTo())){
			query.append("AND b.date between(\'" + report.getFrom() + "\') and DATE_ADD(\'"+ report.getTo() +"\', INTERVAL 1 DAY) ");
		}
		if(CommonUtils.exists(report.getCustomer_id())) {
			query.append("AND c.customer_id = \'" + report.getCustomer_id() + "\' ");
		}
		query.append("order by b.date desc");
		
		return query.toString();
	}
	
	public static String getPaidSumOfCustomerForEmployeeReport(GetEmployeeReportsSearchEntity report) {
		// TODO Auto-generated method stub
		StringBuffer query = new StringBuffer();
		if(!report.getEmployee_id().equalsIgnoreCase("Online")) {
			query.append("select ifnull(sum(b.paid_amount), 0) ");
			query.append("from employee e join billing b on e.employee_id = b.employee_id ");
			query.append("join customer c on c.customer_id = b.customer_id ");
			query.append("where c.client_id = \'" + report.getClient_id() + "\'  AND c.permenent_status = 0 ");
			if(CommonUtils.exists(report.getFrom()) && CommonUtils.exists(report.getTo())){
				query.append("AND b.date between(\'" + report.getFrom() + "\') and DATE_ADD(\'"+ report.getTo() +"\', INTERVAL 1 DAY) ");
			}
			if(CommonUtils.exists(report.getEmployee_id())) {
				query.append("AND e.employee_id = \'" + report.getEmployee_id() + "\' ");
			}
		query.append("order by b.date desc");
		} else {
			
			query.append("select ifnull(sum(b.paid_amount), 0) ");
			query.append("from  billing b ");
			query.append("where payment_type = \'Online\' and  b.client_id = \'" + report.getClient_id() + "\' ");
			if(CommonUtils.exists(report.getFrom()) && CommonUtils.exists(report.getTo())){
				query.append("AND b.date between(\'" + report.getFrom() + "\') and DATE_ADD(\'"+ report.getTo() +"\', INTERVAL 1 DAY) ");
			}
			query.append("order by b.date desc");
		}
		
		return query.toString();
	}
	
}
