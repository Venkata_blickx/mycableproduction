package com.blickx.serviceImpl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;

import com.blickx.dao.PDAReprotDao;
import com.blickx.services.PDAReportService;
import com.blickx.to.PDAReportDto;

public class PDAReportServiceImpl implements PDAReportService {
	
	@Autowired
	private PDAReprotDao pdaDao;

	@Override
	public List<PDAReportDto> getPDAReqport(String client_id)throws Exception {
		return pdaDao.getPDAReqport(client_id);
	}
	
	
	

}
