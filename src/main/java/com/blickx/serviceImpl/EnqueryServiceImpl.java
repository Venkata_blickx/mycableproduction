package com.blickx.serviceImpl;

import org.springframework.beans.factory.annotation.Autowired;

import com.blickx.dao.EnqueryDao;
import com.blickx.domain.PackageEntity;
import com.blickx.mycable.customerAddElements.Items;
import com.blickx.services.EnqueryService;
import com.blickx.to.AutoCId;
import com.blickx.to.DeActiveEnquiryDto;
import com.blickx.to.DeActiveEnquiryResponse;
import com.blickx.to.input.GetCustomer;

public class EnqueryServiceImpl implements EnqueryService {
	
	
	@Autowired
	EnqueryDao enquryDao;

	@Override
	public String enqueryAddCustomer(String client_id, GetCustomer customer) {
		return enquryDao.enqueryAddCustomer(client_id, customer);
	}

	@Override
	public PackageEntity getPackageDetails(String client_id) {
		return enquryDao.getPackageDetails(client_id);
	}

	@Override
	public Items getIteamDetails(String client_id) {
		return enquryDao.getIteamDetails(client_id);
	}

	@Override
	public DeActiveEnquiryResponse addDeActiveEnquiry(String clientId,
			DeActiveEnquiryDto deactiveEnquiry) {
		return enquryDao.addDeActiveEnquiry(clientId,deactiveEnquiry);
	}

	@Override
	public AutoCId getAutoCIdNumber(String clientId) {
		
		return enquryDao.getAutoCIdNumber(clientId);
	}
	
	

}
