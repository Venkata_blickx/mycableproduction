package com.blickx.serviceImpl;

import java.util.List;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;

import com.blickx.dao.AreadDao;
import com.blickx.domain.AreaEntity;
import com.blickx.services.AreaService;
import com.blickx.settings.to.AreaEntityTo;

public class AreaServiceImpl implements AreaService {

	private Logger logger = Logger.getLogger(AreaServiceImpl.class);
	
	@Autowired
	AreadDao areaDao;
	
	@Override
	public String addAreaDetails(AreaEntity area) {
		// TODO Auto-generated method stub
		try{
			areaDao.addAreaDetails(area);
		}catch(Exception e){
			logger.error(e);
			return e.toString();
		}
		return "success";
	}

	@Override
	public AreaEntity getAreaDetailsById(AreaEntity area) {
		// TODO Auto-generated method stub
		return areaDao.getAreaDetailsById(area);
	}

	@Override
	public String updateAreaDetails(AreaEntity area) {
		// TODO Auto-generated method stub
		try{
			areaDao.updateAreaDetails(area);
		}catch(Exception e){
			logger.error(e);
			return e.toString();
		}
		return "success";
	}

	@Override
	public String deleteAreaDetails(AreaEntityTo area) {
		// TODO Auto-generated method stub
		try{
			areaDao.deleteAreaDetails(area);
		}catch(Exception e){
			logger.error(e);
			return e.toString();
		}
		return "success";
	}

	@Override
	public List<AreaEntityTo> getAllAreaDetails(String client_id) {
		// TODO Auto-generated method stub
		return areaDao.getAllAreaDetails(client_id);
	}

	
}
