package com.blickx.serviceImpl;

import org.springframework.beans.factory.annotation.Autowired;

import com.blickx.dao.NfcDao;
import com.blickx.domain.NFCEntity;
import com.blickx.services.NfcService;

public class NfcServiceImpl implements NfcService {
	
	@Autowired
	NfcDao nfcDao;

	@Override
	public int addNfc(NFCEntity nfcEntity) {
		// TODO Auto-generated method stub
		return nfcDao.addNfc(nfcEntity);
	}

}
