package com.blickx.serviceImpl;

import java.math.BigDecimal;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;

import com.blickx.dao.SettingsDao;
import com.blickx.domain.ClientInformationEntity;
import com.blickx.output.EditCustomerCommentTo;
import com.blickx.services.SettingsService;
import com.blickx.settings.to.GetBalanceTo;

public class SettingsServiceImpl implements SettingsService {

	@Autowired
	SettingsDao settingsDao;
	
	@Override
	public int updateBalance(String customer_id, BigDecimal balance, String reason) {
		// TODO Auto-generated method stub
		return settingsDao.updateBalance(customer_id, balance, reason);
	}

	@Override
	public GetBalanceTo getBalanceAmount(String customer_id) {
		// TODO Auto-generated method stub
		return settingsDao.getBalanceAmount(customer_id);
	}

	@Override
	public BigDecimal getServiceCharge(String client_id) {
		// TODO Auto-generated method stub
		return settingsDao.getServiceCharge(client_id);
	}

	@Override
	public List<String> getVc_numbers(String customer_id) {
		// TODO Auto-generated method stub
		return settingsDao.getVc_numbers(customer_id);
	}

	@Override
	public String addClientInfoForBlueToothPrint(
			ClientInformationEntity clientInfo) {
		// TODO Auto-generated method stub
		try{
		settingsDao.addClientInfoForBlueToothPrint(clientInfo);
		}catch(Exception e){
			return e.toString();
		}
		return "Successfully Added";
	}

	@Override
	public ClientInformationEntity getClientInfoForBlueToothPrint(
			ClientInformationEntity clientInformationForPrint, String client_id) {
		// TODO Auto-generated method stub
		return settingsDao.getClientInfoForBlueToothPrint(clientInformationForPrint, client_id);
	}

	@Override
	public String saveOrUpdateComments(
			List<EditCustomerCommentTo> comments, String client_id) {
		// TODO Auto-generated method stub
		return settingsDao.saveOrUpdateComments(comments, client_id);
	}

	@Override
	public String sendWishestoAll(String msg, String client_id) {
		// TODO Auto-generated method stub
		return settingsDao.sendWishestoAll(msg, client_id);
	}

	@Override
	public String getUpdateApkVersion() {
		// TODO Auto-generated method stub
		return settingsDao.getUpdateApkVersion();
	}

	@Override
	public String paymentReminder(String msg, String client_id) {
		// TODO Auto-generated method stub
		return settingsDao.paymentReminder(msg, client_id);
	}

}
