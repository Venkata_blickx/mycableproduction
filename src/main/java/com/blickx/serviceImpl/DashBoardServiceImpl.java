package com.blickx.serviceImpl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;

import com.blickx.dao.DashBoardDao;
import com.blickx.services.DashBoardService;
import com.blickx.settings.to.GetDeActiveAndActiveCustomersAndSetupBoxesCount;
import com.blickx.to.BillCollectorsGraphTO;
import com.blickx.to.CollectionStatistics;
import com.blickx.to.ConsolidatePaymentStatus;
import com.blickx.to.CustomerToMobileTo;
import com.blickx.to.DailyCollectionReport;
import com.blickx.to.DashBoardCollectionDetailsTo;
import com.blickx.to.NoOfPayedCustomerTo;
import com.blickx.to.PackageDetailsForDashBoardTo;
import com.blickx.to.SmartCardIssuedCustomerTo;

public class DashBoardServiceImpl implements DashBoardService {

	@Autowired
	DashBoardDao dashBoardDao;
	
	@Override
	public List<DailyCollectionReport> getDailyCollection(String client_id) {
		// TODO Auto-generated method stub
		return dashBoardDao.getDailyCollection(client_id);
	}

	@Override
	public List<BillCollectorsGraphTO> getBillCollectorsGraph(String client_id) {
		// TODO Auto-generated method stub
		return dashBoardDao.getBillCollectorsGraph(client_id);
	}

	@Override
	public List<PackageDetailsForDashBoardTo> getPackageDetails(String client_id) {
		// TODO Auto-generated method stub
		return dashBoardDao.getPackageDetails(client_id);
	}

	@Override
	public NoOfPayedCustomerTo getNoOfCustomersPayed() {
		// TODO Auto-generated method stub
		return dashBoardDao.getNoOfCustomersPayed();
	}

	@Override
	public DashBoardCollectionDetailsTo getCollectionsForDashBoard(String client_id) {
		// TODO Auto-generated method stub
		return dashBoardDao.getCollectionsForDashBoard(client_id);
	}

	@Override
	public SmartCardIssuedCustomerTo getSmartCardIssuedCustomers(String client_id) {
		// TODO Auto-generated method stub
		return dashBoardDao.getSmartCardIssuedCustomers(client_id);
	}

	@Override
	public GetDeActiveAndActiveCustomersAndSetupBoxesCount getDeActivateAndActiveCustomersSetupBoxCount(String client_id) {
		// TODO Auto-generated method stub
		return dashBoardDao.getDeActivateAndActiveCustomersSetupBoxCount(client_id);
	}

	@Override
	public ConsolidatePaymentStatus getConsolidatedPaymentStatus(
			String client_id) {
		// TODO Auto-generated method stub
		return dashBoardDao.getConsolidatedPaymentStatus(client_id);
	}

	@Override
	public List<CollectionStatistics> getCollectionStatistics(String client_id) {
		// TODO Auto-generated method stub
		return dashBoardDao.getCollectionStatistics(client_id);
	}

}
