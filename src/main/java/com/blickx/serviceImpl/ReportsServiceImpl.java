package com.blickx.serviceImpl;

import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;

import com.blickx.dao.ReportsDao;
import com.blickx.domain.Customer;
import com.blickx.output.SetupBoxReport;
import com.blickx.output.wrapper.CustomerReportsWrapper;
import com.blickx.response.AreaWiseReport;
import com.blickx.response.AreaWiseReportWrap;
import com.blickx.searchEntities.GetEmployeeReportsSearchEntity;
import com.blickx.services.ReportsService;
import com.blickx.to.CustomerBillReportByCidAndDateTo;
import com.blickx.to.CustomerReports;
import com.blickx.to.DailyCollectionByEmployee;
import com.blickx.to.GetCollectionOfEmployeeTo;
import com.blickx.to.GetCustomerBillReportDate;
import com.blickx.to.GetEmployeeReportsTo;
import com.blickx.to.GetLastSixTxsTo;
import com.blickx.to.input.LastSixMonthsTxsInputTo;
import com.blickx.to.input.SetupboxReportInput;
import com.blickx.wrapper.EmployeeReportWrapper;
import com.blickx.wrapper.UnpaidReportWrapper;

public class ReportsServiceImpl implements ReportsService{

	@Autowired
	ReportsDao reportsDao;

	@Override
	public List<DailyCollectionByEmployee> getCollectionByEmployee(String client_id) {
		// TODO Auto-generated method stub
		return reportsDao.getCollectionByEmployee(client_id);
	}

	public CustomerReportsWrapper getCustomerReports(GetCustomerBillReportDate customer) {
		// TODO Auto-generated method stub
		return reportsDao.getCustomerReports(customer);
	}

	@Override
	public CustomerReportsWrapper getCustomerReportsWithDateRange(
			GetCustomerBillReportDate getCustomerBillReportDate) {
		// TODO Auto-generated method stub
		return reportsDao.getCustomerReportsWithDateRange(getCustomerBillReportDate);
	}

	@Override
	public List<GetCollectionOfEmployeeTo> getCollectionOfEmployee(String client_id, String date) {
		// TODO Auto-generated method stub
		return reportsDao.getCollectionOfEmployee(client_id, date);
	}

	@Override
	public EmployeeReportWrapper getBillReportsOfEmployee(
			GetEmployeeReportsSearchEntity searchDetails) {
		// TODO Auto-generated method stub
		return reportsDao.getBillReportsOfEmployee(searchDetails);
	}

	@Override
	public EmployeeReportWrapper getBillReportsOfEmployeeByDate(
			GetEmployeeReportsSearchEntity searchDetails) {
		// TODO Auto-generated method stub
		return reportsDao.getBillReportsOfEmployeeByDate(searchDetails);
	}

	@Override
	public Map<String, Object> getEmployeeCurrentDayCollections(
			String employee_id) {
		// TODO Auto-generated method stub
		return reportsDao.getEmployeeCurrentDayCollections(employee_id);
	}

	@Override
	public UnpaidReportWrapper getCustomer_ReportDateByCid(
			GetCustomerBillReportDate getCustomerBillReportDate) {
		// TODO Auto-generated method stub
		return reportsDao.getCustomerBillReportDate(getCustomerBillReportDate);
	}

	@Override
	public List<GetLastSixTxsTo> getLastSixTxs(LastSixMonthsTxsInputTo input) {
		// TODO Auto-generated method stub
		return reportsDao.getLastSixTxs(input);
	}

	@Override
	public List<SetupBoxReport> getSetupboxReport(SetupboxReportInput input) {
		// TODO Auto-generated method stub
		return reportsDao.getSetupboxReport(input);
	}

	@Override
	public AreaWiseReportWrap areaWiseReport(SetupboxReportInput input) {
		// TODO Auto-generated method stub
		return reportsDao.areaWiseReport(input);
	}
	
}
