package com.blickx.mycable.customerAddElements;

import java.util.List;

import com.blickx.domain.AreaCustomerMapEntity;
import com.blickx.domain.EditCustomerCommentEntity;
import com.blickx.output.EditCustomerCommentTo;

public class CustomerAddInputTo {

	@Override
	public String toString() {
		return "CustomerAddInputTo [customer_id=" + customer_id + ", c_id="
				+ c_id + ", customer_name=" + customer_name + ", line1="
				+ line1 + ", line2=" + line2 + ", city=" + city + ", state="
				+ state + ", pincode=" + pincode + ", areacode=" + areacode
				+ ", contact_number=" + contact_number + ", landline_number="
				+ landline_number + ", email=" + email + ", deposit=" + deposit
				+ ", gender=" + gender + ", category=" + category
				+ ", joining_date=" + joining_date + ", end_date=" + end_date
				+ ", client_id=" + client_id + ", externalId=" + externalId
				+ ", domain=" + domain + ", comments=" + comments
				+ ", setupboxes=" + setupboxes + ", areaCustomerMap="
				+ areaCustomerMap + "]";
	}

	public String getC_id() {
		return c_id;
	}

	public void setC_id(String c_id) {
		this.c_id = c_id;
	}

	public String getClient_id() {
		return client_id;
	}

	public void setClient_id(String client_id) {
		this.client_id = client_id;
	}

	public List<SetupboxForAddCustomer> getSetupboxes() {
		return setupboxes;
	}

	public void setSetupboxes(List<SetupboxForAddCustomer> setupboxes) {
		this.setupboxes = setupboxes;
	}

	public String getCustomer_id() {
		return customer_id;
	}

	public void setCustomer_id(String customer_id) {
		this.customer_id = customer_id;
	}

	public String getCustomer_name() {
		return customer_name;
	}

	public void setCustomer_name(String customer_name) {
		this.customer_name = customer_name;
	}

	public String getLine1() {
		return line1;
	}

	public void setLine1(String line1) {
		this.line1 = line1;
	}

	public String getLine2() {
		return line2;
	}

	public void setLine2(String line2) {
		this.line2 = line2;
	}

	public String getCity() {
		return city;
	}

	public void setCity(String city) {
		this.city = city;
	}

	public String getState() {
		return state;
	}

	public void setState(String state) {
		this.state = state;
	}

	public String getPincode() {
		return pincode;
	}

	public void setPincode(String pincode) {
		this.pincode = pincode;
	}

	public String getAreacode() {
		return areacode;
	}

	public void setAreacode(String areacode) {
		this.areacode = areacode;
	}

	public String getContact_number() {
		return contact_number;
	}

	public void setContact_number(String contact_number) {
		this.contact_number = contact_number;
	}

	public String getLandline_number() {
		return landline_number;
	}

	public void setLandline_number(String landline_number) {
		this.landline_number = landline_number;
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public String getDeposit() {
		return deposit;
	}

	public void setDeposit(String deposit) {
		this.deposit = deposit;
	}

	public String getGender() {
		return gender;
	}

	public void setGender(String gender) {
		this.gender = gender;
	}

	public String getCategory() {
		return category;
	}

	public void setCategory(String category) {
		this.category = category;
	}
	public String getJoining_date() {
		return joining_date;
	}

	public void setJoining_date(String joining_date) {
		this.joining_date = joining_date;
	}

	public String getEnd_date() {
		return end_date;
	}

	public void setEnd_date(String end_date) {
		this.end_date = end_date;
	}

	
	private String customer_id;
	
	private String c_id;
	
	private String customer_name;
	
	private String line1;
	
	private String line2;
	
	private String city;
	
	private String state;
	
	private String pincode;
	
	private String areacode;

	private String contact_number;
	
	private String landline_number;
	
	private String email;
	
	private String deposit;
	
	private String gender;
	
	private String category;
	
	private String joining_date;
	
	private String end_date;
	
	private String client_id;
	
	private String externalId;
	
	private String domain;
	
	private List<EditCustomerCommentTo> comments;
	
	private List<SetupboxForAddCustomer> setupboxes;
	
	private AreaCustomerMapEntity areaCustomerMap;

	public List<EditCustomerCommentTo> getComments() {
		return comments;
	}

	public void setComments(List<EditCustomerCommentTo> comments) {
		this.comments = comments;
	}

	public String getExternalId() {
		return externalId;
	}

	public void setExternalId(String externalId) {
		this.externalId = externalId;
	}

	public String getDomain() {
		return domain;
	}

	public void setDomain(String domain) {
		this.domain = domain;
	}

	public AreaCustomerMapEntity getAreaCustomerMap() {
		return areaCustomerMap;
	}

	public void setAreaCustomerMap(AreaCustomerMapEntity areaCustomerMap) {
		this.areaCustomerMap = areaCustomerMap;
	}

	
}
