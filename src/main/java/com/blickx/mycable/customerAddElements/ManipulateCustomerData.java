package com.blickx.mycable.customerAddElements;


import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import com.blickx.to.SetupBoxTo;
import com.blickx.to.input.GetCustomer;
import com.blickx.utill.CommonUtils;

public class ManipulateCustomerData {

	public static GetCustomer getCustomerTo(CustomerAddInputTo input) throws ParseException{
		GetCustomer customerTo = new GetCustomer();
		
		prepareCustomerData(customerTo, input);
		
		prepareSetupBox(customerTo, input);
		
		System.out.println(" Populated Customer " + customerTo);
		
		return customerTo;
	}
	
	public static GetCustomer prepareCustomerData(GetCustomer customerTo, CustomerAddInputTo input) throws ParseException{
		
		System.out.println("prepareCustomerData------------ In");
		
		SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd");
		
		
		customerTo.setCustomer_name(input.getCustomer_name());
		customerTo.setLine1(input.getLine1());
		customerTo.setLine2(input.getLine2());
		customerTo.setPincode(input.getPincode());
		customerTo.setAreacode(input.getAreacode());
		customerTo.setC_id(input.getC_id());
		customerTo.setContact_number(input.getContact_number());
		customerTo.setLandline_number(input.getLandline_number());
		customerTo.setEmail(input.getEmail());
		customerTo.setDomain(input.getDomain());
		customerTo.setExternalId(input.getExternalId());
		
		Double deposit;
		if(CommonUtils.exists(input.getDeposit())){
			deposit = new Double(input.getDeposit());
		}else{
			deposit = 0.0;
		}
		
		customerTo.setDeposit(deposit);
		customerTo.setGender(input.getGender());
		
		
		Calendar cal = Calendar.getInstance();
		cal.setTime(dateFormat.parse(input.getJoining_date()));
		int customerUserGivenStartMonth = cal.get(Calendar.MONTH);
		Calendar currentDate = Calendar.getInstance();
		currentDate.setTime(new Date());
		int currentMonth = currentDate.get(Calendar.MONTH);
		
		if(customerUserGivenStartMonth != currentMonth){
			currentDate.set(Calendar.DAY_OF_MONTH, 1);
			customerTo.setJoining_date(currentDate.getTime());
		} else {
			customerTo.setJoining_date(dateFormat.parse(input.getJoining_date()));
		}
		
		
		//customerTo.setJoining_date(dateFormat.parse(input.getJoining_date()));
		customerTo.setCategory(input.getCategory());
		customerTo.setState(input.getState());
		customerTo.setCity(input.getCity());
		customerTo.setClient_id(input.getClient_id());
		System.out.println(input.getEnd_date());
		if(CommonUtils.exists(input.getEnd_date()))
		customerTo.setEnd_date(dateFormat.parse(input.getEnd_date()));
		
		System.out.println("prepareCustomerData------------ Out");
		return customerTo;
	}
	
	private static GetCustomer prepareSetupBox(GetCustomer customerTo, CustomerAddInputTo input) throws ParseException{
		
		System.out.println("prepareSetupBox------------ In");
		
		SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd");
		
		Set<SetupBoxTo> setupBoxes = new HashSet<SetupBoxTo>();
		for(SetupboxForAddCustomer inputSetupbox: input.getSetupboxes()){
			
			SetupBoxTo setupBox = new SetupBoxTo();
			
			/*for (Items channel : inputSetupbox.getCart().getItems()) {

				if (channel.get_name().startsWith("PKG-")) {
					setupBox.setAssigned_package(channel.get_name().substring(4, channel.get_name().length()));
					setupBox.setPackage_id(channel.get_id());
				}

			}*/
			setupBox.setAssigned_package(inputSetupbox.getSelectedPackage().getPackage_name());
			setupBox.setPackage_id(new Integer(inputSetupbox.getSelectedPackage().getPackage_id()).toString());
			
			setupBox.setBox_number(inputSetupbox.getBox_number());
			setupBox.setVc_number(inputSetupbox.getVc_number());
			setupBox.setNds_number(inputSetupbox.getNds_number());
			
			Calendar cal = Calendar.getInstance();
			cal.setTime(dateFormat.parse(inputSetupbox.getStart_date()));
			int setupboxUserGivenStartMonth = cal.get(Calendar.MONTH);
			Calendar currentDate = Calendar.getInstance();
			currentDate.setTime(new Date());
			int currentMonth = currentDate.get(Calendar.MONTH);
			
			if(setupboxUserGivenStartMonth != currentMonth){
				currentDate.set(Calendar.DAY_OF_MONTH, 1);
				setupBox.setStart_date(currentDate.getTime());
			} else {
				setupBox.setStart_date(dateFormat.parse(inputSetupbox.getStart_date()));
			}
			
			
			
			//if((CommonUtils.exists(inputSetupbox.getDiscount()))){
				setupBox.setDiscount(new Double(inputSetupbox.getDiscount()));
			//}else{
				//setupBox.setDiscount(new Double(0));
			//}
			setupBox.setAddonNames(getAddonsNameswithDates(inputSetupbox.getCart().getItems(), input, inputSetupbox).toString());
			
			setupBox.setClient_id(input.getClient_id());
			setupBoxes.add(setupBox);
		}
		customerTo.setSetupBox(setupBoxes);
		
		System.out.println("prepareSetupBox------------ Out");
		return customerTo;
	}
	
	private static StringBuffer getAddonsNameswithDates(List<Items> list, CustomerAddInputTo input, SetupboxForAddCustomer setupBox) throws ParseException{
		
		System.out.println("getAddonsNameswithDates------------ In");
		
		StringBuffer formattedString = new StringBuffer("");
		boolean isNotFirst = false;
		
		System.out.println("getAddonNameswithDates.....");
		
		
		for(Items channel:list){
			
			if(!channel.get_name().startsWith("PKG-")){
				formattedString.append(channel.get_name());
				
				SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd");
				Date date = dateFormat.parse(setupBox.getStart_date().toString());
				Date today = new Date();
				long diff = today.getTime()-date.getTime();
	
				int days = (int) (diff/(1000*60*60*24));
				//System.out.println(days/30);
			    Calendar cal = Calendar.getInstance();
			    cal.setTime(date);
			    cal.set(cal.get(Calendar.YEAR), (cal.get(Calendar.MONTH)+channel.get_quantity()) , cal.get(Calendar.DAY_OF_MONTH));
				
			    
			    formattedString.append(",");
			    formattedString.append(dateFormat.format(cal.getTime()).toString());
			    formattedString.append(",");
				
				System.out.println(formattedString);
				
				System.out.println("getAddonsNameswithDates------------ Out");
			}
			
		}
		
		
		
		return formattedString;
	}
	
	/*public static void main(String[] args) throws ParseException {
		SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd");
		Date date = dateFormat.parse("2015-08-01");
		System.out.println(date);
	}*/
	
}
