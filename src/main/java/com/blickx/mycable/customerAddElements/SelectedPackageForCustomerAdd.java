package com.blickx.mycable.customerAddElements;

public class SelectedPackageForCustomerAdd {

    @Override
	public String toString() {
		return "SelectedPackageForCustomerAdd [package_id=" + package_id
				+ ", package_amount=" + package_amount + ", package_name="
				+ package_name + ", status=" + status + "]";
	}

	public int getPackage_id() {
		return package_id;
	}

	public void setPackage_id(int package_id) {
		this.package_id = package_id;
	}

	public double getPackage_amount() {
		return package_amount;
	}

	public void setPackage_amount(double package_amount) {
		this.package_amount = package_amount;
	}

	public String getPackage_name() {
		return package_name;
	}

	public void setPackage_name(String package_name) {
		this.package_name = package_name;
	}

	public String getStatus() {
		return status;
	}

	public void setStatus(String status) {
		this.status = status;
	}

	private int package_id;
    
    private double package_amount;
    
    private String package_name;
    
    private String status;

}
