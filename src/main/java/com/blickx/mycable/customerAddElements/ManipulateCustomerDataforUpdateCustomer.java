package com.blickx.mycable.customerAddElements;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import org.apache.log4j.Logger;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.SessionAttributes;

import com.blickx.dao.CustomerDao;
import com.blickx.dao.EmployeeDaoImpl;
import com.blickx.domain.AddOnsEntity;
import com.blickx.domain.Customer;
import com.blickx.domain.SetupBox;
import com.blickx.services.CustomerService;
import com.blickx.settings.to.AddonsEntityTo;
import com.blickx.to.SetupBoxTo;
import com.blickx.to.input.GetCustomer;
import com.blickx.utill.CommonUtils;

public class ManipulateCustomerDataforUpdateCustomer {
	
	private static Logger logger = Logger.getLogger(ManipulateCustomerDataforUpdateCustomer.class);
	
	
	
	public Customer getCustomerTo(CustomerAddInputTo input, CustomerDao cusDao) throws ParseException{
		Customer customerTo = new Customer();
		
		prepareCustomerData(customerTo, input, cusDao);
		
		try{
			prepareSetupBox(customerTo, input, cusDao);
		}catch(Exception e){
			logger.error("*******************88"+e);
			e.printStackTrace();
			//System.out.println(e);
		}
		
		
		System.out.println(" Populated Customer " + customerTo);
		
		return customerTo;
	}
	
	public static Customer prepareCustomerData(Customer customerTo, CustomerAddInputTo input, CustomerDao cdao) throws ParseException{
		
		logger.info("prepareCustomerData------------ In"+input);
		
		SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd");
		
		customerTo.setCustomer_id(input.getCustomer_id());
		customerTo.setCustomer_name(input.getCustomer_name());
		customerTo.setLine1(input.getLine1());
		customerTo.setLine2(input.getLine2());
		customerTo.setPincode(input.getPincode());
		customerTo.setAreacode(input.getAreacode());
		customerTo.setC_id(input.getC_id());
		customerTo.setContact_number(input.getContact_number());
		customerTo.setLandline_number(input.getLandline_number());
		customerTo.setEmail(input.getEmail());
		customerTo.setDomain(input.getDomain());
		customerTo.setExternalId(input.getExternalId());
		
		Double deposit;
		if(CommonUtils.exists(input.getDeposit())){
			deposit = new Double(input.getDeposit());
		}else{
			deposit = 0.0;
		}
		
		customerTo.setDeposit(deposit);
		customerTo.setGender(input.getGender());
		customerTo.setJoining_date(dateFormat.parse(input.getJoining_date()));
		customerTo.setCategory(input.getCategory());
		customerTo.setState(input.getState());
		customerTo.setCity(input.getCity());
		customerTo.setClient_id(input.getClient_id());
		System.out.println(input.getEnd_date());
		if(CommonUtils.exists(input.getEnd_date()))
		customerTo.setEnd_date(dateFormat.parse(input.getEnd_date()));
		
		logger.info("prepareCustomerData------------ Out"+customerTo);
		return customerTo;
	}
	
	private  Customer prepareSetupBox(Customer customerTo, CustomerAddInputTo input, CustomerDao cdao) throws ParseException{
		
		System.out.println("prepareSetupBox------------ In");
		
		SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd");
		
		Set<SetupBox> setupBoxes = new HashSet<SetupBox>();
		for(SetupboxForAddCustomer inputSetupbox: input.getSetupboxes()){
			
			SetupBox setupBox = new SetupBox();
			
			/*for (Items channel : inputSetupbox.getCart().getItems()) {

				if (channel.get_name().startsWith("PKG-")) {
					setupBox.setAssigned_package(channel.get_name().substring(4, channel.get_name().length()));
					setupBox.setPackage_id(Integer.parseInt(channel.get_id()));
				}

			}*/
			setupBox.setRow_id(inputSetupbox.getRow_id());
			setupBox.setAssigned_package(inputSetupbox.getSelectedPackage().getPackage_name());
			setupBox.setPackage_id(new Integer(inputSetupbox.getSelectedPackage().getPackage_id()));
			setupBox.setStatus(true);
			setupBox.setBox_number(inputSetupbox.getBox_number());
			setupBox.setVc_number(inputSetupbox.getVc_number());
			setupBox.setNds_number(inputSetupbox.getNds_number());
			setupBox.setStart_date(dateFormat.parse(inputSetupbox.getStart_date()));
			//if((CommonUtils.exists(inputSetupbox.getDiscount()))){
				setupBox.setDiscount(new Double(inputSetupbox.getDiscount()));
			//}else{
				//setupBox.setDiscount(new Double(0));
			//}
			if(!(inputSetupbox.getCart() == null)){
			setupBox.setAddonNames(this.getAddonsNameswithDates(inputSetupbox.getCart().getItems(), input, inputSetupbox, cdao).toString());
			}
			setupBox.setClient_id(input.getClient_id());
			setupBox.setUpdatepackage_date(dateFormat.parse(inputSetupbox.getStart_date()));
			setupBoxes.add(setupBox);
		}
		customerTo.setSetupBoxes(setupBoxes);
		
		System.out.println("prepareSetupBox------------ Out");
		return customerTo;
	}
	
	private StringBuffer getAddonsNameswithDates(List<Items> list, CustomerAddInputTo input, SetupboxForAddCustomer setupbox, CustomerDao cdao) throws ParseException{
		
		System.out.println("getAddonsNameswithDates------------ In");
		
		StringBuffer formattedString = new StringBuffer("");
		boolean isNotFirst = false;
		
		System.out.println("getAddonNameswithDates.....");
		
		for(Items channel:list){
			
			if(!channel.get_name().startsWith("PKG-")){
				
				
				
				AddonsEntityTo addonEntity = cdao.getAddonEntity(setupbox.getBox_number(), channel.get_name());
				logger.info("*****************************************************************");
				logger.info(addonEntity);
				logger.info("*****************************************************************");
				
					formattedString.append(channel.get_name());
					
					SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd");
					Date date = new Date();
					if(CommonUtils.exists(addonEntity.getAddon())){
						date = addonEntity.getExpiry_date();
					}else{
					date = dateFormat.parse(setupbox.getStart_date());
					}
					Date today = new Date();
					//long diff = today.getTime()-date.getTime();
		
					//int days = (int) (diff/(1000*60*60*24));
					//System.out.println(days/30);
				    Calendar cal = Calendar.getInstance();
				    cal.setTime(date);
				    cal.set(cal.get(Calendar.YEAR), ((cal.get(Calendar.MONTH))+channel.get_quantity()) , cal.get(Calendar.DAY_OF_MONTH));
					
				    
				    formattedString.append(",");
				    formattedString.append(dateFormat.format(cal.getTime()).toString());
				    formattedString.append(",");
					
					System.out.println(formattedString);
					
					System.out.println("getAddonsNameswithDates------------ Out");
			}
			
			
		}
		
		
		
		return formattedString;
	}
	
	@Autowired
	static CustomerService cService;
	
	/*public static void main(String[] args) throws ParseException {
		SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd");
		Date date = dateFormat.parse("2015-08-01");
		System.out.println(date);
	}*/

}
