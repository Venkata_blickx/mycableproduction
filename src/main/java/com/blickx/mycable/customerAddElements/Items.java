package com.blickx.mycable.customerAddElements;

public class Items {

	public int get_quantity() {
		return _quantity;
	}

	public void set_quantity(int _quantity) {
		this._quantity = _quantity;
	}

	@Override
	public String toString() {
		return "Items [_id=" + _id + ", _name=" + _name + ", _price=" + _price
				+ ", _quantity=" + _quantity + "]";
	}

	public String get_id() {
		return _id;
	}

	public void set_id(String _id) {
		this._id = _id;
	}

	public String get_name() {
		return _name;
	}

	public void set_name(String _name) {
		this._name = _name;
	}

	public double get_price() {
		return _price;
	}

	public void set_price(double _price) {
		this._price = _price;
	}

	
	public double get_data() {
		return _data;
	}

	public void set_data(double _data) {
		this._data = _data;
	}


	private double _data;
	private String _id;
	
	private String _name;
	
	private double _price;
	
	private int _quantity;
	
}