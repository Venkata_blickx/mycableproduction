package com.blickx.mycable.customerAddElements;

import java.util.List;


public class SetupboxForAddCustomer {

	public String getPackage_price() {
		return package_price;
	}

	public void setPackage_price(String package_price) {
		this.package_price = package_price;
	}

	private String box_number;
	
	private String vc_number;
	
	private String nds_number;
	
	private boolean PackageExists;
	
	private boolean status;
	
	public boolean isStatus() {
		return status;
	}

	public void setStatus(boolean status) {
		this.status = status;
	}

	public boolean getPackageExists() {
		return PackageExists;
	}

	public void setPackageExists(boolean isPackageExists) {
		this.PackageExists = isPackageExists;
	}

	public int getRow_id() {
		return row_id;
	}

	public void setRow_id(int row_id) {
		this.row_id = row_id;
	}

	private int row_id;
	
	private double discount;
	
	private String start_date;
	
	private String package_price;
	
	private SelectedPackageForCustomerAdd selectedPackage;
	
	private List<SelectedChannelsForCustomerAdd> selectedChannels;
	
	private Cart cart; 

	
	@Override
	public String toString() {
		return "SetupboxForAddCustomer [box_number=" + box_number
				+ ", vc_number=" + vc_number + ", nds_number=" + nds_number
				+ ", PackageExists=" + PackageExists + ", status=" + status
				+ ", row_id=" + row_id + ", discount=" + discount
				+ ", start_date=" + start_date + ", package_price="
				+ package_price + ", selectedPackage=" + selectedPackage
				+ ", selectedChannels=" + selectedChannels + ", cart=" + cart
				+ "]";
	}

	public String getStart_date() {
		return start_date;
	}

	public void setStart_date(String start_date) {
		this.start_date = start_date;
	}

	public Cart getCart() {
		return cart;
	}

	public void setCart(Cart cart) {
		this.cart = cart;
	}

	
	public String getBox_number() {
		return box_number;
	}

	public void setBox_number(String box_number) {
		this.box_number = box_number;
	}

	public String getVc_number() {
		return vc_number;
	}

	public void setVc_number(String vc_number) {
		this.vc_number = vc_number;
	}

	public String getNds_number() {
		return nds_number;
	}

	public void setNds_number(String nds_number) {
		this.nds_number = nds_number;
	}

	public double getDiscount() {
		return discount;
	}

	public void setDiscount(double discount) {
		this.discount = discount;
	}

	public SelectedPackageForCustomerAdd getSelectedPackage() {
		return selectedPackage;
	}

	public void setSelectedPackage(SelectedPackageForCustomerAdd selectedPackage) {
		this.selectedPackage = selectedPackage;
	}

	public List<SelectedChannelsForCustomerAdd> getSelectedChannels() {
		return selectedChannels;
	}

	public void setSelectedChannels(
			List<SelectedChannelsForCustomerAdd> selectedChannels) {
		this.selectedChannels = selectedChannels;
	}

	
	
}
