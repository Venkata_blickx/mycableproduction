package com.blickx.mycable.customerAddElements;

import java.util.List;

public class Cart {

	@Override
	public String toString() {
		return "Cart [shipping=" + shipping + ", taxRate=" + taxRate + ", tax="
				+ tax + ", items=" + items + "]";
	}

	public double getShipping() {
		return shipping;
	}

	public void setShipping(double shipping) {
		this.shipping = shipping;
	}

	public double getTaxRate() {
		return taxRate;
	}

	public void setTaxRate(double taxRate) {
		this.taxRate = taxRate;
	}

	public boolean getIsPackageExists() {
		return isPackageExists;
	}

	public void setisPackageExists(boolean isPackageExists) {
		this.isPackageExists = isPackageExists;
	}

	public double getTax() {
		return tax;
	}

	public void setTax(double tax) {
		this.tax = tax;
	}

	public double get_data() {
		return _data;
	}

	public void set_data(double _data) {
		this._data = _data;
	}

	public List<Items> getItems() {
		return items;
	}

	public void setItems(List<Items> items) {
		this.items = items;
	}

	private double shipping;
	
	private double taxRate;
	
	private double tax;
	
	private boolean isPackageExists;
	
	private double _data;
	
	private List<Items> items;
	
	
}
