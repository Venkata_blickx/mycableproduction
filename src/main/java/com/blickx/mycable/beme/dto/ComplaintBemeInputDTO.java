package com.blickx.mycable.beme.dto;

import java.util.Date;

import org.springframework.format.annotation.DateTimeFormat;

public class ComplaintBemeInputDTO {

	private String beemeComplaintId;
	private String source;
	private int complaint_id;
	private String customer_id;
	private String employee_id;
	@DateTimeFormat(pattern ="yyyy-MM-dd")
	private Date date;
	private String details;
	private String status;
	private String comment;
	@DateTimeFormat(pattern ="yyyy-MM-dd")
	private Date closed_date;
	private String assigned_to;
	@DateTimeFormat(pattern ="yyyy-MM-dd")
	private Date assigned_date;
	private String closed_by;
	private String employee_uid;
	private String customer_uid;
	private String ime_number;
	private String c_id;
	private String client_id;
	public String getBeemeComplaintId() {
		return beemeComplaintId;
	}
	public void setBeemeComplaintId(String beemeComplaintId) {
		this.beemeComplaintId = beemeComplaintId;
	}
	public String getSource() {
		return source;
	}
	public void setSource(String source) {
		this.source = source;
	}
	public int getComplaint_id() {
		return complaint_id;
	}
	public void setComplaint_id(int complaint_id) {
		this.complaint_id = complaint_id;
	}
	public String getCustomer_id() {
		return customer_id;
	}
	public void setCustomer_id(String customer_id) {
		this.customer_id = customer_id;
	}
	public String getEmployee_id() {
		return employee_id;
	}
	public void setEmployee_id(String employee_id) {
		this.employee_id = employee_id;
	}
	public Date getDate() {
		return date;
	}
	public void setDate(Date date) {
		this.date = date;
	}
	public String getDetails() {
		return details;
	}
	public void setDetails(String details) {
		this.details = details;
	}
	public String getStatus() {
		return status;
	}
	public void setStatus(String status) {
		this.status = status;
	}
	public String getComment() {
		return comment;
	}
	public void setComment(String comment) {
		this.comment = comment;
	}
	public Date getClosed_date() {
		return closed_date;
	}
	public void setClosed_date(Date closed_date) {
		this.closed_date = closed_date;
	}
	public String getAssigned_to() {
		return assigned_to;
	}
	public void setAssigned_to(String assigned_to) {
		this.assigned_to = assigned_to;
	}
	public Date getAssigned_date() {
		return assigned_date;
	}
	public void setAssigned_date(Date assigned_date) {
		this.assigned_date = assigned_date;
	}
	public String getClosed_by() {
		return closed_by;
	}
	public void setClosed_by(String closed_by) {
		this.closed_by = closed_by;
	}
	public String getEmployee_uid() {
		return employee_uid;
	}
	public void setEmployee_uid(String employee_uid) {
		this.employee_uid = employee_uid;
	}
	public String getCustomer_uid() {
		return customer_uid;
	}
	public void setCustomer_uid(String customer_uid) {
		this.customer_uid = customer_uid;
	}
	public String getIme_number() {
		return ime_number;
	}
	public void setIme_number(String ime_number) {
		this.ime_number = ime_number;
	}
	public String getC_id() {
		return c_id;
	}
	public void setC_id(String c_id) {
		this.c_id = c_id;
	}
	public String getClient_id() {
		return client_id;
	}
	public void setClient_id(String client_id) {
		this.client_id = client_id;
	}
	@Override
	public String toString() {
		return "ComplaintBemeInputDTO [beemeComplaintId=" + beemeComplaintId
				+ ", source=" + source + ", complaint_id=" + complaint_id
				+ ", customer_id=" + customer_id + ", employee_id="
				+ employee_id + ", date=" + date + ", details=" + details
				+ ", status=" + status + ", comment=" + comment
				+ ", closed_date=" + closed_date + ", assigned_to="
				+ assigned_to + ", assigned_date=" + assigned_date
				+ ", closed_by=" + closed_by + ", employee_uid=" + employee_uid
				+ ", customer_uid=" + customer_uid + ", ime_number="
				+ ime_number + ", c_id=" + c_id + ", client_id=" + client_id
				+ "]";
	}

}
