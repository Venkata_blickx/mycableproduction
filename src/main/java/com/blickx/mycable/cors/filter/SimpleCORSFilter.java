package com.blickx.mycable.cors.filter;

import java.io.IOException;
import java.util.Enumeration;

import javax.servlet.FilterChain;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.web.filter.OncePerRequestFilter;

import com.blickx.services.AuthenticationService;
import com.blickx.utill.CommonUtils;

@Component("simpleCORSFilter")
public class SimpleCORSFilter extends OncePerRequestFilter {
	
	public static final String AUTHENTICATION_HEADER = "Authorization";

	@Autowired
	public AuthenticationService authService;
	

	@Override
	protected void doFilterInternal(HttpServletRequest request,
			HttpServletResponse response, FilterChain filterChain)
			throws ServletException, IOException {
		System.out.println("SimpleCORSFilter");
		
		/*System.out.println("***********************************************************************************" + request);
		@SuppressWarnings("unused")
		HttpServletResponse resp = (HttpServletResponse) response;
		response.setHeader("Access-Control-Allow-Origin", "*");
		response.setHeader("Access-Control-Allow-Methods", "POST, GET, OPTIONS, DELETE");
		response.setHeader("Access-Control-Max-Age", "3600");
		response.setHeader("Access-Control-Allow-Headers", "Origin, X-Requested-With, Content-Type, Accept, Authorization");
		response.setHeader("Content-Type", "application/json");
		filterChain.doFilter(request, response);
			
		if (authenticate(request,response)) {
			filterChain.doFilter(request, response);
			System.out.println("Success");
		} else {
			if (response instanceof HttpServletResponse) {
				HttpServletResponse httpServletResponse = (HttpServletResponse) response;
				httpServletResponse
						.setStatus(HttpServletResponse.SC_UNAUTHORIZED);
				System.out.println("Fail");
				return;
			}
		}
		
		
		//filterChain.doFilter(request, response);
		System.out.println("***********************************************************************************" + request);*/
		response.addHeader("Access-Control-Allow-Origin", "*");
		
		if (request.getHeader("Access-Control-Request-Method") != null && "OPTIONS".equals(request.getMethod())) {
			//LOG.trace("Sending Header....");
			// CORS "pre-flight" request
			response.addHeader("Access-Control-Allow-Methods", "GET, POST, PUT, DELETE");
			response.setHeader("Access-Control-Allow-Headers", "Origin, X-Requested-With, Content-Type, Accept, Authorization");
            response.addHeader("Access-Control-Max-Age", "1");
		}
		
		filterChain.doFilter(request, response);
		
		
		
	}
	
	
	private boolean authenticate(HttpServletRequest request,
			HttpServletResponse response){
		
			HttpServletRequest httpServletRequest = (HttpServletRequest) request;
			
				
			Enumeration<String> headerNames = request.getHeaderNames();
			
				 
			
				        while (headerNames.hasMoreElements()) {
			
				 
			
				            String headerName = headerNames.nextElement();
			
				            System.out.println("Key: " + headerName);
			
			
				 
			
				            Enumeration<String> headers = request.getHeaders(headerName);
			
				            while (headers.hasMoreElements()) {
			
				                String headerValue = headers.nextElement();
			
				                System.out.println("Value: " + headerValue);
			
			
				            }
			
				 
			
				        }
				        
			
	/*		if(request.getParameter("username")!= null && request.getParameter("password")!= null){
				return true;
			}
			*/
			String authCredentials = httpServletRequest
					.getHeader(AUTHENTICATION_HEADER);
			
			System.out.println("Credetials Are:" + authCredentials);
			
			//SpringBeanAutowiringSupport.processInjectionBasedOnCurrentContext(this);

			// better injected
			//AuthenticationService authenticationService = new AuthenticationService();

			return authService.authenticate(authCredentials);
		
	}

}