package com.blickx.to;

import java.util.Date;

import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import org.springframework.format.annotation.DateTimeFormat;

import com.blickx.domain.DateSerializerWithTimeStamp;

public class DeviceHistoryTo {

	private String employee_id;
	
	private String employee_name;
	
	private String device_name;
	
	private int device_id;
	
	@DateTimeFormat(pattern = "yyyy-MM-dd")
	private Date effective_from;
	
	@DateTimeFormat(pattern = "yyyy-MM-dd")
	private Date effective_to;
	
	private String contact_number;
	
	private String sim_number;
	
	private String ime_number;

	public String getEmployee_id() {
		return employee_id;
	}

	public void setEmployee_id(String employee_id) {
		this.employee_id = employee_id;
	}

	public String getEmployee_name() {
		return employee_name;
	}

	public void setEmployee_name(String employee_name) {
		this.employee_name = employee_name;
	}

	public String getDevice_name() {
		return device_name;
	}

	public void setDevice_name(String device_name) {
		this.device_name = device_name;
	}

	public int getDevice_id() {
		return device_id;
	}

	public void setDevice_id(int device_id) {
		this.device_id = device_id;
	}

	@JsonSerialize(using = DateSerializerWithTimeStamp.class)
	public Date getEffective_from() {
		return effective_from;
	}

	public void setEffective_from(Date effective_from) {
		this.effective_from = effective_from;
	}

	@JsonSerialize(using = DateSerializerWithTimeStamp.class)
	public Date getEffective_to() {
		return effective_to;
	}

	public void setEffective_to(Date effective_to) {
		this.effective_to = effective_to;
	}

	public String getContact_number() {
		return contact_number;
	}

	public void setContact_number(String contact_number) {
		this.contact_number = contact_number;
	}

	public String getSim_number() {
		return sim_number;
	}

	public void setSim_number(String sim_number) {
		this.sim_number = sim_number;
	}

	public String getIme_number() {
		return ime_number;
	}

	public void setIme_number(String ime_number) {
		this.ime_number = ime_number;
	}
	
	public DeviceHistoryTo(){
		
	}

	@Override
	public String toString() {
		return "DeviceHistoryTo [employee_id=" + employee_id
				+ ", employee_name=" + employee_name + ", device_name="
				+ device_name + ", device_id=" + device_id
				+ ", effective_from=" + effective_from + ", effective_to="
				+ effective_to + ", contact_number=" + contact_number
				+ ", sim_number=" + sim_number + ", ime_number=" + ime_number
				+ "]";
	}
	
	
}
