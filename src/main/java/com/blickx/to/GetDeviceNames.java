package com.blickx.to;

import java.math.BigInteger;

public class GetDeviceNames {

	private Integer device_id;
	
	private String device_name;

	public Integer getDevice_id() {
		return device_id;
	}

	public void setDevice_id(Integer device_id) {
		this.device_id = device_id;
	}

	public String getDevice_name() {
		return device_name;
	}

	public void setDevice_name(String device_name) {
		this.device_name = device_name;
	}
	public GetDeviceNames(){
		
	}
}
