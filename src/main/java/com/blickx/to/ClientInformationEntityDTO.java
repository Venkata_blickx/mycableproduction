package com.blickx.to;

import javax.persistence.Column;

public class ClientInformationEntityDTO {
	private int id;
	
	private String client_company_name;
	
	private String address_line1;

	private String address_line2;

	private String address_line3;

	private String address_line4;

	private String address_line5;

	private String address_line6;

	private String item_no1;

	private String item_no2;

	private String item_no3;

	private String item_no4;


	private String item_no5;

	private String item_no6;

	private String note_line1;

	private String note_line2;

	private String client_id;

	private String createdOn;

	private String lastUpdate;

	private String companyTypeId;

	private String stars;

	private String invoice;
	
	private String duplicate_invoice;
	
	private String thanks;
	
	private String printer_name;

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public String getClient_company_name() {
		return client_company_name;
	}

	public void setClient_company_name(String client_company_name) {
		this.client_company_name = client_company_name;
	}

	public String getAddress_line1() {
		return address_line1;
	}

	public void setAddress_line1(String address_line1) {
		this.address_line1 = address_line1;
	}

	public String getAddress_line2() {
		return address_line2;
	}

	public void setAddress_line2(String address_line2) {
		this.address_line2 = address_line2;
	}

	public String getAddress_line3() {
		return address_line3;
	}

	public void setAddress_line3(String address_line3) {
		this.address_line3 = address_line3;
	}

	public String getAddress_line4() {
		return address_line4;
	}

	public void setAddress_line4(String address_line4) {
		this.address_line4 = address_line4;
	}

	public String getAddress_line5() {
		return address_line5;
	}

	public void setAddress_line5(String address_line5) {
		this.address_line5 = address_line5;
	}

	public String getAddress_line6() {
		return address_line6;
	}

	public void setAddress_line6(String address_line6) {
		this.address_line6 = address_line6;
	}

	public String getItem_no1() {
		return item_no1;
	}

	public void setItem_no1(String item_no1) {
		this.item_no1 = item_no1;
	}

	public String getItem_no2() {
		return item_no2;
	}

	public void setItem_no2(String item_no2) {
		this.item_no2 = item_no2;
	}

	public String getItem_no3() {
		return item_no3;
	}

	public void setItem_no3(String item_no3) {
		this.item_no3 = item_no3;
	}

	public String getItem_no4() {
		return item_no4;
	}

	public void setItem_no4(String item_no4) {
		this.item_no4 = item_no4;
	}

	public String getItem_no5() {
		return item_no5;
	}

	public void setItem_no5(String item_no5) {
		this.item_no5 = item_no5;
	}

	public String getItem_no6() {
		return item_no6;
	}

	public void setItem_no6(String item_no6) {
		this.item_no6 = item_no6;
	}

	public String getNote_line1() {
		return note_line1;
	}

	public void setNote_line1(String note_line1) {
		this.note_line1 = note_line1;
	}

	public String getNote_line2() {
		return note_line2;
	}

	public void setNote_line2(String note_line2) {
		this.note_line2 = note_line2;
	}

	public String getClient_id() {
		return client_id;
	}

	public void setClient_id(String client_id) {
		this.client_id = client_id;
	}

	public String getCreatedOn() {
		return createdOn;
	}

	public void setCreatedOn(String createdOn) {
		this.createdOn = createdOn;
	}

	public String getLastUpdate() {
		return lastUpdate;
	}

	public void setLastUpdate(String lastUpdate) {
		this.lastUpdate = lastUpdate;
	}

	public String getCompanyTypeId() {
		return companyTypeId;
	}

	public void setCompanyTypeId(String companyTypeId) {
		this.companyTypeId = companyTypeId;
	}

	public String getStars() {
		return stars;
	}

	public void setStars(String stars) {
		this.stars = stars;
	}

	public String getInvoice() {
		return invoice;
	}

	public void setInvoice(String invoice) {
		this.invoice = invoice;
	}

	public String getDuplicate_invoice() {
		return duplicate_invoice;
	}

	public void setDuplicate_invoice(String duplicate_invoice) {
		this.duplicate_invoice = duplicate_invoice;
	}

	public String getThanks() {
		return thanks;
	}

	public void setThanks(String thanks) {
		this.thanks = thanks;
	}

	public String getPrinter_name() {
		return printer_name;
	}

	public void setPrinter_name(String printer_name) {
		this.printer_name = printer_name;
	}

}
