package com.blickx.to;


public class SearchCustomerTo {

	private String customer_id;
	
	private String customer_name;
	
	private String areacode;
	
	private String contact_number;
	
	private String box_number;
	
	private String line1;
	
	private String line2;
	
	private String c_id;
	
	private String status;
	
	private String permenent_status;
	
	private String vc_number;
	
	private boolean billingType;
	
	

	public boolean getBillingType() {
		return billingType;
	}

	public void setBillingType(boolean billingType) {
		this.billingType = billingType;
	}

	public String getCustomer_id() {
		return customer_id;
	}

	public void setCustomer_id(String customer_id) {
		this.customer_id = customer_id;
	}

	public String getCustomer_name() {
		return customer_name;
	}

	public void setCustomer_name(String customer_name) {
		this.customer_name = customer_name;
	}

	public String getAreacode() {
		return areacode;
	}

	public void setAreacode(String areacode) {
		this.areacode = areacode;
	}

	public String getContact_number() {
		return contact_number;
	}

	public void setContact_number(String contact_number) {
		this.contact_number = contact_number;
	}

	public String getBox_number() {
		return box_number;
	}

	public void setBox_number(String box_number) {
		this.box_number = box_number;
	}

	public String getLine1() {
		return line1;
	}

	public void setLine1(String line1) {
		this.line1 = line1;
	}

	public String getLine2() {
		return line2;
	}

	public void setLine2(String line2) {
		this.line2 = line2;
	}

	public String getC_id() {
		return c_id;
	}

	public void setC_id(String c_id) {
		this.c_id = c_id;
	}

	public String getStatus() {
		return status;
	}

	public void setStatus(String status) {
		this.status = status;
	}

	public String getPermenent_status() {
		return permenent_status;
	}

	public void setPermenent_status(String permenent_status) {
		this.permenent_status = permenent_status;
	}

	public String getVc_number() {
		return vc_number;
	}

	public void setVc_number(String vc_number) {
		this.vc_number = vc_number;
	}
}
