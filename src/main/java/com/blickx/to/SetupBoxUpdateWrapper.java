package com.blickx.to;

import java.util.List;

public class SetupBoxUpdateWrapper {

	private List<SetupBoxTo> setupBox;

	public List<SetupBoxTo> getSetupBox() {
		return setupBox;
	}

	public void setSetupBox(List<SetupBoxTo> setupBox) {
		this.setupBox = setupBox;
	}

	
}
