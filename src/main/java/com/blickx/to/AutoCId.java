package com.blickx.to;

public class AutoCId {
	
	private String autoCid;

	public String getAutoCid() {
		return autoCid;
	}

	public void setAutoCid(String autoCid) {
		this.autoCid = autoCid;
	}

	@Override
	public String toString() {
		return "AutoCId [autoCid=" + autoCid + "]";
	}
	

}
