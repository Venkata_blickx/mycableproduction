package com.blickx.to;

import java.math.BigDecimal;
import java.math.BigInteger;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;

@Entity
public class SearchPackageTO {

	@Id
	@Column
	private int package_id;
	
	@Column
	private String package_name;
	
	@Column
	private BigDecimal package_amount;
	
	/*@Column
	private BigInteger channelCount;*/
	
	@Column
	private String status;
	
	public String getStatus() {
		return status;
	}

	public void setStatus(String status) {
		this.status = status;
	}

	public int getPackage_id() {
		return package_id;
	}

	public void setPackage_id(int package_id) {
		this.package_id = package_id;
	}

	public String getPackage_name() {
		return package_name;
	}

	public void setPackage_name(String package_name) {
		this.package_name = package_name;
	}

	public BigDecimal getPackage_amount() {
		return package_amount;
	}

	public void setPackage_amount(BigDecimal package_amount) {
		this.package_amount = package_amount;
	}

/*	public BigInteger getChannelCount() {
		return channelCount;
	}

	public void setChannelCount(BigInteger channelCount) {
		this.channelCount = channelCount;
	}
*/
	public SearchPackageTO(){
		
	}
}
