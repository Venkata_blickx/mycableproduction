package com.blickx.to;

import java.math.BigDecimal;
import java.math.BigInteger;

public class DashBoardCollectionDetailsTo {

	@Override
	public String toString() {
		return "DashBoardCollectionDetailsTo [day_collection=" + day_collection
				+ ", week_collection=" + week_collection
				+ ", month_collection=" + month_collection
				+ ", active_device_count=" + active_device_count
				+ ", total_davice=" + total_davice + "]";
	}

	public BigInteger getTotal_davice() {
		return total_davice;
	}

	public void setTotal_davice(BigInteger total_davice) {
		this.total_davice = total_davice;
	}

	private BigDecimal day_collection;
	
	private BigDecimal week_collection;
	
	private BigDecimal month_collection;
	
	private BigInteger active_device_count;
	
	private BigInteger total_davice;

	public BigDecimal getDay_collection() {
		return day_collection;
	}

	public void setDay_collection(BigDecimal day_collection) {
		this.day_collection = day_collection;
	}

	public BigDecimal getWeek_collection() {
		return week_collection;
	}

	public void setWeek_collection(BigDecimal week_collection) {
		this.week_collection = week_collection;
	}

	public BigDecimal getMonth_collection() {
		return month_collection;
	}

	public void setMonth_collection(BigDecimal month_collection) {
		this.month_collection = month_collection;
	}

	public BigInteger getActive_device_count() {
		return active_device_count;
	}

	public void setActive_device_count(BigInteger active_device_count) {
		this.active_device_count = active_device_count;
	}
	
	public DashBoardCollectionDetailsTo(){
		
	}
}
