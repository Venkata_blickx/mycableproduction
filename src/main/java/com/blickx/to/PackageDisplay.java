package com.blickx.to;

import java.math.BigDecimal;

public class PackageDisplay {

	private String package_name;
	
	private BigDecimal package_amount;

	public String getPackage_name() {
		return package_name;
	}

	public void setPackage_name(String package_name) {
		this.package_name = package_name;
	}


	public BigDecimal getPackage_amount() {
		return package_amount;
	}

	public void setPackage_amount(BigDecimal package_amount) {
		this.package_amount = package_amount;
	}

	public PackageDisplay(){
		
	}
}
