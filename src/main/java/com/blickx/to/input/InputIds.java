package com.blickx.to.input;

public class InputIds {

	
	@Override
	public String toString() {
		return "InputIds [employee_id=" + employee_id + ", device_id="
				+ device_id + ", channel_id=" + channel_id + ", customer_id="
				+ customer_id + ", package_id=" + package_id + ", box_number="
				+ box_number + ", c_id=" + c_id + ", ime_number=" + ime_number
				+ ", last_sync_timing=" + last_sync_timing + "]";
	}

	public String getBox_number() {
		return box_number;
	}

	public void setBox_number(String box_number) {
		this.box_number = box_number;
	}

	public String getPackage_id() {
		return package_id;
	}

	public void setPackage_id(String package_id) {
		this.package_id = package_id;
	}

	public String getCustomer_id() {
		return customer_id;
	}

	public void setCustomer_id(String customer_id) {
		this.customer_id = customer_id;
	}

	
	public String getDevice_id() {
		return device_id;
	}

	public void setDevice_id(String device_id) {
		this.device_id = device_id;
	}

	public String getChannel_id() {
		return channel_id;
	}

	public void setChannel_id(String channel_id) {
		this.channel_id = channel_id;
	}

	public String getEmployee_id() {
		return employee_id;
	}

	public void setEmployee_id(String employee_id) {
		this.employee_id = employee_id;
	}

	public String getC_id() {
		return c_id;
	}

	public void setC_id(String c_id) {
		this.c_id = c_id;
	}

	String employee_id;
	
	String device_id;
	
	String channel_id;
	
	String customer_id;
	
	String package_id;
	
	String box_number;
	
	String c_id;
	
	String ime_number;
	
	String last_sync_timing;
	
	
	public String getLast_sync_timing() {
		return last_sync_timing;
	}

	public void setLast_sync_timing(String last_sync_timing) {
		this.last_sync_timing = last_sync_timing;
	}

	public String getIme_number() {
		return ime_number;
	}

	public void setIme_number(String ime_number) {
		this.ime_number = ime_number;
	}

	public InputIds(){
		
	}
}
