package com.blickx.to.input;

public class UidInsertInput {

	private String customer_id;
	private String customer_uid;
	
	public String getCustomer_id() {
		return customer_id;
	}
	public void setCustomer_id(String customer_id) {
		this.customer_id = customer_id;
	}
	
	public String getCustomer_uid() {
		return customer_uid;
	}
	public void setCustomer_uid(String customer_uid) {
		this.customer_uid = customer_uid;
	}
	public UidInsertInput(){
		
	}
}
