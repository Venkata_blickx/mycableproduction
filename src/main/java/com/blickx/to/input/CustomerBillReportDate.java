package com.blickx.to.input;

public class CustomerBillReportDate {

	@Override
	public String toString() {
		return "CustomerBillReportDate [customer_id=" + customer_id
				+ ", customer_name=" + customer_name + ", dates=" + dates + "]";
	}

	public String getCustomer_id() {
		return customer_id;
	}

	public void setCustomer_id(String customer_id) {
		this.customer_id = customer_id;
	}

	public String getCustomer_name() {
		return customer_name;
	}

	public void setCustomer_name(String customer_name) {
		this.customer_name = customer_name;
	}

	public String getDates() {
		return dates;
	}

	public void setDates(String dates) {
		this.dates = dates;
	}

	private String customer_id;
	
	private String customer_name;
	
	private String dates;
	
	public CustomerBillReportDate(){
		
	}
}
