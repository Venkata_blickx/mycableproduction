package com.blickx.to.input;

public class EmployeeCredentialsInput {

	private String ime_number;
	
	private String employee_uid;
	
	private String password;

	public String getPassword() {
		return password;
	}

	public void setPassword(String password) {
		this.password = password;
	}

	public String getIme_number() {
		return ime_number;
	}

	public void setIme_number(String ime_number) {
		this.ime_number = ime_number;
	}

	public String getEmployee_uid() {
		return employee_uid;
	}

	public void setEmployee_uid(String employee_uid) {
		this.employee_uid = employee_uid;
	}
	
	public EmployeeCredentialsInput(){
		
	}
}
