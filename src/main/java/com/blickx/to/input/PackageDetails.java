package com.blickx.to.input;

public class PackageDetails {

	private String package_name;
	
	private double package_amount;

	public String getPackage_name() {
		return package_name;
	}

	public void setPackage_name(String package_name) {
		this.package_name = package_name;
	}

	public double getPackage_amount() {
		return package_amount;
	}

	public void setPackage_amount(double package_amount) {
		this.package_amount = package_amount;
	}
	
	public PackageDetails(){
		
	}
	
}
