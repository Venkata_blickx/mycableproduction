package com.blickx.to.input;

import java.math.BigDecimal;

public class BalanceUpdate {

	public String customer_id;
	
	public BigDecimal balance;
	
	public BigDecimal updatedBalance;
	
	public String reason;

	public String getReason() {
		return reason;
	}

	public void setReason(String reason) {
		this.reason = reason;
	}

	public String getCustomer_id() {
		return customer_id;
	}

	public void setCustomer_id(String customer_id) {
		this.customer_id = customer_id;
	}

	public BigDecimal getBalance() {
		return balance;
	}

	public void setBalance(BigDecimal balance) {
		this.balance = balance;
	}

	public BigDecimal getUpdatedBalance() {
		return updatedBalance;
	}

	public void setUpdatedBalance(BigDecimal updatedBalance) {
		this.updatedBalance = updatedBalance;
	}

	@Override
	public String toString() {
		return "BalanceUpdate [customer_id=" + customer_id + ", balance="
				+ balance + ", updatedBalance=" + updatedBalance + ", reason="
				+ reason + "]";
	}
	
	public BalanceUpdate(){
		
	}
}
