package com.blickx.to.input;

import java.math.BigDecimal;
import java.util.Date;
import java.util.Set;

import javax.persistence.Column;

import org.springframework.format.annotation.DateTimeFormat;

import com.blickx.to.SetupBoxTo;

public class GetCustomerUpdate {

	@Override
	public String toString() {
		return "GetCustomerUpdate [client_id=" + client_id + ", customer_id="
				+ customer_id + ", customer_name=" + customer_name
				+ ", contact_number=" + contact_number + ", areacode="
				+ areacode + ", line1=" + line1 + ", line2=" + line2
				+ ", city=" + city + ", state=" + state + ", pincode="
				+ pincode + ", email=" + email + ", landline_number="
				+ landline_number + ", photo_name=" + photo_name
				+ ", photo_type=" + photo_type + ", doc_name=" + doc_name
				+ ", doc_type=" + doc_type + ", card_number=" + card_number
				+ ", c_id=" + c_id + ", total_rent=" + total_rent
				+ ", permenent_status=" + permenent_status + ", billing_type="
				+ billing_type + ", end_date=" + end_date + ", joining_date="
				+ joining_date + ", customer_photo=" + customer_photo
				+ ", customer_doc=" + customer_doc + ", gender=" + gender
				+ ", status=" + status + ", category=" + category
				+ ", setupBox=" + setupBox + ", isaddon_flag=" + isaddon_flag
				+ ", ispackage_flag=" + ispackage_flag + ", totalRent="
				+ totalRent + ", package_amount=" + package_amount
				+ ", addon_amount=" + addon_amount + ", deposit=" + deposit
				+ "]";
	}

	public String getClient_id() {
		return client_id;
	}

	public void setClient_id(String client_id) {
		this.client_id = client_id;
	}

	private String client_id;

	private String customer_id;

	private String customer_name;

	private String contact_number;

	private String areacode;
	
	private String line1;
	
	private String line2;
	
	private String city;
	
	private String state;
	
	private String pincode;
	
	private String email;
	
	private String landline_number;

	private String photo_name;
	
	private String photo_type;
	
	private String doc_name;
	
	private String doc_type;

	private String card_number;
	
	private String c_id;
	
	private BigDecimal total_rent;
	
	private boolean permenent_status;
	
	private boolean billing_type;
	
	@Column
	@DateTimeFormat(pattern = "yyyy-MM-dd")
	private Date end_date;
	
	@Column(name = "joining_date")
	@DateTimeFormat(pattern = "yyyy-MM-dd")
	private Date joining_date;

	private String customer_photo;

	private String customer_doc;

	private String gender;

	private boolean status;
	
	private String category;

	private Set<SetupBoxTo> setupBox;
	
	private boolean isaddon_flag;
	
	private boolean ispackage_flag;
	
	private double totalRent;
	
	private BigDecimal package_amount;
	
	private BigDecimal addon_amount;
	
	private double deposit;
	
	public double getDeposit() {
		return deposit;
	}

	public void setDeposit(double deposit) {
		this.deposit = deposit;
	}

	public boolean isBilling_type() {
		return billing_type;
	}

	public void setBilling_type(boolean billing_type) {
		this.billing_type = billing_type;
	}

	public BigDecimal getAddon_amount() {
		return addon_amount;
	}

	public void setAddon_amount(BigDecimal addon_amount) {
		this.addon_amount = addon_amount;
	}

	public BigDecimal getPackage_amount() {
		return package_amount;
	}

	public boolean isPermenent_status() {
		return permenent_status;
	}

	public void setPermenent_status(boolean permenent_status) {
		this.permenent_status = permenent_status;
	}

	public void setPackage_amount(BigDecimal package_amount) {
		this.package_amount = package_amount;
	}

	public double getTotalRent() {
		return totalRent;
	}

	public void setTotalRent(double totalRent) {
		this.totalRent = totalRent;
	}

	public String getPhoto_name() {
		return photo_name;
	}

	public String getC_id() {
		return c_id;
	}

	public void setC_id(String c_id) {
		this.c_id = c_id;
	}

	public BigDecimal getTotal_rent() {
		return total_rent;
	}

	public void setTotal_rent(BigDecimal total_rent) {
		this.total_rent = total_rent;
	}

	public void setPhoto_name(String photo_name) {
		this.photo_name = photo_name;
	}

	public String getPhoto_type() {
		return photo_type;
	}

	public void setPhoto_type(String photo_type) {
		this.photo_type = photo_type;
	}

	public String getDoc_name() {
		return doc_name;
	}

	public void setDoc_name(String doc_name) {
		this.doc_name = doc_name;
	}

	public String getDoc_type() {
		return doc_type;
	}

	public void setDoc_type(String doc_type) {
		this.doc_type = doc_type;
	}

	public boolean isIsaddon_flag() {
		return isaddon_flag;
	}

	public String getCard_number() {
		return card_number;
	}

	public void setCard_number(String card_number) {
		this.card_number = card_number;
	}

	public void setIsaddon_flag(boolean isaddon_flag) {
		this.isaddon_flag = isaddon_flag;
	}

	public boolean isIspackage_flag() {
		return ispackage_flag;
	}

	public void setIspackage_flag(boolean ispackage_flag) {
		this.ispackage_flag = ispackage_flag;
	}

	public String getState() {
		return state;
	}

	public void setState(String state) {
		this.state = state;
	}

	public String getLine1() {
		return line1;
	}

	public void setLine1(String line1) {
		this.line1 = line1;
	}

	public String getLine2() {
		return line2;
	}

	public void setLine2(String line2) {
		this.line2 = line2;
	}

	public String getCity() {
		return city;
	}

	public void setCity(String city) {
		this.city = city;
	}

	public String getPincode() {
		return pincode;
	}

	public void setPincode(String pincode) {
		this.pincode = pincode;
	}

	public String getCustomer_id() {
		return customer_id;
	}

	public void setCustomer_id(String customer_id) {
		this.customer_id = customer_id;
	}

	public String getCustomer_name() {
		return customer_name;
	}

	public void setCustomer_name(String customer_name) {
		this.customer_name = customer_name;
	}

	public String getContact_number() {
		return contact_number;
	}

	public void setContact_number(String contact_number) {
		this.contact_number = contact_number;
	}

	public String getAreacode() {
		return areacode;
	}

	public void setAreacode(String areacode) {
		this.areacode = areacode;
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public String getLandline_number() {
		return landline_number;
	}

	public void setLandline_number(String landline_number) {
		this.landline_number = landline_number;
	}


	public Date getEnd_date() {
		return end_date;
	}

	public void setEnd_date(Date end_date) {
		this.end_date = end_date;
	}

	public Date getJoining_date() {
		return joining_date;
	}

	public void setJoining_date(Date joining_date) {
		this.joining_date = joining_date;
	}


	public String getCustomer_photo() {
		return customer_photo;
	}

	public void setCustomer_photo(String customer_photo) {
		this.customer_photo = customer_photo;
	}

	public String getCustomer_doc() {
		return customer_doc;
	}

	public void setCustomer_doc(String customer_doc) {
		this.customer_doc = customer_doc;
	}

	public String getGender() {
		return gender;
	}

	public void setGender(String gender) {
		this.gender = gender;
	}

	public boolean isStatus() {
		return status;
	}

	public void setStatus(boolean status) {
		this.status = status;
	}

	public String getCategory() {
		return category;
	}

	public void setCategory(String category) {
		this.category = category;
	}

	public Set<SetupBoxTo> getSetupBox() {
		return setupBox;
	}

	public void setSetupBox(Set<SetupBoxTo> setupBox) {
		this.setupBox = setupBox;
	}

	public GetCustomerUpdate(){
		
	}
}
