package com.blickx.to.input;

import java.util.Date;

public class ContactUpdateTo {

	@Override
	public String toString() {
		return "ContactUpdateTo [oldMobileNumber=" + oldMobileNumber
				+ ", newMobileNumber=" + newMobileNumber + ", customerId="
				+ customerId + ", updateTimeStamp=" + updateTimeStamp + "]";
	}

	public String getOldMobileNumber() {
		return oldMobileNumber;
	}

	public void setOldMobileNumber(String oldMobileNumber) {
		this.oldMobileNumber = oldMobileNumber;
	}

	public String getNewMobileNumber() {
		return newMobileNumber;
	}

	public void setNewMobileNumber(String newMobileNumber) {
		this.newMobileNumber = newMobileNumber;
	}

	public String getCustomerId() {
		return customerId;
	}

	public void setCustomerId(String customerId) {
		this.customerId = customerId;
	}

	public String getUpdateTimeStamp() {
		return updateTimeStamp;
	}

	public void setUpdateTimeStamp(String updateTimeStamp) {
		this.updateTimeStamp = updateTimeStamp;
	}

	private String oldMobileNumber;
	
	private String newMobileNumber;
	
	private String customerId;
	
	private String updateTimeStamp;
	
	
}
