package com.blickx.to.input;

/*
 * LoginCredentials Input for Android
 */
public class LoginCredentialsInput {

	private String username;
	
	private String password;
	
	private String ime_number;
	
	private String client_id;
	

	public String getClient_id() {
		return client_id;
	}

	public void setClient_id(String client_id) {
		this.client_id = client_id;
	}

	public String getUsername() {
		return username;
	}

	public void setUsername(String username) {
		this.username = username;
	}

	public String getPassword() {
		return password;
	}

	public void setPassword(String password) {
		this.password = password;
	}

	public String getIme_number() {
		return ime_number;
	}

	public void setIme_number(String ime_number) {
		this.ime_number = ime_number;
	}
	
	public LoginCredentialsInput(){
		
	}
}
