package com.blickx.to.input;

import java.util.Date;

import org.springframework.format.annotation.DateTimeFormat;

public class MakePaymentDetailsNonSynch {

	@Override
	public String toString() {
		return "MakePaymentDetailsNonSynch [customer_id=" + customer_id
				+ ", employee_id=" + employee_id + ", employee_uid="
				+ employee_uid + ", paid_amount=" + paid_amount
				+ ", balance_amount=" + balance_amount + ", total_amount="
				+ total_amount + ", ime_number=" + ime_number + ", lattitude="
				+ lattitude + ", longitude=" + longitude + "]";
	}

	public String getLattitude() {
		return lattitude;
	}

	public void setLattitude(String lattitude) {
		this.lattitude = lattitude;
	}

	public String getLongitude() {
		return longitude;
	}

	public void setLongitude(String longitude) {
		this.longitude = longitude;
	}

	private String customer_id;
	
	private String employee_id;
	
	private String employee_uid;
	
	double paid_amount;
	
	double balance_amount;
	
	double total_amount;
	
	private String ime_number;
	
	private String lattitude;
	
	private String longitude;
	
	private String paymentTimeStamp;

	public String getPaymentTimeStamp() {
		return paymentTimeStamp;
	}

	public void setPaymentTimeStamp(String paymentTimeStamp) {
		this.paymentTimeStamp = paymentTimeStamp;
	}

	public String getCustomer_id() {
		return customer_id;
	}

	public void setCustomer_id(String customer_id) {
		this.customer_id = customer_id;
	}

	public String getEmployee_id() {
		return employee_id;
	}

	public void setEmployee_id(String employee_id) {
		this.employee_id = employee_id;
	}

	public String getEmployee_uid() {
		return employee_uid;
	}

	public void setEmployee_uid(String employee_uid) {
		this.employee_uid = employee_uid;
	}

	public double getPaid_amount() {
		return paid_amount;
	}

	public void setPaid_amount(double paid_amount) {
		this.paid_amount = paid_amount;
	}

	public double getBalance_amount() {
		return balance_amount;
	}

	public void setBalance_amount(double balance_amount) {
		this.balance_amount = balance_amount;
	}

	public double getTotal_amount() {
		return total_amount;
	}

	public void setTotal_amount(double total_amount) {
		this.total_amount = total_amount;
	}

	public String getIme_number() {
		return ime_number;
	}

	public void setIme_number(String ime_number) {
		this.ime_number = ime_number;
	}
	
	public MakePaymentDetailsNonSynch(){
		
	}

}
