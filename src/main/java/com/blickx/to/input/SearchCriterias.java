package com.blickx.to.input;

public class SearchCriterias {

	private String search_criteria;
	
	public String getSearch_criteria() {
		return search_criteria;
	}

	public void setSearch_criteria(String search_criteria) {
		this.search_criteria = search_criteria;
	}

	public SearchCriterias(String search_criteria) {
		super();
		this.search_criteria = search_criteria;
	}

	public SearchCriterias(){
		
	}
}
