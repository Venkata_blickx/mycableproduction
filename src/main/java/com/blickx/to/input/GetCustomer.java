package com.blickx.to.input;

import java.util.Date;
import java.util.List;
import java.util.Set;

import org.springframework.format.annotation.DateTimeFormat;

import com.blickx.to.SetupBoxTo;

public class GetCustomer {
	
	@Override
	public String toString() {
		return "GetCustomer [addon_amount=" + addon_amount + ", areacode="
				+ areacode + ", c_id=" + c_id + ", category=" + category
				+ ", city=" + city + ", contact_number=" + contact_number
				+ ", customer_doc=" + customer_doc + ", customer_id="
				+ customer_id + ", customer_name=" + customer_name
				+ ", customer_photo=" + customer_photo + ", doc_name="
				+ doc_name + ", doc_type=" + doc_type + ", email=" + email
				+ ", end_date=" + end_date + ", gender=" + gender
				+ ", joining_date=" + joining_date + ", landline_number="
				+ landline_number + ", line1=" + line1 + ", line2=" + line2
				+ ", package_amount=" + package_amount + ", photo_name="
				+ photo_name + ", photo_type=" + photo_type + ", pincode="
				+ pincode + ", state=" + state + ", total_rent=" + total_rent
				+ ", status=" + status + ", customer_uid=" + customer_uid
				+ ", permenent_status=" + permenent_status + ", deposit="
				+ deposit + ", client_id=" + client_id + ", signature_image="
				+ signature_image + ", billing_type=" + billing_type
				+ ", setupBox=" + setupBox + "]";
	}

	private String addon_amount;
	
	private String areacode;
	
	private String c_id;
	
	private String category;
	
	private String city;
	
	private String contact_number;
	
	private String customer_doc;
	
	private String customer_id;
	
	private String customer_name;
	
	private String customer_photo;
	
	private String doc_name;
	
	private String doc_type;
	
	private String email;
	
	@DateTimeFormat(pattern = "yyyy-MM-dd")
	private Date end_date;
	
	private String gender;
	
	@DateTimeFormat(pattern = "yyyy-MM-dd")
	private Date joining_date; 
	
	private String landline_number;
	
	private String line1;
	
	private String line2;
	
	private double package_amount;
	
	private String photo_name;
	
	private String photo_type;
	
	private String pincode;
	
	private String state;
	
	private double total_rent;
	
	private boolean status;
	
	private String customer_uid;
	
	private boolean permenent_status;
	
	private double deposit;
	
	private String client_id;
	
	private String signature_image;
	
	private boolean billing_type;
	
	private Set<SetupBoxTo> setupBox;
	
	
	private String externalId;
	
	private String domain;
	

	
	public String getExternalId() {
		return externalId;
	}

	public void setExternalId(String externalId) {
		this.externalId = externalId;
	}

	public String getDomain() {
		return domain;
	}

	public void setDomain(String domain) {
		this.domain = domain;
	}

	public String getCustomer_uid() {
		return customer_uid;
	}

	public void setCustomer_uid(String customer_uid) {
		this.customer_uid = customer_uid;
	}

	public boolean isBilling_type() {
		return billing_type;
	}

	public void setBilling_type(boolean billing_type) {
		this.billing_type = billing_type;
	}

	public String getSignature_image() {
		return signature_image;
	}

	public void setSignature_image(String signature_image) {
		this.signature_image = signature_image;
	}

	public String getClient_id() {
		return client_id;
	}

	public void setClient_id(String client_id) {
		this.client_id = client_id;
	}

	public boolean isPermenent_status() {
		return permenent_status;
	}

	public double getDeposit() {
		return deposit;
	}

	public void setDeposit(double deposit) {
		this.deposit = deposit;
	}

	public void setPermenent_status(boolean permenent_status) {
		this.permenent_status = permenent_status;
	}

	public String getAddon_amount() {
		return addon_amount;
	}

	public void setAddon_amount(String addon_amount) {
		this.addon_amount = addon_amount;
	}

	public String getAreacode() {
		return areacode;
	}

	public void setAreacode(String areacode) {
		this.areacode = areacode;
	}

	public String getC_id() {
		return c_id;
	}

	public void setC_id(String c_id) {
		this.c_id = c_id;
	}

	public String getCategory() {
		return category;
	}

	public void setCategory(String category) {
		this.category = category;
	}

	public String getCity() {
		return city;
	}

	public void setCity(String city) {
		this.city = city;
	}

	public String getContact_number() {
		return contact_number;
	}

	public void setContact_number(String contact_number) {
		this.contact_number = contact_number;
	}

	public String getCustomer_doc() {
		return customer_doc;
	}

	public void setCustomer_doc(String customer_doc) {
		this.customer_doc = customer_doc;
	}

	public String getCustomer_id() {
		return customer_id;
	}

	public void setCustomer_id(String customer_id) {
		this.customer_id = customer_id;
	}

	public String getCustomer_name() {
		return customer_name;
	}

	public void setCustomer_name(String customer_name) {
		this.customer_name = customer_name;
	}

	public String getCustomer_photo() {
		return customer_photo;
	}

	public void setCustomer_photo(String customer_photo) {
		this.customer_photo = customer_photo;
	}

	public String getDoc_name() {
		return doc_name;
	}

	public void setDoc_name(String doc_name) {
		this.doc_name = doc_name;
	}

	public String getDoc_type() {
		return doc_type;
	}

	public void setDoc_type(String doc_type) {
		this.doc_type = doc_type;
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public Date getEnd_date() {
		return end_date;
	}

	public void setEnd_date(Date end_date) {
		this.end_date = end_date;
	}

	public String getGender() {
		return gender;
	}

	public void setGender(String gender) {
		this.gender = gender;
	}

	public Date getJoining_date() {
		return joining_date;
	}

	public void setJoining_date(Date joining_date) {
		this.joining_date = joining_date;
	}

	public String getLandline_number() {
		return landline_number;
	}

	public void setLandline_number(String landline_number) {
		this.landline_number = landline_number;
	}

	public String getLine1() {
		return line1;
	}

	public void setLine1(String line1) {
		this.line1 = line1;
	}

	public String getLine2() {
		return line2;
	}

	public void setLine2(String line2) {
		this.line2 = line2;
	}

	public double getPackage_amount() {
		return package_amount;
	}

	public void setPackage_amount(double package_amount) {
		this.package_amount = package_amount;
	}

	public String getPhoto_name() {
		return photo_name;
	}

	public void setPhoto_name(String photo_name) {
		this.photo_name = photo_name;
	}

	public String getPhoto_type() {
		return photo_type;
	}

	public void setPhoto_type(String photo_type) {
		this.photo_type = photo_type;
	}

	public String getPincode() {
		return pincode;
	}

	public void setPincode(String pincode) {
		this.pincode = pincode;
	}

	public String getState() {
		return state;
	}

	public void setState(String state) {
		this.state = state;
	}

	public double getTotal_rent() {
		return total_rent;
	}

	public void setTotal_rent(double total_rent) {
		this.total_rent = total_rent;
	}

	public boolean isStatus() {
		return status;
	}

	public void setStatus(boolean status) {
		this.status = status;
	}

	public Set<SetupBoxTo> getSetupBox() {
		return setupBox;
	}

	public void setSetupBox(Set<SetupBoxTo> setupBox) {
		this.setupBox = setupBox;
	}

	public GetCustomer(){
		
	}
}
