package com.blickx.to.input;

import java.util.List;
import java.util.Map;

public class SetupboxReportInput {

	private String fetchType;
	private String searchEntry;
	private String client_id;
	private Boolean status;
	private List<Integer> selectedArea;
	private List<Map<String, Integer>> selectedAreas;
	private String paidType;
	
	public Boolean getStatus() {
		return status;
	}

	public void setStatus(Boolean status) {
		this.status = status;
	}

	public List<Integer> getSelectedArea() {
		return selectedArea;
	}

	public void setSelectedArea(List<Integer> selectedArea) {
		this.selectedArea = selectedArea;
	}

	public List<Map<String, Integer>> getSelectedAreas() {
		return selectedAreas;
	}

	public void setSelectedAreas(List<Map<String, Integer>> selectedAreas) {
		this.selectedAreas = selectedAreas;
	}

	public String getPaidType() {
		return paidType;
	}

	public void setPaidType(String paidType) {
		this.paidType = paidType;
	}

	@Override
	public String toString() {
		return "SetupboxReportInput [fetchType=" + fetchType + ", searchEntry="
				+ searchEntry + ", client_id=" + client_id + ", status="
				+ status + ", selectedArea=" + selectedArea
				+ ", selectedAreas=" + selectedAreas + ", paidType=" + paidType
				+ "]";
	}

	public String getFetchType() {
		return fetchType;
	}

	public void setFetchType(String fetchType) {
		this.fetchType = fetchType;
	}

	public String getSearchEntry() {
		return searchEntry;
	}

	public void setSearchEntry(String searchEntry) {
		this.searchEntry = searchEntry;
	}

	public String getClient_id() {
		return client_id;
	}

	public void setClient_id(String client_id) {
		this.client_id = client_id;
	}
}
