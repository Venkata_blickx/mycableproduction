package com.blickx.to.input;

public class CustomerFetchCredetials {

	private String c_id;
	
	private String customer_id;
	
	private String client_id;

	public String getC_id() {
		return c_id;
	}

	public void setC_id(String c_id) {
		this.c_id = c_id;
	}

	public String getCustomer_id() {
		return customer_id;
	}

	public void setCustomer_id(String customer_id) {
		this.customer_id = customer_id;
	}
	
	public String getClient_id() {
		return client_id;
	}

	public void setClient_id(String client_id) {
		this.client_id = client_id;
	}

	public CustomerFetchCredetials(){
		
	}

	@Override
	public String toString() {
		return "CustomerFetchCredetials [c_id=" + c_id + ", customer_id="
				+ customer_id + ", client_id=" + client_id + "]";
	}

}
