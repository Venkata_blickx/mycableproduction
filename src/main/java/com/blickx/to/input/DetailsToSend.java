package com.blickx.to.input;

public class DetailsToSend {

	private String contact_number;
	
	private String tempId;
	
	private String f1;
	private String f2;
	private String f3;
	private String f4;
	private String f5;
	
	
	
	
	public String getTempId() {
		return tempId;
	}

	public void setTempId(String tempId) {
		this.tempId = tempId;
	}

	public String getF1() {
		return f1;
	}

	public void setF1(String f1) {
		this.f1 = f1;
	}

	public String getF2() {
		return f2;
	}

	public void setF2(String f2) {
		this.f2 = f2;
	}

	public String getF3() {
		return f3;
	}

	public void setF3(String f3) {
		this.f3 = f3;
	}

	public String getF4() {
		return f4;
	}

	public void setF4(String f4) {
		this.f4 = f4;
	}

	public String getF5() {
		return f5;
	}

	public void setF5(String f5) {
		this.f5 = f5;
	}

	private String customer_id;
	
	private String client_id;
	
	private String message;

	public String getCustomer_id() {
		return customer_id;
	}

	public void setCustomer_id(String customer_id) {
		this.customer_id = customer_id;
	}

	public String getContact_number() {
		return contact_number;
	}

	public void setContact_number(String contact_number) {
		this.contact_number = contact_number;
	}

	public String getClient_id() {
		return client_id;
	}

	public void setClient_id(String client_id) {
		this.client_id = client_id;
	}

	public String getMessage() {
		return message;
	}

	public void setMessage(String message) {
		this.message = message;
	}
	
	public DetailsToSend(){
		
	}

	@Override
	public String toString() {
		return "DetailsToSend [contact_number=" + contact_number + ", tempId="
				+ tempId + ", f1=" + f1 + ", f2=" + f2 + ", f3=" + f3 + ", f4="
				+ f4 + ", f5=" + f5 + ", customer_id=" + customer_id
				+ ", client_id=" + client_id + ", message=" + message + "]";
	}

	public DetailsToSend(String customer_id, String client_id, String message) {
		super();
		this.customer_id = customer_id;
		this.client_id = client_id;
		this.message = message;
	}

	public DetailsToSend(String tempId, String f1,
			String f2, String f3, String f4, String f5) {
		super();
		this.tempId = tempId;
		this.f1 = f1;
		this.f2 = f2;
		this.f3 = f3;
		this.f4 = f4;
		this.f5 = f5;
	}
	
	
	
	
}
