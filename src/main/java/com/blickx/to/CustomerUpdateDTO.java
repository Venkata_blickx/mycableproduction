package com.blickx.to;

import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;

import org.springframework.format.annotation.DateTimeFormat;

@Entity
public class CustomerUpdateDTO {

	@Id
	@Column
	private String customer_id;

	@Column
	private String customer_name;

	@Column
	private String card_number;

	@Column
	private String box_number;

	@Column
	private String contact_number;

	@Column
	private String address;

	@Column
	private String areacode;

	@Column
	@DateTimeFormat(pattern = "yyyy-MM-dd")
	private Date start_date_box;

	@Column
	@DateTimeFormat(pattern = "yyyy-MM-dd")
	private Date end_date_box;

	@Column
	@DateTimeFormat(pattern = "yyyy-MM-dd")
	private Date start_date_card;

	@Column
	@DateTimeFormat(pattern = "yyyy-MM-dd")
	private Date end_date_card;

	@Column
	@DateTimeFormat(pattern = "yyyy-MM-dd")
	private Date joining_date;

	@Column
	@DateTimeFormat(pattern = "yyyy-MM-dd")
	private Date end_date;

	@Column
	private String regPackage;

	@Column
	private byte[] customer_photo;

	@Column
	private byte[] customer_doc;

	@Column
	private String gender;

	@Column
	private String add_on;

	@Column
	private boolean status;

	@Column
	private int type;
	
	@Column
	private boolean addon_flag;
	
	@Column
	private boolean package_flag;

	public boolean isAddon_flag() {
		return addon_flag;
	}

	public void setAddon_flag(boolean addon_flag) {
		this.addon_flag = addon_flag;
	}

	public boolean isPackage_flag() {
		return package_flag;
	}

	public void setPackage_flag(boolean package_flag) {
		this.package_flag = package_flag;
	}

	public int getType() {
		return type;
	}

	public void setType(int type) {
		this.type = type;
	}

	public String getCustomer_id() {
		return customer_id;
	}

	public void setCustomer_id(String customer_id) {
		this.customer_id = customer_id;
	}

	public String getCustomer_name() {
		return customer_name;
	}

	public void setCustomer_name(String customer_name) {
		this.customer_name = customer_name;
	}

	public String getCard_number() {
		return card_number;
	}

	public void setCard_number(String card_number) {
		this.card_number = card_number;
	}

	public String getBox_number() {
		return box_number;
	}

	public void setBox_number(String box_number) {
		this.box_number = box_number;
	}

	public String getContact_number() {
		return contact_number;
	}

	public void setContact_number(String contact_number) {
		this.contact_number = contact_number;
	}

	public String getAddress() {
		return address;
	}

	public void setAddress(String address) {
		this.address = address;
	}

	public String getAreacode() {
		return areacode;
	}

	public void setAreacode(String areacode) {
		this.areacode = areacode;
	}

	public Date getStart_date_box() {
		return start_date_box;
	}

	public void setStart_date_box(Date start_date_box) {
		this.start_date_box = start_date_box;
	}

	public Date getEnd_date_box() {
		return end_date_box;
	}

	public void setEnd_date_box(Date end_date_box) {
		this.end_date_box = end_date_box;
	}

	public Date getStart_date_card() {
		return start_date_card;
	}

	public void setStart_date_card(Date start_date_card) {
		this.start_date_card = start_date_card;
	}

	public Date getEnd_date_card() {
		return end_date_card;
	}

	public void setEnd_date_card(Date end_date_card) {
		this.end_date_card = end_date_card;
	}

	public Date getJoining_date() {
		return joining_date;
	}

	public void setJoining_date(Date joining_date) {
		this.joining_date = joining_date;
	}

	public Date getEnd_date() {
		return end_date;
	}

	public void setEnd_date(Date end_date) {
		this.end_date = end_date;
	}

	public String getRegPackage() {
		return regPackage;
	}

	public void setRegPackage(String regPackage) {
		this.regPackage = regPackage;
	}

	public byte[] getCustomer_photo() {
		return customer_photo;
	}

	public void setCustomer_photo(byte[] customer_photo) {
		this.customer_photo = customer_photo;
	}

	public byte[] getCustomer_doc() {
		return customer_doc;
	}

	public void setCustomer_doc(byte[] customer_doc) {
		this.customer_doc = customer_doc;
	}

	public String getGender() {
		return gender;
	}

	public void setGender(String gender) {
		this.gender = gender;
	}

	public String getAdd_on() {
		return add_on;
	}

	public void setAdd_on(String add_on) {
		this.add_on = add_on;
	}

	public boolean isStatus() {
		return status;
	}

	public void setStatus(boolean status) {
		this.status = status;
	}

	public CustomerUpdateDTO(){
		
	}
}
