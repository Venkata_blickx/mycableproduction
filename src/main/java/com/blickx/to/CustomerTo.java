package com.blickx.to;

import java.math.BigDecimal;
import java.util.Date;
import java.util.Set;

import com.fasterxml.jackson.databind.annotation.JsonSerialize;

import com.blickx.domain.DateSerializer;


public class CustomerTo {

	public BigDecimal getDeposit() {
		return deposit;
	}

	public void setDeposit(BigDecimal deposit) {
		this.deposit = deposit;
	}

	@Override
	public String toString() {
		return "CustomerTo [customer_id=" + customer_id + ", customer_name="
				+ customer_name + ", contact_number=" + contact_number
				+ ", line1=" + line1 + ", line2=" + line2 + ", city=" + city
				+ ", areacode=" + areacode + ", email=" + email + ", state="
				+ state + ", pincode=" + pincode + ", landline_number="
				+ landline_number + ", joining_date=" + joining_date
				+ ", end_date=" + end_date + ", gender=" + gender + ", status="
				+ status + ", customer_photo=" + customer_photo
				+ ", customer_doc=" + customer_doc + ", photo_name="
				+ photo_name + ", photo_type=" + photo_type + ", doc_name="
				+ doc_name + ", doc_type=" + doc_type + ", category="
				+ category + ", totalRent=" + totalRent + ", c_id=" + c_id
				+ ", deposit=" + deposit + ", card_number=" + card_number
				+ ", customer_uid=" + customer_uid + ", permenent_status="
				+ permenent_status + ", billing_type=" + billing_type
				+ ", client_id=" + client_id + ", signature_image="
				+ signature_image + ", setupBoxes=" + setupBoxes + "]";
	}

	public boolean getPermenent_status() {
		return permenent_status;
	}

	public void setPermenent_status(boolean permenent_status) {
		this.permenent_status = permenent_status;
	}

	public boolean isBilling_type() {
		return billing_type;
	}

	public void setBilling_type(boolean billing_type) {
		this.billing_type = billing_type;
	}

	public String getSignature_image() {
		return signature_image;
	}

	public void setSignature_image(String signature_image) {
		this.signature_image = signature_image;
	}

	public String getCustomer_uid() {
		return customer_uid;
	}

	public void setCustomer_uid(String customer_uid) {
		this.customer_uid = customer_uid;
	}

	public String getCard_number() {
		return card_number;
	}

	public void setCard_number(String card_number) {
		this.card_number = card_number;
	}

	private String customer_id;

	private String customer_name;

	private String contact_number;

	private String line1;

	private String line2;

	private String city;

	private String areacode;
	
	private String email;

	private String state;

	private String pincode;

	private String landline_number;

	private Date joining_date;

	private Date end_date;

	private String gender;

	private boolean status;

	private String customer_photo;
	
	private String customer_doc;
	
	private String photo_name;
	
	private String photo_type;
	
	private String doc_name;
	
	private String doc_type;
	
	private String category;
	
	private double totalRent;
	
	private String c_id;
	
	private BigDecimal deposit;
	
	private String card_number;
	
	private String customer_uid;
	
	private boolean permenent_status;

	private boolean billing_type;
	
	private String client_id;
	
	private String signature_image;
	
	private String vc_number;
	
	public String getVc_number() {
		return vc_number;
	}

	public void setVc_number(String vc_number) {
		this.vc_number = vc_number;
	}

	private Set<SetUpBoxToo> setupBoxes;
	
		public String getCustomer_id() {
		return customer_id;
	}

	public void setCustomer_id(String customer_id) {
		this.customer_id = customer_id;
	}

	public String getClient_id() {
		return client_id;
	}

	public void setClient_id(String client_id) {
		this.client_id = client_id;
	}

	public String getC_id() {
		return c_id;
	}

	public void setC_id(String c_id) {
		this.c_id = c_id;
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public double getTotalRent() {
		return totalRent;
	}

	public void setTotalRent(double totalRent) {
		this.totalRent = totalRent;
	}

	public String getCustomer_name() {
		return customer_name;
	}

	public void setCustomer_name(String customer_name) {
		this.customer_name = customer_name;
	}

	public String getContact_number() {
		return contact_number;
	}

	public void setContact_number(String contact_number) {
		this.contact_number = contact_number;
	}

	public String getLine1() {
		return line1;
	}

	public void setLine1(String line1) {
		this.line1 = line1;
	}

	public String getLine2() {
		return line2;
	}

	public void setLine2(String line2) {
		this.line2 = line2;
	}

	public String getCity() {
		return city;
	}

	public void setCity(String city) {
		this.city = city;
	}

	public String getAreacode() {
		return areacode;
	}

	public void setAreacode(String areacode) {
		this.areacode = areacode;
	}

	public String getState() {
		return state;
	}

	public void setState(String state) {
		this.state = state;
	}

	public String getPincode() {
		return pincode;
	}

	public void setPincode(String pincode) {
		this.pincode = pincode;
	}

	public String getLandline_number() {
		return landline_number;
	}

	public void setLandline_number(String landline_number) {
		this.landline_number = landline_number;
	}

	@JsonSerialize(using = DateSerializer.class)
	public Date getJoining_date() {
		return joining_date;
	}

	public void setJoining_date(Date joining_date) {
		this.joining_date = joining_date;
	}

	@JsonSerialize(using = DateSerializer.class)
	public Date getEnd_date() {
		return end_date;
	}

	public void setEnd_date(Date end_date) {
		this.end_date = end_date;
	}

	public String getGender() {
		return gender;
	}

	public void setGender(String gender) {
		this.gender = gender;
	}

	public boolean isStatus() {
		return status;
	}

	public void setStatus(boolean status) {
		this.status = status;
	}

	public String getCustomer_photo() {
		return customer_photo;
	}

	public void setCustomer_photo(String customer_photo) {
		this.customer_photo = customer_photo;
	}

	public String getCustomer_doc() {
		return customer_doc;
	}

	public void setCustomer_doc(String customer_doc) {
		this.customer_doc = customer_doc;
	}

	public String getPhoto_name() {
		return photo_name;
	}

	public void setPhoto_name(String photo_name) {
		this.photo_name = photo_name;
	}

	public String getPhoto_type() {
		return photo_type;
	}

	public void setPhoto_type(String photo_type) {
		this.photo_type = photo_type;
	}

	public String getDoc_name() {
		return doc_name;
	}

	public void setDoc_name(String doc_name) {
		this.doc_name = doc_name;
	}

	public String getDoc_type() {
		return doc_type;
	}

	public void setDoc_type(String doc_type) {
		this.doc_type = doc_type;
	}

	public String getCategory() {
		return category;
	}

	public void setCategory(String category) {
		this.category = category;
	}

	public Set<SetUpBoxToo> getSetupBoxes() {
		return setupBoxes;
	}

	public void setSetupBoxes(Set<SetUpBoxToo> setupBoxes) {
		this.setupBoxes = setupBoxes;
	}

	public CustomerTo(){
		
	}
	
}
