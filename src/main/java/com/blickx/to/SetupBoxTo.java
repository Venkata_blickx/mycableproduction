package com.blickx.to;

import java.util.Date;

import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import org.springframework.format.annotation.DateTimeFormat;

import com.blickx.domain.DateSerializer;

public class SetupBoxTo {
	
	@Override
	public String toString() {
		return "SetupBoxTo [row_id=" + row_id + ", customer_id=" + customer_id
				+ ", box_number=" + box_number + ", vc_number=" + vc_number
				+ ", nds_number=" + nds_number + ", addonNames=" + addonNames
				+ ", addon_ids=" + addon_ids + ", assigned_package="
				+ assigned_package + ", discount=" + discount + ", status="
				+ status + ", package_id=" + package_id + ", start_date="
				+ start_date + ", isaddon_flag=" + isaddon_flag
				+ ", ispackage_flag=" + ispackage_flag + ", package_amount="
				+ package_amount + ", addon_amount=" + addon_amount
				+ ", total_rent=" + total_rent + ", total_amount="
				+ total_amount + ", client_id=" + client_id + "]";
	}

	private int row_id;

	private String customer_id;
	
	private String box_number;
	
	private String vc_number;
	
	private String nds_number;
	
	private String addonNames;
	
	private String addon_ids;
	
	private String assigned_package;
	
	private double discount;
	
	private boolean status;
	
	private String package_id;
	
	@DateTimeFormat(pattern = "yyyy-MM-dd")
	private Date start_date;
	
	private boolean isaddon_flag;
	
	private boolean ispackage_flag;
	
	private double package_amount;
	
	private double addon_amount;
	
	private double total_rent;
	
	private double total_amount;
	
	private String client_id;
	
	//private Set<AddOnsEntity> addOns;

	public double getTotal_amount() {
		return total_amount;
	}

	public void setTotal_amount(double total_amount) {
		this.total_amount = total_amount;
	}

	public String getClient_id() {
		return client_id;
	}

	public void setClient_id(String client_id) {
		this.client_id = client_id;
	}

	public double getPackage_amount() {
		return package_amount;
	}

	public void setPackage_amount(double package_amount) {
		this.package_amount = package_amount;
	}

	public double getAddon_amount() {
		return addon_amount;
	}

	public void setAddon_amount(double addon_amount) {
		this.addon_amount = addon_amount;
	}


	public double getTotal_rent() {
		return total_rent;
	}

	public void setTotal_rent(double total_rent) {
		this.total_rent = total_rent;
	}

	public int getRow_id() {
		return row_id;
	}

	public void setRow_id(int row_id) {
		this.row_id = row_id;
	}


	public boolean isStatus() {
		return status;
	}

	public void setStatus(boolean status) {
		this.status = status;
	}

	public String getAddon_ids() {
		return addon_ids;
	}

	public void setAddon_ids(String addon_ids) {
		this.addon_ids = addon_ids;
	}

	public boolean isIsaddon_flag() {
		return isaddon_flag;
	}

	public void setIsaddon_flag(boolean isaddon_flag) {
		this.isaddon_flag = isaddon_flag;
	}

	public boolean isIspackage_flag() {
		return ispackage_flag;
	}

	public void setIspackage_flag(boolean ispackage_flag) {
		this.ispackage_flag = ispackage_flag;
	}

	public String getCustomer_id() {
		return customer_id;
	}

	public void setCustomer_id(String customer_id) {
		this.customer_id = customer_id;
	}

	public String getBox_number() {
		return box_number;
	}

	public void setBox_number(String box_number) {
		this.box_number = box_number;
	}

	public String getVc_number() {
		return vc_number;
	}

	public void setVc_number(String vc_number) {
		this.vc_number = vc_number;
	}

	public String getNds_number() {
		return nds_number;
	}

	public void setNds_number(String nds_number) {
		this.nds_number = nds_number;
	}

	public String getAddonNames() {
		return addonNames;
	}

	public void setAddonNames(String addonNames) {
		this.addonNames = addonNames;
	}

	public String getAssigned_package() {
		return assigned_package;
	}

	public void setAssigned_package(String assigned_package) {
		this.assigned_package = assigned_package;
	}

	public double getDiscount() {
		return discount;
	}

	public void setDiscount(double discount) {
		this.discount = discount;
	}


	@JsonSerialize(using = DateSerializer.class)
	public Date getStart_date() {
		return start_date;
	}

	public void setStart_date(Date start_date) {
		this.start_date = start_date;
	}

	public String getPackage_id() {
		return package_id;
	}

	public void setPackage_id(String package_id) {
		this.package_id = package_id;
	}

/*	public Set<AddOnsEntity> getAddOns() {
		return addOns;
	}

	public void setAddOns(Set<AddOnsEntity> addOns) {
		this.addOns = addOns;
	}
*/
	public SetupBoxTo(){
		
	}
}
