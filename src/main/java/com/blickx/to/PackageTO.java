package com.blickx.to;

import java.util.Date;

import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import org.springframework.format.annotation.DateTimeFormat;

import com.blickx.domain.DateSerializer;

public class PackageTO {

	private int package_id;
	
	private String package_name;
	
	private double package_amount;
	
	@DateTimeFormat(pattern = "yyyy-MM-dd")
	private Date effective_date;
	
	private boolean status;
	
	private String client_id;
	
	
	//private String channelList;
	
	/*@SuppressWarnings("rawtypes")
	private List channels;

	@SuppressWarnings("rawtypes")
	public List getChannels() {
		return channels;
	}

	@SuppressWarnings("rawtypes")
	public void setChannels(List channels) {
		this.channels = channels;
	}*/

	public int getPackage_id() {
		return package_id;
	}

	public String getClient_id() {
		return client_id;
	}

	public void setClient_id(String client_id) {
		this.client_id = client_id;
	}

	@JsonSerialize(using = DateSerializer.class)
	public Date getEffective_date() {
		return effective_date;
	}

	public void setEffective_date(Date effective_date) {
		this.effective_date = effective_date;
	}

	public void setPackage_id(int package_id) {
		this.package_id = package_id;
	}

	public String getPackage_name() {
		return package_name;
	}

	public void setPackage_name(String package_name) {
		this.package_name = package_name;
	}

	public double getPackage_amount() {
		return package_amount;
	}

	public void setPackage_amount(double package_amount) {
		this.package_amount = package_amount;
	}


	public boolean isStatus() {
		return status;
	}

	public void setStatus(boolean status) {
		this.status = status;
	}

/*	public String getChannelList() {
		return channelList;
	}

	public void setChannelList(String channelList) {
		this.channelList = channelList;
	}
*/	

	public PackageTO(){}
}
