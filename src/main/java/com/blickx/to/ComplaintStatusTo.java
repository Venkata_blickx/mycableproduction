package com.blickx.to;

import java.math.BigInteger;

public class ComplaintStatusTo {

	private BigInteger open;
	
	private BigInteger assign;
	
	private BigInteger resolve;
	
	public BigInteger getOpen() {
		return open;
	}

	public void setOpen(BigInteger open) {
		this.open = open;
	}


	public BigInteger getAssign() {
		return assign;
	}

	public void setAssign(BigInteger assign) {
		this.assign = assign;
	}

	public BigInteger getResolve() {
		return resolve;
	}

	public void setResolve(BigInteger resolve) {
		this.resolve = resolve;
	}

	public ComplaintStatusTo(){
		
	}
}
