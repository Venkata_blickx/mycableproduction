package com.blickx.to;

import java.math.BigDecimal;

public class ViewBillsByMonthTo {

	private String customer_id;
	private String customer_name;
	private String contact_number;
	private String line1;
	private String line2;
	private String city;
	
	private BigDecimal balance_amount;
	private BigDecimal package_amount;
	private BigDecimal addon_amount;
	private BigDecimal monthly_amount;
	private BigDecimal total_amount;
	private String date;
	
	public String getDate() {
		return date;
	}
	public void setDate(String date) {
		this.date = date;
	}
	public String getCustomer_id() {
		return customer_id;
	}
	public void setCustomer_id(String customer_id) {
		this.customer_id = customer_id;
	}
	public String getCustomer_name() {
		return customer_name;
	}
	public void setCustomer_name(String customer_name) {
		this.customer_name = customer_name;
	}
	public String getContact_number() {
		return contact_number;
	}
	public void setContact_number(String contact_number) {
		this.contact_number = contact_number;
	}
	public String getLine1() {
		return line1;
	}
	public void setLine1(String line1) {
		this.line1 = line1;
	}
	public String getLine2() {
		return line2;
	}
	public void setLine2(String line2) {
		this.line2 = line2;
	}
	public String getCity() {
		return city;
	}
	public void setCity(String city) {
		this.city = city;
	}
	public BigDecimal getBalance_amount() {
		return balance_amount;
	}
	public void setBalance_amount(BigDecimal balance_amount) {
		this.balance_amount = balance_amount;
	}
	public BigDecimal getPackage_amount() {
		return package_amount;
	}
	public void setPackage_amount(BigDecimal package_amount) {
		this.package_amount = package_amount;
	}
	public BigDecimal getAddon_amount() {
		return addon_amount;
	}
	public void setAddon_amount(BigDecimal addon_amount) {
		this.addon_amount = addon_amount;
	}
	public BigDecimal getMonthly_amount() {
		return monthly_amount;
	}
	public void setMonthly_amount(BigDecimal monthly_amount) {
		this.monthly_amount = monthly_amount;
	}
	public BigDecimal getTotal_amount() {
		return total_amount;
	}
	public void setTotal_amount(BigDecimal total_amount) {
		this.total_amount = total_amount;
	}
	
	public ViewBillsByMonthTo(){
		
	}
}
