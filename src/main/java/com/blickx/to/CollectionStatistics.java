package com.blickx.to;

import java.math.BigDecimal;

public class CollectionStatistics {

	private BigDecimal totalCollected;
	
	private BigDecimal balance;
	
	private BigDecimal billGenerated;
	
	private BigDecimal totalToBeCollected;
	
	private BigDecimal excess;
	
	private String month;

	public BigDecimal getTotalCollected() {
		return totalCollected;
	}

	public void setTotalCollected(BigDecimal totalCollected) {
		this.totalCollected = totalCollected;
	}

	public BigDecimal getBalance() {
		return balance;
	}

	public void setBalance(BigDecimal balance) {
		this.balance = balance;
	}

	public BigDecimal getBillGenerated() {
		return billGenerated;
	}

	public void setBillGenerated(BigDecimal billGenerated) {
		this.billGenerated = billGenerated;
	}

	public BigDecimal getTotalToBeCollected() {
		return totalToBeCollected;
	}

	public void setTotalToBeCollected(BigDecimal totalToBeCollected) {
		this.totalToBeCollected = totalToBeCollected;
	}

	public BigDecimal getExcess() {
		return excess;
	}

	public void setExcess(BigDecimal excess) {
		this.excess = excess;
	}

	public String getMonth() {
		return month;
	}

	public void setMonth(String month) {
		this.month = month;
	}

	@Override
	public String toString() {
		return "CollectionStatistics [totalCollected=" + totalCollected
				+ ", balance=" + balance + ", billGenerated=" + billGenerated
				+ ", totalToBeCollected=" + totalToBeCollected + ", excess="
				+ excess + ", month=" + month + "]";
	}
	
	public CollectionStatistics(){
		
	}
	
}
