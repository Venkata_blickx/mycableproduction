package com.blickx.to;

import java.util.Date;


import com.fasterxml.jackson.databind.annotation.JsonSerialize;

import com.blickx.domain.DateSerializer;

public class DeviceTo {

	private int device_id;
	
	private String device_name;
	
	private Date date_of_purchase;
	
	private String ime_number;
	
	private String service_provider;
	
	private String data_plan;
	
	private boolean status;
	
	private String sim_number;
	
	private String employee_id;
	
	private String employee_name;

	public int getDevice_id() {
		return device_id;
	}

	public void setDevice_id(int device_id) {
		this.device_id = device_id;
	}

	public String getDevice_name() {
		return device_name;
	}

	public void setDevice_name(String device_name) {
		this.device_name = device_name;
	}

	@JsonSerialize(using = DateSerializer.class)
	public Date getDate_of_purchase() {
		return date_of_purchase;
	}

	public void setDate_of_purchase(Date date_of_purchase) {
		this.date_of_purchase = date_of_purchase;
	}

	public String getIme_number() {
		return ime_number;
	}

	public void setIme_number(String ime_number) {
		this.ime_number = ime_number;
	}

	public String getService_provider() {
		return service_provider;
	}

	public void setService_provider(String service_provider) {
		this.service_provider = service_provider;
	}

	public String getData_plan() {
		return data_plan;
	}

	public void setData_plan(String data_plan) {
		this.data_plan = data_plan;
	}

	public boolean isStatus() {
		return status;
	}

	public void setStatus(boolean status) {
		this.status = status;
	}

	public String getSim_number() {
		return sim_number;
	}

	public void setSim_number(String sim_number) {
		this.sim_number = sim_number;
	}

	public String getEmployee_id() {
		return employee_id;
	}

	public void setEmployee_id(String employee_id) {
		this.employee_id = employee_id;
	}

	public String getEmployee_name() {
		return employee_name;
	}

	public void setEmployee_name(String employee_name) {
		this.employee_name = employee_name;
	}
	
	public DeviceTo(){
		
	}
}
