package com.blickx.to;

import java.math.BigDecimal;

public class PDAReportDto {

	
	private String c_id;

	private String customer_id;

	private String customer_name;

	private String contact_number;

	private String 	line1;

	private String 	line2;

	private String box_number;

	private String vc_number;

	private String assigned_package;

	private BigDecimal discount;

	private BigDecimal package_amount;

	private String paid_status;
	
	private BigDecimal balance;

	public BigDecimal getBalance() {
		return balance;
	}

	public void setBalance(BigDecimal balance) {
		this.balance = balance;
	}

	public String getC_id() {
		return c_id;
	}

	public void setC_id(String c_id) {
		this.c_id = c_id;
	}

	public String getCustomer_id() {
		return customer_id;
	}

	public void setCustomer_id(String customer_id) {
		this.customer_id = customer_id;
	}

	public String getCustomer_name() {
		return customer_name;
	}

	public void setCustomer_name(String customer_name) {
		this.customer_name = customer_name;
	}

	public String getContact_number() {
		return contact_number;
	}

	public void setContact_number(String contact_number) {
		this.contact_number = contact_number;
	}

	public String getLine1() {
		return line1;
	}

	public void setLine1(String line1) {
		this.line1 = line1;
	}

	public String getLine2() {
		return line2;
	}

	public void setLine2(String line2) {
		this.line2 = line2;
	}

	public String getBox_number() {
		return box_number;
	}

	public void setBox_number(String box_number) {
		this.box_number = box_number;
	}

	public String getVc_number() {
		return vc_number;
	}

	public void setVc_number(String vc_number) {
		this.vc_number = vc_number;
	}

	public String getAssigned_package() {
		return assigned_package;
	}

	public void setAssigned_package(String assigned_package) {
		this.assigned_package = assigned_package;
	}

	public BigDecimal getDiscount() {
		return discount;
	}

	public void setDiscount(BigDecimal discount) {
		this.discount = discount;
	}

	public BigDecimal getPackage_amount() {
		return package_amount;
	}

	public void setPackage_amount(BigDecimal package_amount) {
		this.package_amount = package_amount;
	}

	public String getPaid_status() {
		return paid_status;
	}

	public void setPaid_status(String paid_status) {
		this.paid_status = paid_status;
	}

	@Override
	public String toString() {
		return "PDAReportDto [c_id=" + c_id + ", customer_id=" + customer_id
				+ ", customer_name=" + customer_name + ", contact_number="
				+ contact_number + ", line1=" + line1 + ", line2=" + line2
				+ ", box_number=" + box_number + ", vc_number=" + vc_number
				+ ", assigned_package=" + assigned_package + ", discount="
				+ discount + ", package_amount=" + package_amount
				+ ", paid_status=" + paid_status + ", balance=" + balance + "]";
	}

	
	
	
	
}
