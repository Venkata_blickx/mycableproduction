package com.blickx.to;

import java.math.BigInteger;

public class SmartCardIssuedCustomerTo {

	private BigInteger total_customers;
	private BigInteger card_holded_customers;
	public BigInteger getTotal_customers() {
		return total_customers;
	}
	public void setTotal_customers(BigInteger total_customers) {
		this.total_customers = total_customers;
	}
	public BigInteger getCard_holded_customers() {
		return card_holded_customers;
	}
	public void setCard_holded_customers(BigInteger card_holded_customers) {
		this.card_holded_customers = card_holded_customers;
	}
	
	public SmartCardIssuedCustomerTo(){
		
	}
}
