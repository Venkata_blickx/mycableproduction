package com.blickx.to;

import java.math.BigDecimal;
import java.math.BigInteger;

public class ComplaintRatingTo {

	@Override
	public String toString() {
		return "ComplaintRatingTo [complaints=" + complaints + ", percentage="
				+ percentage + ", count=" + count + "]";
	}

	public String getComplaints() {
		return complaints;
	}

	public void setComplaints(String complaints) {
		this.complaints = complaints;
	}

	public BigDecimal getPercentage() {
		return percentage;
	}

	public void setPercentage(BigDecimal percentage) {
		this.percentage = percentage;
	}

	public BigInteger getCount() {
		return count;
	}

	public void setCount(BigInteger count) {
		this.count = count;
	}

	private String complaints;
	
	private BigDecimal percentage;
	
	private BigInteger count;

	public ComplaintRatingTo(){
		
	}
}
