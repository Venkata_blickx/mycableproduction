package com.blickx.to;

public class SynchResponse {

	private String customer_id;
	
	private boolean status;

	public String getCustomer_id() {
		return customer_id;
	}

	public void setCustomer_id(String customer_id) {
		this.customer_id = customer_id;
	}

	public boolean isStatus() {
		return status;
	}

	public void setStatus(boolean status) {
		this.status = status;
	}
	
	public SynchResponse(){
		
	}

	@Override
	public String toString() {
		return "SynchResponse [customer_id=" + customer_id + ", status="
				+ status + "]";
	}
}
