package com.blickx.to;

import java.math.BigDecimal;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;

import com.fasterxml.jackson.databind.annotation.JsonSerialize;

import com.blickx.utill.DateSerializerForAndroid;

@Entity
public class MiniStatementForAndroidTo {

	@Id
	@Column
	private Date date;
	
	@Column
	private BigDecimal credit;
	
	@Column
	private BigDecimal debit;
	
	@Column
	private BigDecimal balance;
	
	
	
	@JsonSerialize(using = DateSerializerForAndroid.class)
	public Date getDate() {
		return date;
	}
	public void setDate(Date date) {
		this.date = date;
	}
	
	public BigDecimal getCredit() {
		return credit;
	}
	public void setCredit(BigDecimal credit) {
		this.credit = credit;
	}
	public BigDecimal getDebit() {
		return debit;
	}
	public void setDebit(BigDecimal debit) {
		this.debit = debit;
	}
	public BigDecimal getBalance() {
		return balance;
	}
	public void setBalance(BigDecimal balance) {
		this.balance = balance;
	}
	public MiniStatementForAndroidTo(){
		
	}
	
}
