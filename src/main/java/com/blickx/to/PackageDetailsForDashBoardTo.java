package com.blickx.to;

import java.math.BigDecimal;
import java.math.BigInteger;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;

	/*
	 * This Entity Will return the result of the Stored Procedure "DashboardPackageDetails"
	 */

public class PackageDetailsForDashBoardTo {

	//@Column(name="package")
	private String packages;
	
	//@Column(name="count(package)")
	private BigInteger package_count;
	
	public String getPackages() {
		return packages;
	}
	public void setPackages(String packages) {
		this.packages = packages;
	}

	public BigInteger getPackage_count() {
		return package_count;
	}
	public void setPackage_count(BigInteger package_count) {
		this.package_count = package_count;
	}
	public PackageDetailsForDashBoardTo(){
		
	}
}
