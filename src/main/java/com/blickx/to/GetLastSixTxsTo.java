package com.blickx.to;

import java.math.BigDecimal;
import java.math.BigInteger;
import java.util.Date;

import com.fasterxml.jackson.databind.annotation.JsonSerialize;

import com.blickx.domain.DateSerializerWithTimeStamp;

public class GetLastSixTxsTo {
	
	public Integer getDiscount() {
		return discount;
	}

	public void setDiscount(Integer discount) {
		this.discount = discount;
	}
	private String bill_recipt_no;
	
	private String customer_name;
	
	private String c_id;
	
	private String line1;
	
	private String line2;
	
	private String city;
	
	private Date date;
	
	private BigDecimal total_amount;
	
	private BigDecimal paid_amount;
	
	private Integer discount;
	
	private BigDecimal balance_amount;
	
	private String transaction_id;
	
	private String payment_type;

	@Override
	public String toString() {
		return "GetLastSixTxsTo [bill_recipt_no=" + bill_recipt_no
				+ ", customer_name=" + customer_name + ", c_id=" + c_id
				+ ", line1=" + line1 + ", line2=" + line2 + ", city=" + city
				+ ", date=" + date + ", total_amount=" + total_amount
				+ ", paid_amount=" + paid_amount + ", discount=" + discount
				+ ", balance_amount=" + balance_amount + ", transaction_id="
				+ transaction_id + ", payment_type=" + payment_type + "]";
	}

	public String getTransaction_id() {
		return transaction_id;
	}

	public void setTransaction_id(String transaction_id) {
		this.transaction_id = transaction_id;
	}

	public String getPayment_type() {
		return payment_type;
	}

	public void setPayment_type(String payment_type) {
		this.payment_type = payment_type;
	}

	public String getBill_recipt_no() {
		return bill_recipt_no;
	}

	public void setBill_recipt_no(String bill_recipt_no) {
		this.bill_recipt_no = bill_recipt_no;
	}

	public String getCustomer_name() {
		return customer_name;
	}

	public void setCustomer_name(String customer_name) {
		this.customer_name = customer_name;
	}

	public String getC_id() {
		return c_id;
	}

	public void setC_id(String c_id) {
		this.c_id = c_id;
	}

	public String getLine1() {
		return line1;
	}

	public void setLine1(String line1) {
		this.line1 = line1;
	}

	public String getLine2() {
		return line2;
	}

	public void setLine2(String line2) {
		this.line2 = line2;
	}

	public String getCity() {
		return city;
	}

	public void setCity(String city) {
		this.city = city;
	}

	@JsonSerialize(using = DateSerializerWithTimeStamp.class)
	public Date getDate() {
		return date;
	}

	public void setDate(Date date) {
		this.date = date;
	}

	public BigDecimal getTotal_amount() {
		return total_amount;
	}

	public void setTotal_amount(BigDecimal total_amount) {
		this.total_amount = total_amount;
	}

	public BigDecimal getPaid_amount() {
		return paid_amount;
	}

	public void setPaid_amount(BigDecimal paid_amount) {
		this.paid_amount = paid_amount;
	}

	public BigDecimal getBalance_amount() {
		return balance_amount;
	}

	public void setBalance_amount(BigDecimal balance_amount) {
		this.balance_amount = balance_amount;
	}
	public GetLastSixTxsTo(){
		
	}
}
