package com.blickx.to;

import java.util.List;

public class ComplaintRatingObject {

	@Override
	public String toString() {
		return "ComplaintRatingObject [complaintRatings=" + complaintRatings
				+ ", totalComplaints=" + totalComplaints + "]";
	}

	public List<ComplaintRatingTo> getComplaintRatings() {
		return complaintRatings;
	}

	public void setComplaintRatings(List<ComplaintRatingTo> complaintRatings) {
		this.complaintRatings = complaintRatings;
	}

	public Integer getTotalComplaints() {
		return totalComplaints;
	}

	public void setTotalComplaints(Integer totalComplaints) {
		this.totalComplaints = totalComplaints;
	}

	private List<ComplaintRatingTo> complaintRatings;
	
	private Integer totalComplaints;
	
	
	
}
