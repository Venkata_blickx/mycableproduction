package com.blickx.to;

import java.util.Date;

import org.springframework.format.annotation.DateTimeFormat;

import com.blickx.domain.DateSerializer;
import com.fasterxml.jackson.databind.annotation.JsonSerialize;



public class ChannelsTo {

	private int channel_id;
	
	private String channel_name;
	
	private double channel_amount;
	
	private String category;
	
	private int status;
	
	@DateTimeFormat(pattern = "yyyy-MM-dd")
	private Date channel_effective_date;
	
	private String client_id;

	
	public String getClient_id() {
		return client_id;
	}

	public void setClient_id(String client_id) {
		this.client_id = client_id;
	}

	public int getChannel_id() {
		return channel_id;
	}

	@JsonSerialize(using = DateSerializer.class)
	public Date getChannel_effective_date() {
		return channel_effective_date;
	}


	public void setChannel_effective_date(Date channel_effective_date) {
		this.channel_effective_date = channel_effective_date;
	}


	public void setChannel_id(int channel_id) {
		this.channel_id = channel_id;
	}

	public String getChannel_name() {
		return channel_name;
	}

	public void setChannel_name(String channel_name) {
		this.channel_name = channel_name;
	}

	public double getChannel_amount() {
		return channel_amount;
	}

	public void setChannel_amount(double channel_amount) {
		this.channel_amount = channel_amount;
	}

	public String getCategory() {
		return category;
	}

	public void setCategory(String category) {
		this.category = category;
	}

	public int getStatus() {
		return status;
	}

	public void setStatus(int status) {
		this.status = status;
	}

	public ChannelsTo() {
	}

	@Override
	public String toString() {
		return "ChannelsTo [channel_id=" + channel_id + ", channel_name="
				+ channel_name + ", channel_amount=" + channel_amount
				+ ", category=" + category + ", status=" + status
				+ ", channel_effective_date=" + channel_effective_date
				+ ", client_id=" + client_id + "]";
	}
	
	
}
