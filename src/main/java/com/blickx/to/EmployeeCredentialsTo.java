package com.blickx.to;

import java.math.BigDecimal;

public class EmployeeCredentialsTo {

	private String employee_id;
	
	private String password;
	
	private String ime_number;
	
	private String employee_uid;
	
	private String employee_name;
	
	private String contact_number;
	
	private BigDecimal currentMonthCollection;
	
	public BigDecimal getCurrentMonthCollection() {
		return currentMonthCollection;
	}

	public void setCurrentMonthCollection(BigDecimal currentMonthCollection) {
		this.currentMonthCollection = currentMonthCollection;
	}

	public String getEmployee_name() {
		return employee_name;
	}

	public void setEmployee_name(String employee_name) {
		this.employee_name = employee_name;
	}

	public String getContact_number() {
		return contact_number;
	}

	public void setContact_number(String contact_number) {
		this.contact_number = contact_number;
	}

	@Override
	public String toString() {
		return "EmployeeCredentialsTo [employee_id=" + employee_id
				+ ", password=" + password + ", ime_number=" + ime_number
				+ ", employee_uid=" + employee_uid + ", employee_name="
				+ employee_name + ", contact_number=" + contact_number
				+ ", currentMonthCollection=" + currentMonthCollection + "]";
	}

	public String getEmployee_id() {
		return employee_id;
	}

	public void setEmployee_id(String employee_id) {
		this.employee_id = employee_id;
	}

	public String getEmployee_uid() {
		return employee_uid;
	}

	public void setEmployee_uid(String employee_uid) {
		this.employee_uid = employee_uid;
	}

	public String getPassword() {
		return password;
	}

	public void setPassword(String password) {
		this.password = password;
	}

	public String getIme_number() {
		return ime_number;
	}

	public void setIme_number(String ime_number) {
		this.ime_number = ime_number;
	}
	
	public EmployeeCredentialsTo(){
		
	}
}
