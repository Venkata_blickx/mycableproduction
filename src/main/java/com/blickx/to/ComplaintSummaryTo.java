package com.blickx.to;

public class ComplaintSummaryTo {

	private String details;

	private String status;
	
	public String getStatus() {
		return status;
	}

	public void setStatus(String status) {
		this.status = status;
	}

	public String getDetails() {
		return details;
	}

	public void setDetails(String details) {
		this.details = details;
	}

	public ComplaintSummaryTo(){
		
	}
}
