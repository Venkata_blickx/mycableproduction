package com.blickx.to;

import java.util.Date;

import com.fasterxml.jackson.databind.annotation.JsonSerialize;

import com.blickx.domain.DateSerializer;
import com.blickx.domain.DateSerializerWithTimeStamp;


public class GetOpenClosedComplaintTo {

	@Override
	public String toString() {
		return "GetOpenClosedComplaintTo [complaint_id=" + complaint_id
				+ ", c_id=" + c_id + ", customer_name=" + customer_name
				+ ", employee_name=" + employee_name + ", details=" + details
				+ ", status=" + status + ", assigned_to=" + assigned_to
				+ ", assigned_date=" + assigned_date + ", closed_by="
				+ closed_by + ", closed_date=" + closed_date + ", comment="
				+ comment + ", source=" + source + ", date=" + date + "]";
	}
	public String getSource() {
		return source;
	}
	public void setSource(String source) {
		this.source = source;
	}

	private String complaint_id;
	
	private String c_id;
	public String getC_id() {
		return c_id;
	}
	public void setC_id(String c_id) {
		this.c_id = c_id;
	}

	
	private String customer_name;
	private String employee_name;
	private String details;
	private String status;
	private String assigned_to;
	private Date assigned_date;
	private String closed_by;
	private Date closed_date;
	private String comment;
	private String source;
	private Date date;
	
	@JsonSerialize(using = DateSerializerWithTimeStamp.class)
	public Date getDate() {
		return date;
	}
	public void setDate(Date date) {
		this.date = date;
	}
	public String getComment() {
		return comment;
	}
	public void setComment(String comment) {
		this.comment = comment;
	}
	@JsonSerialize(using = DateSerializerWithTimeStamp.class)
	public Date getAssigned_date() {
		return assigned_date;
	}
	public void setAssigned_date(Date assigned_date) {
		this.assigned_date = assigned_date;
	}
	public String getClosed_by() {
		return closed_by;
	}
	public void setClosed_by(String closed_by) {
		this.closed_by = closed_by;
	}
	@JsonSerialize(using = DateSerializerWithTimeStamp.class)
	public Date getClosed_date() {
		return closed_date;
	}
	public void setClosed_date(Date closed_date) {
		this.closed_date = closed_date;
	}
	public String getAssigned_to() {
		return assigned_to;
	}
	public void setAssigned_to(String assigned_to) {
		this.assigned_to = assigned_to;
	}
	public String getComplaint_id() {
		return complaint_id;
	}
	public void setComplaint_id(String complaint_id) {
		this.complaint_id = complaint_id;
	}
	public String getCustomer_name() {
		return customer_name;
	}
	public void setCustomer_name(String customer_name) {
		this.customer_name = customer_name;
	}
	public String getEmployee_name() {
		return employee_name;
	}
	public void setEmployee_name(String employee_name) {
		this.employee_name = employee_name;
	}
	public String getDetails() {
		return details;
	}
	public void setDetails(String details) {
		this.details = details;
	}
	public String getStatus() {
		return status;
	}
	public void setStatus(String status) {
		this.status = status;
	}
	
	public GetOpenClosedComplaintTo(){
		
	}
}
