package com.blickx.to;

import java.math.BigDecimal;
import java.math.BigInteger;

public class PackageReportsForAndroidTo {

	private String package_name;
	private BigDecimal package_amount;
	private BigInteger units;
	private BigDecimal total;
	public String getPackage_name() {
		return package_name;
	}
	public void setPackage_name(String package_name) {
		this.package_name = package_name;
	}
	public BigDecimal getPackage_amount() {
		return package_amount;
	}
	public void setPackage_amount(BigDecimal package_amount) {
		this.package_amount = package_amount;
	}
	public BigInteger getUnits() {
		return units;
	}
	public void setUnits(BigInteger units) {
		this.units = units;
	}
	public BigDecimal getTotal() {
		return total;
	}
	public void setTotal(BigDecimal total) {
		this.total = total;
	}
	
	public PackageReportsForAndroidTo(){
		
	}
}
