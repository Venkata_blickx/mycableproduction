package com.blickx.to;

import java.math.BigDecimal;

public class SearchCustomerMoreDetails {
	
	private String paidStatus;
	
	private BigDecimal payable;
	
	private BigDecimal packageAmount;
	
	private BigDecimal addAmount;
	
	private BigDecimal balance;
	
	private BigDecimal discountVal;

	public String getPaidStatus() {
		return paidStatus;
	}

	public void setPaidStatus(String paidStatus) {
		this.paidStatus = paidStatus;
	}

	public BigDecimal getPayable() {
		return payable;
	}

	public void setPayable(BigDecimal payable) {
		this.payable = payable;
	}

	public BigDecimal getPackageAmount() {
		return packageAmount;
	}

	public void setPackageAmount(BigDecimal packageAmount) {
		this.packageAmount = packageAmount;
	}

	public BigDecimal getAddAmount() {
		return addAmount;
	}

	public void setAddAmount(BigDecimal addAmount) {
		this.addAmount = addAmount;
	}

	public BigDecimal getBalance() {
		return balance;
	}

	public void setBalance(BigDecimal balance) {
		this.balance = balance;
	}

	public BigDecimal getDiscountVal() {
		return discountVal;
	}

	public void setDiscountVal(BigDecimal discountVal) {
		this.discountVal = discountVal;
	}

	@Override
	public String toString() {
		return "SearchCustomerMoreDetails [paidStatus=" + paidStatus
				+ ", payable=" + payable + ", packageAmount=" + packageAmount
				+ ", addAmount=" + addAmount + ", balance=" + balance
				+ ", discountVal=" + discountVal + "]";
	}

}
