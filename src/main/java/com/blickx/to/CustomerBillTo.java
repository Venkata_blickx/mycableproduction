package com.blickx.to;

import java.math.BigDecimal;

public class CustomerBillTo {

	private String customer_id;
	private String customer_name;
	private String contact_number;
	private String line1;
	private String line2;
	private String city;
	private String state;
	private String pincode;
	private String country;
	private String client_id;
	private String c_id;
	private String email;
	
	private BigDecimal balance_amount;
	private BigDecimal package_amount;
	private BigDecimal addon_amount;
	private BigDecimal monthly_amount;
	private BigDecimal discount;
	private BigDecimal total_amount;
	
	private String packages;
	private String addons;
	
	
	public String getEmail() {
		return email;
	}
	public void setEmail(String email) {
		this.email = email;
	}
	public String getClient_id() {
		return client_id;
	}
	public void setClient_id(String client_id) {
		this.client_id = client_id;
	}
	public String getC_id() {
		return c_id;
	}
	public void setC_id(String c_id) {
		this.c_id = c_id;
	}
	public String getState() {
		return state;
	}
	public void setState(String state) {
		this.state = state;
	}
	public String getPincode() {
		return pincode;
	}
	public void setPincode(String pincode) {
		this.pincode = pincode;
	}
	public String getCountry() {
		return country;
	}
	public void setCountry(String country) {
		this.country = country;
	}
	public BigDecimal getDiscount() {
		return discount;
	}
	public void setDiscount(BigDecimal discount) {
		this.discount = discount;
	}
	public String getPackages() {
		return packages;
	}
	public void setPackages(String packages) {
		this.packages = packages;
	}
	public String getAddons() {
		return addons;
	}
	public void setAddons(String addons) {
		this.addons = addons;
	}
	public String getLine1() {
		return line1;
	}
	public void setLine1(String line1) {
		this.line1 = line1;
	}
	public String getLine2() {
		return line2;
	}
	public void setLine2(String line2) {
		this.line2 = line2;
	}
	public String getCity() {
		return city;
	}
	public void setCity(String city) {
		this.city = city;
	}
	public BigDecimal getMonthly_amount() {
		return monthly_amount;
	}
	public void setMonthly_amount(BigDecimal monthly_amount) {
		this.monthly_amount = monthly_amount;
	}
	public BigDecimal getTotal_amount() {
		return total_amount;
	}
	public void setTotal_amount(BigDecimal total_amount) {
		this.total_amount = total_amount;
	}
	public BigDecimal getBalance_amount() {
		return balance_amount;
	}
	public void setBalance_amount(BigDecimal balance_amount) {
		this.balance_amount = balance_amount;
	}
	public String getCustomer_id() {
		return customer_id;
	}
	public void setCustomer_id(String customer_id) {
		this.customer_id = customer_id;
	}
	public String getCustomer_name() {
		return customer_name;
	}
	public void setCustomer_name(String customer_name) {
		this.customer_name = customer_name;
	}
	public String getContact_number() {
		return contact_number;
	}
	public void setContact_number(String contact_number) {
		this.contact_number = contact_number;
	}
	public BigDecimal getPackage_amount() {
		return package_amount;
	}
	public void setPackage_amount(BigDecimal package_amount) {
		this.package_amount = package_amount;
	}
	public BigDecimal getAddon_amount() {
		return addon_amount;
	}
	public void setAddon_amount(BigDecimal addon_amount) {
		this.addon_amount = addon_amount;
	}
	@Override
	public String toString() {
		return "CustomerBillTo [customer_id=" + customer_id
				+ ", customer_name=" + customer_name + ", contact_number="
				+ contact_number + ", line1=" + line1 + ", line2=" + line2
				+ ", city=" + city + ", state=" + state + ", pincode="
				+ pincode + ", country=" + country + ", client_id=" + client_id
				+ ", c_id=" + c_id + ", email=" + email + ", balance_amount="
				+ balance_amount + ", package_amount=" + package_amount
				+ ", addon_amount=" + addon_amount + ", monthly_amount="
				+ monthly_amount + ", discount=" + discount + ", total_amount="
				+ total_amount + ", packages=" + packages + ", addons="
				+ addons + "]";
	}
	public CustomerBillTo(){
		
	}
}
