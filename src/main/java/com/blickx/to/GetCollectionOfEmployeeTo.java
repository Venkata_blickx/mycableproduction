package com.blickx.to;

import java.math.BigDecimal;
import java.util.Date;

import com.fasterxml.jackson.databind.annotation.JsonSerialize;

import com.blickx.domain.DateSerializerWithTimeStamp;

public class GetCollectionOfEmployeeTo {

	private String bill_recipt_no;
	
	private BigDecimal balance_amount;
	
	private BigDecimal paid_amount;
	
	private String customer_name;
	
	private String contact_number;
	
	private String areacode;
	
	private String card_number;
	
	private String box_number;
	
	private String employee_name;
	
	private String payment_type;
	
	private Date date;
	
	private String c_id;
	
	private String vc_number;

	@Override
	public String toString() {
		return "GetCollectionOfEmployeeTo [bill_recipt_no=" + bill_recipt_no
				+ ", balance_amount=" + balance_amount + ", paid_amount="
				+ paid_amount + ", customer_name=" + customer_name
				+ ", contact_number=" + contact_number + ", areacode="
				+ areacode + ", card_number=" + card_number + ", box_number="
				+ box_number + ", employee_name=" + employee_name
				+ ", payment_type=" + payment_type + ", date=" + date
				+ ", c_id=" + c_id + ", vc_number=" + vc_number + "]";
	}

	public String getVc_number() {
		return vc_number;
	}

	public void setVc_number(String vc_number) {
		this.vc_number = vc_number;
	}

	public String getPayment_type() {
		return payment_type;
	}

	public void setPayment_type(String payment_type) {
		this.payment_type = payment_type;
	}

	public String getBill_recipt_no() {
		return bill_recipt_no;
	}

	public void setBill_recipt_no(String bill_recipt_no) {
		this.bill_recipt_no = bill_recipt_no;
	}

	public String getC_id() {
		return c_id;
	}

	public void setC_id(String c_id) {
		this.c_id = c_id;
	}

	public BigDecimal getBalance_amount() {
		return balance_amount;
	}

	public void setBalance_amount(BigDecimal balance_amount) {
		this.balance_amount = balance_amount;
	}

	public BigDecimal getPaid_amount() {
		return paid_amount;
	}

	public void setPaid_amount(BigDecimal paid_amount) {
		this.paid_amount = paid_amount;
	}

	public String getCustomer_name() {
		return customer_name;
	}

	public void setCustomer_name(String customer_name) {
		this.customer_name = customer_name;
	}

	public String getContact_number() {
		return contact_number;
	}

	public void setContact_number(String contact_number) {
		this.contact_number = contact_number;
	}

	public String getAreacode() {
		return areacode;
	}

	public void setAreacode(String areacode) {
		this.areacode = areacode;
	}

	public String getCard_number() {
		return card_number;
	}

	public void setCard_number(String card_number) {
		this.card_number = card_number;
	}

	public String getBox_number() {
		return box_number;
	}

	public void setBox_number(String box_number) {
		this.box_number = box_number;
	}

	public String getEmployee_name() {
		return employee_name;
	}

	public void setEmployee_name(String employee_name) {
		this.employee_name = employee_name;
	}

	@JsonSerialize(using = DateSerializerWithTimeStamp.class)
	public Date getDate() {
		System.out.println(date);
		return date;
	}

	public void setDate(Date date) {
		this.date = date;
	}

	public GetCollectionOfEmployeeTo(){
		
	}
}
