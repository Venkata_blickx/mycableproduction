package com.blickx.to;

import java.math.BigDecimal;

public class ChannelDisplay {

	private String channel_name;
	
	private BigDecimal channel_amount;

	public String getChannel_name() {
		return channel_name;
	}

	public void setChannel_name(String channel_name) {
		this.channel_name = channel_name;
	}

	public BigDecimal getChannel_amount() {
		return channel_amount;
	}

	public void setChannel_amount(BigDecimal channel_amount) {
		this.channel_amount = channel_amount;
	}
	
	public ChannelDisplay(){
		
	}
}
