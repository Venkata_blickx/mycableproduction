package com.blickx.to;

import java.math.BigDecimal;
import java.util.Date;

import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import org.springframework.format.annotation.DateTimeFormat;

import com.blickx.domain.DateSerializer;

public class ChannelsTOO {

	private int channel_id;
	
	private String channel_name;
	
	private BigDecimal channel_amount;
	
	private String category;
	
	private String status;
	
	@DateTimeFormat(pattern = "yyyy-MM-dd")
	private Date channel_effective_date;
	
	private String client_id;

	
	public String getClient_id() {
		return client_id;
	}

	public void setClient_id(String client_id) {
		this.client_id = client_id;
	}

	public int getChannel_id() {
		return channel_id;
	}

	public void setChannel_id(int channel_id) {
		this.channel_id = channel_id;
	}

	@JsonSerialize(using = DateSerializer.class)
	public Date getChannel_effective_date() {
		return channel_effective_date;
	}

	public void setChannel_effective_date(Date channel_effective_date) {
		this.channel_effective_date = channel_effective_date;
	}

	public String getChannel_name() {
		return channel_name;
	}

	public void setChannel_name(String channel_name) {
		this.channel_name = channel_name;
	}

	public BigDecimal getChannel_amount() {
		return channel_amount;
	}

	public void setChannel_amount(BigDecimal channel_amount) {
		this.channel_amount = channel_amount;
	}

	public String getCategory() {
		return category;
	}

	public void setCategory(String category) {
		this.category = category;
	}

	public String getStatus() {
		return status;
	}

	public void setStatus(String status) {
		this.status = status;
	}

	public ChannelsTOO() {
	}
	
}
