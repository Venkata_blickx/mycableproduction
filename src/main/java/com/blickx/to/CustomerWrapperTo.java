package com.blickx.to;

import java.util.List;

public class CustomerWrapperTo {

	private List<CustomerTo> customerTo;

	public List<CustomerTo> getCustomerTo() {
		return customerTo;
	}

	public void setCustomerTo(List<CustomerTo> customerTo) {
		this.customerTo = customerTo;
	}
	
	public CustomerWrapperTo(List<CustomerTo> customerTo) {
		super();
		this.customerTo = customerTo;
	}

	public CustomerWrapperTo(){
		
	}
}
