package com.blickx.to;

import java.util.Date;
import java.util.List;

import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import org.springframework.format.annotation.DateTimeFormat;

import com.blickx.domain.AreaEmployeeMapEntity;
import com.blickx.domain.DateSerializer;

public class EmployeAddOrUpdateTo {

	private String employee_id;
	
	private String employee_name;
	
	private String contact_number;
	
	private String department;
	
	private String landline_number;
	
	private String areacode;

	@DateTimeFormat(pattern = "yyyy-MM-dd")
	private Date joining_date;
	
	@DateTimeFormat(pattern = "yyyy-MM-dd")
	private Date end_date;
	
	private String assign_device;
	
	private boolean status;
	
	private String employee_photo;
	
	private String employee_doc;
	
	private String gender;
	
	private int device_id;
	
	private String photo_name;
	
	private String doc_name;
	
	private String password;

	private String email;
	
	private String line1;
	
	private String line2;
	
	private String city;
	
	private String state;
	
	private String pincode;
	
	private String doc_type;
	
	private String photo_type;
	
	private String employee_uid;
	
	private String client_id;

	private List<Integer> removedAreaCodes;
	
	private List<Integer> areaCodes;
	
	public List<Integer> getRemovedAreaCodes() {
		return removedAreaCodes;
	}

	public void setRemovedAreaCodes(List<Integer> removedAreaCodes) {
		this.removedAreaCodes = removedAreaCodes;
	}

	private List<AreaEmployeeMapEntity>  areaEmployeeMaps;
	
	public List<AreaEmployeeMapEntity> getAreaEmployeeMaps() {
		return areaEmployeeMaps;
	}

	public void setAreaEmployeeMaps(List<AreaEmployeeMapEntity> areaEmployeeMaps) {
		this.areaEmployeeMaps = areaEmployeeMaps;
	}

	public String getEmployee_id() {
		return employee_id;
	}

	public void setEmployee_id(String employee_id) {
		this.employee_id = employee_id;
	}

	public String getEmployee_name() {
		return employee_name;
	}

	public void setEmployee_name(String employee_name) {
		this.employee_name = employee_name;
	}

	public String getContact_number() {
		return contact_number;
	}

	public void setContact_number(String contact_number) {
		this.contact_number = contact_number;
	}

	public String getDepartment() {
		return department;
	}

	public void setDepartment(String department) {
		this.department = department;
	}

	public String getLandline_number() {
		return landline_number;
	}

	public void setLandline_number(String landline_number) {
		this.landline_number = landline_number;
	}

	public String getAreacode() {
		return areacode;
	}

	public void setAreacode(String areacode) {
		this.areacode = areacode;
	}

	@JsonSerialize(using = DateSerializer.class)
	public Date getJoining_date() {
		return joining_date;
	}

	public void setJoining_date(Date joining_date) {
		this.joining_date = joining_date;
	}
	@JsonSerialize(using = DateSerializer.class)
	public Date getEnd_date() {
		return end_date;
	}

	public void setEnd_date(Date end_date) {
		this.end_date = end_date;
	}

	public String getAssign_device() {
		return assign_device;
	}

	public void setAssign_device(String assign_device) {
		this.assign_device = assign_device;
	}

	public boolean isStatus() {
		return status;
	}

	public void setStatus(boolean status) {
		this.status = status;
	}

	public String getEmployee_photo() {
		return employee_photo;
	}

	public void setEmployee_photo(String employee_photo) {
		this.employee_photo = employee_photo;
	}

	public String getEmployee_doc() {
		return employee_doc;
	}

	public void setEmployee_doc(String employee_doc) {
		this.employee_doc = employee_doc;
	}

	public String getGender() {
		return gender;
	}

	public void setGender(String gender) {
		this.gender = gender;
	}

	public int getDevice_id() {
		return device_id;
	}

	public void setDevice_id(int device_id) {
		this.device_id = device_id;
	}

	public String getPhoto_name() {
		return photo_name;
	}

	public void setPhoto_name(String photo_name) {
		this.photo_name = photo_name;
	}

	public String getDoc_name() {
		return doc_name;
	}

	public void setDoc_name(String doc_name) {
		this.doc_name = doc_name;
	}

	public String getPassword() {
		return password;
	}

	public void setPassword(String password) {
		this.password = password;
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public String getLine1() {
		return line1;
	}

	public void setLine1(String line1) {
		this.line1 = line1;
	}

	public String getLine2() {
		return line2;
	}

	public void setLine2(String line2) {
		this.line2 = line2;
	}

	public String getCity() {
		return city;
	}

	public void setCity(String city) {
		this.city = city;
	}

	public String getState() {
		return state;
	}

	public void setState(String state) {
		this.state = state;
	}

	public String getPincode() {
		return pincode;
	}

	public void setPincode(String pincode) {
		this.pincode = pincode;
	}

	public String getDoc_type() {
		return doc_type;
	}

	public void setDoc_type(String doc_type) {
		this.doc_type = doc_type;
	}

	public String getPhoto_type() {
		return photo_type;
	}

	public void setPhoto_type(String photo_type) {
		this.photo_type = photo_type;
	}

	public String getEmployee_uid() {
		return employee_uid;
	}

	public void setEmployee_uid(String employee_uid) {
		this.employee_uid = employee_uid;
	}

	public String getClient_id() {
		return client_id;
	}

	public void setClient_id(String client_id) {
		this.client_id = client_id;
	}

	public List<Integer> getAreaCodes() {
		return areaCodes;
	}

	public void setAreaCodes(List<Integer> areaCodes) {
		this.areaCodes = areaCodes;
	}

	@Override
	public String toString() {
		return "EmployeAddOrUpdateTo [employee_id=" + employee_id
				+ ", employee_name=" + employee_name + ", contact_number="
				+ contact_number + ", department=" + department
				+ ", landline_number=" + landline_number + ", areacode="
				+ areacode + ", joining_date=" + joining_date + ", end_date="
				+ end_date + ", assign_device=" + assign_device + ", status="
				+ status + ", employee_photo=" + employee_photo
				+ ", employee_doc=" + employee_doc + ", gender=" + gender
				+ ", device_id=" + device_id + ", photo_name=" + photo_name
				+ ", doc_name=" + doc_name + ", password=" + password
				+ ", email=" + email + ", line1=" + line1 + ", line2=" + line2
				+ ", city=" + city + ", state=" + state + ", pincode="
				+ pincode + ", doc_type=" + doc_type + ", photo_type="
				+ photo_type + ", employee_uid=" + employee_uid
				+ ", client_id=" + client_id + ", removedAreaCodes="
				+ removedAreaCodes + ", areaCodes=" + areaCodes
				+ ", areaEmployeeMaps=" + areaEmployeeMaps + "]";
	}
	
	public EmployeAddOrUpdateTo(){
		
	}
}
