package com.blickx.to;

import java.math.BigDecimal;
import java.util.Date;


import com.fasterxml.jackson.databind.annotation.JsonSerialize;

import com.blickx.domain.DateSerializerWithTimeStamp;

public class GetEmployeeReportsTo {

	private String bill_recipt_no;
	
	private String employee_name;
	
	private String employee_id;
	
	private String c_id;
	
	private String contact_number;
	
	private String address;
	
	private String areacode;
	
	private String regPackage;
	
	private String add_on;
	
	private boolean status;
	
	private Date date;
	
	private BigDecimal paid_amount;
	
	private String customer_name;
	
	private BigDecimal balance_amount;
	
	private String transaction_id;
	
	private String vc_number;
	
	
	public String getVc_number() {
		return vc_number;
	}

	public void setVc_number(String vc_number) {
		this.vc_number = vc_number;
	}

	public String getTransaction_id() {
		return transaction_id;
	}

	public void setTransaction_id(String transaction_id) {
		this.transaction_id = transaction_id;
	}

	public String getBill_recipt_no() {
		return bill_recipt_no;
	}

	public void setBill_recipt_no(String bill_recipt_no) {
		this.bill_recipt_no = bill_recipt_no;
	}

	public String getEmployee_name() {
		return employee_name;
	}

	public void setEmployee_name(String employee_name) {
		this.employee_name = employee_name;
	}

	public String getEmployee_id() {
		return employee_id;
	}

	public void setEmployee_id(String employee_id) {
		this.employee_id = employee_id;
	}

	public String getContact_number() {
		return contact_number;
	}

	public void setContact_number(String contact_number) {
		this.contact_number = contact_number;
	}

	public String getAddress() {
		return address;
	}

	public void setAddress(String address) {
		this.address = address;
	}

	public String getAreacode() {
		return areacode;
	}

	public void setAreacode(String areacode) {
		this.areacode = areacode;
	}

	public String getRegPackage() {
		return regPackage;
	}

	public void setRegPackage(String regPackage) {
		this.regPackage = regPackage;
	}

	public String getAdd_on() {
		return add_on;
	}

	public void setAdd_on(String add_on) {
		this.add_on = add_on;
	}

	public boolean isStatus() {
		return status;
	}

	public void setStatus(boolean status) {
		this.status = status;
	}

	@JsonSerialize(using = DateSerializerWithTimeStamp.class)
	public Date getDate() {
		return date;
	}

	public void setDate(Date date) {
		this.date = date;
	}

	public BigDecimal getPaid_amount() {
		return paid_amount;
	}

	public void setPaid_amount(BigDecimal paid_amount) {
		this.paid_amount = paid_amount;
	}

	public String getCustomer_name() {
		return customer_name;
	}

	public void setCustomer_name(String customer_name) {
		this.customer_name = customer_name;
	}

	public BigDecimal getBalance_amount() {
		return balance_amount;
	}

	public void setBalance_amount(BigDecimal balance_amount) {
		this.balance_amount = balance_amount;
	}
	
	public String getC_id() {
		return c_id;
	}

	public void setC_id(String c_id) {
		this.c_id = c_id;
	}

	@Override
	public String toString() {
		return "GetEmployeeReportsTo [bill_recipt_no=" + bill_recipt_no
				+ ", employee_name=" + employee_name + ", employee_id="
				+ employee_id + ", c_id=" + c_id + ", contact_number="
				+ contact_number + ", address=" + address + ", areacode="
				+ areacode + ", regPackage=" + regPackage + ", add_on="
				+ add_on + ", status=" + status + ", date=" + date
				+ ", paid_amount=" + paid_amount + ", customer_name="
				+ customer_name + ", balance_amount=" + balance_amount
				+ ", transaction_id=" + transaction_id + ", vc_number="
				+ vc_number + "]";
	}

	public GetEmployeeReportsTo(){

	}
	
}
