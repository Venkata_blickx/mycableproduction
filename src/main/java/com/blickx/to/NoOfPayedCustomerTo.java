package com.blickx.to;

import java.math.BigInteger;

public class NoOfPayedCustomerTo {

	private BigInteger total_customer;
	
	private BigInteger paid_customer;

	public BigInteger getTotal_customer() {
		return total_customer;
	}

	public void setTotal_customer(BigInteger total_customer) {
		this.total_customer = total_customer;
	}

	public BigInteger getPaid_customer() {
		return paid_customer;
	}

	public void setPaid_customer(BigInteger paid_customer) {
		this.paid_customer = paid_customer;
	}

	public NoOfPayedCustomerTo(){
		
	}
}
