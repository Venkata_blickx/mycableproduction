package com.blickx.to;

import java.math.BigDecimal;
import java.math.BigInteger;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;

	/*
	 * This POJO will Consists the result of "DashboardBillCollectorsGraph" Stored Procedure
	 */
public class BillCollectorsGraphTO {

	//@Column
	private String employee_id;
	
	//@Column
	private String employee_name;
	
	//@Column(name="count(b.employee_id)")
	private BigInteger emp_count;
	
	//@Column
	private String DAYNAME;
	
	public String getEmployee_id() {
		return employee_id;
	}
	public void setEmployee_id(String employee_id) {
		this.employee_id = employee_id;
	}
	public String getEmployee_name() {
		return employee_name;
	}
	public void setEmployee_name(String employee_name) {
		this.employee_name = employee_name;
	}

	public BigInteger getEmp_count() {
		return emp_count;
	}
	public void setEmp_count(BigInteger emp_count) {
		this.emp_count = emp_count;
	}
	public String getDAYNAME() {
		return DAYNAME;
	}
	public void setDAYNAME(String dAYNAME) {
		DAYNAME = dAYNAME;
	}
	 
	public BillCollectorsGraphTO(){
		
	}
}
