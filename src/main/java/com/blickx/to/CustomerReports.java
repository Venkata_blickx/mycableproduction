package com.blickx.to;

import java.math.BigDecimal;
import java.util.Date;

import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import com.blickx.domain.DateSerializerWithTimeStamp;

public class CustomerReports {

	private String customer_id;
	private String customer_name;
	private String contact_number;
	private String areacode;
	private String regPackage;
	private String add_on;
	private boolean status;
	private String bill_recipt_no;
	private Date bill_date;
	private BigDecimal paid_amount;
	private BigDecimal balance_amount;
	private String employee_name;
	private String c_id;
	private String payment_type;
	private String vc_number;
	private Integer setTopBoxCount;

	public Integer getSetTopBoxCount() {
		return setTopBoxCount;
	}

	public void setSetTopBoxCount(Integer setTopBoxCount) {
		this.setTopBoxCount = setTopBoxCount;
	}

	public String getAreacode() {
		return areacode;
	}

	public void setAreacode(String areacode) {
		this.areacode = areacode;
	}

	public String getVc_number() {
		return vc_number;
	}

	public void setVc_number(String vc_number) {
		this.vc_number = vc_number;
	}

	public String getPayment_type() {
		return payment_type;
	}

	public void setPayment_type(String payment_type) {
		this.payment_type = payment_type;
	}

	public String getCustomer_id() {
		return customer_id;
	}

	public void setCustomer_id(String customer_id) {
		this.customer_id = customer_id;
	}

	public String getC_id() {
		return c_id;
	}

	public void setC_id(String c_id) {
		this.c_id = c_id;
	}

	public String getCustomer_name() {
		return customer_name;
	}

	public void setCustomer_name(String customer_name) {
		this.customer_name = customer_name;
	}

	public String getContact_number() {
		return contact_number;
	}

	public void setContact_number(String contact_number) {
		this.contact_number = contact_number;
	}

	public String getRegPackage() {
		return regPackage;
	}

	public void setRegPackage(String regPackage) {
		this.regPackage = regPackage;
	}

	public String getAdd_on() {
		return add_on;
	}

	public void setAdd_on(String add_on) {
		this.add_on = add_on;
	}


	public boolean isStatus() {
		return status;
	}

	public void setStatus(boolean status) {
		this.status = status;
	}

	public String getBill_recipt_no() {
		return bill_recipt_no;
	}

	public void setBill_recipt_no(String bill_recipt_no) {
		this.bill_recipt_no = bill_recipt_no;
	}

	public BigDecimal getPaid_amount() {
		return paid_amount;
	}

	@JsonSerialize(using = DateSerializerWithTimeStamp.class)
	public Date getBill_date() {
		return bill_date;
	}

	public void setBill_date(Date bill_date) {
		this.bill_date = bill_date;
	}

	public void setPaid_amount(BigDecimal paid_amount) {
		this.paid_amount = paid_amount;
	}

	public BigDecimal getBalance_amount() {
		return balance_amount;
	}

	public void setBalance_amount(BigDecimal balance_amount) {
		this.balance_amount = balance_amount;
	}

	@Override
	public String toString() {
		return "CustomerReports [customer_id=" + customer_id
				+ ", customer_name=" + customer_name + ", contact_number="
				+ contact_number + ", areacode=" + areacode + ", regPackage="
				+ regPackage + ", add_on=" + add_on + ", status=" + status
				+ ", bill_recipt_no=" + bill_recipt_no + ", bill_date="
				+ bill_date + ", paid_amount=" + paid_amount
				+ ", balance_amount=" + balance_amount + ", employee_name="
				+ employee_name + ", c_id=" + c_id + ", payment_type="
				+ payment_type + ", vc_number=" + vc_number + "]";
	}

	public String getEmployee_name() {
		return employee_name;
	}

	public void setEmployee_name(String employee_name) {
		this.employee_name = employee_name;
	}
	
	public CustomerReports(){
		
	}
}
