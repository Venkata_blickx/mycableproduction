package com.blickx.to;

import java.math.BigDecimal;
import java.util.Date;

import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import org.springframework.format.annotation.DateTimeFormat;

import com.blickx.domain.DateSerializerWithTimeStamp;

public class CustomerBillReportByCidAndDateTo {

	private String customer_id;
	private String customer_name;
	private String line1;
	private String line2;
	private String contact_number;
	private String c_id;
	private String areacode;
	private String status;
	private String vc_number;
	private BigDecimal package_amount;
	private BigDecimal addon_amount;
	private BigDecimal total;
	private BigDecimal previous_balance;
	private BigDecimal discount;

	public String getLine1() {
		return line1;
	}

	public void setLine1(String line1) {
		this.line1 = line1;
	}

	public String getLine2() {
		return line2;
	}

	public void setLine2(String line2) {
		this.line2 = line2;
	}

	public String getCustomer_id() {
		return customer_id;
	}

	public void setCustomer_id(String customer_id) {
		this.customer_id = customer_id;
	}

	public BigDecimal getDiscount() {
		return discount;
	}

	public void setDiscount(BigDecimal discount) {
		this.discount = discount;
	}

	public String getCustomer_name() {
		return customer_name;
	}

	public void setCustomer_name(String customer_name) {
		this.customer_name = customer_name;
	}


	public String getContact_number() {
		return contact_number;
	}

	public void setContact_number(String contact_number) {
		this.contact_number = contact_number;
	}

	public String getC_id() {
		return c_id;
	}

	public void setC_id(String c_id) {
		this.c_id = c_id;
	}

	public String getAreacode() {
		return areacode;
	}

	public void setAreacode(String areacode) {
		this.areacode = areacode;
	}

	public BigDecimal getPackage_amount() {
		return package_amount;
	}

	public void setPackage_amount(BigDecimal package_amount) {
		this.package_amount = package_amount;
	}

	public BigDecimal getAddon_amount() {
		return addon_amount;
	}

	public void setAddon_amount(BigDecimal addon_amount) {
		this.addon_amount = addon_amount;
	}

	public String getStatus() {
		return status;
	}

	public void setStatus(String status) {
		this.status = status;
	}

	public BigDecimal getTotal() {
		return total;
	}

	public void setTotal(BigDecimal total) {
		this.total = total;
	}

	public String getVc_number() {
		return vc_number;
	}

	public void setVc_number(String vc_number) {
		this.vc_number = vc_number;
	}

	public BigDecimal getPrevious_balance() {
		return previous_balance;
	}

	public void setPrevious_balance(BigDecimal previous_balance) {
		this.previous_balance = previous_balance;
	}

	public CustomerBillReportByCidAndDateTo(){
		
	}
}
