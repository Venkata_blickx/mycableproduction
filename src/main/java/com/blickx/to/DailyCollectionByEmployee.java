package com.blickx.to;

import java.math.BigDecimal;
import java.math.BigInteger;

public class DailyCollectionByEmployee {

	public String employee_name;
	public BigInteger total_customers;
	public BigDecimal total_amount;
	
	public String getEmployee_name() {
		return employee_name;
	}
	public void setEmployee_name(String employee_name) {
		this.employee_name = employee_name;
	}
	
	public BigInteger getTotal_customers() {
		return total_customers;
	}
	public void setTotal_customers(BigInteger total_customers) {
		this.total_customers = total_customers;
	}
	
	public BigDecimal getTotal_amount() {
		return total_amount;
	}
	public void setTotal_amount(BigDecimal total_amount) {
		this.total_amount = total_amount;
	}
	public DailyCollectionByEmployee() {
		// TODO Auto-generated constructor stub
	}
	
	
}
