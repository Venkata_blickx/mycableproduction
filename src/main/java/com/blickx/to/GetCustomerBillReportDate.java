package com.blickx.to;

import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;

import org.springframework.format.annotation.DateTimeFormat;

public class GetCustomerBillReportDate {

	@Override
	public String toString() {
		return "GetCustomerBillReportDate [customer_id=" + customer_id
				+ ", customer_name=" + customer_name + ", c_id=" + c_id
				+ ", from=" + from + ", to=" + to + ", dateRange=" + dateRange
				+ ", client_id=" + client_id + ", status=" + status + "]";
	}

	public String getDateRange() {
		return dateRange;
	}

	public void setDateRange(String dateRange) {
		this.dateRange = dateRange;
	}

	public String getFrom() {
		return from;
	}

	public void setFrom(String from) {
		this.from = from;
	}

	public String getTo() {
		return to;
	}

	public void setTo(String to) {
		this.to = to;
	}

	private String customer_id;
	
	private String customer_name;
	
	private String c_id;
	
	private String from;
	
	private String to;
	
	private String dateRange;
	
	private String client_id;
	
	private Boolean status;

	public Boolean getStatus() {
		return status;
	}

	public void setStatus(Boolean status) {
		this.status = status;
	}

	public String getClient_id() {
		return client_id;
	}

	public void setClient_id(String client_id) {
		this.client_id = client_id;
	}

	public String getCustomer_id() {
		return customer_id;
	}

	public void setCustomer_id(String customer_id) {
		this.customer_id = customer_id;
	}

	public String getCustomer_name() {
		return customer_name;
	}

	public void setCustomer_name(String customer_name) {
		this.customer_name = customer_name;
	}

	

	public String getC_id() {
		return c_id;
	}

	public void setC_id(String c_id) {
		this.c_id = c_id;
	}

	public GetCustomerBillReportDate(){
		
	}

}
