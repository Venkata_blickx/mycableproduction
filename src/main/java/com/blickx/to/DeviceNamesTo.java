package com.blickx.to;

import java.math.BigInteger;

public class DeviceNamesTo {

	private int device_id;
	
	private String device_name;
	
	private BigInteger assigned_status;

	public int getDevice_id() {
		return device_id;
	}

	public void setDevice_id(int device_id) {
		this.device_id = device_id;
	}

	public String getDevice_name() {
		return device_name;
	}

	public void setDevice_name(String device_name) {
		this.device_name = device_name;
	}

	

	public BigInteger getAssigned_status() {
		return assigned_status;
	}

	public void setAssigned_status(BigInteger assigned_status) {
		this.assigned_status = assigned_status;
	}

	@Override
	public String toString() {
		return "DeviceNamesTo [device_id=" + device_id + ", device_name="
				+ device_name + ", assigned_status=" + assigned_status + "]";
	}

	
}
