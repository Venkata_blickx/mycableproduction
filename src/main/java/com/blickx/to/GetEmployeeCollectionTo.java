package com.blickx.to;

import java.math.BigDecimal;
import java.math.BigInteger;

import javax.persistence.Column;
import javax.persistence.Id;

/**
 * 
 * To Send Week collection of Employee, to Android APP
 *
 */
public class GetEmployeeCollectionTo {

	/*@Id
	@Column*/
	private String day;
	
	private BigDecimal collection;
	
	private BigInteger numberOfCustomers;

	public BigInteger getNumberOfCustomers() {
		return numberOfCustomers;
	}

	public void setNumberOfCustomers(BigInteger numberOfCustomers) {
		this.numberOfCustomers = numberOfCustomers;
	}

	public String getDay() {
		return day;
	}

	public void setDay(String day) {
		this.day = day;
	}

	public BigDecimal getCollection() {
		return collection;
	}

	public void setCollection(BigDecimal collection) {
		this.collection = collection;
	}

	public GetEmployeeCollectionTo(){
		
	}
	
}
