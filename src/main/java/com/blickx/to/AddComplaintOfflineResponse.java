package com.blickx.to;

public class AddComplaintOfflineResponse {

	private String customer_id;
	
	private String response;

	
	public String getCustomer_id() {
		return customer_id;
	}

	public void setCustomer_id(String customer_id) {
		this.customer_id = customer_id;
	}

	public String getResponse() {
		return response;
	}

	public void setResponse(String response) {
		this.response = response;
	}
	
	public AddComplaintOfflineResponse(){
		
	}
}
