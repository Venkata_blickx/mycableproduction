package com.blickx.to;

import java.math.BigDecimal;

	/*
	 * This POJO will consists the result of "GetCollectionReport" Store Procedure
	 */

public class DailyCollectionReport {
	private BigDecimal today_paid;
	//private BigDecimal today_balance;
	private BigDecimal today_1_paid;
	//private BigDecimal today_1_balance;
	private BigDecimal today_2_paid;
	//private BigDecimal today_2_balance;
	private BigDecimal today_3_paid;
	//private BigDecimal today_3_balance;
	private BigDecimal today_4_paid;
	//private BigDecimal today_4_balance;
	private BigDecimal today_5_paid;
	//private BigDecimal today_5_balance;
	private BigDecimal today_6_paid;
	//private BigDecimal today_6_balance;
	private BigDecimal week_paid;
	//private BigDecimal week_balance;
	private BigDecimal month_paid;
	//private BigDecimal month_balance;
	
	public BigDecimal getToday_paid() {
		return today_paid;
	}
	public void setToday_paid(BigDecimal today_paid) {
		this.today_paid = today_paid;
	}
	public BigDecimal getToday_1_paid() {
		return today_1_paid;
	}
	public void setToday_1_paid(BigDecimal today_1_paid) {
		this.today_1_paid = today_1_paid;
	}
	public BigDecimal getToday_2_paid() {
		return today_2_paid;
	}
	public void setToday_2_paid(BigDecimal today_2_paid) {
		this.today_2_paid = today_2_paid;
	}
	public BigDecimal getToday_3_paid() {
		return today_3_paid;
	}
	public void setToday_3_paid(BigDecimal today_3_paid) {
		this.today_3_paid = today_3_paid;
	}
	public BigDecimal getToday_4_paid() {
		return today_4_paid;
	}
	public void setToday_4_paid(BigDecimal today_4_paid) {
		this.today_4_paid = today_4_paid;
	}
	public BigDecimal getToday_5_paid() {
		return today_5_paid;
	}
	public void setToday_5_paid(BigDecimal today_5_paid) {
		this.today_5_paid = today_5_paid;
	}
	public BigDecimal getToday_6_paid() {
		return today_6_paid;
	}
	public void setToday_6_paid(BigDecimal today_6_paid) {
		this.today_6_paid = today_6_paid;
	}
	public BigDecimal getWeek_paid() {
		return week_paid;
	}
	public void setWeek_paid(BigDecimal week_paid) {
		this.week_paid = week_paid;
	}
	public BigDecimal getMonth_paid() {
		return month_paid;
	}
	public void setMonth_paid(BigDecimal month_paid) {
		this.month_paid = month_paid;
	}
	
	@Override
	public String toString() {
		// TODO Auto-generated method stub
		return getToday_1_paid() + " :: " + getToday_2_paid();
	}
	
	
}
