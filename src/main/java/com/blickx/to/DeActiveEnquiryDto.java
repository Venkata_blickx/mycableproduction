package com.blickx.to;

public class DeActiveEnquiryDto {
	
	
	private String customerId;

	public String getCustomerId() {
		return customerId;
	}

	public void setCustomerId(String customerId) {
		this.customerId = customerId;
	}

	@Override
	public String toString() {
		return "DeActiveEnquiryDto [customerId=" + customerId + "]";
	}
	

}
