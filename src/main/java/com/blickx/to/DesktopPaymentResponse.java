package com.blickx.to;

public class DesktopPaymentResponse {
	
	private String status;
	private String receipt;
	
	

	public String getStatus() {
		return status;
	}

	public void setStatus(String status) {
		this.status = status;
	}

	public String getReceipt() {
		return receipt;
	}

	public void setReceipt(String receipt) {
		this.receipt = receipt;
	}

	public DesktopPaymentResponse(String receipt) {
		super();
		this.receipt = receipt;
	}

	public DesktopPaymentResponse() {
		super();
		// TODO Auto-generated constructor stub
	}
	
	

}
