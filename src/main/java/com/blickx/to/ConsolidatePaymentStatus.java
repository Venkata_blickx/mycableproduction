/**
 * 
 */
package com.blickx.to;

import java.math.BigDecimal;

/**
 * @author lenovo
 *
 */
public class ConsolidatePaymentStatus {

	public BigDecimal getTotal() {
		return total;
	}





	public void setTotal(BigDecimal total) {
		this.total = total;
	}





	public BigDecimal getPending() {
		return pending;
	}





	public void setPending(BigDecimal pending) {
		this.pending = pending;
	}





	public BigDecimal getCollected() {
		return collected;
	}





	public void setCollected(BigDecimal collected) {
		this.collected = collected;
	}





	@Override
	public String toString() {
		return "ConsolidatePaymentStatus [total=" + total + ", pending="
				+ pending + ", collected=" + collected + "]";
	}

	

	private BigDecimal total;
	private BigDecimal pending;
	private BigDecimal collected;
	
	
	
	

	public ConsolidatePaymentStatus() {
		super();
		// TODO Auto-generated constructor stub
	}
	
}
