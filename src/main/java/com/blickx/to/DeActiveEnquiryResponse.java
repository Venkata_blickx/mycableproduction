package com.blickx.to;

public class DeActiveEnquiryResponse {
	
	
	private String message;

	public String getMessage() {
		return message;
	}

	public void setMessage(String message) {
		this.message = message;
	}

	@Override
	public String toString() {
		return "DeActiveEnquiryResponse [message=" + message + "]";
	}
	

}
