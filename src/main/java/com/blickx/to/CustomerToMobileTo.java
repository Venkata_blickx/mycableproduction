package com.blickx.to;

import java.math.BigDecimal;
import java.math.BigInteger;

public class CustomerToMobileTo {

@Override
public String toString() {
	return "CustomerToMobileTo [customer_id=" + customer_id
			+ ", customer_name=" + customer_name + ", contact_number="
			+ contact_number + ", line1=" + line1 + ", line2=" + line2
			+ ", package_amount=" + package_amount + ", addon_amount="
			+ addon_amount + ", balance_amount=" + balance_amount
			+ ", monthly_rent=" + monthly_rent + ", total_amount="
			+ total_amount + ", customer_uid=" + customer_uid + ", c_id="
			+ c_id + ", discount=" + discount + ", email=" + email
			+ ", paid_status=" + paid_status + ", updated_type=" + updated_type
			+ ", setupboxCount=" + setupboxCount + ", vc_number=" + vc_number
			+ "]";
}

public String getCustomer_id() {
		return customer_id;
	}

	public void setCustomer_id(String customer_id) {
		this.customer_id = customer_id;
	}

	public String getCustomer_name() {
		return customer_name;
	}

	public void setCustomer_name(String customer_name) {
		this.customer_name = customer_name;
	}

	public String getContact_number() {
		return contact_number;
	}

	public void setContact_number(String contact_number) {
		this.contact_number = contact_number;
	}

	public String getLine1() {
		return line1;
	}

	public void setLine1(String line1) {
		this.line1 = line1;
	}

	public String getLine2() {
		return line2;
	}

	public void setLine2(String line2) {
		this.line2 = line2;
	}

	public BigDecimal getPackage_amount() {
		return package_amount;
	}

	public void setPackage_amount(BigDecimal package_amount) {
		this.package_amount = package_amount;
	}

	public BigDecimal getAddon_amount() {
		return addon_amount;
	}

	public void setAddon_amount(BigDecimal addon_amount) {
		this.addon_amount = addon_amount;
	}

	public BigDecimal getBalance_amount() {
		return balance_amount;
	}

	public void setBalance_amount(BigDecimal balance_amount) {
		this.balance_amount = balance_amount;
	}

	public BigDecimal getMonthly_rent() {
		return monthly_rent;
	}

	public void setMonthly_rent(BigDecimal monthly_rent) {
		this.monthly_rent = monthly_rent;
	}

	public BigDecimal getTotal_amount() {
		return total_amount;
	}

	public void setTotal_amount(BigDecimal total_amount) {
		this.total_amount = total_amount;
	}

	public String getCustomer_uid() {
		return customer_uid;
	}

	public void setCustomer_uid(String customer_uid) {
		this.customer_uid = customer_uid;
	}

	public String getC_id() {
		return c_id;
	}

	public void setC_id(String c_id) {
		this.c_id = c_id;
	}

	public BigDecimal getDiscount() {
		return discount;
	}

	public void setDiscount(BigDecimal discount) {
		this.discount = discount;
	}

public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

private String customer_id;
	
	private String customer_name;
	
	private String contact_number;
	
	private String line1;
	
	private String line2;
	
	private BigDecimal package_amount;
	
	private BigDecimal addon_amount;
	
	private BigDecimal balance_amount;
	
	private BigDecimal monthly_rent;
	
	private BigDecimal total_amount;
	
	private String customer_uid;
	
	private String c_id;
	
	private BigDecimal discount;
	
	private String email;
	
	private String paid_status;
	
	private String updated_type;
	
	private BigInteger setupboxCount;
	
	private String vc_number;
	
	public String getVc_number() {
		return vc_number;
	}

	public void setVc_number(String vc_number) {
		this.vc_number = vc_number;
	}

	public BigInteger getSetupboxCount() {
		return setupboxCount;
	}

	public void setSetupboxCount(BigInteger setupboxCount) {
		this.setupboxCount = setupboxCount;
	}

	public String getUpdated_type() {
		return updated_type;
	}

	public void setUpdated_type(String updated_type) {
		this.updated_type = updated_type;
	}

	public String getPaid_status() {
		return paid_status;
	}

	public void setPaid_status(String paid_status) {
		this.paid_status = paid_status;
	}

	public CustomerToMobileTo(){
		
	}
}
