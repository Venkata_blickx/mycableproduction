package com.blickx.to;

import java.util.Date;

import com.fasterxml.jackson.databind.annotation.JsonSerialize;

import com.blickx.domain.DateSerializer;

public class GetTheAddonsToUpdate {

	private String box_number;
	
	private String addon;
	
	private Date expiry_date;

	public String getBox_number() {
		return box_number;
	}

	public void setBox_number(String box_number) {
		this.box_number = box_number;
	}

	public String getAddon() {
		return addon;
	}

	public void setAddon(String addon) {
		this.addon = addon;
	}

	@JsonSerialize(using = DateSerializer.class)
	public Date getExpiry_date() {
		return expiry_date;
	}

	public void setExpiry_date(Date expiry_date) {
		this.expiry_date = expiry_date;
	}
	
	public GetTheAddonsToUpdate(){
		
	}

	@Override
	public String toString() {
		return "GetTheAddonsToUpdate [box_number=" + box_number + ", addon="
				+ addon + ", expiry_date=" + expiry_date + "]";
	}
	
	
}
