package com.blickx.utill;

public class BillGenType {
	
	
	private String customerId;
	private int billType;
	private String employee_id;
	public String getCustomerId() {
		return customerId;
	}
	public void setCustomerId(String customerId) {
		this.customerId = customerId;
	}
	public int getBillType() {
		return billType;
	}
	public void setBillType(int billType) {
		this.billType = billType;
	}
	public String getEmployee_id() {
		return employee_id;
	}
	public void setEmployee_id(String employee_id) {
		this.employee_id = employee_id;
	}
	@Override
	public String toString() {
		return "BillGenType [customerId=" + customerId + ", billType="
				+ billType + ", employee_id=" + employee_id + "]";
	}
	

}
