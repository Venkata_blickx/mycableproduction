package com.blickx.utill;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import org.springframework.beans.BeanUtils;

import com.blickx.domain.EditCustomerCommentEntity;
import com.blickx.output.EditCustomerCommentTo;

public class CommonUtils {

	public static String searchCustoemrQuery = "select bill.customer_id, "
			+ "bill.customer_name, "
			+ "bill.contact_number, "
			+ "bill.c_id, "
			+ "bill.line1, "
			+ "bill.line2, "
			+ "s.vc_number, "
			+ "s.box_number, "
			+ "bill.areacode, "
			+ "case when bill.status = 1 then 'Active' when bill.status = 0 then 'InActive' end as status, "
			+ "case when bill.permenent_status = 1 then 'InActive' when bill.permenent_status = 0 then 'Active' end as permenent_status, "
			+ "bill.package_amount, "
			+ "bill.addon_amount, "
			+ "case when bill.paid_count>0 then \"UnPaid\" WHEN bill.paid_count < 0 THEN \"New\" else \"Paid\" end as paid_status, "
			+ "(select "
			+ "balance_amount "
			+ "from "
			+ "billing b "
			+ "where "
			+ "b.customer_id = bill.customer_id "
			+ "order by row_id desc "
			+ "limit 1) as balance, "
			+ "s.discount, "
			+ "if(bill.paid_count>0, (bill.package_amount +  (select "
			+ "balance_amount "
			+ "from "
			+ "billing b "
			+ "where "
			+ "b.customer_id = bill.customer_id "
			+ "order by row_id desc "
			+ "limit 1) - s.discount), (select "
			+ "balance_amount "
			+ "from "
			+ "billing b "
			+ "where "
			+ "b.customer_id = bill.customer_id "
			+ "order by row_id desc "
			+ "limit 1)) as total "
			+ "from "
			+ "(SELECT "
			+ "c.customer_id, "
			+ "c.customer_name, "
			+ "c.contact_number, "
			+ "c.c_id, "
			+ "c.areacode, "
			+ "c.status, "
			+ "c.line1, "
			+ "c.line2, "
			+ "c.permenent_status, "
			+ "c.client_id, "
			+ "ifnull(SUM(bill_gen.package_monthly_amount),0) package_amount, "
			+ "ifnull(SUM(bill_gen.addon_monthly_amount),0) addon_amount, "
			+ "CASE WHEN paid_status = 0 THEN 1 WHEN paid_status IS NULL THEN -1 ELSE 0 END AS paid_count "
			+ "FROM "
			+ "bill_generation bill_gen, customer c "
			+ "WHERE "
			+ "c.customer_id = bill_gen.customer_id  and "
			+ "((month(joining_date) = month AND year(joining_date) = year(date) AND paid_status is null) OR ( paid_status is not null and paid_status = 0 or 1 and month = case when month(current_date)=1 then 12 else month(current_date)-1 end )) "
			+ "group by c.customer_id, c.customer_name, c.contact_number, c.c_id, c.areacode, c.status, c.permenent_status, paid_status= 0, month = month(current_date)) bill "
			+ "join "
			+ "(SELECT "
			+ "customer_id, SUM(discount) discount, GROUP_CONCAT(vc_number) as vc_number, GROUP_CONCAT(box_number) as box_number "
			+ "FROM "
			+ "setupbox where pda_status = 0 "
			+ "group by customer_id) s ON bill.customer_id = s.customer_id";
	
	public static String permanentDeactiveListQuery = "select bill.customer_id, "
			+ "bill.customer_name, "
			+ "bill.contact_number, "
			+ "bill.c_id, "
			+ "bill.line1, "
			+ "bill.line2, "
			+ "s.vc_number, "
			+ "s.box_number, "
			+ "bill.areacode, "
			+ "case when bill.status = 1 then 'Active' when bill.status = 0 then 'InActive' end as status, "
			+ "case when bill.permenent_status = 1 then 'InActive' when bill.permenent_status = 0 then 'Active' end as permenent_status, "
			+ "bill.package_amount, "
			+ "bill.addon_amount, "
			+ "case when bill.paid_count>0 then \"UnPaid\" WHEN bill.paid_count < 0 THEN \"New\" else \"Paid\" end as paid_status, "
			+ "(select "
			+ "balance_amount "
			+ "from "
			+ "billing b "
			+ "where "
			+ "b.customer_id = bill.customer_id "
			+ "order by row_id desc "
			+ "limit 1) as balance, "
			+ "s.discount, "
			+ "if(bill.paid_count>0, (bill.package_amount +  (select "
			+ "balance_amount "
			+ "from "
			+ "billing b "
			+ "where "
			+ "b.customer_id = bill.customer_id "
			+ "order by row_id desc "
			+ "limit 1) - s.discount), (select "
			+ "balance_amount "
			+ "from "
			+ "billing b "
			+ "where "
			+ "b.customer_id = bill.customer_id "
			+ "order by row_id desc "
			+ "limit 1)) as total "
			+ "from "
			+ "(SELECT "
			+ "c.customer_id, "
			+ "c.customer_name, "
			+ "c.contact_number, "
			+ "c.c_id, "
			+ "c.areacode, "
			+ "c.status, "
			+ "c.line1, "
			+ "c.line2, "
			+ "c.permenent_status, "
			+ "c.client_id, "
			+ "ifnull(SUM(bill_gen.package_monthly_amount),0) package_amount, "
			+ "ifnull(SUM(bill_gen.addon_monthly_amount),0) addon_amount, "
			+ "CASE WHEN paid_status = 0 THEN 1 WHEN paid_status IS NULL THEN -1 ELSE 0 END AS paid_count "
			+ "FROM "
			+ "bill_generation bill_gen, customer c "
			+ "WHERE "
			+ "c.customer_id = bill_gen.customer_id  and "
			+ "((month(joining_date) = month AND year(joining_date) = year(date) AND paid_status is null) OR ( paid_status is not null and paid_status = 0 or 1 and month = case when month(current_date)=1 then 12 else month(current_date)-1 end )) "
			+ "group by c.customer_id, c.customer_name, c.contact_number, c.c_id, c.areacode, c.status, c.permenent_status, paid_status= 0, month = month(current_date)) bill "
			+ "join "
			+ "(SELECT "
			+ "customer_id, SUM(discount) discount, GROUP_CONCAT(vc_number) as vc_number, GROUP_CONCAT(box_number) as box_number "
			+ "FROM "
			+ "setupbox where pda_status = 1 "
			+ "group by customer_id) s ON bill.customer_id = s.customer_id";

	
	public static String searchCustoemrQueryInActive = "select bill.customer_id, "
			+"bill.customer_name, "
			+"bill.contact_number, "
			+"bill.c_id, "
			+"s.vc_number, "
			+"bill.areacode, "
			+"case when bill.status = 1 then 'Active' when bill.status = 0 then 'InActive' end as status, "
			+"case when bill.permenent_status = 1 then 'InActive' when bill.permenent_status = 0 then 'Active' end as permenent_status, "
			+"bill.package_amount, "
			+"bill.addon_amount, "
			+"case when bill.paid_count>0 then \"UnPaid\" WHEN bill.paid_count < 0 THEN \"New\" else \"Paid\" end as paid_status, "
			+"(select "
			+"balance_amount "
			+"from "
			+"billing b "
			+"where "
			+"b.customer_id = bill.customer_id "
			+"order by row_id desc "
			+"limit 1) as balance, "
			+"s.discount, "
			+"if(bill.paid_count>0, (bill.package_amount +  (select "
			+"balance_amount "
			+"from "
			+"billing b "
			+"where "
			+"b.customer_id = bill.customer_id "
			+"order order by row_id"
			+"limit 1) - s.discount), (select "
			+"balance_amount "
			+"from "
			+"billing b "
			+"where "
			+"b.customer_id = bill.customer_id "
			+"order by row_id desc "
			+"limit 1)) as total "
			+"from "
			+"(SELECT "
			+"c.customer_id, "
			+"c.customer_name, "
			+"c.contact_number, "
			+"c.c_id, "
			+"c.areacode, "
			+"c.status, "
			+"c.permenent_status, "
			+"c.client_id, "
			+"ifnull(SUM(bill_gen.package_monthly_amount),0) package_amount, "
			+"ifnull(SUM(bill_gen.addon_monthly_amount),0) addon_amount, "
			+"CASE WHEN paid_status = 0 THEN 1 WHEN paid_status IS NULL THEN -1 ELSE 0 END AS paid_count "
			+"FROM "
			+"bill_generation bill_gen, customer c "
			+"WHERE "
			+"c.customer_id = bill_gen.customer_id  and "
			+" ( paid_status is not null and paid_status = 0 or null and month = case when month(current_date)=1 then 12 else month(current_date)-1 end ) "
			+"group by c.customer_id, c.customer_name, c.contact_number, c.c_id, c.areacode, c.status, c.permenent_status, paid_status= 0, month = month(current_date)) bill "
			+"join "
			+"(SELECT "
			+"customer_id, SUM(discount) discount, GROUP_CONCAT(vc_number) as vc_number "
			+"FROM "
			+"setupbox "
			+"group by customer_id) s ON bill.customer_id = s.customer_id";
	
	
	
	public static String deviceHistoryQuery = "select ed.employee_id, e.employee_name,ed.effective_from,ed.effective_to,ed.device_id, d.device_name, e.contact_number,d.ime_number,d.sim_number "+
												" from employee_device ed join"+
												" employee e" +
												" on ed.employee_id = e.employee_id join"+
												" device d"+
												" on ed.device_id = d.device_id"+
												" where ed.device_id = ? order by ed.effective_from desc";
	
	
	public static boolean exists(String string){
		return string!=null && !string.trim().equals("") && string.trim().length()>0;
	}
	
	public static List<EditCustomerCommentTo> covertTransferObject(List<EditCustomerCommentEntity> list){
		
		List<EditCustomerCommentTo> result = new ArrayList<EditCustomerCommentTo>();
		for(EditCustomerCommentEntity entity: list){
			
			EditCustomerCommentTo entityTo = new EditCustomerCommentTo();
			BeanUtils.copyProperties(entity, entityTo);
			result.add(entityTo);
		
		}
		return result;
		
	}
	
	public static boolean exitsInt(int id){
		return id!=0;
	}
	public static String getDateYYYYMMDD(String dateString){
		SimpleDateFormat formatter = new SimpleDateFormat("dd/MM/yyyy");
		SimpleDateFormat formatter1 = new SimpleDateFormat("yyyy-MM-dd");
		String dateInString = dateString;
		Date date = null;
		
		try {

			date = formatter.parse(dateInString);
			

		} catch (ParseException e) {
			e.printStackTrace();
		}
		return formatter1.format(date);
	}
	
	
}
