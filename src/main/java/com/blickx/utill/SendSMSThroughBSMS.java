package com.blickx.utill;

import java.io.BufferedReader;
import java.io.DataOutputStream;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.URL;
import java.util.ArrayList;
import java.util.List;

import org.apache.log4j.Logger;
import org.json.simple.JSONObject;
import org.json.simple.parser.JSONParser;

public class SendSMSThroughBSMS implements Runnable{
 
	static Logger logger = Logger.getLogger(SendSMSThroughBSMS.class);
    
	JSONObject requestObject;
	String clientId;

	@Override
	public void run() {
		
		 JSONObject jsonResponse = sendSms(requestObject);
		 logger.info(jsonResponse);
	}
	
	@SuppressWarnings("unchecked")                             //var-arg variable  sending single sms
	public SendSMSThroughBSMS(String clientId,String templateId,String mobileNumber,String... templateVars){
	
		JSONObject variableObj = new JSONObject();
		variableObj.put("mobNumber", mobileNumber);
		if(templateVars.length>0){
			this.clientId=clientId;
			JSONObject requestObject = new JSONObject();	
			requestObject.put("campaign", false);
			requestObject.put("campaignName", "");
			requestObject.put("messageBody", "");
			requestObject.put("sendBy", "");
			requestObject.put("templateId", templateId);
			                                                            //making variables json objects
	        List<JSONObject> variableList = new ArrayList<JSONObject>();	
			
			for(int i=0;i<templateVars.length;i++){
				variableObj.put("f"+(i+1), templateVars[i]);
			}
	        
			variableList.add(variableObj);
			 requestObject.put("variableValues",variableList);
			 this.requestObject=requestObject;
			 System.out.println(mobileNumber);
			}
	}
	
	@SuppressWarnings("unchecked")                      //bulk sms
	public SendSMSThroughBSMS(String clientId,String templateId,String campaignName,List<JSONObject> templateVars){
	
		this.clientId=clientId;
		JSONObject requestObject = new JSONObject();	
		requestObject.put("campaign", false);
		requestObject.put("campaignName", campaignName);
		requestObject.put("messageBody", "");
		requestObject.put("sendBy", "");
		requestObject.put("templateId", templateId);
		requestObject.put("variableValues",templateVars);
		this.requestObject=requestObject;
		
	}
	public JSONObject sendSms(JSONObject jsonObject)
	{
		JSONObject jsonResponse = new JSONObject();
       try { 

    	   
    	   logger.info("**********************************\t"+jsonObject);
    	   //Amtron projectId = 6
    	   
		String url = "http://139.59.2.194:8081/bsms/7/"+clientId+"/sendSms";
     // String url = "http://localhost:8081/bsms/2/117511/sendSms";
	     URL obj = new URL(url);
		HttpURLConnection con = (HttpURLConnection) obj.openConnection();      
  		con.setRequestMethod("POST");
		con.setDoOutput(true);
	 	con.setRequestProperty("Content-Type", "application/json");
        con.setRequestProperty("Accept", "application/json");
	 
		DataOutputStream wr = new DataOutputStream(con.getOutputStream());
		wr.writeBytes(jsonObject.toString());
		wr.flush();
		wr.close();

		int responseCode = con.getResponseCode();
		logger.info("\nSending 'POST' request to URL : " + url);
		logger.info("Response Code : " + responseCode);
		BufferedReader in = new BufferedReader(
		        new InputStreamReader(con.getInputStream()));
		String inputLine;
		StringBuffer response = new StringBuffer();

		while ((inputLine = in.readLine()) != null) {
			response.append(inputLine);
		}
		 JSONParser parser = new JSONParser();
         
		 jsonResponse = (JSONObject) parser.parse(response.toString());
			in.close();
		 
			logger.info( "BSMS API called successfully");
		   
		      } catch (IOException e) {  
	             e.printStackTrace();  
	            } 
	            catch (Exception e) {
			    e.printStackTrace();
	            }
       return jsonResponse;
       }

	
	

}