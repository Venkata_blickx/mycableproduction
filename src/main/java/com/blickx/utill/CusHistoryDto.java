package com.blickx.utill;

import java.util.Date;

import com.fasterxml.jackson.databind.annotation.JsonSerialize;

public class CusHistoryDto {
	
	private String customerId;
	
	private String cId;
	
	private String customerName;
	
	private String boxNumber;
	
	private String cusStatus;
	
	private Date statusDate;
	
	private String empName;

	public String getCustomerId() {
		return customerId;
	}

	public void setCustomerId(String customerId) {
		this.customerId = customerId;
	}

	public String getcId() {
		return cId;
	}

	public void setcId(String cId) {
		this.cId = cId;
	}

	public String getCustomerName() {
		return customerName;
	}

	public void setCustomerName(String customerName) {
		this.customerName = customerName;
	}

	public String getBoxNumber() {
		return boxNumber;
	}

	public void setBoxNumber(String boxNumber) {
		this.boxNumber = boxNumber;
	}

	public String getCusStatus() {
		return cusStatus;
	}

	public void setCusStatus(String cusStatus) {
		this.cusStatus = cusStatus;
	}

	@JsonSerialize(using = DateSerializerTimeStamp.class)
	public Date getStatusDate() {
		return statusDate;
	}

	@Override
	public String toString() {
		return "CusHistoryDto [customerId=" + customerId + ", cId=" + cId
				+ ", customerName=" + customerName + ", boxNumber=" + boxNumber
				+ ", cusStatus=" + cusStatus + ", statusDate=" + statusDate
				+ ", empName=" + empName + "]";
	}

	public void setStatusDate(Date statusDate) {
		this.statusDate = statusDate;
	}

	public String getEmpName() {
		return empName;
	}

	public void setEmpName(String empName) {
		this.empName = empName;
	}
	
	

}
