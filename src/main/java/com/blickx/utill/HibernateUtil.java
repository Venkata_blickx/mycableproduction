package com.blickx.utill;

import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;

public class HibernateUtil {

	@Autowired
	public static SessionFactory sfactory;
	
	public static Session getSession(){
		Session session= sfactory.openSession();
		
		return session;
	}
}
