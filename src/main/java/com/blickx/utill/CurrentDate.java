package com.blickx.utill;

import java.util.Date;

import com.fasterxml.jackson.databind.annotation.JsonSerialize;

public class CurrentDate {
	
	
	private Date curDate;

	@JsonSerialize(using = DateSerializerTimeStamp.class)
	public Date getCurDate() {
		return curDate;
	}

	public void setCurDate(Date curDate) {
		this.curDate = curDate;
	}

	@Override
	public String toString() {
		return "CurrentDate [curDate=" + curDate + "]";
	}
	
	

}
