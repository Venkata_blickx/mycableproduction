package com.blickx.utill;

public class RestURIConstants {
	public static final String HOME = "/";
	public static final String GET_REPORT = "/generate_report"; //used in DashBoardController
	public static final String GET_DAILY_REPORT = "/DashboardDailyCollectionSummary"; //used in Reports Controller
	public static final String GET_CUSTOMER_REPORT = "getCustomerReport";  //used in Reports Controller
    
	public static final String ACCESS_DENIED = "/denied";
	public static final String INTERNAL_SERVER_ERROR = "/internal_error";
	public static final String NOT_FOUND = "/not_found";
	
    
    public static final String ADD_CUSTOMER = "/add_customer";
    public static final String DELETE_CUSTOMER = "/delete_customer";
    public static final String UPDATE_CUSTOMER = "/update_customer";
    public static final String ACTIVATE_CUSTOMER = "/activate_customer";
    public static final String DEACTIVATE_CUSTOMER = "/deactivate_customer";
    public static final String SEARCH_CUSTOMER = "/search_customer";
    public static final String GET_CUSTOMER_BY_ID = "/get_customer_by_id";
    public static final String GET_CUSTOMER_BY_NAME = "/get_customer_by_name";
    public static final String ANDROID_TO_MYSQL_SYNC_URL = "/sync_db";
    
    public static final String ADMIN_LOGIN_URL = "/alogin";
    public static final String EMPLOYEE_LOGIN_URL = "/accounts/elogin";
}
