package com.blickx.utill;

import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.Date;

import com.fasterxml.jackson.core.JsonGenerator;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.JsonSerializer;
import com.fasterxml.jackson.databind.SerializerProvider;

public class DateSerializerForAndroid extends JsonSerializer<Date> {  
    
    /* (non-Javadoc)  
     * @see org.codehaus.jackson.map.JsonSerializer#serialize(java.lang.Object, org.codehaus.jackson.JsonGenerator, org.codehaus.jackson.map.SerializerProvider)  
     */  
    @Override  
    public void serialize(Date value_p, JsonGenerator gen, SerializerProvider prov_p)  
      throws IOException, JsonProcessingException  
    {  
      SimpleDateFormat formatter = new SimpleDateFormat("dd-MM-yyyy");  
      String formattedDate = formatter.format(value_p);  
      gen.writeString(formattedDate);  
    }  
  }  