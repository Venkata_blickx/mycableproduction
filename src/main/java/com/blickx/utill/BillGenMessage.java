package com.blickx.utill;

public class BillGenMessage {
	
	private String billGenStatus;

	public String getBillGenStatus() {
		return billGenStatus;
	}

	public void setBillGenStatus(String billGenStatus) {
		this.billGenStatus = billGenStatus;
	}

	@Override
	public String toString() {
		return "BillGenMessage [billGenStatus=" + billGenStatus + "]";
	}
	

}
