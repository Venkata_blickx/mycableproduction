package com.blickx.utill;

import java.util.List;

public class CustomerHistoryDto {

	private List<CusHistoryDto> cusHistory;

	public List<CusHistoryDto> getCusHistory() {
		return cusHistory;
	}

	public void setCusHistory(List<CusHistoryDto> cusHistory) {
		this.cusHistory = cusHistory;
	}

	@Override
	public String toString() {
		return "CustomerHistoryDto [cusHistory=" + cusHistory + "]";
	}
	
	
}
