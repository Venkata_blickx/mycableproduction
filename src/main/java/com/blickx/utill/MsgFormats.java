package com.blickx.utill;

public class MsgFormats {

	public static String getPayedSMS(double paid_amount, String recipt_no, String client_name){
		
		String PayedMsg = "Dear Customer, Thank you for your payment of Rs "+paid_amount+
							" towards Cable Tv Subscription, Receipt No "+recipt_no +"\nRegards,\n"+client_name+".";
		return PayedMsg;
		
	}
	
	public static String getClosedComplaintSMS(String complaint_no, String client_name){
		
		String PayedMsg = "Dear Customer, your complaint no:" + complaint_no +
							" towards Cable Tv is resolved "+ "\nRegards,\n"+client_name+".";
		return PayedMsg;
		
	}
	
	public static String getComplaintRegSMS( String comp_no, String client_name){
		
		System.out.println(comp_no);
		System.out.println(comp_no.substring(8));
		
		String CompMSG = "Dear Customer, We have received your complaint, Ref No "+comp_no.substring(8)+
							" we shall resolve shortly. Sorry for the inconvenience.\nRegards,\n"+client_name+".";
		
		System.out.println("\tLenght of the msg :"+CompMSG.length());
		
		return CompMSG;	
		
	}
}
