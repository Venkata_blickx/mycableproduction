package com.blickx.utill;

import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.GregorianCalendar;
import java.util.List;
import java.util.concurrent.TimeUnit;

public class SimpleUtill {
	
	private static final int HOURS_IN_A_DAY = 24;
	private static final int MINUTES_IN_AN_HOUR = 60;
    private static final int SECONDS_IN_A_MINUTE = 60;
    
    private static int hoursToSeconds(int hours) {
        return hours * MINUTES_IN_AN_HOUR * SECONDS_IN_A_MINUTE;
    }

    private static int minutesToSeconds(int minutes) {
        return minutes * SECONDS_IN_A_MINUTE;
    }
	
	public static String timeConversion(int seconds) {
		
		/*int days = hours / HOURS_IN_A_DAY;
        int hours = totalSeconds / MINUTES_IN_AN_HOUR / SECONDS_IN_A_MINUTE;
        int minutes = (totalSeconds - (hoursToSeconds(hours))) / SECONDS_IN_A_MINUTE;
        int seconds = totalSeconds - ((hoursToSeconds(hours)) + (minutesToSeconds(minutes)));*/
		
		 int days = (int)TimeUnit.SECONDS.toDays(seconds);        
		 long hours = TimeUnit.SECONDS.toHours(seconds) - (days *24);
		 long minutes = TimeUnit.SECONDS.toMinutes(seconds) - (TimeUnit.SECONDS.toHours(seconds)* 60);
		 long second = TimeUnit.SECONDS.toSeconds(seconds) - (TimeUnit.SECONDS.toMinutes(seconds) *60);
        
        if(days > 0){
        	return days + " days " + hours + " hrs";
        }else if(hours > 0){
        	return hours + " hrs " + minutes + " mins " ;
        }else if(minutes > 0){
        	return  minutes + " mins " ;
        }else{
        	return seconds + " secs";
        }
    }
	
public static void main(String[] args) throws ParseException {
	/*SimpleDateFormat sdf = new SimpleDateFormat("dd-MM-yyyy");
	Calendar cal = Calendar.getInstance();
	//Date date=cal.getTime();
	
	Date date = sdf.parse("01-08-2015");
	for (int i=0;i<5;i++){
	   cal.add(Calendar.MONTH,1);
	   //date=cal.getTime();
	   System.out.println(sdf.format(date));
	}*/
	
	
	System.out.println(timeConversion(24*60*60+2*60*60));
	
	
	
	
	/*SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd");
	Date date = dateFormat.parse("2014-1-10");
	Date today = new Date();
	long diff = today.getTime()-date.getTime();

	int days = (int) (diff/(1000*60*60*24));
	//System.out.println(days/30);
    Calendar cal = Calendar.getInstance();
    cal.setTime(date);
    cal.set(cal.get(Calendar.YEAR), (cal.get(Calendar.MONTH)+24) , cal.get(Calendar.DAY_OF_MONTH));
    System.out.println(cal.getTime());
    System.out.println(dateFormat.format(cal.getTime()).toString());*/
    
   // System.out.println(cal.get(Calendar.YEAR)+"-"+ ((cal.get(Calendar.MONTH))==0?12:cal.get(Calendar.MONTH)) +"-"+ cal.get(Calendar.DAY_OF_MONTH));
    
    
    /*
    
	SimpleDateFormat sdf = new SimpleDateFormat("EEEE");
	SimpleDateFormat sdf2 = new SimpleDateFormat("yyyy-MM-dd");
    
	Calendar cal = Calendar.getInstance();
    
    Date date=cal.getTime();
	List<String> dates = new ArrayList<String>();
	System.out.println(sdf.format(date) + "\t" + sdf2.format(date));
	
	for (int i=0;i<6;i++){
	   cal.add(Calendar.DAY_OF_MONTH, -1);
	   
	   date=cal.getTime();
	   System.out.println(sdf.format(date) + "\t" + sdf2.format(date));
	   dates.add(""+sdf.format(date));
	}
    
    */
    
    
	
}
	
/*static String gets(){
	return "PKG-";
}	*/
}
