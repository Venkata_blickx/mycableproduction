package com.blickx.utill;

import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.Date;

import com.fasterxml.jackson.core.JsonGenerator;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.JsonSerializer;
import com.fasterxml.jackson.databind.SerializerProvider;

public class DateSerializerForComplaintsList  extends JsonSerializer<Date> {

	 @Override  
	    public void serialize(Date value_p, JsonGenerator gen, SerializerProvider prov_p)  
	      throws IOException, JsonProcessingException  
	    {
		 //formatter.setTimeZone(TimeZone.getTimeZone("UTC"));
		   // Prints the date in the CET timezone
		  // System.out.println("UTC"+formatter.format(value_p));
		   // Set the formatter to use a different timezone
		   //formatter.setTimeZone(TimeZone.getTimeZone("IST"));
		   // Prints the date in the IST timezone
		  // System.out.println("IST"+formatter.format(value_p));
		 	
	      SimpleDateFormat formatter = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
	     
	      String formattedDate = formatter.format(value_p);
	      gen.writeString(formattedDate);  
	    }  
}