package com.blickx.controller;

import java.util.HashMap;
import java.util.Map;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

import com.blickx.domain.AreaEntity;
import com.blickx.services.AreaService;
import com.blickx.settings.to.AreaEntityTo;

@RestController
@RequestMapping("/service/{client_id}/area")
public class AreaController {

	@Autowired
	AreaService areaService;
	
	private Logger logger = Logger.getLogger(AreaController.class);

	@RequestMapping(value = "/addAreaDetails", method = RequestMethod.POST, headers = "Accept=application/json")
	public @ResponseBody Map<String, Object> addAreaDetails(@PathVariable("client_id")String client_id, @RequestBody AreaEntity area) {

		logger.info("addAreaDetails INPUT: " + area);
		area.setClientId(client_id);
		Map<String, Object> response = new HashMap<String, Object>();
		String resMSG = areaService.addAreaDetails(area);
		if(resMSG.equalsIgnoreCase("success")){
			response.put("Status",new Boolean(true));
			response.put("responseMSG", "Area added Successfully");
		}else{
			response.put("Status", new Boolean(false));
			response.put("responseMSG", resMSG);
		}
		return response;
		
	}
	
	@RequestMapping(value = "/getAreaDetailsById", method = RequestMethod.POST, headers = "Accept=application/json")
	public @ResponseBody AreaEntity getAreadDetailsById(@RequestBody AreaEntity area) {

		logger.info("addAreaDetails INPUT: " + area);
		//Map<String, String> response = new HashMap<String, String>();
		return areaService.getAreaDetailsById(area);
		
		
	}
	
	@RequestMapping(value = "/updateAreaDetails", method = RequestMethod.POST, headers = "Accept=application/json")
	public @ResponseBody Map<String, Object> updateAreaDetails(@PathVariable("client_id")String client_id, @RequestBody AreaEntity area) {

		logger.info("addAreaDetails INPUT: " + area);
		
		Map<String, Object> response = new HashMap<String, Object>();
		String resMSG = areaService.updateAreaDetails(area);
		if(resMSG.equalsIgnoreCase("success")){
			response.put("Status", new Boolean(true));
			response.put("responseMSG", "Area updated successfully");
		}else{
			response.put("Status", new Boolean(false));
			response.put("responseMSG", resMSG);
		}
		return response;
		
	}
	

	@RequestMapping(value = "/deleteAreaDetails", method = RequestMethod.POST, headers = "Accept=application/json")
	public @ResponseBody Map<String, String> deleteAreaDetails(@PathVariable("client_id")String client_id, @RequestBody AreaEntityTo area) {

		logger.info("addAreaDetails INPUT: " + area);
		
		Map<String, String> response = new HashMap<String, String>();
		String resMSG = areaService.deleteAreaDetails(area);
		if(resMSG.equalsIgnoreCase("success")){
			response.put("Status", 1 + "");
			response.put("responseMSG", "Area added Successfully");
		}else{
			response.put("Status", 0 + "");
			response.put("responseMSG", resMSG);
		}
		return response;
		
	}
	
	@RequestMapping(value = "/getAllAreaDetails", method = RequestMethod.POST, headers = "Accept=application/json")
	public @ResponseBody List<AreaEntityTo> getAllAreaDetails(@PathVariable("client_id")String client_id) {

		logger.info("addAreaDetails INPUT: " + client_id);
		return areaService.getAllAreaDetails(client_id);
		
	}
}
