 package com.blickx.controller;

import java.text.ParseException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.log4j.Logger;
import org.hibernate.HibernateException;
import org.hibernate.Query;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.springframework.beans.BeansException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import com.blickx.mycable.customerAddElements.CustomerAddInputTo;
import com.blickx.mycable.customerAddElements.ManipulateCustomerData;
import com.blickx.output.wrapper.UpdateContactResponseTO;
import com.blickx.response.CustomerBillToOnlinePayment;
import com.blickx.response.PackagesAndAddonsOfCustomer;
import com.blickx.response.ResponseObject;
import com.blickx.searchEntities.PaymentFields;
import com.blickx.searchEntities.SearchCustomer;
import com.blickx.dao.UserDao;
import com.blickx.dao.UtillDAO;
import com.blickx.domain.Authentication;
import com.blickx.domain.CustomerBillPayment;
import com.blickx.domain.CustomerBillPaymentWrapper;
import com.blickx.domain.CustomerToMobile;
import com.blickx.domain.SetupBox;
import com.blickx.exceptions.CustomerDeletionFailed;
import com.blickx.services.AuthenticationService;
import com.blickx.services.CustomerService;
import com.blickx.services.DeviceService;
import com.blickx.services.EmployeeService;
import com.blickx.settings.to.AddonsEntityTo;
import com.blickx.to.CustomerBillTo;
import com.blickx.to.CustomerTo;
import com.blickx.to.DesktopPaymentResponse;
import com.blickx.to.MiniStatementForAndroidTo;
import com.blickx.to.SearchCustomerMoreDetails;
import com.blickx.to.SearchCustomerTo;
import com.blickx.to.SetUpBoxToo;
import com.blickx.to.SetUpBoxWrapper;
import com.blickx.to.SetupBoxTo;
import com.blickx.to.SetupBoxUpdateWrapper;
import com.blickx.to.SynchResponse;
import com.blickx.to.ViewBillsByMonthTo;
import com.blickx.to.input.CustomerCredentials;
import com.blickx.to.input.InputIds;
import com.blickx.to.input.MakePaymentDetailsNonSynch;
import com.blickx.to.input.SearchCriterias;
import com.blickx.to.input.UidInsertInput;
import com.blickx.utill.BillGenMessage;
import com.blickx.utill.BillGenType;
import com.blickx.utill.CommonUtils;
import com.blickx.utill.CusHistoryDto;
import com.blickx.utill.CustomerHistoryDto;
import com.blickx.utill.RestURIConstants;
import com.blickx.wrapper.AddonsWrapper;
import com.blickx.wrapper.UpdateAddonsWrapper;
import com.blickx.wrapper.UpdateContactNumberWrapper;

//@Controller
@RestController
@RequestMapping("/service/{client_id}/customer")
public class CustomerController {

	@Autowired
	CustomerService cservice;
	
	@Autowired
	EmployeeService eService;
	
	@Autowired
	DeviceService deviceService;
	
	@Autowired
	UserDao udao;
	
	@Autowired
	UtillDAO utilDao;

	@Autowired
	SessionFactory sfactory;

	
	public static final String AUTHENTICATION_HEADER = "Authorization";

	@Autowired
	public AuthenticationService authService;

	private Logger logger = Logger.getLogger(CustomerController.class);
	
	/*
	 * 
	 * 					Customer Adding
	 * 
	 */
	@RequestMapping(value = "/customerAdd", method = {RequestMethod.POST, RequestMethod.GET}, headers = "Accept=application/json"  )
	public @ResponseBody ResponseObject addCustomer(@PathVariable("client_id")String client_id, @RequestBody CustomerAddInputTo customer) throws ParseException {
		
		logger.info("CustomerController - addCustomer input customer Data :" + customer);
		customer.setClient_id(client_id);
		ResponseObject response = new ResponseObject();
		String res = cservice.addCustomer(client_id, ManipulateCustomerData.getCustomerTo(customer));
			if(res.equalsIgnoreCase("Customer added successfully!")){
				response.setResponseMsg("Customer added successfully!");
				response.setDataValid(true);
			}else{
				response.setResponseMsg(res);
				response.setDataValid(false);
			}
		return response;
		
	}
	
	/*@RequestMapping(value = "/customerAdding", method = {RequestMethod.POST,RequestMethod.GET}, headers = "Accept=application/json",  produces = "application/json")
	public @ResponseBody int addCustomers(@PathVariable("client_id")String client_id,@RequestBody GetCustomer customer) {
		
		logger.info("CustomerController - addCustomer input customer Data :" + customer);
		return cservice.addCustomer(client_id, customer);
		
	}*/
	
	/*
	 * 
	 *					Search Customer Grid
	 *
	 */
	@RequestMapping(value = "/searchCustomerTo", method = RequestMethod.POST, headers = "Accept=application/json")
	public List<SearchCustomerTo> searchCustomerTo(@PathVariable("client_id")String client_id,
			@RequestBody SearchCustomer searchCustomer) {
		
		logger.debug("Customer Controller - /searchCustomerTo INPUT : searchCredentials" + searchCustomer);
		
		return cservice.searchCustomerTo(client_id, searchCustomer);
		
	}
	
	
	@RequestMapping(value = "/getCustomerMoreDetails", method = RequestMethod.GET, headers = "Accept=application/json")
	public List<SearchCustomerMoreDetails> getCustomerMoreDetails(@PathVariable("client_id")String client_id, @RequestParam String customerId) {
		
		logger.debug("Customer Controller - /getCustomerMoreDetails INPUT : searchCredentials" + customerId);
		return cservice.getCustomerMoreDetails(customerId);
		
	}
	
	/*
	 * Permenent Deactive list
	 */
	
	@RequestMapping(value = "/permenentDeactiveList", method = RequestMethod.POST, headers = "Accept=application/json")
	public List<SearchCustomerTo> permenentDeactiveList(@PathVariable("client_id")String client_id,
			@RequestBody SearchCustomer searchCustomer) {
		
		logger.debug("Customer Controller - /searchCustomerTo INPUT : searchCredentials" + searchCustomer);
		
		return cservice.permenentDeactiveList(client_id);
		
	}
	
	/*
	 * 
	 * 		Getting customer details by Customer_id from webApp Search customer grid, when click on edit/view 
	 * 
	 * 		Saving in temporary table called selected_customer
	 * 
	 *		Get_Unique_CustomerProfile Service will send the selected Customer data to WebClient
	 */
	//@PostAuthorize()
	@RequestMapping(value = "/Customer_Details_byID", method = RequestMethod.POST, headers = "Accept=application/json")
	public @ResponseBody CustomerAddInputTo getCustomerByIdd(
			@RequestBody InputIds input, HttpServletRequest req, HttpServletResponse response) {
		
		//logger.info(req.);
		
		logger.info("Customer Controller - /Customer_Details_byID INPUT is customer_id : " + input.getCustomer_id());
		return cservice.getCustomerProfileById(input.getCustomer_id().trim());

	}
	
	@RequestMapping(value = "/Get_Unique_CustomerProfile", method = RequestMethod.POST, headers = "Accept=application/json")
	public @ResponseBody CustomerTo getCustomerById(@PathVariable String client_id) {

		logger.info("Customer Controller - /Get_Unique_CustomerProfile INPUT is client_id : " + client_id);
		return cservice.getUniqueCustomerProfile(client_id);

	}
	
	/*
	*
	*		Update Customer Profile Data
	*
	*/
	
	@RequestMapping(value = "/customerUpdate", method = RequestMethod.POST, headers = "Accept=application/json", consumes = "application/json", produces = "application/json")
	public @ResponseBody ResponseObject updateCusomer(
			@RequestBody CustomerAddInputTo input, @PathVariable String client_id) throws BeansException, ParseException {

		logger.info("Customer Controller - /Get_Unique_CustomerProfile INPUT is updatedCustomer Data : " + input);
		input.setClient_id(client_id);
		System.out.println(client_id);
		ResponseObject response = new ResponseObject();
		if (input != null) {
			
			String res = cservice.updateCustomer(input);
			
			if(res.equalsIgnoreCase("1")){
				response.setResponseMsg("Customer Updated successfully!");
				response.setDataValid(true);
			}else{
				response.setResponseMsg(res);
				response.setDataValid(false);
			}
		}
		return response;
	}
	
	/*
	*
	*		Activate Customer
	*
	*/
	
	@RequestMapping(value = "/activate", method = RequestMethod.POST, headers = "Accept=application/json")
	public String activateStatus(@RequestBody InputIds input) {

		String res = "Given Id is Null";
		String customer_id = input.getCustomer_id();
		
		logger.debug("Customer Controller - /activate INPUT : customer_id " + customer_id);
		
		if (customer_id != null && customer_id.trim().length() != 0) {
			
			res = cservice.activateStatus(input);
			return res;
			
		}
		
		logger.debug("Customer Controller - /activate OUTPUT : " + res);
		return res;
	}

	/*
	*
	*		DeActivate Customer
	*
	*/
	
	@RequestMapping(value = "/deactivate", method = RequestMethod.POST, headers = "Accept=application/json")
	public String changeToDeactivate(@RequestBody InputIds input) {

		String res = "Given Id is Null";
		String customer_id = input.getCustomer_id();
		logger.debug("Customer Controller - /deactivate INPUT : " + customer_id);
	
		if (CommonUtils.exists(customer_id)) {
			res = cservice.deActivateStatus(input);
			return res;
		}
		
		logger.debug("Customer Controller - /deactivate OUTPUT : " + res);
		return res;
	}
	
	/*
	*
	*		Permanent DeActivate Customer
	*
	*/
	
	@RequestMapping(value = "/permanentDeActive", method = RequestMethod.POST, headers = "Accept=application/json")
	public @ResponseBody String simplePost(@RequestBody InputIds input) {
		
		logger.debug("Customer Controller - /permanentDeactivate INPUT : " + input);
		
		int res = cservice.permenentDeActive(input);
		
		if(res == 1){
			return "Customer Deactivated Permenently";
		}
		return "There was some problem! Please contact Server Admin";
		
	}
	
	/*
	*
	*		Get SetTopBoxes Based on Customer_id
	*
	*/
	
	@RequestMapping(value = "/getSetupBoxByCustomerId", method = RequestMethod.POST, headers = "Accept=application/json")
	public @ResponseBody List<SetUpBoxToo> getTheSetupboxesByCustomerId(@RequestBody InputIds input) {
		
		logger.debug("Customer Controller - /getSetupBoxByCustomerId INPUT customer_id : " + input.getCustomer_id());
		return cservice.getTheSetupboxesByCustomerId(input.getCustomer_id());
		
	}
	
	/*
	*
	*		Add SetupBox
	*
	*/
	
	@RequestMapping(value = "/addSetTopBox", method = {RequestMethod.POST,RequestMethod.GET}, headers = "Accept=application/json",  produces = "application/json")
	public @ResponseBody int addExtraSetupbox(@PathVariable("client_id") String client_id, @RequestBody SetupBoxTo setupBoxTo) {
		
		logger.debug("Customer Controller - /addSetTopBox INPUT setTopBox Details : " + setupBoxTo);
		setupBoxTo.setClient_id(client_id);
		return cservice.addExtraSetupBox(setupBoxTo);

	}
	
	/*
	*
	*		Get SetTopBox By SetTopBoxNumber
	*
	*/
	
	@RequestMapping(value = "/getSettopBoxByBoxNumber", method = RequestMethod.POST, headers = "Accept=application/json")
	public @ResponseBody SetUpBoxToo getSettopBoxByBoxNumber(@RequestBody InputIds input) {
		
			logger.debug("Customer Controller - /getSettopBoxByBoxNumber INPUT box_number : " + input.getBox_number());
			return cservice.getSettopBoxByBoxNumber(input.getBox_number());
			
	}
	
	/*
	*
	*		SetTopBoxUpdate
	*
	*/
	
	@RequestMapping(value = "/boxUpdate", method = RequestMethod.POST, headers = "Accept=application/json")
	public @ResponseBody String updateBox(@ModelAttribute SetupBoxTo setupBoxWrapper) {
		
		logger.debug("Customer Controller - /boxUpdate INPUT setTopBox Details : " + setupBoxWrapper);
		return cservice.updateBox(setupBoxWrapper);
		
	}
	
	/*
	 * 
	 * 		SetTopBox Activate and Deactivate
	 * 
	 */
	
	@RequestMapping(value = "/setTopBoxDeActivate", method = RequestMethod.POST, headers = "Accept=application/json")
	public @ResponseBody int deActivateBox(@RequestBody InputIds input) {
		
		logger.debug("");
		return cservice.deActivateBox(input);
		
	}
	
	@RequestMapping(value = "/setTopBoxActivate", method = RequestMethod.POST, headers = "Accept=application/json")
	public @ResponseBody int ActivateBox(@RequestBody InputIds input) {
		return cservice.activateBox(input);
	}
	
	/*
	 * 
	 * 		MakePayment From WebApp
	 * 
	 */
	
	@RequestMapping(value = "/paymentFromDesktop", method = RequestMethod.POST, headers = "Accept=application/json")
	public @ResponseBody DesktopPaymentResponse paymentFromDesktop(
			@RequestBody PaymentFields paymentfields, @PathVariable String client_id) {
		logger.info("CustomerController for BillPayment Payment from desktop Payment Fields : " + paymentfields);
		DesktopPaymentResponse res = new DesktopPaymentResponse();
		
		try{
			paymentfields.setClient_id(client_id);
			String result = cservice.paymentFromDesktop(paymentfields);
			res.setReceipt(result);
			res.setStatus("SUCCESS");
		}catch(Exception e){
			res.setStatus("FAIL");
		}
		return res;
		
	}
	
	
	
	
	@RequestMapping(value = "/getCustomerProfileById", method = RequestMethod.POST, headers = "Accept=application/json")
	public @ResponseBody CustomerTo getCustomerById(@RequestBody InputIds input) {

		//Session session = sfactory.openSession();
		//return (Customer) session.createQuery("from Customer c where c.customer_id='"+customer_id+"'").list().get(0);
		return cservice.getCustomerById(input.getCustomer_id());

	}

	/*
	 * 
	 * Desktop Service
	 * 
	 *  Nfc Card value will be inserted for the first time when assigned to customer into the customer table 
	 * customer_uid column
	 * 
	 */

	@RequestMapping(value = "/uidInsert", method = RequestMethod.GET, headers = "Accept=application/json")
	public @ResponseBody int uidUpdate(@ModelAttribute UidInsertInput uidInsertInput) {
		
		return cservice.uidUpdate(uidInsertInput);
		
	}
	
	

	/*
	 * Android Service
	 * 
	 * Fetching the customer billing details in Android App
	 * 
	 */
	
	
	@RequestMapping(value = "/getCustomer", method = RequestMethod.GET, headers = "Accept=application/json")
	public @ResponseBody CustomerToMobile getCustomerForMobile(@PathVariable("client_id")String client_id,
			@ModelAttribute("searchCustomer") SearchCustomer searchCustomer) {
		CustomerToMobile customerTO = new CustomerToMobile();
		String customer_id = "";
		
		logger.info(searchCustomer);

		if (CommonUtils.exists(searchCustomer.getCustomer_id())) {
			
			customerTO = cservice.getCustomerWithBillingDetails(searchCustomer.getCustomer_id());

		}
		
		if (CommonUtils.exists(searchCustomer.getCustomer_uid())) {
			
			customer_id = cservice.getCustomer_id(searchCustomer.getCustomer_uid(), client_id);
			customerTO = cservice.getCustomerWithBillingDetails(customer_id);

		} if (CommonUtils.exists(searchCustomer.getContact_number())) {

			customer_id = cservice.getCustomerIDUsingCid(client_id, searchCustomer);
				if (CommonUtils.exists(customer_id)) {
					//SearchCustomerTo customerTo = (SearchCustomerTo) cservice.searchCustomerTo(searchCustomer).get(0);
					return cservice.getCustomerWithBillingDetails(customer_id);
				}
		}
		
		if(CommonUtils.exists(searchCustomer.getC_id())){
			
			customer_id = cservice.getCustomerIDUsingCid(client_id, searchCustomer);
			if (CommonUtils.exists(customer_id)) {
				
				return cservice.getCustomerWithBillingDetails(customer_id);
			}
		}
		return customerTO;
	}

	@RequestMapping(value = "/getCustomerBilling", method = RequestMethod.POST, headers = "Accept=application/json")
	public CustomerBillTo getCustomerBillingDetailsForDesktop(@PathVariable String client_id,
			@RequestBody CustomerTo customerCredentials) {

			customerCredentials.setClient_id(client_id);
		return cservice.getCustomerWithBillingDetails(customerCredentials);

	}
	
	@RequestMapping(value = "/getPackageAndAddonDetailsOfCustomer", method = RequestMethod.POST, headers = "Accept=application/json")
	public @ResponseBody Map<String, List> getPackageAndAddonDetailsOfCustomer(@RequestBody InputIds input, @PathVariable String client_id) {
		logger.info("/getAddonsOfCustomer - controller input: " + input);
		List<PackagesAndAddonsOfCustomer> addOns = new ArrayList<PackagesAndAddonsOfCustomer>();
		Map<String, List> response = new HashMap<String, List>();
		addOns = cservice.getTheCustomerAddons(input.getC_id(), client_id);
		response.put("addOns", addOns);
		logger.info("/getAddonsOfCustomer - controller output: " + response);
		return response;
	}
	
	
	
	///online payment bill details fetch
	
	@RequestMapping(value = "/getCustomerBillingDetailsforOnlinePayment", method = RequestMethod.POST, headers = "Accept=application/json")
	public CustomerBillToOnlinePayment getCustomerBillingDetailsforOnlinePayment(@PathVariable String client_id,
			@RequestBody Map<String, String> input) {
		
		CustomerBillToOnlinePayment response = new CustomerBillToOnlinePayment();
		
		List<PackagesAndAddonsOfCustomer> planDetails = new ArrayList<PackagesAndAddonsOfCustomer>();
		CustomerTo searchType = new CustomerTo();
		logger.info("getCustomerBillingDetailsforOnlinePayment input : " + input);
		
		Integer res;
		if(input.get("search_in").equalsIgnoreCase("contact_number")) {
			res = (Integer) utilDao.getStatus (client_id, input.get("search_term"));
			logger.info("Response after checking contact number : " + res.intValue());
		} else {
			res = new Integer(0);
		}
		
		logger.info("Response  : " + res.intValue());
		if(res.intValue() == 0) {
		
			if(input.get("search_in").equalsIgnoreCase("vc_number")) {
				searchType.setVc_number(input.get("search_term"));
			} else {
				searchType.setContact_number(input.get("search_term"));
			}
			CustomerBillTo billDetails = cservice.getCustomerWithBillingDetails(searchType);
			planDetails = cservice.getTheCustomerAddons(billDetails.getC_id(), billDetails.getClient_id());
				if(CommonUtils.exists(billDetails.getClient_id())){
				Authentication auth = getClientInfo(billDetails.getClient_id());
				 response = new CustomerBillToOnlinePayment();
				
					response.setCustomerBill(billDetails);
					logger.info("Got Customer Bill Details " + response);
					response.setPlanDetails(planDetails);
					logger.info("Got Customer Plan Details " + response);
						Map<String, String> authDetails = new HashMap<String, String>();
						authDetails.put("clientName", auth.getClient_name());
						authDetails.put("clientAddress", auth.getAddress());
						authDetails.put("clientContactNumber", auth.getClientContactNumber());
					response.setClientInformations(authDetails);
					logger.info("Got Customer Client Details " + response);
				
				}

				response.setResponseCode("0");
				response.setErrorMsg("Fethced Data");
		} else if(res.intValue() == 1) {
			response.setResponseCode("1");
			response.setErrorMsg("Mobile number entere is registered with multiple account ids, kindly search with Account ID");
		} else if(res.intValue() == 2) {
			response.setResponseCode("1");
			response.setErrorMsg("Sorry! couldn't find details with given data");
			
		}
			
		return response;//cservice.getCustomerWithBillingDetails(customerCredentials);

	}
	
	
	/*
	 * Online Payment Transaction save
	 */
	
	@RequestMapping(value = "/saveTxDetails", method = RequestMethod.POST, headers = "Accept=application/json")
	public @ResponseBody Map<String, String> saveTx(@RequestBody Map<String, String> input) throws ParseException {
		logger.info("/saveTxDetails - controller input: " + input);
		Map<String, String> response = new HashMap<String, String>();
		cservice.saveOnlineTxDetails(input);
		response.put("ResponseCode", "0");
		response.put("ResponseMsg", "Success");
		
		logger.info("/saveTxDetails - controller output: " );
		return response;
	}
	
	@RequestMapping(value = "/saveTransaction", method = RequestMethod.POST, headers = "Accept=application/json")
	public @ResponseBody Map<String, String> saveTransaction(@RequestBody Map<String, String> input) throws ParseException {
		logger.info("/saveTransaction - controller input: " + input);
		logger.info(">>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>" + input);
		
		Map<String, String> response = new HashMap<String, String>();
		logger.info("/saveTransaction - : ResponseCode " + input.get("ResponseCode"));
		if(input.get("ResponseCode").equalsIgnoreCase("0")) {
			cservice.updateTxAfterEBSResponse(input);
			
			logger.info("Payment Successfull ....");			
				
			
		} else {
			cservice.updateTxAfterEBSResponse(input);
		}
		response.put("ResponseCode", input.get("ResponseCode"));
		response.put("ResponseMessage", input.get("ResponseMessage"));
		
		logger.info("<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<< " );
		logger.info("/saveTransaction - controller input: " + input);
		return response;
	}

	/**
	 * @param input
	 * @return
	 * @throws HibernateException
	 */
	private Authentication getClientInfo(String client_id)
			throws HibernateException {
		
		Authentication auth = new Authentication();
		Session session = sfactory.openSession();
		try {

			logger.info("getting client Information for billdetails for onlinepaymet");


			// Query using Hibernate Query Language
			String SQL_QUERY = " from Authentication a where a.client_id = ? and a.role_id = 1";
			Query query = session.createQuery(SQL_QUERY);
				query.setParameter(0, client_id);
			 auth = (Authentication) query.list().get(0);
			
			

		} catch (HibernateException e) {
			e.printStackTrace();
			logger.error("Exception! " + e);
		} finally {
			session.close();
		}

		return auth;
	}
	
	
	
	@RequestMapping(value = "/addExtraSetupBox", method = {RequestMethod.POST,RequestMethod.GET}, headers = "Accept=application/json",
			consumes = "application/json", produces = "application/json")
	public @ResponseBody List<Map<String, String>> addExtraSetupbox(@PathVariable("client_id") String client_id, @RequestBody SetUpBoxWrapper setupBoxWrapper) {

		int res = 0;
		List<Map<String, String>> response = new ArrayList<Map<String, String>>();
		for (SetupBoxTo setupBoxTo:setupBoxWrapper.getSetupBox()){
			Map<String, String> individualResponse =  new HashMap<String, String>();
			setupBoxTo.setClient_id(client_id);
			res = cservice.addExtraSetupBox(setupBoxTo);
			individualResponse.put("box_number", setupBoxTo.getBox_number());
			individualResponse.put("status", new Integer(res).toString());
			response.add(individualResponse);
		}
		return response;

	}
	

	@RequestMapping(value = "/viewBillsByMonth", method = RequestMethod.GET, headers = "Accept=application/json")
	public ViewBillsByMonthTo viewBillsByMonth(
			@ModelAttribute("customerTo") CustomerCredentials customerCredentials) {

		return cservice.viewBillsByMonth(customerCredentials);

	}
	
	/*@RequestMapping(value = "/getDocs", method = RequestMethod.GET, headers = "Accept=application/json")
	public List<String> getCustomerDocs(
			@RequestParam("customer_id") String customer_id) {
		
		return cservice.getCustomerDocs(customer_id);
	}*/
	
	
	
	@RequestMapping(value = "/boxesUpdate", method = RequestMethod.POST, headers = "Accept=application/json")
	public @ResponseBody Map<String, String> updateBox(@RequestBody SetupBoxUpdateWrapper setupBoxWrapper) {

		String res = "";
		Map<String, String> response = new HashMap<String, String>();
		for (SetupBoxTo setupBoxTo:setupBoxWrapper.getSetupBox()){
			res = cservice.updateBox(setupBoxTo);
			response.put("box_number", setupBoxTo.getBox_number());
			response.put("status", res);
		}
		return response;
		
	}
	 
	/*@RequestMapping(value = "/customerDelete", method = RequestMethod.GET, headers = "Accept=application/json")
	public int deleteCustomer(@RequestParam("customer_id") String customer_id)
			throws CustomerDeletionFailed {

		int res = 0;
		if (customer_id != null && customer_id.trim().length() != 0) {
			res = cservice.deleteCustomer(customer_id);
		}
		return res;
	}*/
	
	@RequestMapping(value = "/boxDelete", method = RequestMethod.POST, headers = "Accept=application/json")
	public @ResponseBody String deleteSetupBox(@RequestBody InputIds input)
			throws CustomerDeletionFailed {

		int res = 0;
		if (input.getBox_number() != null && input.getBox_number().trim().length() != 0) {
			res = cservice.deleteSetupBox(input.getBox_number());
			if(res == 1) return "Set top box deleted Successfully";
			else return "Internal server problem";
		}
		return "Given Set top box is not found";
	}
	
	@RequestMapping(value = "/setupboxPermenantDeActivate", method = RequestMethod.POST, headers = "Accept=application/json")
	public @ResponseBody String setupboxPermenantDeActivate(@RequestBody InputIds input) {

		int res = 0;
		if (input.getBox_number() != null && input.getBox_number().trim().length() != 0) {
			res = cservice.setupboxPermenantDeActivate(input);
			if(res == 1) return "Set top Permenently deactivated Successfully";
			else return "Internal server problem";
		}
		return "Given Set top box is not found";
	}

	

	@RequestMapping(value = "/makePayment", method = RequestMethod.GET, headers = "Accept=application/json")
	public String billPayment(@ModelAttribute MakePaymentDetailsNonSynch payDetails, @PathVariable String client_id) {
		
		String employee_id = "e-123";
		//String customer_id = cservice.getCustomer_id(customer_uid);
		if(CommonUtils.exists(payDetails.getEmployee_uid())){
			employee_id = eService.getEmployee_id(payDetails.getEmployee_uid());
		}if(CommonUtils.exists(payDetails.getIme_number())){
			employee_id = udao.getEmployeeIDusingIme(payDetails.getIme_number()); 
		}
		
		if(CommonUtils.exists(payDetails.getIme_number())){
			
			logger.info("******************" + payDetails);
			deviceService.updateDeviceLocation(payDetails.getIme_number(), payDetails.getLongitude(), payDetails.getLattitude());
			
		}
		
		logger.info("Controller Check\t"+payDetails);
		return cservice.billPayment(
				payDetails.getCustomer_id(),
				payDetails.getPaid_amount(),
				payDetails.getBalance_amount(),
				payDetails.getTotal_amount(),
				employee_id,
				client_id);
		
	}

	@RequestMapping(value = "/miniStatement", method = RequestMethod.GET, headers = "Accept=application/json")
	public List<MiniStatementForAndroidTo> miniStatementForAndroid(
			@RequestParam("customer_id") String customer_id) {
		logger.info("CustomerController for MiniStatemt");
		//String customer_id = cservice.getCustomer_id(customer_uid);
		return cservice.miniStatement(customer_id);
	}

	/*@RequestMapping(value = "/getPackageReports", method = RequestMethod.GET, headers = "Accept=application/json")
	public List<PackageReportsForAndroidTo> packageReportsToAndroid() {
		logger.info("CustomerController for Package Reports of Customers By EmployeeID");
		return cservice.getPackageReports();
	}*/

	@RequestMapping(value = "/getDeActivateCustomers", method = RequestMethod.GET, headers = "Accept=application/json")
	public @ResponseBody List<SearchCustomerTo> getDeactivatedCustomers(@PathVariable("client_id")String client_id) {
		logger.info("CustomerController for getting DeActivatedCustomers List");
		return cservice.getDeactivatedCustomers(client_id);
	}

	@RequestMapping(value = "/getUnPaidCustomers", method = RequestMethod.GET, headers = "Accept=application/json")
	public List<SearchCustomerTo> getUnPaidCustomers(@PathVariable("client_id") String client_id) {
		logger.info("CustomerController for getting UnPaid Customers List");
		return cservice.getUnPaidCustomers(client_id);
	}

	

	/* Sync DB Service */
	/*
	 * @RequestMapping(value = RestURIConstants.ANDROID_TO_MYSQL_SYNC_URL,
	 * method = RequestMethod.POST, consumes = "application/json", produces =
	 * "application/json") public @ResponseBody List<String>
	 * syncAndroidToMySQL(@RequestBody PersonWrapper wrapper) {
	 * 
	 * List<String> response = new ArrayList<String>(); for (Person person:
	 * wrapper.getPersons()){ //personService.save(person);
	 * response.add("Saved person: " + person.toString()); } return response; }
	 */

	/* Sync DB Service */
	@RequestMapping(value = RestURIConstants.ANDROID_TO_MYSQL_SYNC_URL, method = RequestMethod.POST, headers = "Accept=application/json",consumes = "application/json", produces = "application/json")
	public @ResponseBody List<SynchResponse> syncAndroidToMySQL(@RequestBody CustomerBillPaymentWrapper wrapper, @PathVariable String client_id) {

		boolean is_inserted = false;

		logger.info("###################################################################### Sync Started Controller String Modified Fields##############################################################");
		logger.info(wrapper);
		
		List<SynchResponse> response = new ArrayList<SynchResponse>();
		
		
		
		for (CustomerBillPayment bill : wrapper.getPayments()) {
			
			if(CommonUtils.exists(bill.getIme_number())){
				
				logger.info("******************" + bill);
				deviceService.updateDeviceLocation(bill.getIme_number(), bill.getLongitude(), bill.getLattitude());
				
			}
			
			SynchResponse synchResponse = new SynchResponse();
			// Now call the service to save the bill payment details:
			
			if(CommonUtils.exists(bill.getIme_number())){
				bill.setEmployee_id(udao.getEmployeeIDusingIme(bill.getIme_number()));
			}
			
			if(CommonUtils.exists(bill.getEmployee_uid())){
				bill.setEmployee_id(eService.getEmployee_id(bill.getEmployee_uid()));
			}
			
			is_inserted = cservice.billPayments(bill.getCustomer_id(),
					bill.getEmployee_id(), Double.parseDouble(bill.getPaid_amt()),
					0.0, 0.0, client_id, bill.getPaymentTimeStamp(), bill.getTransactionId());
			synchResponse.setCustomer_id(bill.getCustomer_id());
			synchResponse.setStatus(is_inserted);
			
			response.add(synchResponse);
		}
		logger.info(response);
		logger.info("###################################################################### Sync End Controller ##############################################################");

		return response;
	}
	
	@RequestMapping(value="/getuid", method = RequestMethod.GET, headers="Accept=application/json" )
	public String getUid(@PathVariable String client_id, @RequestParam String customer_uid)   {
		return cservice.getCustomer_id(customer_uid, client_id);
	}
	
	@RequestMapping(value="/getSearchCriterias", method = RequestMethod.GET, headers="Accept=application/json" )
	public List<SearchCriterias> sendSearchCriterias()   {
		
		List<SearchCriterias> list = new ArrayList<SearchCriterias>();
		list.add(new SearchCriterias("c_id"));
		list.add(new SearchCriterias("contact_number"));
		
		
		return list;
	}
	
	
	
	@RequestMapping(value = "/updateAddons", method = RequestMethod.POST, headers = "Accept=application/json")
	public @ResponseBody HashMap<String, String> updateAddons(@RequestBody UpdateAddonsWrapper addonWrapper,@PathVariable String client_id) {
		HashMap<String, String> result = new HashMap<String, String>();
		String res = "";
		
		if(addonWrapper.getSetupBoxTo()!=null){
			res = cservice.updateBox(addonWrapper.getSetupBoxTo());
			result.put(addonWrapper.getSetupBoxTo().getBox_number(), res);
		}
		
		if(addonWrapper.getDeletedAddonIds()!=null && addonWrapper.getDeletedAddonIds().size()>0){
			Map<String, String> deletedAddonRespose = new HashMap<String, String>();
			Iterator it = addonWrapper.getDeletedAddonIds().iterator();
			while(it.hasNext()){
				Integer addonId =  (Integer) it.next();
				res = cservice.deleteAddon(addonId,client_id);
				deletedAddonRespose.put(addonId.toString(), new Integer(res).toString());
			}
			result.put("Deleted Addons", res);
		}
		
		if(addonWrapper.getUpdatedAddons().size()>0){
			Map<String, String> updatedAddonRespose = new HashMap<String, String>();
			Iterator it = addonWrapper.getUpdatedAddons().iterator();
			while(it.hasNext()){
				AddonsEntityTo addOnEntityTo = (AddonsEntityTo) it.next();
				logger.info("controller UpdateAddons\t"+addOnEntityTo);
				res = cservice.updateAddon(addOnEntityTo, client_id);
				//updatedAddonRespose.put(new Integer(addOnEntityTo.getId()).toString(), new Integer(res).toString());
			}
			result.put("Updated Addons", res);
		}
		
		if(addonWrapper.getAddedAddons().size()>0){
			Map<String, String> addedAddonRespose = new HashMap<String, String>();
				logger.info("controller Adding New Addons\t"+addonWrapper.getAddedAddons());
				res = cservice.addAddon(addonWrapper.getAddedAddons(), client_id);
				
			result.put("Added Addons", res);
		}
		
		return result;
	}
	
	@RequestMapping(value = "/deleteAddons", method = RequestMethod.POST, headers = "Accept=application/json")
	public @ResponseBody Map<String, String> deleteAddons(@RequestBody AddonsWrapper addonWrapper, @PathVariable String client_id) {
		Map<String, String> result = new HashMap<String, String>();
		
		String res = "";
		
		for (AddonsEntityTo addon:addonWrapper.getAddOns()){
			logger.info("controller\t"+addon);
			res = cservice.deleteAddon(addon.getId(), client_id);
			result.put("addon_id", new Integer(addon.getId()).toString());
			result.put("status", new Integer(res).toString());
		}
		
		return result;
	}
	
	@RequestMapping(value = "/getAddonsOfSetupbox", method = RequestMethod.POST, headers = "Accept=application/json")
	public @ResponseBody Map<String, List> getTheSetupboxAddons(@RequestBody InputIds input) {
		logger.info("/getAddonsOfCustomer - controller input: " + input);
		List<AddonsEntityTo> addOns = new ArrayList<AddonsEntityTo>();
		Map<String, List> response = new HashMap<String, List>();
		addOns = cservice.getTheSetupboxAddons(input.getBox_number());
		response.put("addOns", addOns);
		logger.info("/getAddonsOfCustomer - controller output: " + response);
		return response;
	}

	/*@RequestMapping(value = "/getAddonsOfCustomer", method = RequestMethod.POST, headers = "Accept=application/json")
	public @ResponseBody Map<String, List> getTheCustomerupboxAddons(@RequestBody InputIds input) {
		logger.info("/getAddonsOfCustomer - controller input: " + input);
		List<AddonsEntityTo> addOns = new ArrayList<AddonsEntityTo>();
		Map<String, List> response = new HashMap<String, List>();
		addOns = cservice.getTheCustomerAddons(input.getC_id());
		response.put("addOns", addOns);
		logger.info("/getAddonsOfCustomer - controller output: " + response);
		return response;
	}*/
	
	
	
	@RequestMapping(value = "/updateContactNumber", method = RequestMethod.POST, headers = "Accept=application/json")
	public @ResponseBody UpdateContactResponseTO updateContactNumber(@RequestBody UpdateContactNumberWrapper request) {
		
		logger.info("@@@@@@@@@@@@@@@@@@@@@@@@@"+request);
		return cservice.updateContactNumber(request);
		
	}
	
	@RequestMapping(value = "/updateEmail", method = RequestMethod.POST, headers = "Accept=application/json")
	public @ResponseBody UpdateContactResponseTO updateEmail(@RequestBody UpdateContactNumberWrapper request) {
		
		logger.info("@@@@@@@@@@@@@@@@@@@@@@@@@"+request);
		return cservice.updateEmail(request);
		
	}
	
	/*@RequestMapping(value = "/getBillDetailsForOnlinePayment", method = RequestMethod.POST, headers = "Accept=application/json")
	public @ResponseBody UpdateContactResponseTO getBillDetailsForOnlinePayment(@RequestBody Map<String, String> request) {
		
		logger.info("@@@@@@@@@@@@@@@@@@@@@@@@@"+request);
		return cservice.getBillDetailsForOnlinePayment(request);
		
	}*/
	
	
	@RequestMapping(value = "/customBillGen", method = RequestMethod.POST, headers = "Accept=application/json")
	public @ResponseBody BillGenMessage customBillGen(@RequestBody BillGenType billGen,@PathVariable("client_id") String clientId)
	{
		
		return cservice.customBillGen(billGen,clientId);
	}
	
	
	
	
	@RequestMapping(value = "/getCustomerHistory", method = RequestMethod.POST, headers = "Accept=application/json")
	public @ResponseBody CustomerHistoryDto getCustomerHistory(@RequestBody BillGenType cusDetails, @PathVariable("client_id") String clientId)
	{
		return cservice.getCustomerHistory(cusDetails,clientId);
	}

	public CustomerController() {
		
	}
	
	
	
}
