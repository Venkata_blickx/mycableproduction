package com.blickx.controller;

import java.math.BigInteger;
import java.util.List;
import java.util.Map;

import javax.xml.ws.RespectBinding;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import com.blickx.domain.DeviceEntity;
import com.blickx.domain.SelectedDevice;
import com.blickx.output.DeviceLocationTo;
import com.blickx.response.ResponseObject;
import com.blickx.searchEntities.SearchDevice;
import com.blickx.searchEntities.SearchDeviceEntity;
import com.blickx.services.DeviceService;
import com.blickx.to.DeviceHistoryTo;
import com.blickx.to.DeviceNamesTo;
import com.blickx.to.DeviceTo;
import com.blickx.to.input.InputIds;


@RestController
@RequestMapping("/service/{client_id}/device")
public class DeviceController {

	@Autowired
	DeviceService deviceService;
	
	private Logger logger = Logger.getLogger(DeviceController.class);
	
	@RequestMapping(value="/Device_Details_byID", method = RequestMethod.POST, headers="Accept=application/json" )
	public DeviceEntity getDevice(@RequestBody SearchDevice input)   {

			logger.info("getDevicebyId in Controller");
			
			
			return deviceService.getDeviceById(input.getDevice_id());
	}
	
	@RequestMapping(value="/Unique_Device_Details", method = RequestMethod.GET, headers="Accept=application/json" )
	public DeviceEntity getUniqueDeviceDetails(@PathVariable String client_id)   {

			logger.info("getDevicebyId in Controller");
			
			return deviceService.getDeviceDetails(client_id);
			
	}
	
	@RequestMapping(value="/searchDevice", method = RequestMethod.POST, headers="Accept=application/json" )
	public List<DeviceTo> searchDevice(@PathVariable String client_id,@RequestBody SearchDevice searchDevice)   {

		searchDevice.setClient_id(client_id);
		return deviceService.searchDevices(searchDevice);
			
	}
	
	@RequestMapping(value="/Device_Details_byID_activate", method = RequestMethod.POST, headers="Accept=application/json" )
	public String activateStatus(@RequestBody InputIds input)   {

		String res="Given Id is Null";
		int deviceId = Integer.parseInt(input.getDevice_id());
        if(deviceId != 0){
			res=deviceService.activateStatus(deviceId);
	    return res;
		}
	return res;
			
	}
	
	@RequestMapping(value="/Device_Details_byID_deactivate", method = RequestMethod.POST, headers="Accept=application/json" )
	public String deActivateStatuss(@RequestBody InputIds input)   {

		String res="Given Id is Null";
		int deviceId = Integer.parseInt(input.getDevice_id());
        if(deviceId!=0){
			res=deviceService.deActivateStatus(deviceId);
	    return res;
		}
	return res;
			
	}
	
	@RequestMapping(value="/getInActiveDevices", method = RequestMethod.GET, headers="Accept=application/json" )
	public List<DeviceTo> deActivateStatus(@PathVariable String client_id)   {

			return deviceService.getInactiveDevices(client_id);
			
	}
	
	
	
	
	
	
	
	@RequestMapping(value="/deviceAdd", method = RequestMethod.POST, headers="Accept=application/json" )
	public @ResponseBody ResponseObject addCusomer(@PathVariable("client_id")String client_id, @RequestBody DeviceEntity device)   {

		ResponseObject response = new ResponseObject();
			device.setClient_id(client_id);
			logger.info("Device Controller- AddDevice Input:" + device);
			
			String res=deviceService.addDevice (device);
			if(res.equalsIgnoreCase("1")){
				response.setResponseMsg("Device added successfully!");
				response.setDataValid(true);
			}else{
				response.setResponseMsg(res);
				response.setDataValid(false);
			}
			
			logger.info("Device Controller- AddDevice Output:" + response);	
		return response;
	}
	
	@RequestMapping(value="/getDeviceNames", method = RequestMethod.GET, headers="Accept=application/json" )
	public @ResponseBody List<DeviceNamesTo> getDeviceNames(@PathVariable String client_id)   {
		
		return deviceService.getDeviceNames(client_id);
			
	}
	
	@RequestMapping(value="/updateDevice", method = RequestMethod.POST, headers="Accept=application/json" )
	public @ResponseBody ResponseObject updateDevice(@PathVariable("client_id") String client_id, @RequestBody DeviceEntity device)   {
		
		ResponseObject response = new ResponseObject();
		
		device.setClient_id(client_id);
		logger.info("Device Controller- UpdateDevice Input:" + device);
		
		String res = deviceService.updateDevice(device);
			if(res.equalsIgnoreCase("1")){
				response.setResponseMsg("Device updated successfully!");
				response.setDataValid(true);
			}else{
				response.setResponseMsg(res);
				response.setDataValid(false);
			}
			
		logger.info("Device Controller- UpdateDevice Output:" + response);
		return response;
			
	}
	
	
	@RequestMapping(value="/Get_Device_Location", method = RequestMethod.POST, headers="Accept=application/json" )
	public @ResponseBody List<DeviceLocationTo> getDeviceLocation(@PathVariable String client_id)   {

			logger.info("getDevicebyId in Controller");
			return deviceService.getDeviceLocation(client_id);
	}
	
	
	@RequestMapping(value="/delete", method = RequestMethod.GET, headers="Accept=application/json" )
	public String deleteDevice(@ModelAttribute("device_id")String device_id)   {
		String result="Not Done";
		if(device_id!=null&&device_id.trim().length()!=0){
			result=deviceService.deleteDevice(device_id);
		}
		return result;
			
	}
	
	
	
	@RequestMapping(value="/getDeviceHistory", method = RequestMethod.GET, headers="Accept=application/json" )
	public List<DeviceHistoryTo> getDeviceHistory(@ModelAttribute("device_id")int device_id)   {

		return deviceService.getDeviceHistory(device_id);
			
	}
	
}
