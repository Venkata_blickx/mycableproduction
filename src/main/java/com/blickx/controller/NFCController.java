package com.blickx.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import com.blickx.domain.NFCEntity;
import com.blickx.services.NfcService;

@Controller
@RequestMapping("/{client_id}")
public class NFCController {
	
	@Autowired
	NfcService nfcService;

	@RequestMapping(value="/Add", method = RequestMethod.GET, headers="Accept=application/json" )
	public @ResponseBody int addEmployee(@ModelAttribute("nfc") NFCEntity nfcEntity)   {
			int res=0;
			
			if(nfcEntity!=null){
				res=nfcService.addNfc (nfcEntity);
	       		return res;
			}
		return res;
	}
}
