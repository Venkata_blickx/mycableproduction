package com.blickx.controller;

import java.util.List;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import com.blickx.domain.ClientInformationEntity;
import com.blickx.domain.CustomerToMobile;
import com.blickx.output.PaidCustomerTo;
import com.blickx.services.AndroidService;
import com.blickx.to.CustomerToMobileTo;
import com.blickx.to.EmployeeCredentialsTo;
import com.blickx.to.input.InputIds;

@RestController
@RequestMapping("/service/{client_id}")
public class AndroidController {
	
	@Autowired
	AndroidService androidService;
	
	private Logger logger = Logger.getLogger(AndroidController.class);

	@RequestMapping(value = "/getAllCustomerBilling", method = RequestMethod.GET, headers = "Accept=application/json")
	public @ResponseBody List<CustomerToMobile> getAllCustomerBills(@PathVariable("client_id")String client_id) {

		return androidService.getAllCustomerBills(client_id);
			
	}
	
	@RequestMapping(value = "/getAllEmployeCredetials", method = RequestMethod.GET, headers = "Accept=application/json")
	public @ResponseBody List<EmployeeCredentialsTo> getAllEmployeCredetials(@PathVariable("client_id")String client_id) {

		return androidService.getAllEmployeCredetials(client_id);
			
	}
	
	@RequestMapping(value = "/getAllCustomerBillings", method = RequestMethod.POST, headers = "Accept=application/json")
	public @ResponseBody List<CustomerToMobileTo> getAllCustomersBills(@PathVariable("client_id")String client_id, @RequestBody InputIds input) {

		return androidService.getAllCustomersBills(client_id, input.getIme_number());
			
	}
	
	@RequestMapping(value = "/getAllUpdatedCustomerBillings", method = RequestMethod.POST, headers = "Accept=application/json")
	public @ResponseBody List<CustomerToMobileTo> getAllUpdatedCustomersBills(@PathVariable("client_id")String client_id, @RequestBody InputIds input) {

		return androidService.getAllUpdatedCustomersBills(client_id, input);
			
	}
	
	@RequestMapping(value = "/getClientInformationForPrint", method = RequestMethod.POST, headers = "Accept=application/json")
	public @ResponseBody List<ClientInformationEntity> getClientInfo(@PathVariable("client_id")String client_id) {

		logger.info("AndroidController /getClientInformationForPrint - INPUT ClientID : " + client_id);
		return androidService.getClientInfo(client_id);
			
	}
	
	@RequestMapping(value = "/getPaidTxs", method = RequestMethod.POST, headers = "Accept=application/json")
	public @ResponseBody List<PaidCustomerTo> getPaidTxs(@PathVariable("client_id")String client_id, @RequestBody InputIds input) {

		logger.info("AndroidController /getClientInformationForPrint - INPUT ClientID : " + client_id);
		return androidService.getPaidTxs(client_id, input);
			
	}
	
}
