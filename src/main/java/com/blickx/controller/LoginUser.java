package com.blickx.controller;


import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.web.bind.annotation.AuthenticationPrincipal;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import com.blickx.services.SettingsService;
import com.blickx.services.UserService;
import com.blickx.to.input.LoginCredentialsInput;
import com.blickx.utill.CommonUtils;



@RestController
//@RequestMapping("/{client_id}")
public class LoginUser {
	
	private Logger logger = Logger.getLogger(LoginUser.class);
	
	
	@Autowired
	UserService uservice;
	
	@Autowired
	SettingsService settingService;

	@RequestMapping(value="/login",method = RequestMethod.GET, headers="Accept=application/json" )
	public  @ResponseBody String checkUser(@ModelAttribute LoginCredentialsInput login){
		
		logger.info("Login Conroller Android Login : Username" + login.getUsername());
		logger.info("Login Conroller Android Login : Username" + login.getPassword());
		
		logger.info(CommonUtils.exists(login.getUsername()));
		if(!CommonUtils.exists(login.getUsername())){
			
			logger.info("username is not existed");
			return uservice.loginWithImeAndPassword(login.getIme_number(), login.getPassword());
		}
		
		if((CommonUtils.exists(login.getUsername()))&&CommonUtils.exists(login.getPassword())){
		
			logger.info("username is existed");
			return uservice.checkUser(login.getUsername(),login.getPassword(),login.getIme_number());
			
		}
		return "empty";
	}
	
	@RequestMapping(value = "/getUpdateAPKVersion", method = RequestMethod.GET, headers = "Accept=application/json")
	public @ResponseBody String getUpdateApkVersion() {

		return settingService.getUpdateApkVersion();
			
	}
	
	
	@RequestMapping(value="/{client_id}/logout",method = RequestMethod.GET, headers="Accept=application/json" )
	public  @ResponseBody String logout(@PathVariable String client_id, @ModelAttribute LoginCredentialsInput logout){
		
		System.out.println(logout.getUsername());
		String result = "";
		if(CommonUtils.exists(logout.getUsername()) || CommonUtils.exists(logout.getIme_number())){
		
			result=uservice.logout(logout.getUsername(),logout.getIme_number());
		}
			return result;
	}
}
