package com.blickx.controller;

import java.io.IOException;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletResponse;

import org.apache.log4j.Logger;
import org.hibernate.HibernateException;
import org.hibernate.Query;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.Transaction;
import org.hibernate.transform.ResultTransformer;
import org.hibernate.transform.Transformers;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.servlet.ModelAndView;

import com.blickx.domain.Authentication;
import com.blickx.response.ResourcesToAccess;
import com.blickx.response.ResponseObject;
import com.blickx.response.angular.LoginResponse;
import com.blickx.utill.CommonUtils;
import com.blickx.utill.RestURIConstants;

@RestController
@RequestMapping("/accounts")
public class HomeController {

	private Logger logger = Logger.getLogger(HomeController.class);
	
	@Autowired
	SessionFactory sfactory;

	@RequestMapping(value = RestURIConstants.HOME)
	public ModelAndView test(HttpServletResponse response) throws IOException {
		return new ModelAndView("home");
	}

	@RequestMapping(value = RestURIConstants.ACCESS_DENIED)
	public ModelAndView acc_denied(HttpServletResponse response)
			throws IOException {
		return new ModelAndView("access_denied");
	}

	@RequestMapping(value = RestURIConstants.INTERNAL_SERVER_ERROR)
	public ModelAndView internal_error(HttpServletResponse response)
			throws IOException {
		ModelAndView mav = new ModelAndView();
		mav.addObject("current_page", "Internal Server Error");
		mav.setViewName("common");
		return mav;
	}

	@RequestMapping(value = RestURIConstants.NOT_FOUND)
	public ModelAndView not_found(HttpServletResponse response)
			throws IOException {
		ModelAndView mav = new ModelAndView();
		mav.addObject("current_page", "Page Not Found");
		mav.setViewName("common");
		return mav;
	}

	@RequestMapping(value = RestURIConstants.ADD_CUSTOMER)
	public ModelAndView addCustomer(HttpServletResponse response)
			throws IOException {
		ModelAndView mav = new ModelAndView();
		mav.addObject("current_page", "Add Customer");
		mav.setViewName("common");
		return mav;
	}

	/* Admin Login Service /accounts/alogin/{username}/{encpwd} */
	@RequestMapping(value = RestURIConstants.ADMIN_LOGIN_URL, method = RequestMethod.POST)
	public @ResponseBody Map<String, Object> adminLogin(@RequestBody Map<String, String> input) {

		Map<String, Object> response = new HashMap<String, Object>();
		System.out.println("username : " + input.get("username"));
		System.out.println("password : " + input.get("password"));

		Session session = sfactory.openSession();
		try {

			logger.info("Authenticating Admin..");

			boolean userFound = false;

			// Query using Hibernate Query Language
			String SQL_QUERY = " from Authentication o where o.username=? and o.password=?";
			Query query = session.createQuery(SQL_QUERY);
			query.setParameter(0, input.get("username"));
			query.setParameter(1, input.get("password"));
			List list = query.list();
			Authentication auth = (Authentication) session.createQuery("from Authentication o where o.username=? and o.password=?")
										.setString(0, input.get("username"))
										.setString(1, input.get("password")).uniqueResult();
			
			
			

			if ((list != null) && (list.size() > 0)) {
				userFound = true;
				response.put("success", new Boolean(true));
				response.put("message", new String("Login Successfull...."));
				response.put("client_id", auth.getClient_id());
				response.put("longitude", auth.getLongitude());
				response.put("latitude", auth.getLatitude());
				response.put("client_name", auth.getClient_name());
				response.put("employee_id", auth.getEmployee_id());
				response.put("Admin", auth.getRole_id() == 1 ? new Boolean(true) : new Boolean(false));
				//response.put("resourcesToAccess", resourcesToAccess);
				/*response.put("Address", auth.getAddress());
				logger.info("password matched");*/
			} else {
				response.put("success", new Boolean(false));
				response.put("message", new String("Wrong username or password"));
				response.put("client_id", "");
				/*response.put("client_name", "");
				response.put("Address", "");*/
				logger.info("password mismatched");
			}

			

		} catch (HibernateException e) {
			e.printStackTrace();
			logger.error("Exception! " + e);
		} finally {
			session.close();
		}

		return response;
	}
	
	
	@SuppressWarnings("unused")
	@RequestMapping(value = "/getByUsername", method = RequestMethod.POST,  headers = "Accept=application/json")
	public @ResponseBody LoginResponse getByUsername(@RequestBody Map<String, String> input) {

		LoginResponse response = new LoginResponse();
		System.out.println("username : " + input.get("username"));

		Session session = sfactory.openSession();
		try {

			logger.info("Authenticating Admin..");

			boolean userFound = false;

			// Query using Hibernate Query Language
			//String SQL_QUERY = " from Authentication o where o.username=?";
			/*Query query = session.createQuery(SQL_QUERY);
			query.setParameter(0, input.get("username"));
			List list = query.list();*/
			Authentication auth = (Authentication) session.createQuery("from Authentication o where o.username=? ")
										.setParameter(0, input.get("username"))
										.uniqueResult();
			
			List<ResourcesToAccess> resourcesToAccess = session.createSQLQuery("select resource, case when action_type = 'all' then 1 else 0 end as access from role_privilages where role_id = ?")
					.setInteger(0, auth.getRole_id()).setResultTransformer(Transformers.aliasToBean(ResourcesToAccess.class)).list();
			Map<String, Boolean> resources = new HashMap<String, Boolean>();
			for(ResourcesToAccess resource1 : resourcesToAccess){
				resources.put(resource1.getResource(), resource1.getAccess().intValue() > 0 ? true : false);
			}
			

			Map<String, Object> coords = null;
			
			if (auth != null) {
				userFound = true;
				if(CommonUtils.exists(auth.getLongitude()) && CommonUtils.exists(auth.getLatitude())){
				Map<String, String> coordinates = new HashMap<String, String>();
				coordinates.put("longitude", auth.getLongitude());
				coordinates.put("latitude", auth.getLatitude());
				
				coords = new HashMap<String, Object>();
				coords.put("coords", coordinates);
				} 
				
				response.setClientId(auth.getClient_id());
				response.setFirstName("User");
				response.setClientName(auth.getClient_name());
				response.setMessage("Login Successful");
				response.setSuccess(true);
				response.setCurrentLocation(coords);
				response.setBroadbandUser(auth.isBroadbandUser());
				response.setUsername(input.get("username"));
				response.setIsBalanceUpdateAllowed(auth.isBalanceUpdateAllowed());
				response.setSmsActivate(auth.isMsg_alert());
				response.setAreacode_activated(auth.isAreacode_activated());
				response.setResourceToAccess(resources);
				response.setEmployee_id(auth.getEmployee_id());
				
				logger.info("password matched");
			} else {
				response.setClientId("");
				response.setFirstName("");
				response.setClientName("");
				response.setMessage("Login Fail");
				response.setSuccess(false);
				logger.info("password mismatched");
			}

			

		} catch (HibernateException e) {
			e.printStackTrace();
			logger.error("Exception! " + e);
		} finally {
			session.close();
		}

		return response;
	}
	
	@RequestMapping(value="/changePassword", method = RequestMethod.POST, headers="Accept=application/json" )
	public @ResponseBody ResponseObject changePassword(@RequestBody Map<String, String> input)   {

			ResponseObject res = new ResponseObject();
			
			Session session = sfactory.openSession();
			Transaction tx = session.beginTransaction();
			try {

				logger.info("Change Password..");


				// Query using Hibernate Query Language
				String SQL_QUERY = " from Authentication a where a.username=? and a.client_id = ? and a.password = ?";
				Query query = session.createQuery(SQL_QUERY);
					query.setParameter(0, input.get("username"));
					query.setParameter(1, input.get("client_id"));
					query.setParameter(2, input.get("currentPassword"));
				List list = query.list();
				
				if(list.size() > 0){
					Authentication auth = (Authentication) list.get(0);
					auth.setPassword(input.get("newPassword"));
					session.update(auth);
					res.setDataValid(true);
					res.setResponseMsg("Password successfully changed");

				} else {
					res.setDataValid(false);
					res.setResponseMsg("Current password given is wrong");
				}

				tx.commit();

			} catch (HibernateException e) {
				e.printStackTrace();
				logger.error("Exception! " + e);
			} finally {
				session.close();
			}

			return res;
	}
	
	
}
