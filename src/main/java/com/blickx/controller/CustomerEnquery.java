package com.blickx.controller;

import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import com.blickx.domain.PackageEntity;
import com.blickx.mycable.customerAddElements.Cart;
import com.blickx.mycable.customerAddElements.CustomerAddInputTo;
import com.blickx.mycable.customerAddElements.Items;
import com.blickx.mycable.customerAddElements.ManipulateCustomerData;
import com.blickx.mycable.customerAddElements.SelectedPackageForCustomerAdd;
import com.blickx.mycable.customerAddElements.SetupboxForAddCustomer;
import com.blickx.response.ResponseObject;
import com.blickx.services.EnqueryService;
import com.blickx.to.AutoCId;
import com.blickx.to.DeActiveEnquiryDto;
import com.blickx.to.DeActiveEnquiryResponse;


@RestController
@RequestMapping("/service/{client_id}/enquery")
public class CustomerEnquery {

	
	
	@Autowired
	EnqueryService enquryService;
	
	private Logger logger = Logger.getLogger(CustomerEnquery.class);
	
	
	
	
	
	@RequestMapping(value = "/enquryCustomerAdd", method = {RequestMethod.POST, RequestMethod.GET}, headers = "Accept=application/json"  )
	public @ResponseBody ResponseObject enqueryAddCustomer(@PathVariable("client_id")String client_id, @RequestBody CustomerAddInputTo customer) throws ParseException {
		DateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd");
		customer.setJoining_date(dateFormat.format(new Date()));
		PackageEntity pe = enquryService.getPackageDetails(client_id);
		List<SetupboxForAddCustomer> sf = new ArrayList<SetupboxForAddCustomer>();
		SetupboxForAddCustomer sfa = new SetupboxForAddCustomer();
		sfa.setBox_number(customer.getCustomer_name().substring(0, 2)+Math.random());
		sfa.setVc_number(customer.getCustomer_name().substring(0, 2)+Math.random());
		sfa.setNds_number(customer.getCustomer_name().substring(0, 2)+Math.random());
		sfa.setDiscount(0);
		sfa.setStart_date(dateFormat.format(new Date()));
		sfa.setRow_id(0);
		SelectedPackageForCustomerAdd spf = new SelectedPackageForCustomerAdd();
		spf.setPackage_id(pe.getPackage_id());
		spf.setPackage_name(pe.getPackage_name());
		spf.setPackage_amount(pe.getPackage_amount());
		spf.setStatus("1");
		sfa.setSelectedPackage(spf);
		
		
		List<Items> li = new ArrayList<Items>();
		Items it = enquryService.getIteamDetails(client_id);
		li.add(it);
		
		Cart ca = new Cart();
		ca.set_data(0);
		ca.setisPackageExists(false);
		ca.setTax(0);
		ca.setTaxRate(0);
		ca.setShipping(100);
		ca.setItems(li);
		sfa.setCart(ca);
		sf.add(sfa);
		customer.setSetupboxes(sf);
		
		logger.info("CustomerController - addCustomer input customer Data :" + customer);
		customer.setClient_id(client_id);
		ResponseObject response = new ResponseObject();
		String res = enquryService.enqueryAddCustomer(client_id, ManipulateCustomerData.getCustomerTo(customer));
			if(res.equalsIgnoreCase("Customer added successfully!")){
				response.setResponseMsg("Customer added successfully!");
				response.setDataValid(true);
			}else{
				response.setResponseMsg(res);
				response.setDataValid(false);
			}
		return response;
		
	}
	
	
	@RequestMapping(value="/deActiveEnquiry", method=RequestMethod.POST, headers="Accept=application/json")
	private @ResponseBody DeActiveEnquiryResponse addDeActiveEnquiry(@PathVariable("client_id")String clientId, @RequestBody DeActiveEnquiryDto deactiveEnquiry)throws ParseException
	{
		logger.info("deactiveEnquiry Service Calling input"+deactiveEnquiry);
		return enquryService.addDeActiveEnquiry(clientId,deactiveEnquiry);
	}
	
	
	@RequestMapping(value="/autoCustomerCId", method=RequestMethod.GET, headers="Accept=application/json")
	private AutoCId autoCidDetails(@PathVariable("client_id")String clientId)
	{
		logger.info("/autoCustomerCId:\t"+clientId);
		return enquryService.getAutoCIdNumber(clientId);
	}
	
}
