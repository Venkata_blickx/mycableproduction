package com.blickx.controller;

import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import com.blickx.utill.CommonUtils;
import com.blickx.utill.RestURIConstants;
import com.blickx.wrapper.EmployeeReportWrapper;
import com.blickx.wrapper.UnpaidReportWrapper;
import com.blickx.dao.UserDao;
import com.blickx.domain.Customer;
import com.blickx.output.SetupBoxReport;
import com.blickx.output.wrapper.CustomerReportsWrapper;
import com.blickx.response.AreaWiseReport;
import com.blickx.response.AreaWiseReportWrap;
import com.blickx.searchEntities.GetEmployeeReportsSearchEntity;
import com.blickx.services.EmployeeService;
import com.blickx.services.ReportsService;
import com.blickx.to.CustomerBillReportByCidAndDateTo;
import com.blickx.to.CustomerReports;
import com.blickx.to.DailyCollectionByEmployee;
import com.blickx.to.GetCollectionOfEmployeeTo;
import com.blickx.to.GetCustomerBillReportDate;
import com.blickx.to.GetEmployeeReportsTo;
import com.blickx.to.GetLastSixTxsTo;
import com.blickx.to.input.CustomerBillReportDate;
import com.blickx.to.input.EmployeeCredentialsInput;
import com.blickx.to.input.InputIds;
import com.blickx.to.input.LastSixMonthsTxsInputTo;
import com.blickx.to.input.SetupboxReportInput;

@RestController
@RequestMapping("/service/{client_id}")
public class ReportsController {

	private Logger logger = Logger.getLogger(ReportsController.class);
	
	@Autowired
	ReportsService reportService;
	
	@Autowired
	EmployeeService eService;
	
	@Autowired
	UserDao udao;
	
	/**
	 * Mapping Daily_Collection_Statatics
	 * 
	 * */
	@RequestMapping(value = "/Daily_Collection_Statatics", method = RequestMethod.GET)
	public @ResponseBody List<DailyCollectionByEmployee> getDaily_Report(@PathVariable String client_id) {

		return reportService.getCollectionByEmployee(client_id);
		
	}
	
	/**
	 * Mapping for Live_Collection_Status
	 * 
	 * */
	@RequestMapping(value = "/Live_Collection_Status", method = {RequestMethod.GET, RequestMethod.POST})
	public @ResponseBody List<GetCollectionOfEmployeeTo> getCollectionByEmployee(@PathVariable String client_id, @RequestBody String inputDay) {
		
		String inputDate = "";
		
		logger.info("InputDay: " + inputDay);
		
		
		SimpleDateFormat sdf = new SimpleDateFormat("EEEE");
		SimpleDateFormat sdf2 = new SimpleDateFormat("yyyy-MM-dd");
	    
		Calendar cal = Calendar.getInstance();
	    
	    Date date=cal.getTime();
		//System.out.println(sdf.format(date) + "\t" + sdf2.format(date));
		
		for (int i=0;i<6;i++){
		   cal.add(Calendar.DAY_OF_MONTH, -1);
		   
		   date=cal.getTime();
		   logger.info(sdf.format(date));
		   if(sdf.format(date).equalsIgnoreCase(inputDay)){
			   logger.info("Day Matched: " + sdf.format(date));
			   inputDate = sdf2.format(date).toString();
			   logger.info("Date Generated: " + inputDate);
		   }
		   
		   //System.out.println(sdf.format(date) + "\t" + sdf2.format(date));
		}
		

		return reportService.getCollectionOfEmployee(client_id, inputDate);
		
	}

	
	
	/**
	 *  Mapping for Generate Customer Reports
	 *  
	 *  /getCustomerReport
	 * 
	 * */
	@RequestMapping(value = RestURIConstants.GET_CUSTOMER_REPORT, method = RequestMethod.POST)
	public @ResponseBody CustomerReportsWrapper getCustomer_Report(
			@PathVariable String client_id,
			@RequestBody GetCustomerBillReportDate input) {
		logger.info("ReportsController-getCustomerReports() INPUT :" + input);
		input.setClient_id(client_id);
		
		if (CommonUtils.exists(input.getFrom()) && CommonUtils.exists(input.getTo())) {
			try {
				logger.info("ReportsController-getCustomerReports() dateRange is existed");
				return reportService.getCustomerReportsWithDateRange(input);
			} catch (Exception e) {
				logger.error("ReportsController-getCustomerReports()" + e);
				return null;
			}
		} else if(CommonUtils.exists(input.getCustomer_id()) || CommonUtils.exists(input.getCustomer_name())) {
			logger.info("ReportsController-getCustomerReports() dateRange is not existed");
			return reportService.getCustomerReports(input);
		}
		return null;
	}
	
	/**
	
	
	/**
	 * Mapping for Collection By Emoloyee
	 * 
	 * - Calls getEmployeeBillReports Stored Procedure
	 * 
	 * */
	@RequestMapping(value = "/getEmployeeBillReports", method = RequestMethod.POST)
	public @ResponseBody EmployeeReportWrapper getEmployeeReports(
			@PathVariable String client_id,
			@RequestBody GetEmployeeReportsSearchEntity input) {

		logger.info("ReportsController-getEmployeReports() INPUT :" + input);
		
		input.setClient_id(client_id);
		
		if (CommonUtils.exists(input.getFrom()) && CommonUtils.exists(input.getTo())) {
			try {
				logger.info("ReportsController-getCustomerReports() dateRange is existed");
				return reportService.getBillReportsOfEmployeeByDate(input);
			} catch (Exception e) {
				logger.error("ReportsController-getEmployeeReports()" + e);
				return null;
			}
		} else if(CommonUtils.exists(input.getEmployee_id()) || CommonUtils.exists(input.getEmployee_name())){
			logger.info("ReportsController-getCustomerReports() dateRange is not existed");
			return reportService.getBillReportsOfEmployee(input);
		}
		
		return null;
		
		
	}
	
	@RequestMapping(value = "/UnPaidReport", method = RequestMethod.POST)
	public @ResponseBody UnpaidReportWrapper getCustomer_ReportDateByCid(@PathVariable String client_id, @RequestBody GetCustomerBillReportDate getCustomerBillReportDate) {

		logger.info(getCustomerBillReportDate);
		getCustomerBillReportDate.setClient_id(client_id);
		return reportService.getCustomer_ReportDateByCid(getCustomerBillReportDate);
		
	}
	
	@RequestMapping(value = "/lastSixMonthsTxs", method = RequestMethod.POST)
	public @ResponseBody List<GetLastSixTxsTo> getLastSixTxs(
			@PathVariable String client_id,
			@RequestBody LastSixMonthsTxsInputTo input) {

		logger.info(input);
		input.setClient_id(client_id);
		return reportService.getLastSixTxs(input);
		
	}
	
	@RequestMapping(value = "/setupboxReport", method = RequestMethod.POST)
	public @ResponseBody List<SetupBoxReport> setupboxReport(
			@PathVariable String client_id,
			@RequestBody SetupboxReportInput input) {

		
		input.setClient_id(client_id);logger.info(input);
		return reportService.getSetupboxReport(input);
		
	}
	
	@RequestMapping(value = "/areaWiseReport", method = RequestMethod.POST)
	public @ResponseBody AreaWiseReportWrap areaWiseReport(
			@PathVariable String client_id,
			@RequestBody SetupboxReportInput input) {

		
		input.setClient_id(client_id);logger.info(input);
		return reportService.areaWiseReport(input);
		
	}
	
	
	/****************************************************Android Services*****************************************************************/

	/**
	 * 
	 * @param employee_id
	 * @return
	 * 
	 * Idividual Employees Current Day Collection For Android App
	 * 
	 * From the device Employee wants to know current day collection 
	 * 
	 */
	@RequestMapping(value = "/getEmployeeCurrentDayCollection", method = RequestMethod.GET)
	public @ResponseBody Map<String, Object> getEmployeeCurrentDayCollections(@ModelAttribute EmployeeCredentialsInput emp) {

		if(CommonUtils.exists(emp.getEmployee_uid())){
		return reportService.getEmployeeCurrentDayCollections(eService.getEmployee_id(emp.getEmployee_uid()));
		}
		if(CommonUtils.exists(emp.getIme_number())){
			return reportService.getEmployeeCurrentDayCollections(udao.getEmployeeIDusingIme(emp.getIme_number()));
		}
		return new HashMap<String, Object>();
	}
	
	
	
}
