package com.blickx.controller;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import com.blickx.channels.categories.ChannelSeraliser;
import com.blickx.domain.Channels;
import com.blickx.domain.PackageEntity;
import com.blickx.services.PackageServiceImpl;
import com.blickx.to.ChannelNamesWithCost;
import com.blickx.to.ChannelsTOO;
import com.blickx.to.ChannelsTo;
import com.blickx.to.GetExpireAddonAlertTo;
import com.blickx.to.GetTheAddonsToUpdate;
import com.blickx.to.PackageDisplay;
import com.blickx.to.PackageTO;
import com.blickx.to.SearchPackageTO;
import com.blickx.to.input.CustomerFetchCredetials;
import com.blickx.to.input.InputIds;
import com.blickx.to.input.PackageDetails;
import com.blickx.wrapper.ChangePackageAmountWrapper;
import com.blickx.wrapper.ChannelsWrapper;
import com.blickx.wrapper.PackageWrapper;

	/**
	 * Package Controller Class which is acting as controller, which takes the incoming request and send to business Logic
	 * Channels also
	 */
@RestController
@RequestMapping("/service/{client_id}")
public class PackageController {
	
	@SuppressWarnings("unused")
	private Logger logger = Logger.getLogger(PackageController.class);
	
	@Autowired
	PackageServiceImpl packService;
	
	
	@RequestMapping(value="/getChannelById",method = RequestMethod.POST, headers="Accept=application/json" )
	public ChannelsTo getChannelById(@RequestBody InputIds input)   {
		
		int channelId = Integer.parseInt(input.getChannel_id());
			logger.info("Input " + channelId);	
		return packService.getChannelById(channelId);
		
	}
	
	
	@RequestMapping(value="/searchChannel",method = RequestMethod.GET, headers="Accept=application/json" )
	public List<ChannelsTOO> getAllChannels(@PathVariable String client_id)   {
		
		return packService.getAllChannels(client_id);
	}
	
	@RequestMapping(value="/updatePackage",method = RequestMethod.POST, headers="Accept=application/json" )
	public @ResponseBody int update(@RequestBody PackageTO packages)   {
		
		return packService.updatePackage(packages);
	}
	
	@RequestMapping(value="/getPackageDetailsById",method = RequestMethod.POST, headers="Accept=application/json" )
	public PackageTO getPackageById(@RequestBody InputIds input)   {
		
		return packService.getPackageListById(Integer.parseInt(input.getPackage_id()));
	}
	
	@RequestMapping(value="/deletePackage",method = RequestMethod.POST, headers="Accept=application/json" )
	public int delete(@RequestBody InputIds input)   {
		
		return Integer.parseInt(packService.deletePackage(Integer.parseInt(input.getPackage_id())));
	}
	
	@RequestMapping(value="/ChannelNamesWithCost",method = RequestMethod.GET, headers="Accept=application/json" )
	public List<ChannelNamesWithCost> getAllChannelNamesWithAmount(@PathVariable String client_id)   {
		
		return packService.getAllChannelNamesWithAmount(client_id);
	}
	
	
	
	
	
	
	
	
	
	
	
	
	
	@RequestMapping(value="/addPackage",method = RequestMethod.POST, headers="Accept=application/json" )
	public @ResponseBody int addPackage(@PathVariable("client_id")String client_id, @RequestBody PackageTO packageTO)   {
		packageTO.setClient_id(client_id);
		return packService.addPackage(packageTO);
		
	}
	
	@RequestMapping(value="/getPack",method = RequestMethod.GET, headers="Accept=application/json" )
	public List<PackageDisplay> getPackage(@PathVariable String client_id)   {
		
		List<PackageDisplay> packList = new ArrayList<PackageDisplay>();
		
			for(SearchPackageTO pack:packService.searchPackages(client_id)){
				PackageDisplay packagedis = new PackageDisplay();
				packagedis.setPackage_amount(pack.getPackage_amount());
				packagedis.setPackage_name(pack.getPackage_name());
				
				packList.add(packagedis);
				
				
			}
		
		return packList;
	}
	
	
	
	/*@RequestMapping(value="/getAssignedChannelsForPackage",method = RequestMethod.GET, headers="Accept=application/json" )
	public List<String> getChannelsListFromPackage(@ModelAttribute("package_id") int package_id)   {
		
		return packService.getChannelsListFromPackage(package_id);
	}*/
	
	
	
	
	@RequestMapping(value="/getChannels",method = RequestMethod.GET, headers="Accept=application/json" )
	public List<ChannelSeraliser> getChannels()   {
		
		return packService.getChannelNames();
	}
	
	
	
	@RequestMapping(value="/addChannel",method = RequestMethod.POST, headers="Accept=application/json" )
	public @ResponseBody int addChannel(@PathVariable String client_id, @RequestBody ChannelsTo channel)   {
		
		channel.setClient_id(client_id);
		return packService.addChannel(channel);
		
	}
	
	@RequestMapping(value="/updateChannel",method = RequestMethod.POST, headers="Accept=application/json" )
	public @ResponseBody int updateChannel(@RequestBody Channels channel)   {
		
		return packService.updateChannel(channel);
	}
	
	/*@RequestMapping(value="/updateChannels",method = RequestMethod.POST, headers="Accept=application/json" )
	public @ResponseBody Map<String , Integer> updateChannel(@RequestBody ChannelsWrapper channels)   {
		Map<String , Integer> result = new HashMap<String, Integer>();
		Integer res = new Integer(0);
		for(Channels channel:channels.getChannels()){
			res = packService.updateChannel(channel);
			result.put(channel.getChannel_name(), res);
		}
			return result;
	}*/
	
	/*@RequestMapping(value="/updatePackages",method = RequestMethod.POST, headers="Accept=application/json" )
	public @ResponseBody Map<String , Integer> update(@RequestBody PackageWrapper packages)   {
		Map<String , Integer> result = new HashMap<String, Integer>();
		Integer res = new Integer(0);
		for(PackageTO packagee:packages.getPackages()){
			res = packService.updatePackage(packagee);
			result.put(packagee.getPackage_name(), res);
		}
		return result;
	}*/
	
	/*@RequestMapping(value="/deleteChannel",method = RequestMethod.GET, headers="Accept=application/json" )
	public String deleteChannel(@ModelAttribute("channel_id") int channel_id)   {
		
		return packService.deleteChannel(channel_id);
	}*/
	
	@RequestMapping(value="/activateChannel",method = RequestMethod.GET, headers="Accept=application/json" )
	public String channelStatusActivate(@ModelAttribute("channel_id") int channel_id)   {
		
		return packService.activateChannel(channel_id);
	}
	
	@RequestMapping(value="/deactivateChannel",method = RequestMethod.GET, headers="Accept=application/json" )
	public String channelStatusDeActivate(@ModelAttribute("channel_id") int channel_id)   {
		
		return packService.deactivateChannel(channel_id);
	}
	
	@RequestMapping(value="/activatePackage",method = RequestMethod.GET, headers="Accept=application/json" )
	public String packageStatusActivate(@ModelAttribute("package_id") int package_id)   {
		
		return packService.activatePackage(package_id);
	}
	
	@RequestMapping(value="/deactivatePackage",method = RequestMethod.GET, headers="Accept=application/json" )
	public String packageStatusDeActivate(@ModelAttribute("package_id") int package_id)   {
		
		return packService.deactivatePackage(package_id);
	}
	
	@RequestMapping(value="/searchPackage",method = RequestMethod.GET, headers="Accept=application/json" )
	public List<SearchPackageTO> getPackages(@PathVariable String client_id)   {
		
		return packService.searchPackages(client_id);
		
		/*JSONArray array=new JSONArray();
		array.put(packService.searchPackages());*/
	}
	
	
	
	/*@RequestMapping(value = "/getPackageAmount", method = RequestMethod.GET, headers = "Accept=application/json")
	public @ResponseBody double getPackageAmount(@RequestParam String package_name){
		return packService.getPackageAmount(package_name);
	}
	
	@RequestMapping(value = "/getAddonAmount", method = RequestMethod.GET, headers = "Accept=application/json")
	public @ResponseBody double getAddOnAmount(@RequestParam String addons){
		return packService.getAddOnAmount(addons);
	}
	
	@RequestMapping(value="/priceChangeForPackage",method = RequestMethod.POST, headers="Accept=application/json" )
	public @ResponseBody Map<String , Integer> priceChange(@RequestBody ChangePackageAmountWrapper packages)   {
		Map<String , Integer> result = new HashMap<String, Integer>();
		Integer res = new Integer(0);
		for(PackageDetails packageDetails:packages.getPackageDetails()){
			//res = packService.updateAmount(packageDetails);
			//result.put(packagee.getPackage_name(), res);
		}
		return result;
	}*/
	
	@RequestMapping(value = "/displayChannelsOfSetupBox", method = RequestMethod.GET, headers = "Accept=application/json")
	public @ResponseBody List<String> displayChannelsOfSetupBox(@RequestParam String customer_id,@RequestParam String box_number){
		return packService.displayChannelsOfSetupBox(customer_id, box_number);
	}
	
	@RequestMapping(value = "/getChannelsCategories", method = RequestMethod.GET, headers = "Accept=application/json")
	public @ResponseBody List<String> getChannelsCategories(@PathVariable String client_id){
		return packService.getChannelsCategories(client_id);
	}
	
	@RequestMapping(value = "/Add_on_Status_Statistics", method = RequestMethod.GET, headers = "Accept=application/json")
	public @ResponseBody List<GetExpireAddonAlertTo> getAlertsForAddonExpiry(@PathVariable String client_id){
		return packService.getAlertsForAddonExpiry(client_id);
	}
	
	@RequestMapping(value = "/getTheAddonsToUpdate", method = RequestMethod.GET, headers = "Accept=application/json")
	public @ResponseBody List<GetTheAddonsToUpdate> getTheAddonsToUpdate(@ModelAttribute CustomerFetchCredetials fetchCredetials,
			@PathVariable String client_id){
		fetchCredetials.setClient_id(client_id);
		return packService.getTheAddonsToUpdate(fetchCredetials);
	}
}
