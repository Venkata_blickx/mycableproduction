package com.blickx.controller;

import java.math.BigDecimal;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import com.blickx.domain.ClientInformationEntity;
import com.blickx.domain.CustomerToMobile;
import com.blickx.output.EditCustomerCommentTo;
import com.blickx.services.SettingsService;
import com.blickx.settings.to.GetBalanceTo;
import com.blickx.to.input.BalanceUpdate;

@RestController
@RequestMapping("/service/{client_id}")
public class SetttingsController {
	
	@Autowired
	SettingsService settingsService;
	
	private Logger logger = Logger.getLogger(SetttingsController.class);

	@RequestMapping(value = "/balanceUpdate", method = RequestMethod.POST, headers = "Accept=application/json")
	public @ResponseBody Map<String, String> updateBalanceAmount(@RequestBody BalanceUpdate input) {

		Map<String, String> res = new HashMap<String, String>();
		logger.info("Balance Update Service Controller - INput:" + input);
		res.put("Repsonse", settingsService.updateBalance(input.getCustomer_id(), input.getUpdatedBalance(), input.getReason())+"");
		logger.info("Balance Update Service Controller - Output:" + res);
		return res;
			
	}
	
	@RequestMapping(value = "/getBalance", method = RequestMethod.POST, headers = "Accept=application/json")
	public @ResponseBody GetBalanceTo getBalanceAmount(@RequestBody String customer_id) {

		return settingsService.getBalanceAmount(customer_id);
			
	}
	
	@RequestMapping(value = "/getServiceChargeForAlCarte", method = RequestMethod.GET, headers = "Accept=application/json")
	public @ResponseBody BigDecimal getServiceCharge(@PathVariable String client_id) {

		return settingsService.getServiceCharge(client_id);
			
	}
	
	@RequestMapping(value = "/getVc_numbers", method = RequestMethod.GET, headers = "Accept=application/json")
	public @ResponseBody List<String> getVc_numbers(@RequestParam("customer_id")String customer_id) {

		return settingsService.getVc_numbers(customer_id);
			
	}
	
	@RequestMapping(value = "/addClientInfoForBlueToothPrint", method = RequestMethod.POST, headers = "Accept=application/json")
	public @ResponseBody Object addClientInfoForBlueToothPrint(@RequestBody ClientInformationEntity clientInformationForPrint) {

		Map<String, Object> response = new HashMap<String, Object>();
		String result = settingsService.addClientInfoForBlueToothPrint(clientInformationForPrint);
		if(result.equalsIgnoreCase("Successfully Added")) {
			response.put("status", new Boolean(true));
			response.put("response", "Successfully updated");
		} else  {
			response.put("status", new Boolean(false));
			response.put("response", result);
		}
		return response;
			
	}

	@RequestMapping(value = "/getClientInfoForBlueToothPrint", method = RequestMethod.POST, headers = "Accept=application/json")
	public @ResponseBody ClientInformationEntity addClientInfoForBlueToothPrint(@RequestBody ClientInformationEntity clientInformationForPrint, @PathVariable("client_id") String client_id) {

		return settingsService.getClientInfoForBlueToothPrint(clientInformationForPrint, client_id);
			
	}
	
	@RequestMapping(value = "/SaveOrUpdateComments", method = RequestMethod.POST, headers = "Accept=application/json")
	public @ResponseBody String saveOrUpdateComments(@RequestBody List<EditCustomerCommentTo> comments, @PathVariable("client_id") String client_id) {

		return settingsService.saveOrUpdateComments(comments, client_id);
			
	}
	
	@RequestMapping(value = "/sendWishestoAll", method = RequestMethod.POST, headers = "Accept=application/json")
	public @ResponseBody String sendWishestoAll(@RequestBody String msg, @PathVariable("client_id") String client_id) {

		return settingsService.sendWishestoAll(msg, client_id);
			
	}
	
	@RequestMapping(value = "/paymentReminder", method = RequestMethod.POST, headers = "Accept=application/json")
	public @ResponseBody String paymentReminder(@RequestBody String msg, @PathVariable("client_id") String client_id) {

		return settingsService.paymentReminder(msg, client_id);
			
	}
	
	
	
	
}
