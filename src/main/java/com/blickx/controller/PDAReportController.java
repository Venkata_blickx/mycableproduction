package com.blickx.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import com.blickx.services.PDAReportService;
import com.blickx.to.PDAReportDto;

@RestController
@RequestMapping("/service/{client_id}")
public class PDAReportController {
	
	
	@Autowired
	private PDAReportService pdaService;
	
	
	@RequestMapping(value = "/showDeletedCustomers", method= RequestMethod.GET, headers = "Accept=application/json")
	public @ResponseBody List<PDAReportDto> getPDAReport(@PathVariable("client_id")String client_id)throws Exception{
		
		
		return pdaService.getPDAReqport(client_id);
		
	}

}
