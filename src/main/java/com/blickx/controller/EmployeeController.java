package com.blickx.controller;

import java.util.List;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import com.blickx.dao.UserDao;
import com.blickx.domain.Employee;
import com.blickx.response.ResponseObject;
import com.blickx.searchEntities.SearchEmployee;
import com.blickx.services.EmployeeService;
import com.blickx.to.EmployeAddOrUpdateTo;
import com.blickx.to.GetEmployeeCollectionTo;
import com.blickx.to.GetEmployeeNamesWithIds;
import com.blickx.to.input.EmployeeCredentialsInput;
import com.blickx.to.input.InputIds;
import com.blickx.utill.CommonUtils;

@RestController
@RequestMapping("/service/{client_id}")

public class EmployeeController {

	@Autowired
	EmployeeService empService;
	
	@Autowired
	UserDao udao;
	
	private Logger logger = Logger.getLogger(EmployeeController.class);
	
	@RequestMapping(value="/Add", method = RequestMethod.POST, headers="Accept=application/json")
	public @ResponseBody ResponseObject addEmployee(@PathVariable("client_id")String client_id, @RequestBody EmployeAddOrUpdateTo employee)   {
		
		ResponseObject response = new ResponseObject();
		employee.setClient_id(client_id);
		logger.info("EmployeeController addEmployee Input: " + employee);

		String res = empService.addEmployee(employee);
		
			if (res.equalsIgnoreCase("1")) {
				response.setResponseMsg("Employee added successfully!");
				response.setDataValid(true);
			} else {
				response.setResponseMsg(res);
				response.setDataValid(false);
			}

		logger.info("EmployeeController addEmployee Output: " + response);
		return response;
	       		
	}
	
	@RequestMapping(value="/Employee_Details_byID", method = RequestMethod.POST, headers="Accept=application/json" )
	public EmployeAddOrUpdateTo getEmployee(@RequestBody InputIds input)   {
		EmployeAddOrUpdateTo emp=null;
			if(input.getEmployee_id()!=null&&input.getEmployee_id().trim().length()!=0){
				logger.info(input.getEmployee_id());
				emp=empService.getEmployeeById (input.getEmployee_id());
				logger.info("Controller get Employee: " + emp);
				return emp;
			}
		return emp;
	}
	
	@RequestMapping(value="/Unique_Employee_Details", method = RequestMethod.GET, headers="Accept=application/json" )
	public Employee Unique_Employee_Details(@PathVariable String client_id)   {
			Employee emp=null;
			if(client_id!=null&&client_id.trim().length()!=0){
				emp=empService.Unique_Employee_Details (client_id);
				logger.info("Controller get Employee: " + emp);
				return emp;
			}
		return emp;
	}
	
	@RequestMapping(value="/updateEmployee", method = RequestMethod.POST, headers="Accept=application/json" )
	public @ResponseBody ResponseObject updateEmployee(@RequestBody EmployeAddOrUpdateTo employee)   {

		ResponseObject response = new ResponseObject();
		
		
		String res = empService.updateEmployee(employee);
        logger.info("EmployeeController updateEmployee - Input: " + employee);
		
			
			if(res.equalsIgnoreCase("1")){
				response.setResponseMsg("Employee updated successfully!");
				response.setDataValid(true);
			}else{
				response.setResponseMsg(res);
				response.setDataValid(false);
			}
			
		logger.info("EmployeeController updateEmployee - Output: " + response);
		return response;
			
	}
	
	@RequestMapping(value="/Employee_Details_byID_activate", method = RequestMethod.POST, headers="Accept=application/json" )
	public String activateStatus(@RequestBody InputIds input)   {

		String res="Given Id is Null";
		
        if(input.getEmployee_id()!=null && input.getEmployee_id().trim().length()!=0){
			res=empService.activateStatus(input.getEmployee_id());
	    return res;
		}
	return res;
			
	}
	
	@RequestMapping(value="/Employee_Details_byID_deactivate", method = RequestMethod.POST, headers="Accept=application/json" )
	public String deactivateStatus(@RequestBody InputIds employee_id)   {

		String res="Given Id is Null";
		
        if(employee_id.getEmployee_id()!=null&&employee_id.getEmployee_id().trim().length()!=0){
			res=empService.deactivateStatus(employee_id.getEmployee_id());
	    return res;
		}
	return res;
			
	}
	
	
	@RequestMapping(value = "/Employee_Details", method = RequestMethod.POST, headers = "Accept=application/json")
    public List<Employee> searchEmployee(@PathVariable("client_id")String client_id, @RequestBody SearchEmployee searchEmployee) {
		 		
		logger.info("Employe_Details"+searchEmployee);
		searchEmployee.setClient_id(client_id);
	     return empService.searchEmployees(searchEmployee);
	}
	
	@RequestMapping(value = "/Employee_Detail", method = RequestMethod.POST, headers = "Accept=application/json")
    public List<Employee> getEmployee(@PathVariable("client_id")String client_id, @RequestBody SearchEmployee searchEmployee) {
		 		
		logger.info("Employe_Details"+searchEmployee);
		searchEmployee.setClient_id(client_id);
	    return empService.searchEmployees(searchEmployee);
	}
	
	
	
	
	
	
	@RequestMapping(value="/getWeekCollection", method = RequestMethod.GET, headers="Accept=application/json" )
	public @ResponseBody List<GetEmployeeCollectionTo> getWeekCollectionDaybyDay(
			@ModelAttribute EmployeeCredentialsInput employeeCredentials
			//@RequestBody EmployeeCredentialsInput employeeCredentials
			)   {

		List<GetEmployeeCollectionTo> res = null;
		
		logger.info(employeeCredentials.getEmployee_uid()+"\t"+employeeCredentials.getIme_number()+"\t"+employeeCredentials.getPassword());
		
        if(CommonUtils.exists(employeeCredentials.getEmployee_uid())){
			res=empService.getEmployeeCollection(empService.getEmployee_id(employeeCredentials.getEmployee_uid()));
			return res;
		}else if(CommonUtils.exists(employeeCredentials.getIme_number())){
			//res=empService.getEmployeeCollection(udao.getEmployeeIDusingIme(employeeCredentials.getIme_number()));
			res=empService.getEmployeeCollection(employeeCredentials.getIme_number());
		}
	return res;
			
	}
	
	/*@RequestMapping(value="/getEmployeeNames", method = RequestMethod.GET, headers="Accept=application/json")
	public @ResponseBody List<String> getEmployeeNameList(@PathVariable String client_id)   {

	    return empService.getEmployeeCollection();
			
	}*/
	
	@RequestMapping(value="/getCollectionBoyEmployeeNames", method = RequestMethod.GET, headers="Accept=application/json" )
	public @ResponseBody List<String> getEmployeeNamesToAssignComplaints(@PathVariable String client_id)   {

	    return empService.getEmployeeNamesToAssignComplaints(client_id);
			
	}
	
	@RequestMapping(value="/getInactiveEmployees", method = RequestMethod.GET, headers="Accept=application/json" )
	public @ResponseBody List<Employee> getInactiveEmployees(@PathVariable String client_id)   {

	    return empService.getInactiveEmployees(client_id);
			
	}
	
	@RequestMapping(value="/changePassword", method = RequestMethod.GET, headers="Accept=application/json" )
	public @ResponseBody String changePassword(@ModelAttribute EmployeeCredentialsInput emp)   {

		if(CommonUtils.exists(emp.getEmployee_uid())){
	    return empService.changePassword(empService.getEmployee_id(emp.getEmployee_uid()), emp.getPassword());
		}if(CommonUtils.exists(emp.getIme_number())){
			return empService.changePassword(udao.getEmployeeIDusingIme(emp.getIme_number()), emp.getPassword());
		}
		return "";
			
	}

	@RequestMapping(value="/getEmployeeNamesWithIds", method = RequestMethod.GET, headers="Accept=application/json" )
	public @ResponseBody List<GetEmployeeNamesWithIds> getEmployeeNamesWithIds(@PathVariable String client_id)   {

	    return empService.getEmployeeNamesWithIds(client_id);
			
	}
	
	@RequestMapping(value="/uidUpdate", method = RequestMethod.GET, headers="Accept=application/json" )
	public int uidUpdate(@RequestParam String employee_id,@RequestParam String employee_uid)   {
		return empService.uidUpdate(employee_id,employee_uid);
	}
	
	@RequestMapping(value="/getuid", method = RequestMethod.GET, headers="Accept=application/json" )
	public String getUid(@RequestParam String uid_number)   {
		return empService.getEmployee_id(uid_number);
	}
	
	
}
