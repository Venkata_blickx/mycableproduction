package com.blickx.controller;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.format.annotation.DateTimeFormat;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import com.blickx.response.angular.TotalCollectionStatistics;
import com.blickx.services.AuthenticationService;
import com.blickx.services.DashBoardService;
import com.blickx.settings.to.GetDeActiveAndActiveCustomersAndSetupBoxesCount;
import com.blickx.to.CollectionStatistics;
import com.blickx.to.ConsolidatePaymentStatus;
import com.blickx.to.DailyCollectionReport;
import com.blickx.to.DashBoardCollectionDetailsTo;
import com.blickx.to.PackageDetailsForDashBoardTo;
import com.blickx.utill.CommonUtils;

@RestController
@RequestMapping("/service/{client_id}")
public class DashBoardController {

	private Logger logger = Logger.getLogger(ReportsController.class);

	@Autowired
	DashBoardService dashBoardService;
	
	/**
	 * DashBoardCollectionDetails Stored Procedure
	 * @return
	 * 
	 * DashBoard 1
	 * 
	 */
	
	public static final String AUTHENTICATION_HEADER = "Authorization";

	@Autowired
	public AuthenticationService authService;
	
	@RequestMapping(value = "/Total_Collection_Statistics", method = RequestMethod.GET, headers = "Accept=application/json" )
	public @ResponseBody TotalCollectionStatistics getCollectionsForDashBoard(@PathVariable String client_id,  HttpServletRequest req, HttpServletResponse response) {
		
		/*String authCredentials = req.getHeader(AUTHENTICATION_HEADER);
		
		boolean valid = authService.isAuthorizedUser(user, client_id);
		if(!valid)
		CheckAuthorize(response, authCredentials);
		else{*/
		logger.info("DashBoard Controller Collection Details");
		DashBoardCollectionDetailsTo res = dashBoardService.getCollectionsForDashBoard(client_id);
		TotalCollectionStatistics resultToReturn = new TotalCollectionStatistics();
		resultToReturn.setTotal_Current_Day_Collection_Amount(res.getDay_collection().intValue());
		resultToReturn.setTotal_Current_Week_Collection_Amount(res.getWeek_collection().intValue());
		resultToReturn.setTotal_Current_Month_Collection_Amount(res.getMonth_collection().intValue());
		return resultToReturn;
		/*}
		return null;*/
	}

	/**
	 * @param response
	 * @param authCredentials
	 */
	private void CheckAuthorize(HttpServletResponse response,
			String authCredentials) {
		if(!CommonUtils.exists(authCredentials)){
			if (response instanceof HttpServletResponse) {
				HttpServletResponse httpServletResponse = (HttpServletResponse) response;
				httpServletResponse.setStatus(HttpServletResponse.SC_UNAUTHORIZED);
				//return null;
			}
		}
	}
	
	/**
	 * DashBoard Device_Status_Summary
	 * @return
	 * 
	 * DashBoard 1
	 * 
	 */
	
	@RequestMapping(value = "/Device_Status_Statistics", method = RequestMethod.GET)
	public @ResponseBody List<Object> getDevice_Status_Summary(@PathVariable String client_id) {

		logger.info("DashBoard Controller Collection Details");
		DashBoardCollectionDetailsTo res = dashBoardService.getCollectionsForDashBoard(client_id);
		List<Object> resultToAngular = new ArrayList<Object>();
		String[] conNames = {"","Active","Inactive"};
		resultToAngular.add(conNames);
		Object[] rowValues = {"Device status",res.getActive_device_count(), 2};
		resultToAngular.add(rowValues);
			
		return resultToAngular;
		
	}
	
	/**
	 * DashboardPackageDetails StoredProcedure
	 * 
	 * DashBoard 3
	 * 
	 * 
	 */
	
	@RequestMapping(value = "/Package_Usage_Statistics", method = RequestMethod.GET)
	public @ResponseBody List<Object> dashBoardPackageDetails(@PathVariable String client_id) {

		logger.info("DashBoard Controller Package Details");
		
		List<Object> resultToAngular = new ArrayList<Object>();
		Object[] colNames = {"Plan", "No of subscriptions"};
		resultToAngular.add(colNames);
		for(PackageDetailsForDashBoardTo re : dashBoardService.getPackageDetails(client_id)){
			Object[] res = {re.getPackages().toString(), re.getPackage_count().intValue()};
			resultToAngular.add(res);
		}
		
		return resultToAngular;
		
	}
	
	/**
	 * DeActivate and Activated Customers Count
	 * call DashBoard_Active_DeActive_Count_Of_CustomersAndSetupBoxes SP
	 * 
	 * DashBoard 2
	 * @return
	 */
	
	@RequestMapping(value = "/Customers_Status_Statistics", method = RequestMethod.GET)
	public @ResponseBody List<Object> getDeActivateAndActiveCustomersSetupBoxCount(@PathVariable String client_id) {

		logger.info("DashBoard Controller Smart Card Issued Customers");
		GetDeActiveAndActiveCustomersAndSetupBoxesCount res = dashBoardService.getDeActivateAndActiveCustomersSetupBoxCount(client_id);
		
		List<Object> resultToAngular = new ArrayList<Object>();
		Object[] colNames = {"Status", "No of subscriptions"};
		Object[] rowValues = {"Active", res.getActiveCustomersCount().intValue()};
		Object[] rowValues2 = {"Inactive", res.getDeActiveCustomersCount().intValue()};
		
		resultToAngular.add(colNames);
		resultToAngular.add(rowValues);
		resultToAngular.add(rowValues2);
		
		return resultToAngular;
		
	}
	
	/**
	 * Mapping for Generate Reports
	 * 
	 * - Calls GetCollectionReport Stored Procedure
	 * 
	 * Method Mapping Name : 
	 * 
	 * 
	 * Collection Tab
	 * 
	 * */
	

	@RequestMapping(value = "/Total_Collection_Status", method = RequestMethod.GET)
	public @ResponseBody List<Object> getDailyCollection(@PathVariable String client_id) {

		logger.info("DashBoard Controller getDaily collection");
		List<Object> resultToAngular = new ArrayList<Object>();
		
		List<DailyCollectionReport> res = dashBoardService.getDailyCollection(client_id);
		SimpleDateFormat sdf = new SimpleDateFormat("EEEE");
		Calendar cal = Calendar.getInstance();
		
		Date date=cal.getTime();
		List<String> dates = new ArrayList<String>();
		for (int i=0;i<6;i++){
		   cal.add(Calendar.DAY_OF_MONTH,-1);
		   
		   date=cal.getTime();
		   System.out.println(sdf.format(date));
		   dates.add(""+sdf.format(date));
		}
		Object[] colNames = {"Date", "Paid"};
		Object[] row1 = {"Today", res.get(0).getToday_paid()};
		Object[] row2 = {dates.get(0).toString(), res.get(0).getToday_1_paid()};
		Object[] row3 = {dates.get(1), res.get(0).getToday_2_paid()};
		Object[] row4 = {dates.get(2), res.get(0).getToday_3_paid()};
		Object[] row5 = {dates.get(3), res.get(0).getToday_4_paid()};
		Object[] row6 = {dates.get(4), res.get(0).getToday_5_paid()};
		Object[] row7 = {dates.get(5), res.get(0).getToday_6_paid()};
		
		resultToAngular.add(colNames);
		resultToAngular.add(row1);
		resultToAngular.add(row2);
		resultToAngular.add(row3);
		resultToAngular.add(row4);
		resultToAngular.add(row5);
		resultToAngular.add(row6);
		resultToAngular.add(row7);
		
		
		
		
		return resultToAngular;
		
	}
	
	@RequestMapping(value = "/getConsolidatedPaymentStatus", method = RequestMethod.GET, headers = "Accept=application/json")
	public @ResponseBody List<Object> getConsolidatedPaymentStatus(@PathVariable("client_id")String client_id) {

		List<Object> resultToAngular = new ArrayList<Object>();
		ConsolidatePaymentStatus status = dashBoardService.getConsolidatedPaymentStatus(client_id);
		System.out.println(status);
		resultToAngular.add(new Object[]{"Task","Hours Per Day"});
		//resultToAngular.add(new Object[]{"Total",status.getTotal()});
		System.out.println(status.getPending());
		resultToAngular.add(new Object[]{"Pending",status.getPending()});
		resultToAngular.add(new Object[]{"Collected",status.getCollected()});
		
		
		/*resultToAngular.add(new Object[]{"Pending",750});
		resultToAngular.add(new Object[]{"Collected", 250});*/
		
		List<Object> result = new ArrayList<Object>();
		result.add(resultToAngular);
		result.add(new Object[]{"Total",status.getTotal()});
		
		return result;
			
	}
	
	@RequestMapping(value = "/CollectionStatistics", method = RequestMethod.GET, headers = "Accept=application/json")
	public @ResponseBody List<CollectionStatistics> getCollectionStatistics(@PathVariable("client_id")String client_id) {

		return dashBoardService.getCollectionStatistics(client_id);
			
	}

}
