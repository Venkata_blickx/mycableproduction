package com.blickx.controller;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.apache.log4j.Logger;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import com.blickx.dao.UserDao;
import com.blickx.domain.ComplaintEntity;
//import com.blickx.mycable.beme.dto.ComplaintBemeInputDTO;
import com.blickx.output.GetComplaintList;
import com.blickx.searchEntities.AssignComplaintSearch;
import com.blickx.searchEntities.CloseComplaint;
import com.blickx.searchEntities.GetComplaint;
import com.blickx.searchEntities.SearchByDateEntity;
import com.blickx.services.ComplaintService;
import com.blickx.services.CustomerService;
import com.blickx.services.EmployeeService;
import com.blickx.to.AddComplaintOfflineResponse;
import com.blickx.to.ComplaintRatingObject;
import com.blickx.to.ComplaintStatusTo;
import com.blickx.to.ComplaintSummaryTo;
import com.blickx.to.GetOpenClosedComplaintTo;
import com.blickx.to.input.ComplaintInputTo;
import com.blickx.utill.CommonUtils;
import com.blickx.wrapper.CloseComplaintsWrapper;
import com.blickx.wrapper.ComplaintWrapper;

	/**
	 * Complaints Controller
	 *
	 */
@RestController
@RequestMapping("/service/{client_id}/complaint")
public class ComplaintsController {
	
	@Autowired
	ComplaintService complaintService;
	
	@Autowired
	EmployeeService eService;
	
	@Autowired
	CustomerService cService;
	
	@Autowired
	UserDao udao;
	
	private Logger logger = Logger.getLogger(ComplaintsController.class);
	
	/*public static String client_id ;
	static{
	@PathVariable
	}
	//static String client_id = getClient_id() 
*/	
	
	/*@RequestMapping(value="/addComplaintBeme",method = {RequestMethod.POST,RequestMethod.GET}, headers="Accept=application/json" )
	public String addComplaintBeme(@PathVariable String client_id,
			@RequestBody ComplaintBemeInputDTO complaintTo)   {
		
		logger.info("************* addComplaintDesktop complaint Input************" + complaintTo);
		
		complaintTo.setClient_id(client_id);
		return complaintService.addComplaintBeeMe(complaintTo);
		
	}*/
	
	
	@RequestMapping(value="/addComplaintDesktop",method = {RequestMethod.POST,RequestMethod.GET}, headers="Accept=application/json" )
	public String addComplaintDesktop(@PathVariable String client_id,
			@RequestBody ComplaintInputTo complaintTo)   {
		
		logger.info("************* addComplaintDesktop complaint Input************" + complaintTo);
		
		complaintTo.setClient_id(client_id);
		return complaintService.addComplaintDesktop(complaintTo);
		
	}
	
	@RequestMapping(value="/addComplaint",method = {RequestMethod.POST, RequestMethod.GET}, headers="Accept=application/json" )
	public String addComplaint(@PathVariable String client_id,
			@ModelAttribute("complaint") ComplaintInputTo complaintTo)   {
		
		logger.info("*************  addComplaint complaint Input************" + complaintTo);
		
		ComplaintEntity complaint = new ComplaintEntity();
		BeanUtils.copyProperties(complaintTo, complaint);
		//complaint.setCustomer_id(cService.getCustomer_id(complaintTo.getCustomer_uid()));
		if(CommonUtils.exists(complaintTo.getEmployee_uid()))
			complaint.setEmployee_id(eService.getEmployee_id(complaintTo.getEmployee_uid()));
		if(CommonUtils.exists(complaintTo.getIme_number()))
			complaint.setEmployee_id(udao.getEmployeeIDusingIme(complaintTo.getIme_number()));
		complaint.setClient_id(client_id);
		return complaintService.addComplaint(complaint);
		
	}
	
	@RequestMapping(value="/addComplaintOffline",method = {RequestMethod.POST,RequestMethod.GET}, headers="Accept=application/json", consumes = "application/json", produces = "application/json" )
	public @ResponseBody List<AddComplaintOfflineResponse> addComplaints(
			@PathVariable String client_id,
			@RequestBody ComplaintWrapper complaintsTo)   {
		
		logger.info("*************  addComplaint complaint Input************" + complaintsTo);
		
		List<AddComplaintOfflineResponse> result = new ArrayList<AddComplaintOfflineResponse>();
		for(ComplaintInputTo complaintTo:complaintsTo.getComplaintsTo()){
			
			AddComplaintOfflineResponse addComplaintResponse = new AddComplaintOfflineResponse();
			
			ComplaintEntity complaint = new ComplaintEntity();
			BeanUtils.copyProperties(complaintTo, complaint);
			complaint.setClient_id(client_id);
			//String customer_id = cService.getCustomer_id(complaintTo.getCustomer_uid());
			//complaint.setCustomer_id(customer_id);
			//complaint.setEmployee_id(eService.getEmployee_id(complaintTo.getEmployee_uid()));
			if(CommonUtils.exists(complaintTo.getEmployee_uid()))
				complaint.setEmployee_id(eService.getEmployee_id(complaintTo.getEmployee_uid()));
			if(CommonUtils.exists(complaintTo.getIme_number()))
				complaint.setEmployee_id(udao.getEmployeeIDusingIme(complaintTo.getIme_number()));
			String res =  complaintService.addComplaint(complaint);
			//addComplaintResponse.setCustomer_uid(complaintTo.getCustomer_uid());
			addComplaintResponse.setCustomer_id(complaint.getCustomer_id());
			addComplaintResponse.setResponse(res);
			result.add(addComplaintResponse);
		
		}
		return result;
	}
	
	@SuppressWarnings("unchecked")
	@RequestMapping(value="/getComplaint", method = {RequestMethod.GET, RequestMethod.POST}, headers="Accept=application/json" )
	public ComplaintEntity getComplaint(
			@PathVariable String client_id,
			@RequestBody GetComplaint getComplaint)   {
		
			getComplaint.setClient_id(client_id);
		return complaintService.getComplaint(getComplaint);
		
	}
	
	@RequestMapping(value="/getOpenOrClosedComplaint",method = RequestMethod.GET, headers="Accept=application/json" )
	public List<GetOpenClosedComplaintTo> getOpenOrClosedComplaint(@PathVariable String client_id, @ModelAttribute("type") String type)   {
		
		return complaintService.getOpenOrClosedComplaints(client_id, type);
		
	}
	
	@RequestMapping(value="/getOpenOrClosedComplaintByDate",method = RequestMethod.GET, headers="Accept=application/json" )
	public List<GetOpenClosedComplaintTo> getOpenOrClosedComplaint(
			@PathVariable String client_id,
			@ModelAttribute("SearchByDateEntity")SearchByDateEntity dates)   {
		dates.setClient_id(client_id);
		return complaintService.getOpenOrClosedComplaintsByDate(dates);
		
	}
	
	@RequestMapping(value = "/updateComplaint", method = RequestMethod.GET, headers = "Accept=application/json")
	public @ResponseBody Map<String, String> getComplaintById(
			@ModelAttribute ComplaintEntity complaint) {

		return complaintService.updateComplaint(complaint);

	}
	
	@RequestMapping(value="/getComplaintStatusDetails",method = RequestMethod.GET, headers="Accept=application/json" )
	public @ResponseBody List<Object> getComplaintStatus(@PathVariable("client_id") String client_id)   {

		
		List<Object> resultToAngular = new ArrayList<Object>();
		ComplaintStatusTo status = complaintService.getComplaintStatus(client_id);
		resultToAngular.add(new Object[]{"Task","Hours Per Day"});
		/*resultToAngular.add(new Object[]{"total",status.getTotal()});
		resultToAngular.add(new Object[]{"pending",status.getPending()});
		resultToAngular.add(new Object[]{"collected",status.getCollected()});
		*/
		
		resultToAngular.add(new Object[]{"Open",status.getOpen()});
		resultToAngular.add(new Object[]{"Assigned", status.getAssign()});
		resultToAngular.add(new Object[]{"Resolved", status.getResolve()});
		
		List<Object> result = new ArrayList<Object>();
		result.add(resultToAngular);
		result.add(new Object[]{"total",(status.getOpen().intValue() + status.getAssign().intValue())});
		
		
		
		
		
		return result;
		
	}
	
	@RequestMapping(value="/getComplaintSummaryOpen",method = RequestMethod.GET, headers="Accept=application/json" )
	public @ResponseBody List<ComplaintSummaryTo> getComplaintSummaryOpen(@PathVariable("client_id") String client_id)   {
		
		return complaintService.getComplaintSummaryOpen(client_id);
		
	}
	
	@RequestMapping(value="/assignComplaint",method = RequestMethod.POST, headers="Accept=application/json" )
	public @ResponseBody int makeAssign(@RequestBody AssignComplaintSearch assign)   {
		
		return complaintService.makeAssign(assign);
		
	}
	
	@RequestMapping(value="/closeComplaint",method = RequestMethod.POST, headers="Accept=application/json" )
	public @ResponseBody int getComplaintSummaryOpen(@RequestBody CloseComplaint closeComplaint)   {
		
		return complaintService.closeComplaint(closeComplaint);
		
	}
	
	@RequestMapping(value="/resolveComplaints",method = RequestMethod.POST, headers="Accept=application/json" )
	public @ResponseBody Map<String, String> resolveComplaints(@RequestBody CloseComplaintsWrapper closeComplaintsWrap)   {
		Map<String, String> response = new HashMap<String, String>();
		
		logger.info("resolveComplaint Controller INput:" + closeComplaintsWrap);
		
		for(CloseComplaint closeComplaint : closeComplaintsWrap.getResloveComplaints()) {
			Integer res = complaintService.resolveComplaint(closeComplaint);
			logger.info("resolveComplaint Controller Individual Complaint :" + closeComplaint);
			response.put("status", res.toString());
			response.put("complaint_id", closeComplaint.getComplaint_id());
		}
		
		return response;
		
	}
	
	@RequestMapping(value="/complaintRating",method = RequestMethod.GET, headers="Accept=application/json" )
	public @ResponseBody ComplaintRatingObject getComplaintRating(@PathVariable String client_id)   {
		
		return complaintService.getComplaintRating(client_id);
		
	}
	
	@RequestMapping(value="/getC_ids",method = RequestMethod.GET, headers="Accept=application/json" )
	public @ResponseBody List<String> getC_ids(@PathVariable String client_id)   {
		
		return complaintService.getC_ids(client_id);
		
	}
	// Android Service
	@RequestMapping(value="/getComplaintsList",method = RequestMethod.POST, headers="Accept=application/json" )
	public @ResponseBody List<GetComplaintList> GetComplaintList(@PathVariable String client_id, @RequestBody Map<String, String> input)   {
		
		return complaintService.GetComplaintList(client_id, input);
		
	}
	
	@RequestMapping(value="/getUpdatedComplaintsList",method = RequestMethod.POST, headers="Accept=application/json" )
	public @ResponseBody List<GetComplaintList> GetUpdatedComplaintList(@PathVariable String client_id, @RequestBody Map<String, String> input)   {
		
		return complaintService.GetUpdatedComplaintList(client_id, input);
		
	}
}
