package com.blickx.sms.gateway.send;

import java.io.BufferedReader;
import java.io.DataOutputStream;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.URL;

import org.apache.log4j.Logger;

import com.blickx.domain.SMSGateWayEntity;
import com.blickx.to.input.DetailsToSend;

public class SendSMS {
	
	
public static String sendSMSToCustomer(/*Session session,*/ DetailsToSend details, SMSGateWayEntity smsGateWay) {
	
	Logger logger = Logger.getLogger(SendSMS.class);
	

	String res = "";
	try{
	
	//String url = "http://123.63.33.43/blank/sms/user/urlsmstemp.php";
	URL obj = new URL(smsGateWay.getUrl());
	HttpURLConnection con = (HttpURLConnection) obj.openConnection();
	
	//logger.info("Details to Send SMS: " + details);
	logger.info("GateWay Details : " + smsGateWay);

	//add reuqest header
	con.setRequestMethod("GET");

	 String requestUrl  = ""
         		+ "username=" + smsGateWay.getUsername()
         		+ "&pass=" + smsGateWay.getPassword()
         		+ "&senderid=" + smsGateWay.getFrom()
         		+ "&dest_mobileno=" + details.getContact_number()
         		+ "&tempid=" + details.getTempId()
         		+ "&F1=" + details.getF1()
         		+ "&F2=" + details.getF2()
         		+ "&F3=" + details.getF3()
         		+ "&F4=" + details.getF4()
         		+ "&F5=" + details.getF5()
         		+ "&response=Y" ;
	
	// Send post request
	con.setDoOutput(true);
	DataOutputStream wr = new DataOutputStream(con.getOutputStream());
	wr.writeBytes(requestUrl);
	wr.flush();
	wr.close();

	int responseCode = con.getResponseCode();
	

	BufferedReader in = new BufferedReader(
	        new InputStreamReader(con.getInputStream()));
	String inputLine;
	StringBuffer response = new StringBuffer();

	while ((inputLine = in.readLine()) != null) {
		response.append(inputLine);
	}
	logger.info("\nSending 'POST' request to URL : " + smsGateWay.getUrl());
	logger.info("Post parameters : " + requestUrl);
	logger.info("Response Code : " + responseCode);
	logger.info("Response MSG : " + response);
	in.close();

	} catch(Exception e ) {
		logger.error("SendSMS Catch Block Exception found : " + e);
	}
	
	
	
	
	
	

	
	/*
	 * Old sms code
	 * 
	 */
	
	 /*try {
		// String contactNumber = details.getContact_number();
		 
		 String msg = details.getMessage();
		 String contactNumber = details.getContact_number();
		 String user = smsGateWay.getUsername();
		 String password = smsGateWay.getPassword();
		 String from = smsGateWay.getFrom();
		 logger.info("Send SMS Contact MSGSent: " + details.getMessage());
		 logger.info("Send SMS CustomerId: " + details.getCustomer_id());
		 	//String contact_number = (String) session.createQuery("select c.contact_number from Customer c where c.customer_id = ?").setString(0, details.getCustomer_id()).uniqueResult();
		 //details.setContact_number();
		 	logger.info("Send SMS Contact Number: " + details.getContact_number());
		 
		// SMSGateWayEntity smsGateWay = (SMSGateWayEntity) session.load(SMSGateWayEntity.class, 1);
		 
         String requestUrl  = "http://mobiprom.com/smsclient/pushsms.jsp?" +
						"user=" + URLEncoder.encode(user, "UTF-8") +
						"&password=" + URLEncoder.encode(password, "UTF-8") +
						"&message=" + URLEncoder.encode(msg, "UTF-8") +
						"&from=" + URLEncoder.encode(from, "UTF-8") +
						"&to=" + URLEncoder.encode(contactNumber, "UTF-8");
						


        // http://mobiprom.com/smsclient/pushsms.jsp?user=CL20746&password=QPTp7m6&message=message&from=MCABLE&to=9886182699
         
         URL url = new URL(requestUrl);
         HttpURLConnection uc = (HttpURLConnection)url.openConnection();
         
         uc.addRequestProperty("User-Agent", 
        	        "Mozilla/4.0 (compatible; MSIE 6.0; Windows NT 5.0)");

        	        uc.connect();
         
         res = uc.getResponseMessage();
         logger.info("Send SMS Contact MSGSent Response: " + res);
         
         if (uc.getResponseCode() != 200) {
 			logger.info("Failed : HTTP error code : " + uc.getResponseCode());
 		}else{

         //session.createSQLQuery("update authentication set msg_count = msg_count+1 where client_id = ?").setParameter(0, details.getClient_id()).executeUpdate();
         
 		}	
         
         uc.disconnect();
        

 } catch(Exception ex) {
         logger.error("***********"+ex);
 }*/
	 return res;
}
}
