package com.blickx.sms.gateway.send;

import org.hibernate.Session;

import com.blickx.domain.SMSGateWayEntity;
import com.blickx.to.input.DetailsToSend;

public class SendSMSRunnable implements Runnable {

    Session session;
    DetailsToSend details;
    SMSGateWayEntity smsGateWay;

    public SendSMSRunnable(/*Session session,*/ DetailsToSend details, SMSGateWayEntity smsGateWay) {
    	System.out.println("SendSMSRunnable Thread Constructor.............");
      // this.session = session;
       this.details = details;
       this.smsGateWay = smsGateWay;
    }

    public void run() {
        System.out.println("SendSMSRunnable Thread run started................................");
    	SendSMS.sendSMSToCustomer(/*session,*/ details, smsGateWay);
    	System.out.println("SendSMSRunnable Thread run ended................................");
    	
    }
}