package com.blickx.domain;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name = "temp_customerid")
public class TempCustomerId {

	@Override
	public String toString() {
		return "TempCustomerId [customer_id=" + customer_id + ", client_id="
				+ client_id + "]";
	}

	public String getCustomer_id() {
		return customer_id;
	}

	public void setCustomer_id(String customer_id) {
		this.customer_id = customer_id;
	}

	public String getClient_id() {
		return client_id;
	}

	public void setClient_id(String client_id) {
		this.client_id = client_id;
	}

	@Id
	@Column(name = "id")
	private String customer_id;
	
	@Column(name = "client_id")
	private String client_id;
	
	public TempCustomerId(){
		
	}
	
}
