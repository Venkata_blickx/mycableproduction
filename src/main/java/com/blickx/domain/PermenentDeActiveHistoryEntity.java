package com.blickx.domain;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name = "permenent_deactive_history")
public class PermenentDeActiveHistoryEntity implements Serializable {

	/**
	 * Permenently Deactivated Customers C_id history
	 */
	private static final long serialVersionUID = 8652075394738578920L;
	
	@Id
	@GeneratedValue(strategy=GenerationType.AUTO)
	@Column(name = "pd_history_id")
	private int id;
	
	@Column
	private String customer_id;
	
	@Column
	private String client_id;
	
	@Column
	private String removed_cid;
	
	@Column
	private Date deactivated_on;
	
	@Column
	private String employee_id;

	public String getEmployee_id() {
		return employee_id;
	}

	public void setEmployee_id(String employee_id) {
		this.employee_id = employee_id;
	}

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public String getCustomer_id() {
		return customer_id;
	}

	public void setCustomer_id(String customer_id) {
		this.customer_id = customer_id;
	}

	public String getClient_id() {
		return client_id;
	}

	public void setClient_id(String client_id) {
		this.client_id = client_id;
	}

	public String getRemoved_cid() {
		return removed_cid;
	}

	public void setRemoved_cid(String removed_cid) {
		this.removed_cid = removed_cid;
	}

	public Date getDeactivated_on() {
		return deactivated_on;
	}

	public void setDeactivated_on(Date deactivated_on) {
		this.deactivated_on = deactivated_on;
	}
	
	public PermenentDeActiveHistoryEntity(){
		
	}

	@Override
	public String toString() {
		return "PermenentDeActiveHistoryEntity [id=" + id + ", customer_id="
				+ customer_id + ", client_id=" + client_id + ", removed_cid="
				+ removed_cid + ", deactivated_on=" + deactivated_on
				+ ", employee_id=" + employee_id + "]";
	}

}
