package com.blickx.domain;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name="package_channel")
public class PackageChannel implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = -9087824125592563421L;

	@Id
	@GeneratedValue(strategy=GenerationType.AUTO)
	@Column(name="id")
	private int id;
	
	@Column
	private int package_id;
	
	@Column
	private int channel_id;

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public int getPackage_id() {
		return package_id;
	}

	public void setPackage_id(int package_id) {
		this.package_id = package_id;
	}

	public int getChannel_id() {
		return channel_id;
	}

	public void setChannel_id(int channel_id) {
		this.channel_id = channel_id;
	}

	public PackageChannel(int package_id, int channel_id) {
		super();
		this.package_id = package_id;
		this.channel_id = channel_id;
	}

	public PackageChannel() {
		// TODO Auto-generated constructor stub
	}
	
	@Override
	public String toString() {
		return "PackageChannel [id=" + id + ", package_id=" + package_id
				+ ", channel_id=" + channel_id + "]";
	}
	
	
}
