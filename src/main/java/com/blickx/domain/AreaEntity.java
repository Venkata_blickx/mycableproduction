package com.blickx.domain;

import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

import com.fasterxml.jackson.databind.annotation.JsonDeserialize;
import com.fasterxml.jackson.databind.annotation.JsonSerialize;

import com.blickx.utill.CustomJsonDateDeserializer;
import com.blickx.utill.DateSerializerTimeStamp;

@Entity
@Table(name = "area")
public class AreaEntity {

	@Id
	@GeneratedValue(strategy=GenerationType.AUTO)
	@Column(name = "area_id")
	private int id;
	
	@Column(name = "area_code")
	private String areaCode;
	
	@Column(name = "area_description")
	private String areaDescription;
	
	@Column(name = "client_id")
	private String clientId;
	
	@Column(name = "created_timestamp")
	private Date createdTimeStamp;
	
	@Column(name = "last_update_timestamp")	
	private Date lastUpdatedTimestamp;

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public String getAreaCode() {
		return areaCode;
	}

	public void setAreaCode(String areaCode) {
		this.areaCode = areaCode;
	}

	public String getAreaDescription() {
		return areaDescription;
	}

	public void setAreaDescription(String areaDescription) {
		this.areaDescription = areaDescription;
	}

	public String getClientId() {
		return clientId;
	}

	public void setClientId(String clientId) {
		this.clientId = clientId;
	}

	@JsonSerialize(using = DateSerializerTimeStamp.class)
	public Date getCreatedTimeStamp() {
		return createdTimeStamp;
	}
	@JsonDeserialize(using = CustomJsonDateDeserializer.class)	
	public void setCreatedTimeStamp(Date createdTimeStamp) {
		this.createdTimeStamp = createdTimeStamp;
	}

	@JsonSerialize(using = DateSerializerTimeStamp.class)
	public Date getLastUpdatedTimestamp() {
		return lastUpdatedTimestamp;
	}

	@JsonDeserialize(using = CustomJsonDateDeserializer.class)
	public void setLastUpdatedTimestamp(Date lastUpdatedTimestamp) {
		this.lastUpdatedTimestamp = lastUpdatedTimestamp;
	}

	@Override
	public String toString() {
		return "AreaEntity [id=" + id + ", areaCode=" + areaCode
				+ ", areaDescription=" + areaDescription + ", clientId="
				+ clientId + ", createdTimeStamp=" + createdTimeStamp
				+ ", lastUpdatedTimestamp=" + lastUpdatedTimestamp + "]";
	}
	
	public AreaEntity(){
		
	}
}
