package com.blickx.domain;

import java.io.Serializable;
import java.util.Date;
import java.util.Set;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.persistence.Table;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.databind.annotation.JsonSerialize;

import org.springframework.format.annotation.DateTimeFormat;

@Entity
@Table(name = "customer")
@JsonIgnoreProperties({"hibernateLazyInitializer", "handler"})
public class Customer implements Serializable {

	@Override
	public String toString() {
		return "Customer [customer_id=" + customer_id + ", customer_name="
				+ customer_name + ", contact_number=" + contact_number
				+ ", areacode=" + areacode + ", email=" + email
				+ ", landline_number=" + landline_number + ", end_date="
				+ end_date + ", joining_date=" + joining_date
				+ ", customer_photo=" + customer_photo + ", customer_doc="
				+ customer_doc + ", gender=" + gender + ", status=" + status
				+ ", category=" + category + ", line1=" + line1 + ", line2="
				+ line2 + ", city=" + city + ", state=" + state + ", pincode="
				+ pincode + ", card_number=" + card_number + ", photo_name="
				+ photo_name + ", photo_type=" + photo_type + ", doc_name="
				+ doc_name + ", doc_type=" + doc_type + ", customer_uid="
				+ customer_uid + ", c_id=" + c_id + ", deposit=" + deposit
				+ ", permenent_status=" + permenent_status + ", client_id="
				+ client_id + ", billing_type=" + billing_type
				+ ", signature_image=" + signature_image + ", externalId="
				+ externalId + ", domain=" + domain + ", setupBoxes="
				+ setupBoxes + "]";
	}

	private static final long serialVersionUID = 3085688839418384991L;

	@Id
	@Column(name = "customer_id")
	private String customer_id;

	@Column(name = "customer_name")
	private String customer_name;

	@Column(name = "contact_number")
	private String contact_number;

	@Column(name = "areacode")
	private String areacode;
	
	@Column
	private String email;
	
	@Column
	private String landline_number;
	
	@Column
	@DateTimeFormat(pattern = "yyyy-MM-dd")
	private Date end_date;
	
	@Column(name = "joining_date")
	@DateTimeFormat(pattern = "yyyy-MM-dd")
	private Date joining_date;

	@Column(name = "customer_photo")
	private String customer_photo;

	@Column(name = "customer_doc")
	private String customer_doc;

	@Column(name = "gender")
	private String gender;

	@Column(name = "status")
	private boolean status;
	
	@Column
	private String category;
	
	@Column
	private String line1;
	
	@Column
	private String line2;
	
	@Column
	private String city;
	
	@Column
	private String state;
	
	@Column
	private String pincode;
	
	@Column
	private String card_number;
	
	@Column
	private String photo_name;
	
	@Column
	private String photo_type;
	
	@Column
	private String doc_name;
	
	@Column
	private String doc_type;
	
	@Column
	private String customer_uid;
	
	@Column
	private String c_id;
	
	@Column
	private double deposit;
	
	@Column
	private boolean permenent_status;
	
	@Column
	private String client_id;
	
	@Column
	private boolean billing_type;
	
	@Column
	private String signature_image;
	
	@Column(name = "external_id")
	private String externalId;
	
	@Column
	private String domain;
	

	@OneToMany(mappedBy="customer", fetch = FetchType.LAZY, cascade=CascadeType.ALL)
	private Set<SetupBox> setupBoxes;

	public String getExternalId() {
		return externalId;
	}

	public void setExternalId(String externalId) {
		this.externalId = externalId;
	}

	public String getDomain() {
		return domain;
	}

	public void setDomain(String domain) {
		this.domain = domain;
	}

	public String getCustomer_id() {
		return customer_id;
	}

	public String getCard_number() {
		return card_number;
	}

	public String getSignature_image() {
		return signature_image;
	}

	public void setSignature_image(String signature_image) {
		this.signature_image = signature_image;
	}

	public String getClient_id() {
		return client_id;
	}

	public void setClient_id(String client_id) {
		this.client_id = client_id;
	}

	public boolean isPermenent_status() {
		return permenent_status;
	}

	public void setPermenent_status(boolean permenent_status) {
		this.permenent_status = permenent_status;
	}

	public String getCustomer_uid() {
		return customer_uid;
	}

	public void setCustomer_uid(String customer_uid) {
		this.customer_uid = customer_uid;
	}

	public String getC_id() {
		return c_id;
	}

	public void setC_id(String c_id) {
		this.c_id = c_id;
	}

	public String getPhoto_name() {
		return photo_name;
	}

	public void setPhoto_name(String photo_name) {
		this.photo_name = photo_name;
	}

	public String getPhoto_type() {
		return photo_type;
	}

	public void setPhoto_type(String photo_type) {
		this.photo_type = photo_type;
	}

	public String getDoc_name() {
		return doc_name;
	}

	public void setDoc_name(String doc_name) {
		this.doc_name = doc_name;
	}

	public String getDoc_type() {
		return doc_type;
	}

	public void setDoc_type(String doc_type) {
		this.doc_type = doc_type;
	}

	public void setCard_number(String card_number) {
		this.card_number = card_number;
	}

	public void setCustomer_id(String customer_id) {
		this.customer_id = customer_id;
	}

	public String getLine1() {
		return line1;
	}

	public void setLine1(String line1) {
		this.line1 = line1;
	}

	public String getLine2() {
		return line2;
	}

	public void setLine2(String line2) {
		this.line2 = line2;
	}

	public String getCity() {
		return city;
	}

	public void setCity(String city) {
		this.city = city;
	}

	public String getState() {
		return state;
	}

	public void setState(String state) {
		this.state = state;
	}

	public String getPincode() {
		return pincode;
	}

	public void setPincode(String pincode) {
		this.pincode = pincode;
	}

	public String getCustomer_name() {
		return customer_name;
	}

	public void setCustomer_name(String customer_name) {
		this.customer_name = customer_name;
	}

	public String getContact_number() {
		return contact_number;
	}

	public void setContact_number(String contact_number) {
		this.contact_number = contact_number;
	}

	public String getAreacode() {
		return areacode;
	}

	public void setAreacode(String areacode) {
		this.areacode = areacode;
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public String getLandline_number() {
		return landline_number;
	}

	public void setLandline_number(String landline_number) {
		this.landline_number = landline_number;
	}

	@JsonSerialize(using = DateSerializer.class)
	public Date getJoining_date() {
		return joining_date;
	}

	@JsonSerialize(using = DateSerializer.class)
	public Date getEnd_date() {
		return end_date;
	}

	public void setEnd_date(Date end_date) {
		this.end_date = end_date;
	}

	public void setJoining_date(Date joining_date) {
		this.joining_date = joining_date;
	}


	public String getCustomer_photo() {
		return customer_photo;
	}

	public void setCustomer_photo(String customer_photo) {
		this.customer_photo = customer_photo;
	}

	public String getCustomer_doc() {
		return customer_doc;
	}

	public void setCustomer_doc(String customer_doc) {
		this.customer_doc = customer_doc;
	}

	public String getGender() {
		return gender;
	}

	public void setGender(String gender) {
		this.gender = gender;
	}

	public boolean isStatus() {
		return status;
	}

	public void setStatus(boolean status) {
		this.status = status;
	}

	public String getCategory() {
		return category;
	}

	public void setCategory(String category) {
		this.category = category;
	}

	public Customer() {
	}

	public Set<SetupBox> getSetupBoxes() {
		return setupBoxes;
	}

	public void setSetupBoxes(Set<SetupBox> setupBoxes) {
		this.setupBoxes = setupBoxes;
	}

	public double getDeposit() {
		return deposit;
	}

	public void setDeposit(double deposit) {
		this.deposit = deposit;
	}

	public boolean isBilling_type() {
		return billing_type;
	}

	public void setBilling_type(boolean billing_type) {
		this.billing_type = billing_type;
	}

	

}
