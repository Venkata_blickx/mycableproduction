package com.blickx.domain;

import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;

import org.springframework.format.annotation.DateTimeFormat;

@Entity(name = "employee_device")
public class EmployeeDeviceMap {

	@Id
	@GeneratedValue(strategy=GenerationType.AUTO)
	@Column
	private int id;
	
	@Column
	private String employee_id;
	
	@Column
	private int device_id;
	
	@Column
	@DateTimeFormat(pattern = "yyyy-MM-dd")
	private Date effective_from;
	
	@Column
	@DateTimeFormat(pattern = "yyyy-MM-dd")
	private Date effective_to;

	public String getEmployee_id() {
		return employee_id;
	}

	public void setEmployee_id(String employee_id) {
		this.employee_id = employee_id;
	}

	public int getDevice_id() {
		return device_id;
	}

	public void setDevice_id(int device_id) {
		this.device_id = device_id;
	}

	public Date getEffective_from() {
		return effective_from;
	}

	public void setEffective_from(Date effective_from) {
		this.effective_from = effective_from;
	}

	public Date getEffective_to() {
		return effective_to;
	}

	public void setEffective_to(Date effective_to) {
		this.effective_to = effective_to;
	}

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}
	
	public EmployeeDeviceMap(){
		
	}
}
