package com.blickx.domain;

import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name = "device_locations")
public class DeviceLocation {

	@Id
	@Column(name = "device_id")
	private int deviceId;
	
	@Column(name = "location_longitude")
	private String locationLangitude;
	
	@Column(name = "location_lattitude")
	private String locationLattitude;
	
	@Column(name = "updated_timestamp")
	private Date updatedTimestamp;
	

	@Override
	public String toString() {
		return "DeviceLocation [deviceId=" + deviceId + ", locationLangitude="
				+ locationLangitude + ", locationLattitude="
				+ locationLattitude + ", updatedTimestamp=" + updatedTimestamp
				+ "]";
	}

	public int getDeviceId() {
		return deviceId;
	}

	public void setDeviceId(int deviceId) {
		this.deviceId = deviceId;
	}

	public String getLocationLangitude() {
		return locationLangitude;
	}

	public void setLocationLangitude(String locationLangitude) {
		this.locationLangitude = locationLangitude;
	}

	public String getLocationLattitude() {
		return locationLattitude;
	}

	public void setLocationLattitude(String locationLattitude) {
		this.locationLattitude = locationLattitude;
	}

	public Date getUpdatedTimestamp() {
		return updatedTimestamp;
	}

	public void setUpdatedTimestamp(Date updatedTimestamp) {
		this.updatedTimestamp = updatedTimestamp;
	}
	
}
