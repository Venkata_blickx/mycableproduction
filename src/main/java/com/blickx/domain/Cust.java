package com.blickx.domain;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;



@Entity
@Table(name="cust")
public class Cust {

	@Id
	@GeneratedValue(strategy=GenerationType.AUTO)
	@Column(name="id")
	private int id;
	
	@Override
	public String toString() {
		return "Cust [id=" + id + ", custId=" + custId + ", custName="
				+ custName + "]";
	}
	@Column(name="custId")
	private String custId;
	
	@Column(name="custName")
	private String custName;
	
	public Cust(){}
	public Cust(String custId, String custName) {
		super();
		this.custId = custId;
		this.custName = custName;
	}
	public String getCustId() {
		return custId;
	}
	public void setCustId(String custId) {
		this.custId = custId;
	}
	public String getCustName() {
		return custName;
	}
	public void setCustName(String custName) {
		this.custName = custName;
	}
	
	
}
