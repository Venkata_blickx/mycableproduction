package com.blickx.domain;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import org.springframework.format.annotation.DateTimeFormat;


@Entity
@Table(name="selected_device")
public class SelectedDevice implements Serializable{

	@Override
	public String toString() {
		return "SelectedDevice [device_id=" + device_id + ", device_name="
				+ device_name + ", date_of_purchase=" + date_of_purchase
				+ ", ime_number=" + ime_number + ", service_provider="
				+ service_provider + ", data_plan=" + data_plan + ", status="
				+ status + ", sim_number=" + sim_number + ", employee_id="
				+ employee_id + ", employee_name=" + employee_name
				+ ", login_status=" + login_status + ", client_id=" + client_id
				+ "]";
	}

	/**
	 * This Refers device Table in Data Base
	 */
	private static final long serialVersionUID = 1488208501614174053L;

	/*@Id
	@GeneratedValue(strategy=GenerationType.AUTO)
	private int id;
	*/
	
	@Id
	@Column(name="device_id")
	private int device_id;
	
	@Column(name="device_name")
	private String device_name;
	
	@Column(name="date_of_purchase")
	@DateTimeFormat(pattern = "yyyy-MM-dd")
	private Date date_of_purchase;
	
	@Column(name="ime_number")
	private String ime_number;
	
	@Column(name="service_provider")
	private String service_provider;
	
	@Column(name="data_plan")
	private String data_plan;
	
	@Column(name="status")
	private boolean status;
	
	@Column(name="sim_number")
	private String sim_number;
	
	@Column
	private String employee_id;
	
	@Column
	private String employee_name;
	
	@Column
	private boolean login_status;
	
	@Column
	private String client_id;

	public String getClient_id() {
		return client_id;
	}

	public void setClient_id(String client_id) {
		this.client_id = client_id;
	}

	public boolean isLogin_status() {
		return login_status;
	}

	public void setLogin_status(boolean login_status) {
		this.login_status = login_status;
	}

	public String getEmployee_id() {
		return employee_id;
	}

	public void setEmployee_id(String employee_id) {
		this.employee_id = employee_id;
	}

	public String getEmployee_name() {
		return employee_name;
	}

	public void setEmployee_name(String employee_name) {
		this.employee_name = employee_name;
	}

	public String getSim_number() {
		return sim_number;
	}

	public void setSim_number(String sim_number) {
		this.sim_number = sim_number;
	}

	public boolean isStatus() {
		return status;
	}

	public void setStatus(boolean status) {
		this.status = status;
	}

	@JsonSerialize(using = DateSerializer.class)
	public Date getDate_of_purchase() {
		return date_of_purchase;
	}

	public void setDate_of_purchase(Date date_of_purchase) {
		this.date_of_purchase = date_of_purchase;
	}

	public int getDevice_id() {
		return device_id;
	}

	public void setDevice_id(int device_id) {
		this.device_id = device_id;
	}

	public String getDevice_name() {
		return device_name;
	}

	public void setDevice_name(String device_name) {
		this.device_name = device_name;
	}

	@JsonSerialize(using = DateSerializer.class)
	public Date getDateOfPurchage() {
		return date_of_purchase;
	}

	public void setDateOfPurchage(Date dateOfPurchage) {
		this.date_of_purchase = dateOfPurchage;
	}

	public String getIme_number() {
		return ime_number;
	}

	public void setIme_number(String ime_number) {
		this.ime_number = ime_number;
	}

	public String getService_provider() {
		return service_provider;
	}

	public void setService_provider(String service_provider) {
		this.service_provider = service_provider;
	}

	public String getData_plan() {
		return data_plan;
	}

	public void setData_plan(String data_plan) {
		this.data_plan = data_plan;
	}
		
	public SelectedDevice(){}
	
}
