/*package com.blickx.domain;

import java.io.Serializable;
import java.util.Date;
import java.util.List;
import java.util.Set;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.Table;

import org.codehaus.jackson.annotate.JsonIgnoreProperties;
import org.codehaus.jackson.map.annotate.JsonSerialize;
import org.hibernate.annotations.LazyCollection;
import org.hibernate.annotations.LazyCollectionOption;
import org.springframework.format.annotation.DateTimeFormat;

@Entity
@Table(name="selected_setupbox")
@JsonIgnoreProperties({"hibernateLazyInitializer", "handler"})
public class SelectedSetupBox implements Serializable{

	*//**
	 * 
	 * 
	 * 
	 *//*
	private static final long serialVersionUID = 451998180203189122L;
	
	@Id
	@Column(name="row_id")
	private int row_id;
	
	
	@Column(insertable = false , updatable = false)
	private String customer_id;
	
	//@Id
	@Column
	private String box_number;
	
	@Column
	private String vc_number;
	
	@Column
	private String nds_number;
	
	@Column(name = "addons")
	private String addonNames;
	
	@Column
	private String addon_ids;
	
	@Column
	private String assigned_package;
	
	@Column
	private int package_id;
	
	@Column
	private double discount;
	
	@Column
	@DateTimeFormat(pattern = "yyyy-MM-dd")
	private Date start_date;
	
	@Column
	private boolean status;
	
	@Column
	private String client_id;
	
	@ManyToOne
	@JoinColumn(name="customer_id", nullable = false)
	private SelectedCustomerEntity customer;
	
	
	public int getRow_id() {
		return row_id;
	}

	public void setRow_id(int row_id) {
		this.row_id = row_id;
	}

	public int getPackage_id() {
		return package_id;
	}

	public void setPackage_id(int package_id) {
		this.package_id = package_id;
	}

	public String getClient_id() {
		return client_id;
	}

	public void setClient_id(String client_id) {
		this.client_id = client_id;
	}

	public boolean isStatus() {
		return status;
	}

	public void setStatus(boolean status) {
		this.status = status;
	}

	public String getAddon_ids() {
		return addon_ids;
	}

	public void setAddon_ids(String addon_ids) {
		this.addon_ids = addon_ids;
	}

	public String getCustomer_id() {
		return customer_id;
	}

	public void setCustomer_id(String customer_id) {
		this.customer_id = customer_id;
	}

	public String getBox_number() {
		return box_number;
	}

	public void setBox_number(String box_number) {
		this.box_number = box_number;
	}

	public String getVc_number() {
		return vc_number;
	}

	public void setVc_number(String vc_number) {
		this.vc_number = vc_number;
	}

	public String getNds_number() {
		return nds_number;
	}

	public void setNds_number(String nds_number) {
		this.nds_number = nds_number;
	}

	public String getAddonNames() {
		return addonNames;
	}

	public void setAddonNames(String addonNames) {
		this.addonNames = addonNames;
	}

	public String getAssigned_package() {
		return assigned_package;
	}

	public void setAssigned_package(String assigned_package) {
		this.assigned_package = assigned_package;
	}

	public double getDiscount() {
		return discount;
	}

	public void setDiscount(double discount) {
		this.discount = discount;
	}

	@JsonSerialize(using = DateSerializer.class)
	public Date getStart_date() {
		return start_date;
	}

	public void setStart_date(Date start_date) {
		this.start_date = start_date;
	}

	@Override
	public String toString() {
		return "SetupBox [customer_id=" + customer_id + ", box_number="
				+ box_number + ", vc_number=" + vc_number + ", nds_number="
				+ nds_number + ", addonNames=" + addonNames + ", addon_ids="
				+ addon_ids + ", assigned_package=" + assigned_package
				+ ", discount=" + discount + ", start_date=" + start_date
				+ ", status=" + status + ", client_id=" + client_id
				+ ", customer=" + customer + "]";
	}

	public SelectedSetupBox(){
		
	}
}
*/