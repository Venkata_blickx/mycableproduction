package com.blickx.domain;

import java.util.List;

public class CustomerBillPaymentWrapper {
	private List<CustomerBillPayment> payments;

   /* *//**
     * @return the customer bills
     *//*
    public List<CustomerBillPayment> getPayments() {
        return payments;
    }

    *//**
     * @param list of customerBills
     *//*
    public void setBills(List<CustomerBillPayment> customerPayments) {
        this.payments = customerPayments;
    }*/
    
    public CustomerBillPaymentWrapper(){
    	
    }

	public List<CustomerBillPayment> getPayments() {
		return payments;
	}

	public void setPayments(List<CustomerBillPayment> payments) {
		this.payments = payments;
	}

	@Override
	public String toString() {
		return "CustomerBillPaymentWrapper [payments=" + payments + "]";
	}
	
	
}
