package com.blickx.domain;

import java.math.BigDecimal;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name = "online_payments")
public class OnlinePaymentDetailsEntity {
	
	@Id
	@GeneratedValue(strategy=GenerationType.AUTO)
	@Column(name="tr_id")
	private int tr_id;

	@Column(name="customer_id")
	private String customer_id;

	@Column(name="contact_number")
	private String contact_number;

	@Column(name="email_address")
	private String email_address;

	@Column(name="client_id")
	private String client_id;

	@Column(name="trn_ref")
	private String trn_ref;

	@Column(name="trn_datetime")
	private Date trn_datetime;

	@Column(name="trn_amount")
	private BigDecimal trn_amount;

	@Column(name="trn_remarks")
	private String trn_remarks;

	@Column(name="trn_status")
	private boolean trn_status;

	@Column(name="created_at")
	private Date created_at;

	@Column(name="updated_at")
	private Date updated_at;

	public int getTr_id() {
		return tr_id;
	}

	public void setTr_id(int tr_id) {
		this.tr_id = tr_id;
	}

	public String getCustomer_id() {
		return customer_id;
	}

	public void setCustomer_id(String customer_id) {
		this.customer_id = customer_id;
	}

	public String getContact_number() {
		return contact_number;
	}

	public void setContact_number(String contact_number) {
		this.contact_number = contact_number;
	}

	public String getEmail_address() {
		return email_address;
	}

	public void setEmail_address(String email_address) {
		this.email_address = email_address;
	}

	public String getClient_id() {
		return client_id;
	}

	public void setClient_id(String client_id) {
		this.client_id = client_id;
	}

	public String getTrn_ref() {
		return trn_ref;
	}

	public void setTrn_ref(String trn_ref) {
		this.trn_ref = trn_ref;
	}

	public Date getTrn_datetime() {
		return trn_datetime;
	}

	public void setTrn_datetime(Date trn_datetime) {
		this.trn_datetime = trn_datetime;
	}

	public BigDecimal getTrn_amount() {
		return trn_amount;
	}

	public void setTrn_amount(BigDecimal trn_amount) {
		this.trn_amount = trn_amount;
	}

	public String getTrn_remarks() {
		return trn_remarks;
	}

	public void setTrn_remarks(String trn_remarks) {
		this.trn_remarks = trn_remarks;
	}

	public boolean getTrn_status() {
		return trn_status;
	}

	public void setTrn_status(boolean trn_status) {
		this.trn_status = trn_status;
	}

	public Date getCreated_at() {
		return created_at;
	}

	public void setCreated_at(Date created_at) {
		this.created_at = created_at;
	}

	public Date getUpdated_at() {
		return updated_at;
	}

	public void setUpdated_at(Date updated_at) {
		this.updated_at = updated_at;
	}

	@Override
	public String toString() {
		return "OnlinePaymentDetailsEntity [tr_id=" + tr_id + ", customer_id="
				+ customer_id + ", contact_number=" + contact_number
				+ ", email_address=" + email_address + ", client_id="
				+ client_id + ", trn_ref=" + trn_ref + ", trn_datetime="
				+ trn_datetime + ", trn_amount=" + trn_amount
				+ ", trn_remarks=" + trn_remarks + ", trn_status=" + trn_status
				+ ", created_at=" + created_at + ", updated_at=" + updated_at
				+ "]";
	}

}
