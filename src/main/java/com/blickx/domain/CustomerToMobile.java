package com.blickx.domain;

import java.math.BigDecimal;

public class CustomerToMobile {
	
	private String customer_id;
	
	private String customer_name;
	
	private String contact_number;
	
	private BigDecimal package_amount;
	
	private BigDecimal addon_amount;
	
	private BigDecimal balance_amount;
	
	private BigDecimal monthly_rent;
	
	private BigDecimal total_amount;
	
	private String customer_uid;
	
	private String c_id;
	
	private BigDecimal discount;
	
	public String getC_id() {
		return c_id;
	}
	public void setC_id(String c_id) {
		this.c_id = c_id;
	}
	public String getCustomer_uid() {
		return customer_uid;
	}
	public void setCustomer_uid(String customer_uid) {
		this.customer_uid = customer_uid;
	}
	public String getCustomer_id() {
		return customer_id;
	}
	public void setCustomer_id(String customer_id) {
		this.customer_id = customer_id;
	}
	public String getCustomer_name() {
		return customer_name;
	}
	public BigDecimal getDiscount() {
		return discount;
	}
	public void setDiscount(BigDecimal discount) {
		this.discount = discount;
	}
	public void setCustomer_name(String customer_name) {
		this.customer_name = customer_name;
	}
	public String getContact_number() {
		return contact_number;
	}
	public void setContact_number(String contact_number) {
		this.contact_number = contact_number;
	}
	
	public BigDecimal getPackage_amount() {
		return package_amount;
	}
	public void setPackage_amount(BigDecimal package_amount) {
		this.package_amount = package_amount;
	}
	public BigDecimal getAddon_amount() {
		return addon_amount;
	}
	public void setAddon_amount(BigDecimal addon_amount) {
		this.addon_amount = addon_amount;
	}
	public BigDecimal getBalance_amount() {
		return balance_amount;
	}
	public void setBalance_amount(BigDecimal balance_amount) {
		this.balance_amount = balance_amount;
	}
	public BigDecimal getMonthly_rent() {
		return monthly_rent;
	}
	public void setMonthly_rent(BigDecimal monthly_rent) {
		this.monthly_rent = monthly_rent;
	}
	public BigDecimal getTotal_amount() {
		return total_amount;
	}
	public void setTotal_amount(BigDecimal total_amount) {
		this.total_amount = total_amount;
	}
	
	@Override
	public String toString() {
		return "CustomerToMobile [customer_id=" + customer_id
				+ ", customer_name=" + customer_name + ", contact_number="
				+ contact_number + ", package_amount=" + package_amount
				+ ", addon_amount=" + addon_amount + ", balance_amount="
				+ balance_amount + ", monthly_rent=" + monthly_rent
				+ ", total_amount=" + total_amount + ", customer_uid="
				+ customer_uid + ", c_id=" + c_id + ", discount=" + discount
				+ "]";
	}
	public CustomerToMobile(){}
}
