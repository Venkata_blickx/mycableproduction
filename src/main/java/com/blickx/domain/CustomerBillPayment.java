package com.blickx.domain;



/*
 * Android Bill Payment Credentials
 */
public class CustomerBillPayment {
	
	private String bill_id;
	private String customer_id;
	private String employee_id;
	private String employee_uid;
	private String paid_amt;
	private String  bal_amt;
	private String tot_amt;
	private String ime_number;
	
	private String lattitude;
	
	private String longitude;
	
	private String paymentTimeStamp;
	
	private String transactionId;

	public String getTransactionId() {
		return transactionId;
	}

	public void setTransactionId(String transactionId) {
		this.transactionId = transactionId;
	}

	public String getPaymentTimeStamp() {
		return paymentTimeStamp;
	}

	public void setPaymentTimeStamp(String paymentTimeStamp) {
		this.paymentTimeStamp = paymentTimeStamp;
	}

	
	
	public String getLattitude() {
		return lattitude;
	}
	public void setLattitude(String lattitude) {
		this.lattitude = lattitude;
	}
	public String getLongitude() {
		return longitude;
	}
	public void setLongitude(String longitude) {
		this.longitude = longitude;
	}
	public String getBill_id() {
		return bill_id;
	}
	public void setBill_id(String bill_id) {
		this.bill_id = bill_id;
	}
	public String getEmployee_uid() {
		return employee_uid;
	}
	public void setEmployee_uid(String employee_uid) {
		this.employee_uid = employee_uid;
	}
	public String getIme_number() {
		return ime_number;
	}
	public void setIme_number(String ime_number) {
		this.ime_number = ime_number;
	}
	public String getCustomer_id() {
		return customer_id;
	}
	public void setCustomer_id(String customer_id) {
		this.customer_id = customer_id;
	}
	public String getEmployee_id() {
		return employee_id;
	}
	public void setEmployee_id(String employee_id) {
		this.employee_id = employee_id;
	}
	
	public String getPaid_amt() {
		return paid_amt;
	}

	public void setPaid_amt(String paid_amt) {
		this.paid_amt = paid_amt;
	}

	public String getBal_amt() {
		return bal_amt;
	}

	public void setBal_amt(String bal_amt) {
		this.bal_amt = bal_amt;
	}

	public String getTot_amt() {
		return tot_amt;
	}

	public void setTot_amt(String tot_amt) {
		this.tot_amt = tot_amt;
	}

	public CustomerBillPayment() {
		// TODO Auto-generated constructor stub
	}
	@Override
	public String toString() {
		return "CustomerBillPayment [bill_id=" + bill_id + ", customer_id="
				+ customer_id + ", employee_id=" + employee_id
				+ ", employee_uid=" + employee_uid + ", paid_amt=" + paid_amt
				+ ", bal_amt=" + bal_amt + ", tot_amt=" + tot_amt
				+ ", ime_number=" + ime_number + ", lattitude=" + lattitude
				+ ", longitude=" + longitude + ", paymentTimeStamp="
				+ paymentTimeStamp + ", transactionId=" + transactionId + "]";
	}
	
}
