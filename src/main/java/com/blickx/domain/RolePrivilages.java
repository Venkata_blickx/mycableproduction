package com.blickx.domain;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name = "role_privilages")
public class RolePrivilages {

	@Id
	@GeneratedValue(strategy=GenerationType.AUTO)
	@Column(name = "privilage_id")
	private int privilageId;
	
	@Column(name = "role_id")
	private int roleId;
	
	@Column(name = "resource")
	private String resource;
	
	@Column(name = "action_type")
	private String actionType;
	
	@Column(name = "privilage_comment")
	private String privilageComment;

	public int getPrivilageId() {
		return privilageId;
	}

	public void setPrivilageId(int privilageId) {
		this.privilageId = privilageId;
	}

	public int getRoleId() {
		return roleId;
	}

	public void setRoleId(int roleId) {
		this.roleId = roleId;
	}

	public String getResource() {
		return resource;
	}

	public void setResource(String resource) {
		this.resource = resource;
	}

	public String getActionType() {
		return actionType;
	}

	public void setActionType(String actionType) {
		this.actionType = actionType;
	}

	public String getPrivilageComment() {
		return privilageComment;
	}

	public void setPrivilageComment(String privilageComment) {
		this.privilageComment = privilageComment;
	}

	@Override
	public String toString() {
		return "RolePrivilages [privilageId=" + privilageId + ", roleId="
				+ roleId + ", resource=" + resource + ", actionType="
				+ actionType + ", privilageComment=" + privilageComment + "]";
	}
	
	public RolePrivilages() {
		
	}
}
