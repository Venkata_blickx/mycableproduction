package com.blickx.domain;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name = "authentication")
public class Authentication {

	@Id
    @GeneratedValue
    @Column(name = "id", length = 11 )
    private Long id;
	
	@Column(name = "client_contact_number")
	private String clientContactNumber;
	
	@Column(name = "username")
    String username;

    @Column(name = "password")
    String password;
    
    @Column(name = "employee_id")
    String employee_id;
    
    @Column(name = "role_id")
    int role_id;
    
    @Column
    private String client_id;
    
    @Column(name = "client_name")
    private String client_name;
    
    @Column
    private String address;
    
    @Column
    private boolean msg_alert;
    
    @Column
    private String longitude;
    
    @Column 
    private String latitude;
    
    @Column(name = "is_balance_update_allowed")
    private boolean isBalanceUpdateAllowed;
    
    @Column(name = "is_broadband_user")
    private boolean isBroadbandUser;
    
    @Column(name = "sms_regards_msg")
    private String smsRegardsMsg;
    
    public String getSmsRegardsMsg() {
		return smsRegardsMsg;
	}

	public void setSmsRegardsMsg(String smsRegardsMsg) {
		this.smsRegardsMsg = smsRegardsMsg;
	}

	public boolean isBroadbandUser() {
		return isBroadbandUser;
	}

	public void setBroadbandUser(boolean isBroadbandUser) {
		this.isBroadbandUser = isBroadbandUser;
	}

	private boolean areacode_activated;

	public boolean isAreacode_activated() {
		return areacode_activated;
	}

	public void setAreacode_activated(boolean areacode_activated) {
		this.areacode_activated = areacode_activated;
	}

	public boolean isBalanceUpdateAllowed() {
		return isBalanceUpdateAllowed;
	}

	public void setBalanceUpdateAllowed(boolean isBalanceUpdateAllowed) {
		this.isBalanceUpdateAllowed = isBalanceUpdateAllowed;
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getLongitude() {
		return longitude;
	}

	public void setLongitude(String longitude) {
		this.longitude = longitude;
	}

	public String getLatitude() {
		return latitude;
	}

	public void setLatitude(String latitude) {
		this.latitude = latitude;
	}

	public String getUsername() {
		return username;
	}

	public String getClient_id() {
		return client_id;
	}

	public void setClient_id(String client_id) {
		this.client_id = client_id;
	}

	public String getClient_name() {
		return client_name;
	}

	public void setClient_name(String client_name) {
		this.client_name = client_name;
	}

	public void setUsername(String username) {
		this.username = username;
	}

	public void setUsernname(String username) {
		this.username = username;
	}

	public String getPassword() {
		return password;
	}

	public void setPassword(String password) {
		this.password = password;
	}

	public String getEmployee_id() {
		return employee_id;
	}

	public void setEmployee_id(String employee_id) {
		this.employee_id = employee_id;
	}

	public String getAddress() {
		return address;
	}

	public void setAddress(String address) {
		this.address = address;
	}

	public boolean isMsg_alert() {
		return msg_alert;
	}

	public void setMsg_alert(boolean msg_alert) {
		this.msg_alert = msg_alert;
	}

	public int getRole_id() {
		return role_id;
	}

	public void setRole_id(int role_id) {
		this.role_id = role_id;
	}

	public String getClientContactNumber() {
		return clientContactNumber;
	}

	public void setClientContactNumber(String clientContactNumber) {
		this.clientContactNumber = clientContactNumber;
	}

	public Authentication(){
		
	}

	@Override
	public String toString() {
		return "Authentication [id=" + id + ", clientContactNumber="
				+ clientContactNumber + ", username=" + username
				+ ", password=" + password + ", employee_id=" + employee_id
				+ ", role_id=" + role_id + ", client_id=" + client_id
				+ ", client_name=" + client_name + ", address=" + address
				+ ", msg_alert=" + msg_alert + ", longitude=" + longitude
				+ ", latitude=" + latitude + ", isBalanceUpdateAllowed="
				+ isBalanceUpdateAllowed + ", isBroadbandUser="
				+ isBroadbandUser + ", smsRegardsMsg=" + smsRegardsMsg
				+ ", areacode_activated=" + areacode_activated + "]";
	}

	
}
