package com.blickx.domain;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import org.springframework.format.annotation.DateTimeFormat;

@Entity
@Table(name="channels")
public class Channels implements Serializable{

	/**
	 * This refers Channels table in Data Base
	 */
	private static final long serialVersionUID = 4659190248665173966L;

	@Id
	@Column(name="channel_id")
	private int channel_id;
	
	@Column(name="channel_name")
	private String channel_name;
	
	@Column(name="channel_amount")
	private double channel_amount;
	
	@Column(name="category")
	private String category;
	
	@Column(name="status")
	private String status;
	
	@Column
	@DateTimeFormat(pattern = "yyyy-MM-dd")
	private Date channel_effective_date;
	
	@Column
	private String client_id;

	/*//@Cascade(CascadeType.ALL)
	@ManyToMany(mappedBy="channels", targetEntity=PackageEntity.class)
	List<Package> packages;
	
	public List<Package> getPackages() {
		return packages;
	}

	public void setPackages(List<Package> packages) {
		this.packages = packages;
	}*/

	
	public String getStatus() {
		return status;
	}

	public String getClient_id() {
		return client_id;
	}

	public void setClient_id(String client_id) {
		this.client_id = client_id;
	}

	@JsonSerialize(using = DateSerializer.class)
	public Date getChannel_effective_date() {
		return channel_effective_date;
	}

	public void setChannel_effective_date(Date channel_effective_date) {
		this.channel_effective_date = channel_effective_date;
	}

	public void setStatus(String status) {
		this.status = status;
	}

	public String getCategory() {
		return category;
	}

	public void setCategory(String category) {
		this.category = category;
	}

	public int getChannel_id() {
		return channel_id;
	}

	public void setChannel_id(int channel_id) {
		this.channel_id = channel_id;
	}

	public String getChannel_name() {
		return channel_name;
	}

	public void setChannel_name(String channel_name) {
		this.channel_name = channel_name;
	}

	public double getChannel_amount() {
		return channel_amount;
	}

	public void setChannel_amount(double channel_amount) {
		this.channel_amount = channel_amount;
	}

	public Channels(int channel_id, String channel_name, double channel_amount,
			String category) {
		super();
		this.channel_id = channel_id;
		this.channel_name = channel_name;
		this.channel_amount = channel_amount;
		this.category = category;
	}

	
	@Override
	public String toString() {
		return "Channels [channel_id=" + channel_id + ", channel_name="
				+ channel_name + ", channel_amount=" + channel_amount
				+ ", category=" + category + ", status=" + status
				+ ", channel_effective_date=" + channel_effective_date
				+ ", client_id=" + client_id + "]";
	}

	public Channels(){}
	
}
