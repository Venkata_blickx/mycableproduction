package com.blickx.domain;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import org.springframework.format.annotation.DateTimeFormat;


@Entity
@Table(name="selected_employee")
public class SelectedEmployee implements Serializable {

	/**
	 * This Refers Employee Table in Data Base
	 */
	private static final long serialVersionUID = -6610852438554805858L;

	@Id
	@Column(name="employee_id")
	private String employee_id;
	
	@Column(name="employee_name")
	private String employee_name;
	
	@Column(name="contact_number")
	private String contact_number;
	
	@Column
	private String department;
	
	@Column
	private String landline_number;
	
	@Column(name="areacode")
	private String areacode;

	@Column(name="joining_date")
	@DateTimeFormat(pattern = "yyyy-MM-dd")
	private Date joining_date;
	
	@Column(name="end_date")
	@DateTimeFormat(pattern = "yyyy-MM-dd")
	private Date end_date;
	
	@Column(name="assign_device")
	private String assign_device;
	
	@Column(name="status")
	private boolean status;
	
	@Column(name="employee_photo")
	private String employee_photo;
	
	@Column(name="employee_doc")
	private String employee_doc;
	
	@Column(name="gender")
	private String gender;
	
	@Column(name="device_id")
	private int device_id;
	
	@Column(name="photo_name")
	private String photo_name;
	
	@Column(name="doc_name")
	private String doc_name;
	
	@Column(name="password")
	private String password;

	@Column
	private String email;
	
	@Column
	private String line1;
	
	@Column
	private String line2;
	
	@Column
	private String city;
	
	@Column
	private String state;
	
	@Column
	private String pincode;
	
	@Column
	private String doc_type;
	
	@Column
	private String photo_type;
	
	@Column
	private String employee_uid;
	
	@Column String client_id;
	
	public String getClient_id() {
		return client_id;
	}


	public void setClient_id(String client_id) {
		this.client_id = client_id;
	}


	public String getEmployee_uid() {
		return employee_uid;
	}


	public void setEmployee_uid(String employee_uid) {
		this.employee_uid = employee_uid;
	}


	public String getDoc_type() {
		return doc_type;
	}


	public void setDoc_type(String doc_type) {
		this.doc_type = doc_type;
	}


	public String getPhoto_type() {
		return photo_type;
	}


	public void setPhoto_type(String photo_type) {
		this.photo_type = photo_type;
	}


	public String getLine1() {
		return line1;
	}


	public void setLine1(String line1) {
		this.line1 = line1;
	}


	public String getLine2() {
		return line2;
	}


	public void setLine2(String line2) {
		this.line2 = line2;
	}


	public String getCity() {
		return city;
	}


	public void setCity(String city) {
		this.city = city;
	}


	public String getState() {
		return state;
	}


	public void setState(String state) {
		this.state = state;
	}


	public String getPincode() {
		return pincode;
	}


	public void setPincode(String pincode) {
		this.pincode = pincode;
	}


	public String getEmail() {
		return email;
	}


	public void setEmail(String email) {
		this.email = email;
	}


	public String getDepartment() {
		return department;
	}


	public void setDepartment(String department) {
		this.department = department;
	}

	public String getLandline_number() {
		return landline_number;
	}


	public void setLandline_number(String landline_number) {
		this.landline_number = landline_number;
	}


	public String getPassword() {
		return password;
	}


	public void setPassword(String password) {
		this.password = password;
	}


	public String getEmployee_id() {
		return employee_id;
	}


	public void setEmployee_id(String employee_id) {
		this.employee_id = employee_id;
	}


	public String getEmployee_name() {
		return employee_name;
	}


	public void setEmployee_name(String employee_name) {
		this.employee_name = employee_name;
	}


	public String getContact_number() {
		return contact_number;
	}


	public void setContact_number(String contact_number) {
		this.contact_number = contact_number;
	}

	public String getAreacode() {
		return areacode;
	}


	public void setAreacode(String areacode) {
		this.areacode = areacode;
	}

	@JsonSerialize(using = DateSerializer.class)
	public Date getJoining_date() {
		return joining_date;
	}


	public void setJoining_date(Date joining_date) {
		this.joining_date = joining_date;
	}

	@JsonSerialize(using = DateSerializer.class)
	public Date getEnd_date() {
		return end_date;
	}


	public void setEnd_date(Date end_date) {
		this.end_date = end_date;
	}


	public String getAssign_device() {
		return assign_device;
	}


	public void setAssign_device(String assign_device) {
		this.assign_device = assign_device;
	}

	

	public boolean isStatus() {
		return status;
	}


	public void setStatus(boolean status) {
		this.status = status;
	}


	public String getEmployee_photo() {
		return employee_photo;
	}


	public void setEmployee_photo(String employee_photo) {
		this.employee_photo = employee_photo;
	}


	public String getEmployee_doc() {
		return employee_doc;
	}


	public void setEmployee_doc(String employee_doc) {
		this.employee_doc = employee_doc;
	}


	public String getGender() {
		return gender;
	}


	public void setGender(String gender) {
		this.gender = gender;
	}


	public int getDevice_id() {
		return device_id;
	}


	public void setDevice_id(int device_id) {
		this.device_id = device_id;
	}


	public String getPhoto_name() {
		return photo_name;
	}


	public void setPhoto_name(String photo_name) {
		this.photo_name = photo_name;
	}


	public String getDoc_name() {
		return doc_name;
	}


	public void setDoc_name(String doc_name) {
		this.doc_name = doc_name;
	}

	


	@Override
	public String toString() {
		return "Employee [employee_id=" + employee_id + ", employee_name="
				+ employee_name + ", contact_number=" + contact_number
				+ ", address=" +  ", areacode=" + areacode
				+ ", joining_date=" + joining_date + ", end_date=" + end_date
				+ ", assign_device=" + assign_device + ", status=" + status
				+ ", employee_photo=" + employee_photo + ", employee_doc="
				+ employee_doc + ", gender=" + gender + "]";
	}
	
	
	public SelectedEmployee(){}
	
	
}