package com.blickx.domain;

import java.util.Date;
import java.util.List;

import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.JoinColumn;
import javax.persistence.JoinTable;
import javax.persistence.ManyToMany;
import javax.persistence.Table;

import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import org.hibernate.annotations.Cascade;
import org.springframework.format.annotation.DateTimeFormat;

@Entity
@Table(name="packages")
public class PackageEntity {
	
	
	@Id
	@GeneratedValue(strategy=GenerationType.AUTO)
	@Column(name="package_id", unique=true, nullable= false)
	private int package_id;
	
	@Column(name="package_name")
	private String package_name;
	
	@Column(name="package_amount")
	private double package_amount;
	
	@Column
	@DateTimeFormat(pattern = "yyyy-MM-dd")
	private Date effective_date;
	
	@Column(name="status")
	private boolean status;
	
	@Column
	private String client_id;
	
	
	/*@CollectionTable
	@JoinTable(name="channels", joinColumns=@JoinColumn(name="package_id"))
	@IndexColumn(name="idx")
	@Column(name="channel_list")
	private String
	*/
	@Cascade(org.hibernate.annotations.CascadeType.ALL)
	@ManyToMany(targetEntity=Channels.class, fetch=FetchType.EAGER)
	@JoinTable(name="package_channel",joinColumns=@JoinColumn(name="package_id",referencedColumnName="package_id"),
			inverseJoinColumns=@JoinColumn(name="channel_id",referencedColumnName="channel_id"))
	private List<Channels> channels;
		
	public List<Channels> getChannels() {
		return channels;
	}

	public void setChannels(List<Channels> channels) {
		this.channels = channels;
	}

	public String getClient_id() {
		return client_id;
	}

	public void setClient_id(String client_id) {
		this.client_id = client_id;
	}

	public int getPackage_id() {
		return package_id;
	}

	public boolean isStatus() {
		return status;
	}

	public void setStatus(boolean status) {
		this.status = status;
	}

	public void setPackage_id(int package_id) {
		this.package_id = package_id;
	}

	public String getPackage_name() {
		return package_name;
	}

	public void setPackage_name(String package_name) {
		this.package_name = package_name;
	}

	public double getPackage_amount() {
		return package_amount;
	}

	public void setPackage_amount(double package_amount) {
		this.package_amount = package_amount;
	}

	@JsonSerialize(using = DateSerializer.class)
	public Date getEffective_date() {
		return effective_date;
	}

	public void setEffective_date(Date effective_date) {
		this.effective_date = effective_date;
	}

	@Override
	public String toString() {
		return "PackageManagement [package_id=" + package_id
				+ ", package_name=" + package_name + ", package_amount="
				+ package_amount + ", catagory=" +  "]";
	}

	public PackageEntity(int package_id, String package_name,
			double package_amount) {
		super();
		this.package_id = package_id;
		this.package_name = package_name;
		this.package_amount = package_amount;
	}
	
	public PackageEntity() {
		
	}
	
}
