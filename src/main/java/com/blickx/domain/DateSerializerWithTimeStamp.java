package com.blickx.domain;

import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.Date;

import com.fasterxml.jackson.core.JsonGenerator;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.JsonSerializer;
import com.fasterxml.jackson.databind.SerializerProvider;

public class DateSerializerWithTimeStamp extends JsonSerializer<Date> {

	 @Override  
	    public void serialize(Date value_p, JsonGenerator gen, SerializerProvider prov_p)  
	      throws IOException, JsonProcessingException  
	    {
	      SimpleDateFormat formatter = new SimpleDateFormat("dd/MM/yyyy HH:mm:ss");
	      //formatter.setTimeZone(TimeZone.getTimeZone("UTC"));
	      
		   // Prints the date in the CET timezone
		  // System.out.println("UTC"+formatter.format(value_p));
	     
		   // Set the formatter to use a different timezone
		   //formatter.setTimeZone(TimeZone.getTimeZone("IST"));
	     
		   // Prints the date in the IST timezone
		  // System.out.println("IST"+formatter.format(value_p));
	      
	      System.out.println(" Before convertions" + value_p);
	      String formattedDate = formatter.format(value_p);
	     // System.out.println("Acutal Date To be Displayed"+formattedDate);
	      gen.writeString(formattedDate);  
	    }  
}
