package com.blickx.domain;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import org.springframework.format.annotation.DateTimeFormat;


@Entity
@Table(name="complaint")
public class ComplaintEntity implements Serializable{

	public String getSource() {
		return source;
	}

	public void setSource(String source) {
		this.source = source;
	}

	@Override
	public String toString() {
		return "ComplaintEntity [complaint_id=" + complaint_id + ", id=" + id
				+ ", customer_id=" + customer_id + ", employee_id="
				+ employee_id + ", date=" + date + ", details=" + details
				+ ", status=" + status + ", comment=" + comment
				+ ", closed_date=" + closed_date + ", assigned_to="
				+ assigned_to + ", assigned_date=" + assigned_date
				+ ", closed_by=" + closed_by + ", source=" + source
				+ ", client_id=" + client_id + "]";
	}




	/**
	 * This refers Complaints Table in DataBase 
	 */
	private static final long serialVersionUID = -766755042505636641L;
	
	@Id
	//@GeneratedValue(strategy=GenerationType.AUTO)
	@Column(name="complaint_id")
	private String complaint_id;
	
	@Column(name = "row_id")
	@GeneratedValue(strategy=GenerationType.AUTO)
	private Integer id;
	
	@Column
	private String customer_id;
	
	@Column 
	private String employee_id;
	
	@Column
	private Date date;
	
	@Column
	private String details;
	
	@Column
	private String status;
	
	@Column
	private String comment;
	
	@Column
	@DateTimeFormat(pattern="yyyy-MM-dd")
	private Date closed_date;
	
	@Column
	private String assigned_to;
	
	@Column
	@DateTimeFormat(pattern="yyyy-MM-dd")
	private Date assigned_date;
	
	@Column
	private String closed_by;
	
	@Column
	private String source;
	
	@Column
	private String client_id;

	public String getClosed_by() {
		return closed_by;
	}

	public void setClosed_by(String closed_by) {
		this.closed_by = closed_by;
	}

	public String getAssigned_to() {
		return assigned_to;
	}

	public String getClient_id() {
		return client_id;
	}

	public void setClient_id(String client_id) {
		this.client_id = client_id;
	}

	public void setAssigned_to(String assigned_to) {
		this.assigned_to = assigned_to;
	}

	@JsonSerialize(using = DateSerializer.class)
	public Date getAssigned_date() {
		return assigned_date;
	}

	public void setAssigned_date(Date assigned_date) {
		this.assigned_date = assigned_date;
	}

	public String getComplaint_id() {
		return complaint_id;
	}

	public void setComplaint_id(String complaint_id) {
		this.complaint_id = complaint_id;
	}

	public String getCustomer_id() {
		return customer_id;
	}

	public void setCustomer_id(String customer_id) {
		this.customer_id = customer_id;
	}

	public String getEmployee_id() {
		return employee_id;
	}

	public void setEmployee_id(String employee_id) {
		this.employee_id = employee_id;
	}

	@JsonSerialize(using = DateSerializer.class)
	public Date getDate() {
		return date;
	}

	public void setDate(Date date) {
		this.date = date;
	}

	public String getDetails() {
		return details;
	}

	public void setDetails(String details) {
		this.details = details;
	}

	public String getStatus() {
		return status;
	}

	public void setStatus(String status) {
		this.status = status;
	}

	public String getComment() {
		return comment;
	}

	public void setComment(String comment) {
		this.comment = comment;
	}

	@JsonSerialize(using = DateSerializer.class)
	public Date getClosed_date() {
		return closed_date;
	}

	public void setClosed_date(Date closed_date) {
		this.closed_date = closed_date;
	}



	
	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public ComplaintEntity(){}
}
