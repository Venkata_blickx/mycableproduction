package com.blickx.domain;

import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name = "email_update_history")
public class EmailUpdateHistoryEntity {

	@Id
	@GeneratedValue(strategy=GenerationType.AUTO)
	@Column(name = "email_update_history_id")
	private int id;
	
	@Column(name = "customer_id")
	private String customerId;
	
	@Column(name = "old_email")
	private String oldEmail;
	
	@Column(name = "new_email")
	private String newEmail;
	
	@Column(name = "updated_on")
	private Date updateOn;

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public String getCustomerId() {
		return customerId;
	}

	public void setCustomerId(String customerId) {
		this.customerId = customerId;
	}

	public String getOldEmail() {
		return oldEmail;
	}

	public void setOldEmail(String oldEmail) {
		this.oldEmail = oldEmail;
	}

	public String getNewEmail() {
		return newEmail;
	}

	public void setNewEmail(String newEmail) {
		this.newEmail = newEmail;
	}

	public Date getUpdateOn() {
		return updateOn;
	}

	public void setUpdateOn(Date updateOn) {
		this.updateOn = updateOn;
	}
	
	public EmailUpdateHistoryEntity(){
		
	}
}
