package com.blickx.domain;

import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name = "contact_number_update_history")
public class ContactNumberUpdateHistory {

	@Override
	public String toString() {
		return "ContactNumberUpdateHistory [id=" + id + ", customerId="
				+ customerId + ", oldContactNumber=" + oldContactNumber
				+ ", newContactNumber=" + newContactNumber + ", updateOn="
				+ updateOn + "]";
	}

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public String getCustomerId() {
		return customerId;
	}

	public void setCustomerId(String customerId) {
		this.customerId = customerId;
	}

	public String getOldContactNumber() {
		return oldContactNumber;
	}

	public void setOldContactNumber(String oldContactNumber) {
		this.oldContactNumber = oldContactNumber;
	}

	public String getNewContactNumber() {
		return newContactNumber;
	}

	public void setNewContactNumber(String newContactNumber) {
		this.newContactNumber = newContactNumber;
	}

	public Date getUpdateOn() {
		return updateOn;
	}

	public void setUpdateOn(Date updateOn) {
		this.updateOn = updateOn;
	}

	@Id
	@GeneratedValue(strategy=GenerationType.AUTO)
	@Column(name = "contact_number_update_history_id")
	private int id;
	
	@Column(name = "customer_id")
	private String customerId;
	
	@Column(name = "old_contact_number")
	private String oldContactNumber;
	
	@Column(name = "new_contact_number")
	private String newContactNumber;
	
	@Column(name = "updated_on")
	private Date updateOn;
	
	public ContactNumberUpdateHistory(){
		
	}
	
}
