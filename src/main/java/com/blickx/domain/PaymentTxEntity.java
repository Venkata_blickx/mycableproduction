package com.blickx.domain;

import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name = "online_payment_tx")
public class PaymentTxEntity {

	@Id
	@GeneratedValue(strategy=GenerationType.AUTO)
	@Column(name = "payment_tx_id")
	private int id;
	
	@Column
	private String customer_id;
	
	@Column(name = "response_code")
	private int responseCode; 
	
	@Column(name = "response_msg")
	private String responseMsg; 
	
	@Column(name = "created_date")
	private Date createdDate; 
	
	@Column(name = "payment_id")
	private String paymentId; 
	
	@Column(name = "merchant_ref_no")
	private String merchantRefNo; 
	
	@Column(name = "amount")
	private String amount; 
	
	@Column(name = "mode")
	private String mode; 
	
	@Column(name = "billing_name")
	private String billingName; 
	
	@Column(name = "billing_address")
	private String billingAddress; 
	
	@Column(name = "billing_city")
	private String billingCity; 
	
	@Column(name = "billing_state")
	private String billingState; 
	
	@Column(name = "billing_postal_code")
	private String billingPostalCode; 
	
	@Column(name = "billing_country")
	private String billingCountry; 
	
	@Column(name = "billing_phone")
	private String billingPhone; 
	
	@Column(name = "billing_email")
	private String billingEmail; 
	
	@Column(name = "delivery_name")
	private String deliveryName; 
	
	@Column(name = "delivery_address")
	private String deliveryAddress; 
	
	@Column(name = "delivery_city")
	private String deliveryCity; 
	
	@Column(name = "delivery_state")
	private String deliveryState; 
	
	@Column(name = "delivery_postal_code")
	private String deliveryPostalCode; 
	
	@Column(name = "delivery_country")
	private String deliveryCountry; 
	
	@Column(name = "delivery_phone")
	private String deliveryPhone; 
	
	@Column(name = "description")
	private String description; 
	
	@Column(name = "is_flagged")
	private String flagged; 
	
	@Column(name = "transaction_id")
	private String transactionid; 
	
	@Column(name = "payment_method")
	private String paymentMethod; 
	
	@Column(name = "request_id")
	private String requestId; 
	
	@Column(name = "secure_hash")
	private String secureHash;

	@Column(name = "client_id")
	private String client_id;

	@Column(name = "updated_timestamp")
	private Date updatedTimestamp;

	public String getClient_id() {
		return client_id;
	}

	public void setClient_id(String client_id) {
		this.client_id = client_id;
	}

	public Date getUpdatedTimestamp() {
		return updatedTimestamp;
	}

	public void setUpdatedTimestamp(Date updatedTimestamp) {

		this.updatedTimestamp = updatedTimestamp;
	}

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public int getResponseCode() {
		return responseCode;
	}

	public void setResponseCode(int responseCode) {
		this.responseCode = responseCode;
	}

	public String getResponseMsg() {
		return responseMsg;
	}

	public void setResponseMsg(String responseMsg) {
		this.responseMsg = responseMsg;
	}

	public Date getCreatedDate() {
		return createdDate;
	}

	public void setCreatedDate(Date createdDate) {
		this.createdDate = createdDate;
	}

	public String getPaymentId() {
		return paymentId;
	}

	public void setPaymentId(String paymentId) {
		this.paymentId = paymentId;
	}

	public String getMerchantRefNo() {
		return merchantRefNo;
	}

	public void setMerchantRefNo(String merchantRefNo) {
		this.merchantRefNo = merchantRefNo;
	}

	public String getAmount() {
		return amount;
	}

	public void setAmount(String amount) {
		this.amount = amount;
	}

	public String getMode() {
		return mode;
	}

	public void setMode(String mode) {
		this.mode = mode;
	}

	public String getBillingName() {
		return billingName;
	}

	public void setBillingName(String billingName) {
		this.billingName = billingName;
	}

	public String getBillingAddress() {
		return billingAddress;
	}

	public void setBillingAddress(String billingAddress) {
		this.billingAddress = billingAddress;
	}

	public String getBillingCity() {
		return billingCity;
	}

	public void setBillingCity(String billingCity) {
		this.billingCity = billingCity;
	}

	public String getBillingState() {
		return billingState;
	}

	public void setBillingState(String billingState) {
		this.billingState = billingState;
	}

	public String getBillingPostalCode() {
		return billingPostalCode;
	}

	public void setBillingPostalCode(String billingPostalCode) {
		this.billingPostalCode = billingPostalCode;
	}

	public String getBillingCountry() {
		return billingCountry;
	}

	public void setBillingCountry(String billingCountry) {
		this.billingCountry = billingCountry;
	}

	public String getBillingPhone() {
		return billingPhone;
	}

	public void setBillingPhone(String billingPhone) {
		this.billingPhone = billingPhone;
	}

	public String getBillingEmail() {
		return billingEmail;
	}

	public void setBillingEmail(String billingEmail) {
		this.billingEmail = billingEmail;
	}

	public String getDeliveryName() {
		return deliveryName;
	}

	public void setDeliveryName(String deliveryName) {
		this.deliveryName = deliveryName;
	}

	public String getDeliveryAddress() {
		return deliveryAddress;
	}

	public void setDeliveryAddress(String deliveryAddress) {
		this.deliveryAddress = deliveryAddress;
	}

	public String getDeliveryCity() {
		return deliveryCity;
	}

	public void setDeliveryCity(String deliveryCity) {
		this.deliveryCity = deliveryCity;
	}

	public String getDeliveryState() {
		return deliveryState;
	}

	public void setDeliveryState(String deliveryState) {
		this.deliveryState = deliveryState;
	}

	public String getDeliveryPostalCode() {
		return deliveryPostalCode;
	}

	public void setDeliveryPostalCode(String deliveryPostalCode) {
		this.deliveryPostalCode = deliveryPostalCode;
	}

	public String getDeliveryCountry() {
		return deliveryCountry;
	}

	public void setDeliveryCountry(String deliveryCountry) {
		this.deliveryCountry = deliveryCountry;
	}

	public String getDeliveryPhone() {
		return deliveryPhone;
	}

	public void setDeliveryPhone(String deliveryPhone) {
		this.deliveryPhone = deliveryPhone;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public String getFlagged() {
		return flagged;
	}

	public void setFlagged(String flagged) {
		this.flagged = flagged;
	}

	public String getTransactionid() {
		return transactionid;
	}

	public void setTransactionid(String transactionid) {
		this.transactionid = transactionid;
	}

	public String getPaymentMethod() {
		return paymentMethod;
	}

	public void setPaymentMethod(String paymentMethod) {
		this.paymentMethod = paymentMethod;
	}

	public String getRequestId() {
		return requestId;
	}

	public void setRequestId(String requestId) {
		this.requestId = requestId;
	}

	public String getSecureHash() {
		return secureHash;
	}

	public void setSecureHash(String secureHash) {
		this.secureHash = secureHash;
	}

	public String getCustomer_id() {
		return customer_id;
	}

	public void setCustomer_id(String customer_id) {
		this.customer_id = customer_id;
	}

	@Override
	public String toString() {
		return "PaymentTxEntity [id=" + id + ", customer_id=" + customer_id
				+ ", responseCode=" + responseCode + ", responseMsg="
				+ responseMsg + ", createdDate=" + createdDate + ", paymentId="
				+ paymentId + ", merchantRefNo=" + merchantRefNo + ", amount="
				+ amount + ", mode=" + mode + ", billingName=" + billingName
				+ ", billingAddress=" + billingAddress + ", billingCity="
				+ billingCity + ", billingState=" + billingState
				+ ", billingPostalCode=" + billingPostalCode
				+ ", billingCountry=" + billingCountry + ", billingPhone="
				+ billingPhone + ", billingEmail=" + billingEmail
				+ ", deliveryName=" + deliveryName + ", deliveryAddress="
				+ deliveryAddress + ", deliveryCity=" + deliveryCity
				+ ", deliveryState=" + deliveryState + ", deliveryPostalCode="
				+ deliveryPostalCode + ", deliveryCountry=" + deliveryCountry
				+ ", deliveryPhone=" + deliveryPhone + ", description="
				+ description + ", flagged=" + flagged + ", transactionid="
				+ transactionid + ", paymentMethod=" + paymentMethod
				+ ", requestId=" + requestId + ", secureHash=" + secureHash
				+ ", client_id=" + client_id + ", updatedTimestamp="
				+ updatedTimestamp + "]";
	} 
	
	public PaymentTxEntity() {
		
	}
	
	
}
