package com.blickx.domain;

import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name = "bill_generation")
public class BillGeneration {

	@Id
	@Column
	private int row_id;
	
	@Column
	private String customer_id;
	
	@Column
	private String client_id;
	
	@Column
	private String month;
	
	@Column
	private Double package_daily_amount;
	
	@Column
	private Double addon_daily_amount;
	
	@Column
	private Double package_monthly_amount;
	
	@Column
	private Double addon_monthly_amount;
	
	@Column
	private Double discount;
	
	@Column
	private Boolean paid_status;
	
	@Column
	private Date date;
	
	@Column
	private String box_number;

	public Boolean getPaid_status() {
		return paid_status;
	}

	public int getRow_id() {
		return row_id;
	}

	public void setRow_id(int row_id) {
		this.row_id = row_id;
	}

	public String getCustomer_id() {
		return customer_id;
	}

	public void setCustomer_id(String customer_id) {
		this.customer_id = customer_id;
	}

	public String getClient_id() {
		return client_id;
	}

	public void setClient_id(String client_id) {
		this.client_id = client_id;
	}

	public String getMonth() {
		return month;
	}

	public void setMonth(String month) {
		this.month = month;
	}

	public Double getPackage_daily_amount() {
		return package_daily_amount;
	}

	public void setPackage_daily_amount(Double package_daily_amount) {
		this.package_daily_amount = package_daily_amount;
	}

	public Double getAddon_daily_amount() {
		return addon_daily_amount;
	}

	public void setAddon_daily_amount(Double addon_daily_amount) {
		this.addon_daily_amount = addon_daily_amount;
	}

	public Double getPackage_monthly_amount() {
		return package_monthly_amount;
	}

	public void setPackage_monthly_amount(Double package_monthly_amount) {
		this.package_monthly_amount = package_monthly_amount;
	}

	public Double getAddon_monthly_amount() {
		return addon_monthly_amount;
	}

	public void setAddon_monthly_amount(Double addon_monthly_amount) {
		this.addon_monthly_amount = addon_monthly_amount;
	}

	public Double getDiscount() {
		return discount;
	}

	public void setDiscount(Double discount) {
		this.discount = discount;
	}

	public Boolean isPaid_status() {
		return paid_status;
	}

	public void setPaid_status(Boolean paid_status) {
		this.paid_status = paid_status;
	}

	public Date getDate() {
		return date;
	}

	public void setDate(Date date) {
		this.date = date;
	}

	public String getBox_number() {
		return box_number;
	}

	public void setBox_number(String box_number) {
		this.box_number = box_number;
	}

	@Override
	public String toString() {
		return "BillGeneration [row_id=" + row_id + ", customer_id="
				+ customer_id + ", client_id=" + client_id + ", month=" + month
				+ ", package_daily_amount=" + package_daily_amount
				+ ", addon_daily_amount=" + addon_daily_amount
				+ ", package_monthly_amount=" + package_monthly_amount
				+ ", addon_monthly_amount=" + addon_monthly_amount
				+ ", discount=" + discount + ", paid_status=" + paid_status
				+ ", date=" + date + ", box_number=" + box_number + "]";
	}
	
	public BillGeneration(){
		
	}
	
	
}
