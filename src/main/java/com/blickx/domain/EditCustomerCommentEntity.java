package com.blickx.domain;

import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

import com.fasterxml.jackson.databind.annotation.JsonDeserialize;
import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import com.blickx.utill.CustomJsonDateDeserializer;

@Entity
@Table(name = "customer_comments")
public class EditCustomerCommentEntity {

	@Id
	@GeneratedValue(strategy=GenerationType.AUTO)
	@Column(name="row_id", unique=true, nullable= false)
	private int customerCommentsId;
	
	@Column(name = "user")
	private String user;
	
	@Column(name = "customer_id")
	private String customer_id;
	
	@Column
	private Date created_timestamp;
	
	@Column
	private int priority;
	
	@Column
	private String comment;

	public String getComment() {
		return comment;
	}

	public void setComment(String comment) {
		this.comment = comment;
	}

	public int getCustomerCommentsId() {
		return customerCommentsId;
	}

	public void setCustomerCommentsId(int customerCommentsId) {
		this.customerCommentsId = customerCommentsId;
	}

	public String getUser() {
		return user;
	}

	public void setUser(String user) {
		this.user = user;
	}

	public String getCustomer_id() {
		return customer_id;
	}

	public void setCustomer_id(String customer_id) {
		this.customer_id = customer_id;
	}

	@JsonSerialize(using = DateSerializerWithTimeStamp.class)
	public Date getCreated_timestamp() {
		return created_timestamp;
	}

	@JsonDeserialize(using = CustomJsonDateDeserializer.class)
	public void setCreated_timestamp(Date created_timestamp) {
		this.created_timestamp = created_timestamp;
	}

	public int isPriority() {
		return priority;
	}

	public void setPriority(int priority) {
		this.priority = priority;
	}

	@Override
	public String toString() {
		return "EditCustomerCommentEntity [customerCommentsId="
				+ customerCommentsId + ", user=" + user + ", customer_id="
				+ customer_id + ", created_timestamp=" + created_timestamp
				+ ", priority=" + priority + ", comment=" + comment + "]";
	}
	
	public EditCustomerCommentEntity(){
		
	}
}
