package com.blickx.domain;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

import com.fasterxml.jackson.databind.annotation.JsonSerialize;

@Entity
@Table(name="billing")
public class Billing implements Serializable {

	
	/**Billing Entity
	 * 
	 */
	private static final long serialVersionUID = 2484107627619494663L;

	@Id
	@GeneratedValue(strategy=GenerationType.AUTO)
	@Column(name = "row_id")
	private int row_id;
	
	@Column(name="bill_recipt_no")
	private String bill_recipt_no;
	
	@Column(name="date")
	private Date date;
	
	@Column(name="paid_amount")
	private Double paid_amount;
	
	@Column(name="balance_amount")
	private Double balance_amount;
	
	@Column(name="total_amount")
	private double total_amount;
	
	@Column(name="employee_id")
	private String employee_id;
	
	@Column
	private String customer_id;
	
	@Column
	private String client_id;
	
	@Column
	private String payment_type;
	
	@Column
	private String cheque_No;
	
	@Column
	private int discount;
	
	@Column
	private String transaction_id;
	
	
	
	/*@ManyToOne
	@JoinColumn(name="customer_id")
	private Customer customer;
	
	
	public Customer getCustomer() {
		return customer;
	}

	public void setCustomer(Customer customer) {
		this.customer = customer;
	}*/

	public int getRow_id() {
		return row_id;
	}

	public void setRow_id(int row_id) {
		this.row_id = row_id;
	}

	public String getTransaction_id() {
		return transaction_id;
	}

	public void setTransaction_id(String transaction_id) {
		this.transaction_id = transaction_id;
	}

	public double getTotal_amount() {
		return total_amount;
	}

	public void setTotal_amount(double total_amount) {
		this.total_amount = total_amount;
	}

	public String getEmployee_id() {
		return employee_id;
	}

	public void setEmployee_id(String employee_id) {
		this.employee_id = employee_id;
	}

	public String getBill_recipt_no() {
		return bill_recipt_no;
	}

	public void setBill_recipt_no(String bill_recipt_no) {
		this.bill_recipt_no = bill_recipt_no;
	}

	public String getCustomer_id() {
		return customer_id;
	}

	public void setCustomer_id(String customer_id) {
		this.customer_id = customer_id;
	}

	public String getClient_id() {
		return client_id;
	}

	public void setClient_id(String client_id) {
		this.client_id = client_id;
	}

	public String getPayment_type() {
		return payment_type;
	}

	public void setPayment_type(String payment_type) {
		this.payment_type = payment_type;
	}

	public String getCheque_No() {
		return cheque_No;
	}

	public void setCheque_No(String cheque_No) {
		this.cheque_No = cheque_No;
	}

	public int getDiscount() {
		return discount;
	}

	public void setDiscount(int discount) {
		this.discount = discount;
	}

	@JsonSerialize(using = DateSerializer.class)
	public Date getDate() {
		return date;
	}

	@JsonSerialize(using=DateSerializer.class)
	public void setDate(Date date) {
		this.date = date;
	}

	public Double getPaid_amount() {
		return paid_amount;
	}

	public void setPaid_amount(Double paid_amount) {
		this.paid_amount = paid_amount;
	}

	public Double getBalance_amount() {
		return balance_amount;
	}

	public void setBalance_amount(Double balance_amount) {
		this.balance_amount = balance_amount;
	}

	@Override
	public String toString() {
		return "Billing [row_id=" + row_id + ", bill_recipt_no="
				+ bill_recipt_no + ", date=" + date + ", paid_amount="
				+ paid_amount + ", balance_amount=" + balance_amount
				+ ", total_amount=" + total_amount + ", employee_id="
				+ employee_id + ", customer_id=" + customer_id + ", client_id="
				+ client_id + ", payment_type=" + payment_type + ", cheque_No="
				+ cheque_No + ", discount=" + discount + ", transaction_id="
				+ transaction_id + "]";
	}
	
	
	public Billing(){}
	

}
