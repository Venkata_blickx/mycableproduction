package com.blickx.domain;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

import org.springframework.format.annotation.DateTimeFormat;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.databind.annotation.JsonSerialize;

@Entity
@Table(name = "addons")
@JsonIgnoreProperties({"hibernateLazyInitializer", "handler"})
public class AddOnsEntity implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = -8466835199811812956L;

	@Id
	@GeneratedValue(strategy=GenerationType.AUTO)
	@Column
	private int id;
	
	@Column
	private String customer_id;
	
	@Column
	private String box_number;
	
	@Column
	private String addon;
	
	@Column
	@DateTimeFormat(pattern = "yyyy-MM-dd")
	private Date expiry_date;
	
	@Column
	@DateTimeFormat(pattern = "yyyy-MM-dd")
	private Date start_date;
	
	/*@ManyToOne
	@JoinColumn(name="box_number")
	private SetupBox setupbox;
*/
	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public String getCustomer_id() {
		return customer_id;
	}

	public void setCustomer_id(String customer_id) {
		this.customer_id = customer_id;
	}

	public String getBox_number() {
		return box_number;
	}

	public void setBox_number(String box_number) {
		this.box_number = box_number;
	}

	public String getAddon() {
		return addon;
	}

	public void setAddon(String addon) {
		this.addon = addon;
	}

	@JsonSerialize(using = DateSerializer.class)
	public Date getExpiry_date() {
		return expiry_date;
	}

	public void setExpiry_date(Date expiry_date) {
		this.expiry_date = expiry_date;
	}
	
	@JsonSerialize(using = DateSerializer.class)
	public Date getStart_date() {
		return start_date;
	}

	public void setStart_date(Date start_date) {
		this.start_date = start_date;
	}

	public AddOnsEntity(){
		
	}

	@Override
	public String toString() {
		return "AddOnsEntity [id=" + id + ", customer_id=" + customer_id
				+ ", box_number=" + box_number + ", addon=" + addon
				+ ", expiry_date=" + expiry_date + "]";
	}
	
	
}
