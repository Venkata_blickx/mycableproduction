package com.blickx.exceptions;

public class ImeiNumberDuplicatedException extends Exception {

	String errorCode;
	
	public ImeiNumberDuplicatedException(String errorCode){
		super(errorCode);
		this.errorCode = errorCode;
	}

	@Override
	public String toString() {
		return errorCode;
	}
	
}
