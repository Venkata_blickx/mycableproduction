package com.blickx.exceptions;

import java.util.Calendar;
import java.util.List;

import org.apache.log4j.Logger;
import org.hibernate.Query;
import org.hibernate.Session;

import com.blickx.id.generator.BillIdGenerator;

public class TestClass {

public String getNextSid(Session session, String client_id){
		
	
		Calendar now = Calendar.getInstance();
		int year = now.get(Calendar.YEAR);
		String s = Integer.toString(year).substring(2);
		String s1 = "";
		if (now.MONTH > 4) {
			s1 = Integer.toString(year - 1).substring(2) + "-"
					+ s;
		} else {
			s1 = Integer.parseInt(Integer.toString(year).substring(2))
					+ "-" + Integer.toString((Integer.parseInt(s) + 1));
		}
		
		
		String bill_id = client_id + s1 +"/";
		
		Logger logger = Logger.getLogger(BillIdGenerator.class);
			
			try{
				// session=sfactory.openSession();
				Query q = session.createQuery("from Billing bil where bil.client_id = ? and bill.paid > 0").setString(0, client_id);
				int size = q.list().size();
				
				if(size!=0 ){
					Query query=session.createQuery("select max(bill_recipt_no) from Billing bil where bil.client_id = ? and bil.paid_amount > 0").setParameter(0, client_id);
					List list = query.list();
					String recipt_no = (String) list.get(0);
					String pre_string = recipt_no.substring(8,13);
					if(pre_string.equalsIgnoreCase(s1)){
						
						String id_no = recipt_no.substring(14);
						bill_id = bill_id + (id_no+1);
						
					}
					else{
						bill_id = bill_id + "1";
					}
					
					
				}else{
					bill_id = bill_id + "1";
				}
				}catch (Exception e) {
				
				logger.error("From ID generator"+e);
				
			}
			return bill_id;
	}
}
