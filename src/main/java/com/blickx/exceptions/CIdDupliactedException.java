package com.blickx.exceptions;

public class CIdDupliactedException extends Exception{

public String errorCode;
	
	public CIdDupliactedException(String msg){
		super(msg);
		this.errorCode = msg;
	}

	@Override
	public String toString() {
		return errorCode;
	}
}
