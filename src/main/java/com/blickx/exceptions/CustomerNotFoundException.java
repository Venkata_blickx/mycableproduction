package com.blickx.exceptions;

public class CustomerNotFoundException extends Exception{
	private int errorCode;
	private String errorMsg;
	public CustomerNotFoundException(int errorCode, String errorMsg) {
		super();
		this.errorCode = errorCode;
		this.errorMsg = errorMsg;
	}
	@Override
	public String toString() {
		return "CustomerNotFoundException [errorCode=" + errorCode
				+ ", errorMsg=" + errorMsg + "]";
	}
	
	
	
}
