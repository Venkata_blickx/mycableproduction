package com.blickx.exceptions;

public class VcNumberDuplicatedException extends Exception {

	public String errorCode;
	
	public VcNumberDuplicatedException(String msg){
		super(msg);
		this.errorCode = msg;
	}

	@Override
	public String toString() {
		return errorCode;
	}

		
	
}
