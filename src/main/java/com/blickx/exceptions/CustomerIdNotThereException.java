package com.blickx.exceptions;


public class CustomerIdNotThereException extends Exception {
	
	private static final long serialVersionUID = -2151515147355511072L;
	
	String errorCode;
	
	public CustomerIdNotThereException(String errorCode){
		super(errorCode);
		this.errorCode = errorCode;
	}

	@Override
	public String toString() {
		return errorCode;
	}
	
	

}
