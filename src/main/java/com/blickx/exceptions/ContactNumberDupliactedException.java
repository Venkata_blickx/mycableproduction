package com.blickx.exceptions;

public class ContactNumberDupliactedException extends Exception {

	String errorCode;
	
	public ContactNumberDupliactedException(String errorCode){
		super(errorCode);
		this.errorCode = errorCode;
	}

	@Override
	public String toString() {
		return errorCode;
	}
}
