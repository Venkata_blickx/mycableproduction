package com.blickx.exceptions;

public class SimNumberDuplicatedException extends Exception {

	String errorCode;
	
	public SimNumberDuplicatedException(String errorCode){
		super(errorCode);
		this.errorCode = errorCode;
	}

	@Override
	public String toString() {
		return errorCode;
	}
	
}
