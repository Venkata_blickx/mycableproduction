package com.blickx.exceptions;

public class DeviceNameDuplicatedException extends Exception{

	String errorCode;
	
	public DeviceNameDuplicatedException(String errorCode){
		super(errorCode);
		this.errorCode = errorCode;
	}

	@Override
	public String toString() {
		return errorCode;
	}
}
