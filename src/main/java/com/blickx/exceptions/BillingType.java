package com.blickx.exceptions;

public enum BillingType {

	PREPAID(true), POSTPAID(false);
	
	private boolean s;
	
	private BillingType(boolean s){
		this.s = s;
	}
	
	public boolean getStatusCode() {
		return s;
	}
}
