package com.blickx.exceptions;

public class EmployeeNotFoundException extends RuntimeException {
	
	int errorCode;
	String errorMsg;
	
	public EmployeeNotFoundException(int errorCode, String errorMsg) {
		super();
		this.errorCode = errorCode;
		this.errorMsg = errorMsg;
	}
	
	
}
