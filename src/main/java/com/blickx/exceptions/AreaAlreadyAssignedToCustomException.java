package com.blickx.exceptions;

public class AreaAlreadyAssignedToCustomException extends Exception {

	String errorCode;
	
	public AreaAlreadyAssignedToCustomException(String errorCode){
		super(errorCode);
		this.errorCode = errorCode;
	}

	@Override
	public String toString() {
		return errorCode;
	}
}
