package com.blickx.exceptions;

public class CustomerDeletionFailed extends RuntimeException{

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private int errorCode;
	private String errorMsg;
	public CustomerDeletionFailed(int errorCode, String errorMsg) {
		super();
		this.errorCode = errorCode;
		this.errorMsg = errorMsg;
	}
	
	
}
