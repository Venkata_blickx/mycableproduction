package com.blickx.exceptions;

public class BoxNumberDupliactedException extends Exception {

	String errorCode;
	
	public BoxNumberDupliactedException(String errorCode){
		super(errorCode);
		this.errorCode = errorCode;
	}

	@Override
	public String toString() {
		return errorCode;
	}
	
	
}
