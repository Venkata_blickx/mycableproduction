package com.blickx.exceptions;

public class ErrorCodes {
	
	public static String getContactNumberDuplicateMsg(String contactNumber) {
		return "MCD001 Data validation error: Contact number " + contactNumber + " is already exists!";
	}
	public static String getcIdDuplicateMsg(String cid) {
		return "MCD002 Data validation error: Customer id " + cid + " is already exists!";
	}
	public static String getBoxNumberDuplicateMsg(String boxNumber) {
		return "MCD003 Data validation error: SetTopBox number " + boxNumber + " is already exists!";
	}
	public static String getVcNumberDuplicateMsg(String vcNumber) {
		return "MCD004 Data validation error: Vc number " + vcNumber + " is already exists!";
	}
	public static String getImeiNumberDuplicateMsg(String imeiNumber) {
		return "MCD005 Data validation error: Imei number " + imeiNumber + " is already exists!";
	}
	public static String getSimNumberDuplicateMsg(String simNumber) {
		return "MCD006 Data validation error: Sim number " + simNumber + " is already exists!";
	}
	public static String getDeviceNameDuplicateMsg(String deviceName) {
		return "MCD006 Data validation error: Device name " + deviceName + " is already exists!";
	}
	
	public static String getDeviceAlreadyExists(String deviceName) {
		return "MCD006 Data validation error: Device " + deviceName + " is already assigned to some other employee!";
	}
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	public static final String CONTACT_NUMBER_DUPLICATE_CODE = "MCD001 Data validation error: Customer contact number is already exists!";
	public static final String C_ID_DUPLICATE_CODE = "MCD002 Data validation error: Customer id is already exists!";
	
	public static final String BOX_NUMBER_DUPLICATE_CODE = "MCD003 Data validation error: SetTopBox number is already exists!";
	public static final String VC_NUMBER_DUPLICATE_CODE = "MCD004 Data validation error: Vc number is already exists!";

	
	public static final String CONTACT_NUMBER_DUPLICATE_MSG = "Customer Contact Number is Duplicated";
	public static final String C_ID_DUPLICATE_MSG = "Customer Id is Duplicated";
	
	public static final String BOX_NUMBER_DUPLICATE_MSG = "SetupBox Number is Duplicated";
	public static final String VC_NUMBER_DUPLICATE_MSG = "Vc Number is Duplicated";
	
}
