package com.blickx.exceptions;

public class DeviceAlreadyAssignedException extends Exception {

	String errorCode;
	
	public DeviceAlreadyAssignedException(String errorCode){
		super(errorCode);
		this.errorCode = errorCode;
	}

	@Override
	public String toString() {
		return errorCode;
	}
}
