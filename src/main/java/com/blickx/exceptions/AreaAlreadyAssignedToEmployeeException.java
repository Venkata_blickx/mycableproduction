package com.blickx.exceptions;

public class AreaAlreadyAssignedToEmployeeException extends Exception {

	String errorCode;
	
	public AreaAlreadyAssignedToEmployeeException(String errorCode){
		super(errorCode);
		this.errorCode = errorCode;
	}

	@Override
	public String toString() {
		return errorCode;
	}
}
