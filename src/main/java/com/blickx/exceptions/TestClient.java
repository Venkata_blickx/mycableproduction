package com.blickx.exceptions;

import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;

public class TestClient {

	@Autowired
	static SessionFactory sfactory;
	
	public static void main(String[] args) {
		
		Session session = sfactory.openSession();
		TestClass tes = new TestClass();
		System.out.println(tes.getNextSid(session, "MYA00002"));
		
		
	}
}
