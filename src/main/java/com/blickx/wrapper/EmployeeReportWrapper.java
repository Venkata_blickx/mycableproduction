package com.blickx.wrapper;

import java.math.BigDecimal;
import java.util.List;

import com.blickx.to.GetEmployeeReportsTo;

public class EmployeeReportWrapper {

	private BigDecimal totalSum;
	
	private List<GetEmployeeReportsTo> report;

	public BigDecimal getTotalSum() {
		return totalSum;
	}

	public void setTotalSum(BigDecimal totalSum) {
		this.totalSum = totalSum;
	}

	public List<GetEmployeeReportsTo> getReport() {
		return report;
	}

	public void setReport(List<GetEmployeeReportsTo> report) {
		this.report = report;
	}

	@Override
	public String toString() {
		return "EmployeeReportWrapper [totalSum=" + totalSum + ", report="
				+ report + "]";
	}
	
	public EmployeeReportWrapper() {
		
	}
}
