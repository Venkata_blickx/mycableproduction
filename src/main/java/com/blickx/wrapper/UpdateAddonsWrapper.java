package com.blickx.wrapper;

import java.util.List;
import java.util.Set;

import com.blickx.settings.to.AddonsEntityTo;
import com.blickx.to.SetupBoxTo;

public class UpdateAddonsWrapper {

	private SetupBoxTo setupBoxTo;
	
	private Set<Integer> deletedAddonIds;
	
	private Set<AddonsEntityTo> updatedAddons;
	
	private Set<AddonsEntityTo> addedAddons;

	public SetupBoxTo getSetupBoxTo() {
		return setupBoxTo;
	}

	public void setSetupBoxTo(SetupBoxTo setupBoxTo) {
		this.setupBoxTo = setupBoxTo;
	}

	public Set<Integer> getDeletedAddonIds() {
		return deletedAddonIds;
	}

	public void setDeletedAddonIds(Set<Integer> deletedAddonIds) {
		this.deletedAddonIds = deletedAddonIds;
	}

	public Set<AddonsEntityTo> getUpdatedAddons() {
		return updatedAddons;
	}

	public void setUpdatedAddons(Set<AddonsEntityTo> updatedAddons) {
		this.updatedAddons = updatedAddons;
	}

	public Set<AddonsEntityTo> getAddedAddons() {
		return addedAddons;
	}

	public void setAddedAddons(Set<AddonsEntityTo> addedAddons) {
		this.addedAddons = addedAddons;
	}
	
	@Override
	public String toString() {
		return "UpdateAddonsWrapper [deletedAddonIds=" + deletedAddonIds
				+ ", updatedAddons=" + updatedAddons + ", addedAddons="
				+ addedAddons + "]";
	}

	public UpdateAddonsWrapper(){
		
	}
}
