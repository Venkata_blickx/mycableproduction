package com.blickx.wrapper;

import java.util.List;

import com.blickx.to.input.ContactUpdateTo;

public class UpdateContactNumberWrapper {

	public List<ContactUpdateTo> getUpdateContactTOs() {
		return updateContactTOs;
	}

	public void setUpdateContactTOs(List<ContactUpdateTo> updateContactTOs) {
		this.updateContactTOs = updateContactTOs;
	}

	@Override
	public String toString() {
		return "UpdateContactNumberWrapper [updateContactTOs="
				+ updateContactTOs + "]";
	}

	private List<ContactUpdateTo> updateContactTOs;
	
	public UpdateContactNumberWrapper(){
		
	}
	
}
