package com.blickx.wrapper;

import java.util.List;

import com.blickx.to.input.ComplaintInputTo;

public class ComplaintWrapper {

	@Override
	public String toString() {
		return "ComplaintWrapper [complaintsTo=" + complaintsTo + "]";
	}

	private List<ComplaintInputTo> complaintsTo;

	public List<ComplaintInputTo> getComplaintsTo() {
		return complaintsTo;
	}

	public void setComplaintsTo(List<ComplaintInputTo> complaintsTo) {
		this.complaintsTo = complaintsTo;
	}
	
	public ComplaintWrapper(){
		
	}
}
