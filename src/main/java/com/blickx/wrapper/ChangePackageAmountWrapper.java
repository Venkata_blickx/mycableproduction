package com.blickx.wrapper;

import java.util.List;

import com.blickx.to.input.PackageDetails;

public class ChangePackageAmountWrapper {

	private double newAmount;

	private List<PackageDetails> packageDetails;
	
	public double getNewAmount() {
		return newAmount;
	}

	public void setNewAmount(double newAmount) {
		this.newAmount = newAmount;
	}

	public List<PackageDetails> getPackageDetails() {
		return packageDetails;
	}

	public void setPackageDetails(List<PackageDetails> packageDetails) {
		this.packageDetails = packageDetails;
	}
	
	public ChangePackageAmountWrapper(){
		
	}
}
