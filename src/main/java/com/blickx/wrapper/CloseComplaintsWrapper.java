package com.blickx.wrapper;

import java.util.List;

import com.blickx.searchEntities.CloseComplaint;

public class CloseComplaintsWrapper {

	private List<CloseComplaint> resloveComplaints;

	public List<CloseComplaint> getResloveComplaints() {
		return resloveComplaints;
	}

	public void setResloveComplaints(List<CloseComplaint> resloveComplaints) {
		this.resloveComplaints = resloveComplaints;
	}

	@Override
	public String toString() {
		return "CloseComplaintsResolve [resloveComplaints=" + resloveComplaints
				+ "]";
	}
	
	public CloseComplaintsWrapper() {
		// TODO Auto-generated constructor stub
	}
}
