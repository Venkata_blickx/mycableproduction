package com.blickx.wrapper;

import java.util.List;

import com.blickx.to.PackageTO;

public class PackageWrapper {

	private List<PackageTO> packages;

	public List<PackageTO> getPackages() {
		return packages;
	}

	public void setPackages(List<PackageTO> packages) {
		this.packages = packages;
	}
	
	public PackageWrapper(){
		
	}
}
