package com.blickx.wrapper;

import java.math.BigDecimal;
import java.util.List;

import com.blickx.to.CustomerBillReportByCidAndDateTo;

public class UnpaidReportWrapper {

	private BigDecimal totalSum;
	
	private List<CustomerBillReportByCidAndDateTo> report;

	public BigDecimal getTotalSum() {
		return totalSum;
	}

	public void setTotalSum(BigDecimal totalSum) {
		this.totalSum = totalSum;
	}

	public List<CustomerBillReportByCidAndDateTo> getReport() {
		return report;
	}

	public void setReport(List<CustomerBillReportByCidAndDateTo> report) {
		this.report = report;
	}

	@Override
	public String toString() {
		return "UnpaidReportWrapper [totalSum=" + totalSum + ", report="
				+ report + "]";
	}
	
	public UnpaidReportWrapper() {
		
	}
}
