package com.blickx.wrapper;

import java.util.List;

import com.blickx.domain.Channels;

public class ChannelsWrapper {
	
	private List<Channels> channels;

	public List<Channels> getChannels() {
		return channels;
	}

	public void setChannels(List<Channels> channels) {
		this.channels = channels;
	}
	
	public ChannelsWrapper(){
		
	}
}
