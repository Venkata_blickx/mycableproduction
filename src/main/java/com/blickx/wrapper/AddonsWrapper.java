package com.blickx.wrapper;

import java.util.Set;

import com.blickx.settings.to.AddonsEntityTo;

public class AddonsWrapper {

	private Set<AddonsEntityTo> addOns;

	public Set<AddonsEntityTo> getAddOns() {
		return addOns;
	}

	public void setAddOns(Set<AddonsEntityTo> addOns) {
		this.addOns = addOns;
	}

	public AddonsWrapper(){
		
	}
	
}
