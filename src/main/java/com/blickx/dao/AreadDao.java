package com.blickx.dao;

import java.util.List;

import com.blickx.domain.AreaEntity;
import com.blickx.exceptions.AreaAlreadyAssignedToCustomException;
import com.blickx.exceptions.AreaAlreadyAssignedToEmployeeException;
import com.blickx.settings.to.AreaEntityTo;

public interface AreadDao {

	public void addAreaDetails(AreaEntity area);

	public AreaEntity getAreaDetailsById(AreaEntity area);

	public void updateAreaDetails(AreaEntity area);

	public void deleteAreaDetails(AreaEntityTo area) throws AreaAlreadyAssignedToCustomException, AreaAlreadyAssignedToEmployeeException;

	public List<AreaEntityTo> getAllAreaDetails(String client_id);

}
