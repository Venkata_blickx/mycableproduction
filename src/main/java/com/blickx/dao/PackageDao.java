package com.blickx.dao;

import java.util.List;

import com.blickx.channels.categories.ChannelSeraliser;
import com.blickx.domain.Channels;
import com.blickx.to.ChannelsTOO;
import com.blickx.to.ChannelsTo;
import com.blickx.to.PackageTO;

public interface PackageDao {
	
	public List<String> getPackages();
	
	public int addPackage(PackageTO packageTO);
	
	public PackageTO getPackageById(int package_id);
	
	public int updatePackage(PackageTO packages);
	
	public String deletePackage(int package_id);
	
	public String deactivatePackage(int package_id);
	
	public String activatePackage(int package_id);
	
	public int addChannel(ChannelsTo channel);
	
	public List<ChannelSeraliser> getChannelNames();
	
	public ChannelsTo getChannelById(int channel_id);

	public int updateChannel(Channels channel);
	
	public List<ChannelsTOO> getAllChannels(String client_id);
	
}
