package com.blickx.dao;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;

import org.apache.log4j.Logger;
import org.hibernate.HibernateException;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.Transaction;
import org.hibernate.transform.Transformers;
import org.springframework.beans.factory.annotation.Autowired;

import com.blickx.controller.CustomerController;
import com.blickx.domain.ClientInformationEntity;
import com.blickx.domain.CustomerToMobile;
import com.blickx.output.PaidCustomerTo;
import com.blickx.to.CustomerToMobileTo;
import com.blickx.to.EmployeeCredentialsTo;
import com.blickx.to.input.InputIds;


public class AndroidDaoImpl implements AndroidDao{

	@Autowired
	SessionFactory sfactory;
	
	@Autowired
	CustomerDao cDao;
	
	private Logger logger = Logger.getLogger(CustomerController.class);

	
	public List<CustomerToMobile> getAllCustomerBills(String client_id) {
		
		List<CustomerToMobile> result = new ArrayList<CustomerToMobile>();
		Session session = sfactory.openSession();
		Transaction tx = null;
		try{
			tx = session.beginTransaction();
			
			@SuppressWarnings("unchecked")
			List<String> customer_ids = session.createQuery("select customer_id from Customer c where c.client_id = ?").setString(0, client_id).list();
			
			
			for(String customer_id:customer_ids){
				result.add(cDao.getCustomerWithBillingDetails(customer_id));
			}
			
			tx.commit();
			
		}catch(HibernateException e){
			if(tx != null)tx.rollback();
			return result;
		}catch(Exception e){
			if(tx != null)tx.rollback();
			return result;
		}finally{
			session.close();
		}
		return result;
	}

	@SuppressWarnings("unchecked")
	public List<EmployeeCredentialsTo> getAllEmployeCredetials(String client_id) {
		
		List<EmployeeCredentialsTo> result = new ArrayList<EmployeeCredentialsTo>();
		Session session = sfactory.openSession();
		Transaction tx = null;
		
		try{
			
			tx = session.beginTransaction();
			
			result = session.createSQLQuery("select e.employee_id, e.password, d.ime_number, e.employee_uid, e.employee_name, e.contact_number from employee e inner join device d "+
					"where e.employee_id = d.employee_id and e.client_id = '"+client_id+"'").setResultTransformer(Transformers.aliasToBean(EmployeeCredentialsTo.class)).list();
			for(EmployeeCredentialsTo empCred: result){
				empCred.setCurrentMonthCollection((BigDecimal)session.createSQLQuery("select sum(paid_amount) from billing where employee_id = ? and month(date) = month(current_date) and year(date) = year(current_date)")
				.setString(0, empCred.getEmployee_id()).uniqueResult());
			}
			
			//logger.info("select e.employee_id, e.password, d.ime_number, e.employee_uid from employee e inner join device d "+
				//	"where e.employee_id = d.employee_id and e.client_id = '"+client_id+"'");
			
			tx.commit();
		}catch(HibernateException e){
			if(tx != null)tx.rollback();
			return result;
		}finally{
			session.clear();
		}
		
		return result;
	}

	public List<CustomerToMobileTo> getAllCustomersBills(String client_id, String ime_number) {
		List<CustomerToMobileTo> result = new ArrayList<CustomerToMobileTo>();
		Session session = sfactory.openSession();
		Transaction tx = null;
		try{
			tx = session.beginTransaction();
			logger.info("getAllCustomerBillDetails For Android");
			@SuppressWarnings("unchecked")
			List<CustomerToMobileTo> resultset = session.createSQLQuery("call GetAllCustomer_BillDetails(?, ?)")
												.setParameter(0, client_id)
												.setParameter(1, ime_number)
												.setResultTransformer(Transformers.aliasToBean(CustomerToMobileTo.class))
												.list();
			//logger.info(result.get(0).toString());
			result = resultset;
			
			logger.info("GetAllCustomerBillingDetails Ending");
			
			tx.commit();
			
		}catch(HibernateException e){
			if(tx != null)tx.rollback();
			logger.error(e);
			return result;
		}catch(Exception e){
			if(tx != null)tx.rollback();
			logger.error(e);
			return result;
		}finally{
			session.close();
		}
		return result;
	}
	
	@SuppressWarnings("unchecked")
	@Override
	public List<ClientInformationEntity> getClientInfo(String client_id) {
		List<ClientInformationEntity> result = null;
		Session session = sfactory.openSession();
		Transaction tx = null;
		try{
			tx = session.beginTransaction();
			logger.info("/getClientInformationForPrint - AndroidDaoImpl - getClientInfo() : Starting...");
			result = (List<ClientInformationEntity>) session.createQuery("from ClientInformationEntity a where a.client_id = ?")
												.setParameter(0, client_id)
												.list();
			
			logger.info("/getClientInformationForPrint - AndroidDaoImpl - getClientInfo() : Ending...");
			
			tx.commit();
			
		}catch(HibernateException e){
			if(tx != null)tx.rollback();
			logger.error("/getClientInformationForPrint - AndroidDaoImpl - getClientInfo() HibernateException:" + e);
			return result;
		}catch(Exception e){
			if(tx != null)tx.rollback();
			logger.error("/getClientInformationForPrint - AndroidDaoImpl - getClientInfo() Exception:" + e);
			return result;
		}finally{
			session.close();
		}
		logger.info("/getClientInformationForPrint - AndroidDaoImpl - getClientInfo() : OUTPUT" + result);
		return result;
	}

	@Override
	public List<CustomerToMobileTo> getAllUpdatedCustomersBills(
			String client_id, InputIds input) {
		List<CustomerToMobileTo> result = new ArrayList<CustomerToMobileTo>();
		Session session = sfactory.openSession();
		Transaction tx = null;
		try{
			tx = session.beginTransaction();
			logger.info("getAllUpdatedCustomerBillDetails For Android Starting INput: " + input);
			@SuppressWarnings("unchecked")
			List<CustomerToMobileTo> resultset = session.createSQLQuery("call updated_customer_billings_for_android(?, ?, ?)")
												.setParameter(0, client_id)
												.setParameter(1, input.getIme_number())
												.setParameter(2, input.getLast_sync_timing())
												.setResultTransformer(Transformers.aliasToBean(CustomerToMobileTo.class))
												.list();
			//logger.info(result.get(0).toString());
			result = resultset;
			
			logger.info("GetAllUpdatedCustomerBillingDetails Ending Result :" + result);
			
			tx.commit();
			
		}catch(HibernateException e){
			if(tx != null)tx.rollback();
			logger.error(e);
			return result;
		}catch(Exception e){
			if(tx != null)tx.rollback();
			logger.error(e);
			return result;
		}finally{
			session.close();
		}
		return result;
	}

	@Override
	public List<PaidCustomerTo> getPaidTxs(String client_id, InputIds input) {
		List<PaidCustomerTo> result = new ArrayList<PaidCustomerTo>();
		Session session = sfactory.openSession();
		Transaction tx = null;
		try{
			tx = session.beginTransaction();
			logger.info("getPaidTxs For Android Starting INput: " + input.getIme_number());
			@SuppressWarnings("unchecked")
			List<PaidCustomerTo> resultset = session.createSQLQuery("call paidTxs(?, ?)")
												.setParameter(0, client_id)
												.setParameter(1, input.getIme_number())
												.setResultTransformer(Transformers.aliasToBean(PaidCustomerTo.class))
												.list();
			//logger.info(result.get(0).toString());
			result = resultset;
			
			logger.info("getPaidTXs Ending Result :" + result);
			
			tx.commit();
			
		}catch(HibernateException e){
			if(tx != null)tx.rollback();
			logger.error(e);
			return result;
		}catch(Exception e){
			if(tx != null)tx.rollback();
			logger.error(e);
			return result;
		}finally{
			session.close();
		}
		return result;

	}

	
}
