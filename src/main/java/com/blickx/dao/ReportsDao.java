package com.blickx.dao;

import java.util.List;
import java.util.Map;

import com.blickx.domain.Customer;
import com.blickx.output.SetupBoxReport;
import com.blickx.output.wrapper.CustomerReportsWrapper;
import com.blickx.response.AreaWiseReport;
import com.blickx.response.AreaWiseReportWrap;
import com.blickx.searchEntities.GetEmployeeReportsSearchEntity;
import com.blickx.to.CustomerBillReportByCidAndDateTo;
import com.blickx.to.CustomerReports;
import com.blickx.to.DailyCollectionByEmployee;
import com.blickx.to.GetCollectionOfEmployeeTo;
import com.blickx.to.GetCustomerBillReportDate;
import com.blickx.to.GetEmployeeReportsTo;
import com.blickx.to.GetLastSixTxsTo;
import com.blickx.to.input.LastSixMonthsTxsInputTo;
import com.blickx.to.input.SetupboxReportInput;
import com.blickx.wrapper.EmployeeReportWrapper;
import com.blickx.wrapper.UnpaidReportWrapper;

public interface ReportsDao {

	public List<DailyCollectionByEmployee> getCollectionByEmployee(String client_id);

	public CustomerReportsWrapper getCustomerReports(GetCustomerBillReportDate customer);

	public CustomerReportsWrapper getCustomerReportsWithDateRange(
			GetCustomerBillReportDate getCustomerBillReportDate);

	public List<GetCollectionOfEmployeeTo> getCollectionOfEmployee(String client_id, String date);

	public EmployeeReportWrapper getBillReportsOfEmployee(
			GetEmployeeReportsSearchEntity searchDetails);

	public EmployeeReportWrapper getBillReportsOfEmployeeByDate(
			GetEmployeeReportsSearchEntity searchDetails);

	public Map<String, Object> getEmployeeCurrentDayCollections(
			String employee_id);

	public UnpaidReportWrapper getCustomerBillReportDate(
			GetCustomerBillReportDate getCustomerBillReportDate);

	public List<GetLastSixTxsTo> getLastSixTxs(LastSixMonthsTxsInputTo input);

	public List<SetupBoxReport> getSetupboxReport(SetupboxReportInput input);

	public AreaWiseReportWrap areaWiseReport(SetupboxReportInput input);
	
}
