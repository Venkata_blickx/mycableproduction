package com.blickx.dao;

import java.math.BigDecimal;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import org.apache.log4j.Logger;
import org.hibernate.HibernateException;
import org.hibernate.Query;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.Transaction;
import org.hibernate.exception.ConstraintViolationException;
import org.hibernate.exception.DataException;
import org.hibernate.transform.Transformers;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;

import com.blickx.domain.AreaEmployeeMapEntity;
import com.blickx.domain.DeviceEntity;
import com.blickx.domain.Employee;
import com.blickx.domain.SelectedEmployee;
import com.blickx.exceptions.ContactNumberDupliactedException;
import com.blickx.exceptions.DeviceAlreadyAssignedException;
import com.blickx.exceptions.ErrorCodes;
import com.blickx.id.generator.EmployeeIdGenerator;
import com.blickx.searchEntities.SearchEmployee;
import com.blickx.to.EmployeAddOrUpdateTo;
import com.blickx.to.GetEmployeeCollectionTo;
import com.blickx.to.GetEmployeeNamesWithIds;
import com.blickx.utill.CommonUtils;

public class EmployeeDaoImpl implements EmployeeDao {

	@Autowired
	SessionFactory sfactory;
	
	private Logger logger = Logger.getLogger(EmployeeDaoImpl.class);
	
	@Autowired
	EmployeeIdGenerator employeeIdGenerator;
	
	public String addEmployee(Employee emp, List<AreaEmployeeMapEntity> areaMapEntities){
		
		int res = 0;
		Session session=sfactory.openSession();
		Transaction tx = null;
		
		String employee_id = employeeIdGenerator.getNextSid(emp.getClient_id());
		
		try{
			tx = session.beginTransaction();
			
			isContactNumberExistsForAddEmployee(emp.getClient_id(), session, emp);
			
				if(CommonUtils.exists(emp.getAssign_device())){
					isDeviceAlreadyAssignedForAddEmployee(session, emp);
					@SuppressWarnings("unchecked")
					List<DeviceEntity> result = (List<DeviceEntity>) session.createSQLQuery("select * from device d where d.device_id ="+emp.getDevice_id() +"")
						.setResultTransformer(Transformers.aliasToBean(DeviceEntity.class))
						.list();
					DeviceEntity device = result.get(0);
					device.setEmployee_id(employee_id);
					device.setEmployee_name(emp.getEmployee_name());
					session.update(device);
					emp.setDevice_id(device.getDevice_id());
					
					/*session.createSQLQuery("insert into employee_device(employee_id,device_id,effective_from) values(?,?,?)")
						.setString(0, employee_id)
						.setInteger(1, device.getDevice_id())
						.setDate(2, new Date())
						.executeUpdate();*/
					
					
				}
			
			emp.setEmployee_id(employee_id);
			emp.setStatus(true);
			Object obj=session.save(emp);
			
			for(AreaEmployeeMapEntity areaEmpMap: areaMapEntities){
				areaEmpMap.setEmployeeId(employee_id);
				session.save(areaEmpMap);
			}
			
			
			if(obj!=null)res=1;
			tx.commit();
		} catch (ContactNumberDupliactedException e) {
			if (tx != null)tx.rollback();
			logger.error(e);
			return e.toString();
		}  catch (ConstraintViolationException e) {
			if (tx != null)tx.rollback();
			logger.error(e);
			return "MCI1001 Internal Server Error: Please contact your site adminstrator!";
		} catch (DataException e) {
			if (tx != null)tx.rollback();
			logger.error(e);
			return "MCI1002 Internal Server Error: Please contact your site adminstrator!";
		}catch (HibernateException e) {
			if (tx != null)tx.rollback();
			logger.error(e);
			return "MCI1000 Internal Server Error: Please contact your site adminstrator!";
		} catch (Exception e) {
			if (tx != null)tx.rollback();
			logger.error(e);
			return "MCI100 Internal Server Error: Please contact your site adminstrator!";
		}finally{
			if(session != null)session.close();
		}
		return "1";
		
	}
	
	private void isContactNumberExistsForAddEmployee(String client_id, Session session, Employee employee)
			throws ContactNumberDupliactedException {
		if(session.createSQLQuery("select * from employee where contact_number = '" + employee.getContact_number() + "' and client_id = '" + client_id + "'").list().size()>0)throw new ContactNumberDupliactedException(ErrorCodes.getContactNumberDuplicateMsg(employee.getContact_number()));
	}
	
	private void isContactNumberExistsForUpdateEmployee(String client_id, Session session, Employee employee)
			throws ContactNumberDupliactedException {
		if (session.createSQLQuery("select * from employee where contact_number = '"
								+ employee.getContact_number()
								+ "' and client_id = '"
								+ client_id
								+ "' and employee_id not in('"
								+ employee.getEmployee_id() + "')").list()
				.size() > 0)
			throw new ContactNumberDupliactedException(ErrorCodes.getContactNumberDuplicateMsg(employee.getContact_number()));
	}
	
	private void isDeviceAlreadyAssignedForUpdateEmployee(Session session, Employee employee)
			throws DeviceAlreadyAssignedException {
		if(CommonUtils.exists(employee.getAssign_device())){
		if (session.createSQLQuery("select * from employee where assign_device = '"
								+ employee.getAssign_device()
								+ "' and client_id = '"
								+ employee.getClient_id()
								+ "' and employee_id not in('"
								+ employee.getEmployee_id() + "')").list()
				.size() > 0)
			throw new DeviceAlreadyAssignedException(ErrorCodes.getDeviceAlreadyExists(employee.getAssign_device()));
		}
	}
	
	private void isDeviceAlreadyAssignedForAddEmployee(Session session, Employee employee)
			throws DeviceAlreadyAssignedException {
		if (session.createSQLQuery("select * from employee where assign_device = '"
								+ employee.getAssign_device()
								+ "' and client_id = '"
								+ employee.getClient_id()
								+ "' and employee_id in('"
								+ employee.getEmployee_id() + "')").list()
				.size() > 0)
			throw new DeviceAlreadyAssignedException(ErrorCodes.getDeviceAlreadyExists(employee.getAssign_device()));
	}
	
	@Override
	public String updateEmployee(Employee employeeTo, EmployeAddOrUpdateTo forAreaCodes) {
		
		Session session = sfactory.openSession();
		Transaction tx = null;
		try{
		tx = session.beginTransaction();
		
		isContactNumberExistsForUpdateEmployee(employeeTo.getClient_id(), session, employeeTo);
		isDeviceAlreadyAssignedForUpdateEmployee(session, employeeTo);
			/*if (employee.getDevice_id() > 0) {
				if (CommonUtils.exists(employee.getAssign_device())) {

					String device_name = (String) session.createSQLQuery("select device_name from device d where d.device_id=\'"
									+ employee.getDevice_id() + "\'").uniqueResult();

						if (device_name != employee.getAssign_device()) {
	
							performUpdate(employee, session,true);
	
						}
					employee.setAssign_device("");
					employee.setDevice_id(0);
				}else{
					DeviceEntity device = (DeviceEntity) session.load(DeviceEntity.class, employee.getDevice_id());
					
					device.setEmployee_name("");
					device.setEmployee_id("");
					session.update(device);
				}
					
				}else{
				performUpdate(employee, session,false);
			}*/
		Employee employee = (Employee) session.load(Employee.class, employeeTo.getEmployee_id());
		employee.setAreacode(employeeTo.getAreacode());
		employee.setAssign_device(employeeTo.getAssign_device());
		employee.setCity(employeeTo.getCity());
		employee.setContact_number(employeeTo.getContact_number());
		employee.setDepartment(employeeTo.getDepartment());
		employee.setDevice_id(employeeTo.getDevice_id());
		employee.setDoc_name(employeeTo.getDoc_name());
		employee.setDoc_type(employeeTo.getDoc_type());
		employee.setEmail(employeeTo.getEmail());
		employee.setEmployee_doc(employeeTo.getEmployee_doc());
		employee.setEmployee_name(employeeTo.getEmployee_name());
		employee.setEmployee_photo(employeeTo.getEmployee_photo());
		employee.setEnd_date(employeeTo.getEnd_date());
		employee.setGender(employeeTo.getGender());
		employee.setLandline_number(employeeTo.getLandline_number());
		employee.setLine1(employeeTo.getLine1());
		employee.setLine2(employeeTo.getLine2());
		employee.setPassword(employeeTo.getPassword());
		employee.setPhoto_name(employeeTo.getPhoto_name());
		employee.setPhoto_type(employeeTo.getPhoto_type());
		employee.setPincode(employeeTo.getPincode());
		employee.setState(employeeTo.getState());
		employee.setJoining_date(employeeTo.getJoining_date());
		employee.setStatus(employeeTo.isStatus());
		session.update(employee);
		
		StringBuffer areaIds = new StringBuffer();
		
		List<Integer> areaIDs = new ArrayList<Integer>();
		System.out.println("*****" + forAreaCodes.getAreaCodes());
		if(forAreaCodes.getAreaCodes().size() >0  ){
			System.out.println("*****" + forAreaCodes.getAreaCodes());
		for(Integer selectedAreaCode: forAreaCodes.getAreaCodes()){
			
			//AreaEmployeeMapEntity areaEmpMap = new AreaEmployeeMapEntity();
			System.out.println("*****");
			List<AreaEmployeeMapEntity> areaEmpMaps =  session
					.createQuery("from AreaEmployeeMapEntity aem where aem.employeeId = ? and aem.areaId = ? and aem.endDate is null")
					.setString(0, employee.getEmployee_id())
					.setInteger(1, selectedAreaCode)
					.list();
			if(areaEmpMaps.size() == 0) {
				AreaEmployeeMapEntity areaEmpMap = new AreaEmployeeMapEntity();
				areaEmpMap.setAreaId(selectedAreaCode);
				areaEmpMap.setEmployeeId(employee.getEmployee_id());
				areaEmpMap.setStartDate(new Date());
				areaEmpMap.setCreatedTimeStamp(new Date());
				areaEmpMap.setLastUpdatedTimestamp(new Date());
				
				session.save(areaEmpMap);
			} 
			areaIDs.add(new Integer(selectedAreaCode));
			areaIds.append(selectedAreaCode);
			areaIds.append(",");
		}
		String ids = "";
		if(areaIds.toString().trim().length() > 0 ){
			ids = ids + areaIds.toString().substring(0, areaIds.length() - 1);
		} 
		
		
		session
				.createSQLQuery("update area_employee_map aem set aem.end_date = current_timestamp where aem.employee_id = '"+employee.getEmployee_id()+"' and aem.area_id not in ("+ ids +") and aem.end_date is null")
				//.setParameter("date", new Date())
				//.setParameter("employee_id", employee.getEmployee_id())
				//.setParameter("areaIds", "(" + areaIds.toString().substring(0, areaIds.length() - 1) +")")
				//.setParameter("areaIds", arg1)
				.executeUpdate();
		} else {
			session
			.createSQLQuery("update area_employee_map aem set aem.end_date = current_timestamp where aem.employee_id = '"+employee.getEmployee_id()+"' and aem.end_date is null")
			.executeUpdate();
		}
		
		/*for(Integer removedAreaCode: forAreaCodes.getRemovedAreaCodes()){
			AreaEmployeeMapEntity areaEmpMap = (AreaEmployeeMapEntity) session
					.createQuery("from AreaEmployeeMapEntity aem where aem.employeeId = ? and aem.areaId = ? order by aem.id desc")
					.setString(0, employee.getEmployee_id())
					.setInteger(1, removedAreaCode)
					.list().get(0);
			areaEmpMap.setEndDate(new Date());
			areaEmpMap.setLastUpdatedTimestamp(new Date());
			session.update(areaEmpMap);
		}*/
		
		tx.commit();
		} catch (ContactNumberDupliactedException e) {
			if (tx != null)tx.rollback();
			logger.error(e);
			return e.toString();
		}  catch (DeviceAlreadyAssignedException e) {
			if (tx != null)tx.rollback();
			logger.error(e);
			return e.toString();
		}  catch (ConstraintViolationException e) {
			if (tx != null)tx.rollback();
			logger.error(e);
			return "MCI1001 Internal Server Error: Please contact your site adminstrator!";
		} catch (DataException e) {
			if (tx != null)tx.rollback();
			logger.error(e);
			return "MCI1002 Internal Server Error: Please contact your site adminstrator!";
		} catch (HibernateException e) {
			if (tx != null)tx.rollback();
			logger.error(e);
			return "MCI1000 Internal Server Error: Please contact your site adminstrator!";
		} catch (Exception e) {
			if (tx != null)tx.rollback();
			logger.error(e);
			return "MCI100 Internal Server Error: Please contact your site adminstrator!";
		} finally{
			if(session != null)session.close();
		}
		return "1";
		
	}


	/*@SuppressWarnings("unchecked")
	private void performUpdate(Employee employee, Session session,boolean isUpdateMapDetails) {
			if(CommonUtils.exists(employee.getAssign_device())){
				List<DeviceEntity> result = (List<DeviceEntity>) session.createSQLQuery("select * from device d where d.device_name=\'"+employee.getAssign_device()+"\'")
						.setResultTransformer(Transformers.aliasToBean(DeviceEntity.class))
						.list();
				DeviceEntity device = result.get(0);
				device.setEmployee_id(employee.getEmployee_id());
				device.setEmployee_name(employee.getEmployee_name());
				session.update(device);
				
				employee.setDevice_id(device.getDevice_id());
					if(isUpdateMapDetails){
					EmployeeDeviceMap mapDetails = (EmployeeDeviceMap) session.createSQLQuery("select * from employee_device where "+
							"employee_id='"+employee.getEmployee_id()+"' and (date(effective_from) < date(now())) and (date(now()) < '9999-12-30') ;")
							.setResultTransformer(Transformers.aliasToBean(EmployeeDeviceMap.class))
							.list().get(0);
					
					mapDetails.setEffective_to(new Date());
					session.update(mapDetails);
					}
			session.createSQLQuery("insert into employee_device(employee_id,device_id,effective_from) values(?,?,?)")
				.setString(0, employee.getEmployee_id())
				.setInteger(1, device.getDevice_id())
				.setDate(2, new Date())
				.executeUpdate();
			
			}
	}*/
	


	@SuppressWarnings("unchecked")
	@Override
	public EmployeAddOrUpdateTo getEmployeeById(String employee_id) {
		EmployeAddOrUpdateTo employeeTo = new EmployeAddOrUpdateTo();
		Employee employee = null;
		Session session = null;
		Transaction tx = null;
		try{
			session = sfactory.openSession();
			tx = session.beginTransaction();
			logger.info(employee_id);
			String hql = "from Employee e where e.employee_id=?";
			Query q = session.createQuery(hql);
			q.setParameter(0, employee_id);
			List<Employee> list=q.list();
			employee = (Employee) list.get(0);
				
				List<AreaEmployeeMapEntity> areaEmpMaps = session.createQuery("from AreaEmployeeMapEntity aem where aem.employeeId = ? and aem.endDate is null").setString(0, employee_id).list();
				String areaCode = (String) session.createSQLQuery("select group_concat(distinct a.area_code) from area a, area_employee_map e "
						+"where a.area_id = e. area_id and e.employee_id = ? "
						+"and e.end_date is null ").setString(0, employee.getEmployee_id()).uniqueResult();
			
			BeanUtils.copyProperties(employee, employeeTo);
			employeeTo.setAreaEmployeeMaps(areaEmpMaps);
			employeeTo.setAreacode(areaCode);
			
			/*SelectedEmployee selectedEmployee = new SelectedEmployee();
			BeanUtils.copyProperties(employee, selectedEmployee);
			session.save(selectedEmployee);*/
			logger.info(employee);
			tx.commit();
		}catch(Exception e){
			logger.error(e);
			return employeeTo;
		}finally{
			if(session != null)session.close();
		}
		// TO DO
		// Convert blob byte array into string here. 
		// Link: http://studyjava.org/client-side-scripts/convert-string-to-blob-and-blob-to-string
		// Another useful link: http://snehaprashant.blogspot.in/2008/08/how-to-store-and-retrieve-blob-object.html
		logger.info("Dao get employee: " + employee);
		return employeeTo;
		
	}

	@SuppressWarnings("unchecked")
	@Override
	public List<Employee> getEmployeeByIdAndName(String employee_id, String employee_name) {
		List<Employee> list = null;
		
		try{
			Session session=sfactory.openSession();
			
			String hql="from Employee e where e.employee_id=?";
			Query q=session.createQuery(hql);
			q=q.setParameter(0, employee_id);
			list = q.list();
			Employee emp=(Employee) list.get(0);
			if(emp.getEmployee_name().equalsIgnoreCase(employee_name)){
				return list;
			}
			else {
				list=null;
				return list;
			}
			
		}catch(Exception e){
			logger.error(e);
			list=null;
			return list;
		}
	}


	@SuppressWarnings("unchecked")
	@Override
	public List<Employee> getEmpByName(String employee_name) {
		
		List<Employee> list = null;
		Session session = null;
		try{
		session=sfactory.openSession();
		String hql = "from Employee e where e.employee_name=?";
		Query q = session.createQuery(hql);
		q=q.setParameter(0, employee_name);
		list = q.list();
		
		}catch(Exception e){
			return list;
		}finally{
			if(session != null)session.close();
		}
		return list;
	
	}

	@Override
	public String activateStatus(String employee_id) {
	
		String res="Done";
		Session session = null;
		try{
			session=sfactory.openSession();
			Transaction tx = session.beginTransaction(); 
			Employee emp=(Employee) session.load(Employee.class, employee_id);
			emp.setStatus(true);
			session.update(emp);
			tx.commit();
			}catch(Exception e){
				logger.error(e);
				return "Id not Found";
			}finally{
				if(session != null)session.close();
			}
		return res;
		
	}
	
	@Override
	public String deactivateStatus(String employee_id) {
		
		String res="Done";
		Session session = null;
		try{
			session=sfactory.openSession();
			Transaction tx = session.beginTransaction(); 
			Employee emp=(Employee) session.load(Employee.class, employee_id);
			
			if(CommonUtils.exists(emp.getAssign_device()) & !emp.getAssign_device().equalsIgnoreCase("Select the device")){
				
				DeviceEntity device = (DeviceEntity) session.load(DeviceEntity.class, emp.getDevice_id());
				emp.setAssign_device("");
				emp.setDevice_id(0);
				
				device.setEmployee_id(null);
				device.setEmployee_name(null);
				
				session.update(device);
			
			}
			emp.setStatus(false);
			session.update(emp);
			
			tx.commit();
			}catch(Exception e){
				logger.error(e);
				return "Id Not Found";
			}finally{
				if(session != null)session.close();
			}
		return res;

	}

	@SuppressWarnings("unchecked")
	@Override
	public List<Employee> searchEmployee(SearchEmployee searchEmployee) {
		
		StringBuffer attachQuery= new StringBuffer("from Employee e ");
		attachQuery.append("where e.client_id='"+searchEmployee.getClient_id()+"' And ");
		boolean isSearchParametersExist = true;
		if(CommonUtils.exists(searchEmployee.getEmployee_id()) || CommonUtils.exists(searchEmployee.getEmployee_name())
				||CommonUtils.exists(searchEmployee.getContact_number()) || CommonUtils.exists(searchEmployee.getAreacode())){
			//attachQuery.append("where ");
			isSearchParametersExist =true;
			
		}
		
		if(CommonUtils.exists(searchEmployee.getEmployee_id())){
			
			attachQuery.append("e.employee_id like \'%");
			attachQuery.append(searchEmployee.getEmployee_id());
			attachQuery.append("%\'");
			attachQuery.append(" AND ");
		}
		if(CommonUtils.exists(searchEmployee.getEmployee_name())){
			
			
			attachQuery.append("e.employee_name like \'%");
			attachQuery.append(searchEmployee.getEmployee_name());
			attachQuery.append("%\'");
			attachQuery.append(" AND ");
		}
		
		if(CommonUtils.exists(searchEmployee.getContact_number())){
			
			attachQuery.append("e.contact_number like \'%");
			attachQuery.append(searchEmployee.getContact_number());
			attachQuery.append("%\'");
			attachQuery.append(" AND ");
		}

		if(CommonUtils.exists(searchEmployee.getAreacode())){
			
			attachQuery.append("e.areacode like \'%");
			attachQuery.append(searchEmployee.getAreacode());
			attachQuery.append("%\'");
			attachQuery.append(" AND");
		}
		String searchQuery = attachQuery.toString();
		
		if(isSearchParametersExist){
			searchQuery = attachQuery.toString().substring(0,attachQuery.length()-4);
		}
		 
		logger.info(searchQuery);
		Session session = null;
		Query query = null;
		//Transaction tx = null;
		List<Employee> list = null;
		try{
		session = sfactory.openSession();
		//tx = session.beginTransaction();
		query = session.createQuery(searchQuery);
		list = query.list();
		for(int i = 0; i < list.size(); i++){
			String areaCode = (String) session.createSQLQuery("select group_concat(distinct a.area_code) from area a, area_employee_map e "
			+"where a.area_id = e. area_id and e.employee_id = ? "
			+"and e.end_date is null ").setString(0, list.get(i).getEmployee_id()).uniqueResult();
			
			list.get(i).setAreacode(areaCode);
		}
		//tx.commit();
		}catch(HibernateException e){
			e.printStackTrace();
			logger.error(e);
		}finally{
			if(session != null)session.close();
		}
		
		return list;
	}


	@Override
	public List<GetEmployeeCollectionTo> getEmployeeCollection(
			String employee_id) {
		List<GetEmployeeCollectionTo> resultset = new ArrayList<GetEmployeeCollectionTo>();
		
		Session session = sfactory.openSession();
		Transaction tx = null;
		try {
			tx = session.beginTransaction();
			
			logger.info("Calling DashboardDailyCollectionSummary");
			
			@SuppressWarnings({ "rawtypes", "unchecked" })
			List<GetEmployeeCollectionTo> resultWithAliasedBean  = (List) session.createSQLQuery(
			"CALL GetEmployeeBillReportAndroidTest(:ime_id)")
			.setString("ime_id", employee_id)
			.setResultTransformer(Transformers.aliasToBean(GetEmployeeCollectionTo.class))
			.list();
			/*List<GetEmployeeCollectionTo> res = new ArrayList<GetEmployeeCollectionTo>();
			for(GetEmployeeCollectionTo test:resultWithAliasedBean){
				if(test.getCollection() == null)test.setCollection(new BigDecimal(0));
				res.add(test);
			}*/
			
			resultset = resultWithAliasedBean;
			
			tx.commit();
		} catch (HibernateException e) {
			if (tx != null)
				tx.rollback();
			e.printStackTrace();
			logger.error("Exception! " + e);
		} finally {
			session.close();
		}
		return resultset;
	}


	@Override
	public List<String> getEmployeeNames() {
		List<String> resultset = new ArrayList<String>();
		
		Session session = sfactory.openSession();
		Transaction tx = null;
		try {
			tx = session.beginTransaction();
			
			
			@SuppressWarnings({ "rawtypes", "unchecked" })
			List<String> resultWithAliasedBean  = (List) session.createSQLQuery(
			"select employee_name from employee where employee_id not in (select employee_id from device)")
			.list();
			
			resultset = resultWithAliasedBean;
			
			tx.commit();
		} catch (HibernateException e) {
			if (tx != null)
				tx.rollback();
			e.printStackTrace();
			logger.error("Exception! " + e);
		} finally {
			session.close();
		}
		return resultset;
	}
	
	@Override
	public List<String> getEmployeeNamesToAssignComplaints(String client_id) {
		List<String> resultset = new ArrayList<String>();
		
		Session session = sfactory.openSession();
		Transaction tx = null;
		try {
			tx = session.beginTransaction();
			
			
			@SuppressWarnings("unchecked")
			List<Employee> employees = session.createQuery("from Employee e where client_id=? ")
					.setString(0, client_id)
					.list();
			
			for(Employee emp:employees){
				resultset.add(emp.getEmployee_name());
			}
			
			tx.commit();
		} catch (HibernateException e) {
			if (tx != null)
				tx.rollback();
			e.printStackTrace();
			logger.error("Exception! " + e);
		} finally {
			session.close();
		}
		return resultset;
	}


	@SuppressWarnings("unchecked")
	@Override
	public List<Employee> getInactiveEmployees(String client_id) {
		List<Employee> resultset = new ArrayList<Employee>();
		
		Session session = sfactory.openSession();
		Transaction tx = null;
		try {
			tx = session.beginTransaction();
			
			resultset = session.createQuery("from Employee e where status = ? AND e.client_id = ?")
					.setBoolean(0, false)
					.setString(1, client_id)
					.list();
						
			tx.commit();
		}catch (HibernateException e) {
			if (tx != null)
				tx.rollback();
			logger.error("Exception! " + e);
		}finally {
			session.close();
		}
		return resultset;	
	}


	@Override
	public String changePassword(String employee_id, String password) {
		
		Session session = sfactory.openSession();
		Transaction tx = null;
		try{
			tx = session.beginTransaction();
			
			Employee employee = (Employee) session.load(Employee.class, employee_id);
				employee.setPassword(password);
				
			session.update(employee);
			
			tx.commit();
		}catch(HibernateException e){
			if(tx != null)tx.rollback();
			logger.error(e);
			return "fail";
		}catch(Exception e){
			if(tx != null)tx.rollback();
			logger.equals(e);
			return "fail";
		}
		return "Success";
	}


	@SuppressWarnings("unchecked")
	@Override
	public List<GetEmployeeNamesWithIds> getEmployeeNamesWithIds(String client_id) {
		List<GetEmployeeNamesWithIds> resultset = new ArrayList<GetEmployeeNamesWithIds>();
		
		Session session = sfactory.openSession();
		Transaction tx = null;
		try {
			tx = session.beginTransaction();
			
			resultset = session.createSQLQuery("select distinct e.employee_id, e.employee_name from employee e where client_id = ? ")
					.setString(0, client_id)
					.setResultTransformer(Transformers.aliasToBean(GetEmployeeNamesWithIds.class))
					.list();
						
			tx.commit();
		}catch (HibernateException e) {
			if (tx != null)
				tx.rollback();
			logger.error("Exception! " + e);
		}finally {
			session.close();
		}
		return resultset;	
	}


	@Override
	public int uidUpdate(String employee_id, String employee_uid) {
		
		Session session = sfactory.openSession();
		Transaction tx = null;
		try{
			
			tx = session.beginTransaction();
			Employee employee = (Employee) session.load(Employee.class, employee_id);
			employee.setEmployee_uid(employee_uid);
			session.update(employee);
			
			tx.commit();
		}catch(HibernateException e){
			if(tx != null)tx.rollback();
			logger.error(e);
			return 0;
		}finally{
			session.close();
		}
		
		return 1;
	}


	@Override
	public String getEmployee_id(String uid_number) {
		// TODO Auto-generated method stub
		Session session = sfactory.openSession();
		Transaction tx = null;
		String employee_id = "";
		
		try{
			tx = session.beginTransaction();
			logger.info("getEmployee_id () by uid");
			employee_id = (String) session.createQuery("select employee_id from Employee e where e.employee_uid = ?")
					.setString(0, uid_number).uniqueResult();
			
			tx.commit();
		}catch(HibernateException e){
			if(tx != null)tx.rollback();
			logger.error(e);
			return employee_id;
		}finally{
			session.close();
		}
		return employee_id;
	}


	@Override
	public Employee Unique_Employee_Details(String client_id) {
		SelectedEmployee selectedEmployee = null;
		Employee employee = null;
		Session session = null;
		Transaction tx = null;
		try{
			session = sfactory.openSession();
			tx = session.beginTransaction();
			String hql = "from SelectedEmployee e where e.client_id=?";
			Query q = session.createQuery(hql);
			q.setParameter(0, client_id);
			List<SelectedEmployee> list=q.list();
			selectedEmployee =  list.get(0);
			employee = new Employee();
			BeanUtils.copyProperties(selectedEmployee, employee);
			session.delete(selectedEmployee);
			logger.info(employee);
			tx.commit();
		}catch(Exception e){
			logger.error(e);
			return employee;
		}finally{
			if(session != null)session.close();
		}
		// TO DO
		// Convert blob byte array into string here. 
		// Link: http://studyjava.org/client-side-scripts/convert-string-to-blob-and-blob-to-string
		// Another useful link: http://snehaprashant.blogspot.in/2008/08/how-to-store-and-retrieve-blob-object.html
		logger.info("Dao get employee: " + employee);
		return employee;
		
	}


}
