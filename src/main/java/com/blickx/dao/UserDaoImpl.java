package com.blickx.dao;



import java.util.List;

import org.apache.log4j.Logger;
import org.hibernate.Query;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.Transaction;
import org.hibernate.transform.Transformers;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.blickx.domain.DeviceEntity;
import com.blickx.domain.Employee;
import com.blickx.utill.CommonUtils;

@Component
public class UserDaoImpl implements UserDao {

	@Autowired
	SessionFactory sfactory;
	
	
	
	public UserDaoImpl() {
		super();
		// TODO Auto-generated constructor stub
	}


	public UserDaoImpl(SessionFactory sfactory) {
		super();
		this.sfactory = sfactory;
	}

	@Autowired
	EmployeeDao edao;
	
	private Logger logger = Logger.getLogger(UserDaoImpl.class);
	
	
	@SuppressWarnings({ "rawtypes", "unchecked" })
	@Override
	public String checkUser(String username,String password, String ime_number) {
		
		String res=null;
		
		Session session=sfactory.openSession();
		Transaction tx = null;
		
		boolean isActive = false;
		try{
			tx = session.beginTransaction();
			
			isActive = (boolean) session.createQuery("select status from Employee e where e.employee_id=?").setString(0, edao.getEmployee_id(username)).uniqueResult();
			
				if(isActive){  
				String hql="from Employee e where e.employee_id=? and e.password=?";
				Query q=session.createQuery(hql);
				q.setParameter(0, edao.getEmployee_id(username));
				q.setParameter(1, password);
				List list=q.list();
				  
					  if(list != null && list.size()>0){
						  
						 Employee emp = (Employee) list.get(0);
						 List<DeviceEntity> devices =  session.createQuery("from DeviceEntity de where de.employee_id = ? ")
								 .setString(0, edao.getEmployee_id(username))
						  				.list();
						  DeviceEntity device = devices.get(0);
						  
						  device.setLogin_status(true);
						  session.update(device);
						  res = emp.getClient_id();
						  if(!device.getIme_number().equalsIgnoreCase(ime_number) )res = "Employeecard and Device are Mismatched";
					  }
					  else{
						  res = "fail";
					  }
				}else{
					res = "inactive";
				}
				
		  tx.commit();
		}catch(Exception e){
			if(tx != null)tx.rollback();
			logger.error(e);
		}finally{
			session.close();
		}
		
			 return res;
		
	}


	@SuppressWarnings("unchecked")
	@Override
	public String logout(String username, String ime_number) {
		// TODO Auto-generated method stub
		
		Session session = sfactory.openSession();
		Transaction tx = null;
		String res = "";
		try{
			tx = session.beginTransaction();
			if(CommonUtils.exists(username)){
			
					List<DeviceEntity> devices =  session.createSQLQuery("select * from device de where de.employee_id = '"+edao.getEmployee_id(username)+"';")
			  				.setResultTransformer(Transformers.aliasToBean(DeviceEntity.class)).list();
			  DeviceEntity device = devices.get(0);
			  device.setLogin_status(false);
			  session.update(device);
			  res = "success";
			}else if(CommonUtils.exists(ime_number)){
				
				List<DeviceEntity> devices =  session.createSQLQuery("select * from device de where de.employee_id = '"+getEmployeeIDusingIme(ime_number)+"';")
		  				.setResultTransformer(Transformers.aliasToBean(DeviceEntity.class)).list();
					  DeviceEntity device = devices.get(0);
					  device.setLogin_status(false);
					  session.update(device);
					  res = "success";
			}
			tx.commit();
		}catch(Exception e){
			if(tx != null)tx.rollback();
			logger.error(e);
			return "fail";
		}finally{
			session.close();
		}
		return res;
	}


	@Override
	@SuppressWarnings("unchecked")
	public String loginWithImeAndPassword(String ime_number, String password) {
		String res=null;
		
		Session session=sfactory.openSession();
		Transaction tx = null;
				
		boolean isActive = false;
		try{
			tx = session.beginTransaction();
			
			String employee_id = getEmployeeIDusingIme(session, ime_number);
			if(CommonUtils.exists(employee_id)){
			logger.info("*********************" + employee_id);
			isActive = (boolean) session.createQuery("select status from Employee e where e.employee_id=?").setString(0, employee_id).uniqueResult();

			if(isActive){  
				String hql="from Employee e where e.employee_id=? and e.password=?";
				Query q=session.createQuery(hql);
				q.setParameter(0, employee_id);
				q.setParameter(1, password);
				List list=q.list();
				  
					  if(list != null && list.size()>0){
						  
						Employee emp = (Employee) list.get(0);
						@SuppressWarnings("unchecked")
						List<DeviceEntity> devices = session
								.createQuery(
										"from DeviceEntity de where de.employee_id = ? ")
								.setString(0, employee_id).list();
						DeviceEntity device = devices.get(0);
						if(device.isStatus()){
						device.setLogin_status(true);
						session.update(device);
						res = emp.getClient_id();
						
						}else{
							res = "Device is Inactive";
						}
					  }
					  else{
						  logger.info("Invalid Password");
						  res = "Invalid Password";
					  }
				}else{
					 logger.info("Employee is Inactive");
					res = "Employee is Inactive";
				}
			}else{
				res = "Device Not Found/Invalid IMEI Number";
			}
		  tx.commit();
		}catch(Exception e){
			if(tx != null)tx.rollback();
			logger.error(e);
		}finally{
			session.close();
		}
		return res;
	}


	public String getEmployeeIDusingIme(String ime_number) {
		
		
		Session session = sfactory.openSession();
		Transaction tx = null;
		String employee_id = "";
		try{
			
			tx = session.beginTransaction();
			logger.info("getEmployee_id");
			employee_id = getEmployeeIDusingIme(session, ime_number);
			tx.commit();
		}catch(Exception e){
			if(tx != null)tx.rollback();
			logger.error(e);
			return "fail";
		}finally{
			session.close();
		}
		
		return employee_id;
	}

	public String getEmployeeIDusingIme(Session session,String ime_number) {
		
		
		String employee_id = "";
		try{
			logger.info("getEmployeeIDusingIme() - Input: "+ime_number);
			logger.info("getEmployee_id ");
			employee_id = (String) session.createSQLQuery("select e.employee_id from employee e join device d where e.employee_id= d.employee_id and ime_number = ?").setString(0, ime_number).uniqueResult();
			logger.info("getEmployeeId Found is: " + employee_id);
		}catch(Exception e){
			logger.error(e);
			return "fail";
		}
		
		return employee_id;
	}
	
public boolean basicAuthentication(String username, String password){
		
		Session session = sfactory.openSession();
		try {
			System.out.println("Authentication is happening");
			String SQL_QUERY = " from Authentication o where o.username=? and o.password=?";
			Query query = session.createQuery(SQL_QUERY);
			query.setParameter(0, username);
			query.setParameter(1, password);
			List list = query.list();
			
			if(list.size() > 0){
				return true;
			}
		}catch(Exception e){
			e.printStackTrace();
			return false;
		}
		
		return false;
	}


public boolean basicAuthenticationAuthorisedOrNot(String username,
		String client_id) {
	Session session = sfactory.openSession();
	try {
		System.out.println("yes authoriztion");
		System.out.println("Authentication is happening");
		String SQL_QUERY = " from Authentication o where o.username=? and o.client_id=?";
		Query query = session.createQuery(SQL_QUERY);
		query.setParameter(0, username);
		query.setParameter(1, client_id);
		List list = query.list();
		
		if(list.size() > 0){
			return true;
		}
	}catch(Exception e){
		e.printStackTrace();
		return false;
	}
	
	return false;
}
	
}
