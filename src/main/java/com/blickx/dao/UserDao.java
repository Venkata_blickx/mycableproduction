package com.blickx.dao;

import org.hibernate.Session;


public interface UserDao {

	public String checkUser(String username,String password, String ime_number);

	public String logout(String username, String ime_number);

	public String loginWithImeAndPassword(String ime_number, String password);
	
	public String getEmployeeIDusingIme(String ime_number);
	
	public String getEmployeeIDusingIme(Session session, String ime_number);
}
