package com.blickx.dao;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;

import org.apache.log4j.Logger;
import org.hibernate.HibernateException;
import org.hibernate.Query;
import org.hibernate.SQLQuery;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.Transaction;
import org.hibernate.exception.ConstraintViolationException;
import org.hibernate.transform.Transformers;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;

import com.blickx.channels.categories.ChannelSeraliser;
import com.blickx.domain.Channels;
import com.blickx.domain.PackageEntity;
import com.blickx.to.ChannelDisplay;
import com.blickx.to.ChannelNamesWithCost;
import com.blickx.to.ChannelsTOO;
import com.blickx.to.ChannelsTo;
import com.blickx.to.GetExpireAddonAlertTo;
import com.blickx.to.GetTheAddonsToUpdate;
import com.blickx.to.PackageTO;
import com.blickx.to.SearchPackageTO;
import com.blickx.to.input.CustomerFetchCredetials;
import com.blickx.utill.CommonUtils;

public class PackagDaoImpl implements PackageDao{

	@Autowired
	SessionFactory sfactory;
	
	private Logger logger = Logger.getLogger(PackagDaoImpl.class);
	
	
	@SuppressWarnings({ "rawtypes", "unchecked" })
	@Override
	public List<String> getPackages() {
		Session session=sfactory.openSession();
		String hql="select package_name from PackageEntity";
		Query q=session.createQuery(hql);
		List list=q.list();
		session.close();
		return list;
	}

	

	public int addPackage(PackageTO packageTO) {
		
		Session session = sfactory.openSession();
		Transaction tx = null;
		try{
			  
			tx = session.beginTransaction();
			/*List<Channels> chlist=new ArrayList<Channels>();
			if(CommonUtils.exists(packageTO.getChannelList())){
				String channels[]=packageTO.getChannelList().split(",");
				for(String ch:channels){
					Query query=session.createQuery("from Channels c where c.channel_name in (\'"+ch+"\')");
					chlist.add((Channels) query.list().get(0));
				}
				logger.info(chlist.hashCode());
				logger.info("Add Package after getting channels");
			}*/
			PackageEntity packageEntitiy=new PackageEntity();
			//packageEntitiy.setPackage_id(packageTO.getPackage_id());
			packageEntitiy.setPackage_name(packageTO.getPackage_name());
			packageEntitiy.setPackage_amount(packageTO.getPackage_amount());
			packageEntitiy.setStatus(packageTO.isStatus());
			packageEntitiy.setEffective_date(packageTO.getEffective_date());
			packageEntitiy.setClient_id(packageTO.getClient_id());
			packageEntitiy.setStatus(true);
			session.save(packageEntitiy);
			//packageEntitiy.setChannels(chlist);
			logger.info("Add Package after Adding channels");
			tx.commit();  
		}catch (ConstraintViolationException e) {
			if (tx != null)tx.rollback();
			logger.error(e);
			return 101;
		}catch(Exception e){
			if(tx != null)tx.rollback();
			e.printStackTrace();
			logger.error(e);
			return 0;
		}finally{
			if(session != null)session.close();
		}
		return 1;
		
	}

	@Override
	public int updatePackage(PackageTO packages) {
		Session session=sfactory.openSession();
		Transaction tx=null;
		try{
			tx=session.beginTransaction();
			/*List<Channels> chlist=new ArrayList<Channels>();
			if(CommonUtils.exists(packages.getChannelList())){
				String channels[]=packages.getChannelList().split(",");
				for(String ch:channels){
					Query query=session.createQuery("from Channels c where c.channel_name = '"+ch+"'");
					chlist.add((Channels) query.list().get(0));
				}
				logger.info(chlist.hashCode());
			}*/
			PackageEntity pack = (PackageEntity) session.load(PackageEntity.class, packages.getPackage_id());
			pack.setPackage_amount(packages.getPackage_amount());
			//pack.setPackage_name(packages.getPackage_name());
			pack.setStatus(packages.isStatus());
			BeanUtils.copyProperties(packages, pack);
			
			//pack.setChannels(chlist);
			session.update(pack);
			tx.commit();
		}catch(Exception e){
			if(tx!=null)tx.rollback();
			logger.error(e);
			return 0;
		}finally{
			session.close();
		}
		return 1;
	}
	
	
	@SuppressWarnings("rawtypes")
	public PackageTO getPackageById(int package_id) {
		
		PackageEntity packages=null;
		PackageTO pto = new  PackageTO();
		Session session=sfactory.openSession();
		try{
			
			String hql="from PackageEntity p where p.package_id=?";
			Query q=session.createQuery(hql);
			q.setParameter(0, package_id);
			List list=q.list();
			packages=(PackageEntity)list.get(0);
			/*//StringBuffer channelNames = new StringBuffer("");
			List<String> list2=new ArrayList<String>();
			for(Channels channel: packages.getChannels() ){
				channelNames.append(channel.getChannel_name());
				channelNames.append(",");
				list2.add(channel.getChannel_name());
			}*/
			
			
			BeanUtils.copyProperties(packages, pto);
			//pto.setChannels(list2);
			
		}catch(Exception e){
			logger.error(e);
			return null;
		}finally{
			session.close();
		}
		return pto;
	}

	@Override
	public String deletePackage(int package_id) {
		try{
			Session session=sfactory.openSession();
			Transaction tx=session.beginTransaction();
			Query query=session.createQuery("from PackageEntity p where p.package_id=?");
			query.setParameter(0, package_id);
			@SuppressWarnings("rawtypes")
			List list=query.list();
			PackageEntity object=(PackageEntity)list.get(0);
			session.delete(object);
			tx.commit();
			session.close();
		
		}catch(Exception e){
			logger.error(e);
			return "0";
		}
		return "1";
	}

	public int addChannel(ChannelsTo channelTo) {
		
		Session session = sfactory.openSession();
		Transaction tx = null;
		try{
			  
			tx = session.beginTransaction(); 
			logger.info(channelTo);
			Channels channel = new Channels();
			BeanUtils.copyProperties(channelTo, channel);
			//if(channelTo.getStatus()==1)
			channel.setStatus("Active");
			logger.info(channel);
			session.save(channel);  
			tx.commit();  
			//Serializable id = session.getIdentifier(channel);  
			//logger.info(id);
			
		}catch (ConstraintViolationException e) {
			if (tx != null)tx.rollback();
			logger.error(e);
			return 101;
		}catch(Exception e){
			logger.error(e);
			return 100;
		}finally{
			session.close();
		}
		return 1;
	}

	@SuppressWarnings("rawtypes")
	public ChannelsTo getChannelById(int channel_id) {
		
		Channels channel = new Channels();
		ChannelsTo cto = new ChannelsTo();
		Session session = null;
		try{
			session = sfactory.openSession();
			String hql="from Channels c where c.channel_id="+channel_id;
			Query q=session.createQuery(hql);
			List list = q.list();
			
			channel=(Channels)list.get(0);
			
			BeanUtils.copyProperties(channel, cto);
			
		}catch(Exception e){
			logger.error(e);
			return null;
		}finally{
			session.close();
		}
		return cto;
	}

	public int updateChannel(Channels channel) {
		
		Session session = sfactory.openSession();
		Transaction tx = null;
		try{
			tx = session.beginTransaction();
			
			Channels loadedChannel = (Channels) session.load(Channels.class, channel.getChannel_id());
			
			loadedChannel.setCategory(channel.getCategory());
			loadedChannel.setChannel_amount(channel.getChannel_amount());
			loadedChannel.setChannel_name(channel.getChannel_name());
			loadedChannel.setChannel_effective_date(channel.getChannel_effective_date());
			
			session.update(loadedChannel);
			tx.commit();
		}catch(Exception e){
			if(tx != null)tx.rollback();
			logger.error(e);
			return 0;
		}finally{
			session.close();
		}
		return 1;
	}

	public String deleteChannel(int channel_id) {
		try{
			Session session=sfactory.openSession();
			Transaction tx=session.beginTransaction();
			Query query=session.createQuery("from Channels c where c.channel_id=?");
			query.setParameter(0, channel_id);
			@SuppressWarnings("rawtypes")
			List list=query.list();
			Channels channel=(Channels)list.get(0);
			session.delete(channel);
			tx.commit();
			session.close();
		
		}catch(Exception e){
			logger.error(e);
			return "Exception";
		}
		return "Deleted Successfully";
	}

	public String activateChannel(int channel_id) {
		String res="Done";
		
		try{
			Session session=sfactory.openSession();
			Transaction tx = session.beginTransaction(); 
			Channels channel=(Channels) session.load(Channels.class, channel_id);
			channel.setStatus("active");
			session.update(channel);
			
			tx.commit();
			session.close();
			}catch(Exception e){
				logger.error(e);
				return "Id Not Found";
			}
		
		return res;
	}
	
	public String deactivateChannel(int channel_id) {
		String res="Done";
		
		try{
			Session session=sfactory.openSession();
			Transaction tx = session.beginTransaction(); 
			Channels channel=(Channels) session.load(Channels.class, channel_id);
			channel.setStatus("deactive");
			session.update(channel);
			
			tx.commit();
			session.close();
			}catch(Exception e){
				logger.error(e);
				return "Id Not Found";
			}
		
		return res;
	}

	public String deactivatePackage(int package_id) {
		String res="Done";
		
		try{
			Session session=sfactory.openSession();
			Transaction tx = session.beginTransaction(); 
			PackageEntity packages=(PackageEntity) session.load(PackageEntity.class, package_id);
			packages.setStatus(false);
			session.update(packages);
			
			tx.commit();
			session.close();
			}catch(Exception e){
				logger.error(e);
				return "Id Not Found";
			}
		
		return res;
	}

	public String activatePackage(int package_id) {
		String res="Done";
		
		try{
			Session session=sfactory.openSession();
			Transaction tx = session.beginTransaction(); 
			PackageEntity packages=(PackageEntity) session.load(PackageEntity.class, package_id);
			packages.setStatus(true);
			session.update(packages);
			
			tx.commit();
			session.close();
			}catch(Exception e){
				logger.error(e);
				return "Id Not Found";
			}
		
		return res;
	}

	@SuppressWarnings("unchecked")
	public List<SearchPackageTO> searchPackages(String client_id) {
		StringBuffer attachQuery = new StringBuffer();
		attachQuery.append("SELECT package_id,package_name,package_amount,status ");
		//attachQuery.append(", (SELECT count(*) FROM package_channel P2 ");
		//attachQuery.append("WHERE P2.package_id = P1.package_id) AS ChannelCount ");
		attachQuery.append("FROM packages P where client_id='"+client_id+"'");
		
		 logger.info(attachQuery);
		 Session session=sfactory.openSession();
		
		return session.createSQLQuery(attachQuery.toString()).addEntity(SearchPackageTO.class).list();
	}

	@SuppressWarnings("unchecked")
	public List<ChannelsTOO> getAllChannels(String client_id) {
		Session session = sfactory.openSession();
		
		logger.info("SearchChannels Getting Channels List");
		List<ChannelsTOO> resultWithAliasedBean = (List<ChannelsTOO>) session
				.createSQLQuery("select * from channels where client_id = ?;")
				.setString(0, client_id)
				.setResultTransformer(Transformers.aliasToBean(ChannelsTOO.class))
				.list();
		
		
		return resultWithAliasedBean;
	}

	@SuppressWarnings("rawtypes")
	public List<String> getChannelsListFromPackage(int package_id) {
		
		List<String> list2=new ArrayList<String>();
		PackageEntity packages = new  PackageEntity();
		try{
			Session session=sfactory.openSession();
			String hql="from PackageEntity p where p.package_id=?";
			Query q=session.createQuery(hql);
			q.setParameter(0, package_id);
			List list=q.list();
			packages=(PackageEntity)list.get(0);
			//StringBuffer channelNames = new StringBuffer("");
			
			for(Channels channel: packages.getChannels() ){
				/*channelNames.append(channel.getChannel_name());
				channelNames.append(",");*/
				list2.add(channel.getChannel_name());
			}
			
			session.close();
			
		}catch(Exception e){
			logger.error(e);
			return null;
		}
		return list2;
	}

	public double getPackageAmount(String package_name) {
		double resultset = 0.0;
		
		Session session = sfactory.openSession();
		Transaction tx = null;
		try {
			tx = session.beginTransaction();
			
			
			BigDecimal result  = (BigDecimal) session.createSQLQuery(
			"select package_amount from packages where package_name='"+package_name+"'")
			.uniqueResult();
			
			resultset = result.doubleValue();
			
			tx.commit();
		} catch (HibernateException e) {
			if (tx != null)
				tx.rollback();
			e.printStackTrace();
			logger.error("Exception! " + e);
		} finally {
			session.close();
		}
		return resultset;
	}

	public double getAddOnAmount(String addons) {
		double resultset = 0.0;
		
		Session session = sfactory.openSession();
		Transaction tx = null;
		try {
			tx = session.beginTransaction();
			
			BigDecimal totalAmount = new BigDecimal(0);
			
			if(CommonUtils.exists(addons)){
			String[] add_ons = addons.split(",");
			StringBuffer  addOnSearch = new StringBuffer("(");
			
			for(String s:add_ons){
				addOnSearch.append("\'"+s+"\',");
			}
			logger.info(addOnSearch);
			logger.info(addOnSearch.toString().substring(0,addOnSearch.length()-1));
			String addOnQuery = "SELECT SUM(channel_amount) from channels WHERE channel_name in "+addOnSearch.toString().substring(0,addOnSearch.length()-1)+");";
			
			SQLQuery query = session.createSQLQuery(addOnQuery);
			totalAmount = (BigDecimal)query.uniqueResult();
			
			resultset = totalAmount.doubleValue();
			}
			tx.commit();
		} catch (HibernateException e) {
			if (tx != null)
				tx.rollback();
			e.printStackTrace();
			logger.error("Exception! " + e);
		} finally {
			session.close();
		}
		return resultset;
	}
	
	@SuppressWarnings("unchecked")
	public List<ChannelSeraliser> getChannelNames() {
		List<ChannelSeraliser> channelSeraliser = new ArrayList<ChannelSeraliser>();
		Session session=sfactory.openSession();
		Transaction tx = null;
		try{
			tx = session.beginTransaction();
			
			List<String> categories = session.createQuery("select distinct category from Channels").list();
			
			for(String category:categories){
				
				List<Channels> channels = session.createQuery("from Channels where category =? ").setString(0, category).list();
				List<ChannelDisplay> displayChannel = new ArrayList<ChannelDisplay>();
					for(Channels channel:channels){
						ChannelDisplay channelDisplay = new ChannelDisplay();
						channelDisplay.setChannel_amount(new BigDecimal(channel.getChannel_amount()));
						channelDisplay.setChannel_name(channel.getChannel_name());
						displayChannel.add(channelDisplay);
					}
				ChannelSeraliser channelSeraliser2 = new ChannelSeraliser(category, displayChannel);
				channelSeraliser.add(channelSeraliser2);
			}
			
			tx.commit();
			
		}catch(HibernateException e){
			if(tx != null)tx.rollback();
			logger.error(e);
		}finally{
			session.close();
		}
		return channelSeraliser;
	}



	@SuppressWarnings("unchecked")
	public List<ChannelNamesWithCost> getAllChannelNamesWithAmount(String client_id) {
		List<ChannelNamesWithCost> channelList = new ArrayList<ChannelNamesWithCost>();
		Session session=sfactory.openSession();
		Transaction tx = null;
		try{
			tx = session.beginTransaction();
			
			
				channelList = session.createSQLQuery("select c.channel_name, c.channel_amount from channels c where c.client_id=?")
						.setString(0, client_id)
						.setResultTransformer(Transformers.aliasToBean(ChannelNamesWithCost.class)).list();
			
			tx.commit();
			
		}catch(HibernateException e){
			if(tx != null)tx.rollback();
			logger.error(e);
			return channelList;
		}finally{
			session.close();
		}
		return channelList;
	}



	public List<String> displayChannelsOfSetupBox(String customer_id,
			String box_number) {
		List<String> channelList = new ArrayList<String>();
		Session session=sfactory.openSession();
		Transaction tx = null;
		try{
			tx = session.beginTransaction();
			
			
				String channels = (String) session.createSQLQuery("select addons from setupbox where customer_id= ? and box_number = ?")
						.setString(0, customer_id)
						.setString(1, box_number).uniqueResult();
				String channel[];
				if(CommonUtils.exists(channels)){
					channel = channels.split(",");
					for(String s:channel){
						channelList.add(s);
					}
				}
			
			tx.commit();
			
		}catch(HibernateException e){
			if(tx != null)tx.rollback();
			logger.error(e);
			return channelList;
		}finally{
			session.close();
		}
		return channelList;
	}



	@SuppressWarnings("unchecked")
	public List<String> getChannelsCategories(String client_id) {
		List<String> channelSeraliser = new ArrayList<String>();
		Session session=sfactory.openSession();
		Transaction tx = null;
		try{
			tx = session.beginTransaction();
			
			channelSeraliser = session.createQuery("select distinct category from Channels where client_id=?")
					.setString(0, client_id)
					.list();
			
			tx.commit();
			
		}catch(HibernateException e){
			if(tx != null)tx.rollback();
			logger.error(e);
		}finally{
			session.close();
		}
		return channelSeraliser;	
		}



	@SuppressWarnings("unchecked")
	public List<GetExpireAddonAlertTo> getAlertsForAddonExpiry(String client_id) {
		List<GetExpireAddonAlertTo> result = new ArrayList<GetExpireAddonAlertTo>();
		Session session=sfactory.openSession();
		Transaction tx = null;
		try{
			tx = session.beginTransaction();
			
			result = session.createSQLQuery("select group_concat(a.addon)as addons, a.id, c.customer_name, c.contact_number, c.c_id, a.expiry_date from customer c, addons a where c.customer_id = a.customer_id and c.client_id = '"+client_id+"' and a.expiry_date between (current_date - 2) and (current_date+3)")
					.setResultTransformer(Transformers.aliasToBean(GetExpireAddonAlertTo.class))
					.list();
			
			tx.commit();
			
		}catch(HibernateException e){
			if(tx != null)tx.rollback();
			logger.error(e);
		}finally{
			session.close();
		}
		return result;	
	}



	@SuppressWarnings("unchecked")
	public List<GetTheAddonsToUpdate> getTheAddonsToUpdate(
			CustomerFetchCredetials fetchCredetials) {
		List<GetTheAddonsToUpdate> result = new ArrayList<GetTheAddonsToUpdate>();
		Session session=sfactory.openSession();
		Transaction tx = null;
		try{
			tx = session.beginTransaction();
			
			String customer_id =  (String) session.createSQLQuery("select customer_id from customer where c_id=? and client_id=?")
					.setString(0, fetchCredetials.getC_id())
					.setString(1, fetchCredetials.getClient_id()).uniqueResult();
			
			result = session.createSQLQuery("select a.box_number, a.addon, a.expiry_date from addons a where a.customer_id = '"+customer_id+"' and expiry_date between(current_date - 2) and (current_date+3) ;")
					.setResultTransformer(Transformers.aliasToBean(GetTheAddonsToUpdate.class))
					.list();
			
			tx.commit();
			
		}catch(HibernateException e){
			if(tx != null)tx.rollback();
			logger.error(e);
		}finally{
			session.close();
		}
		return result;	
	}
	

}
