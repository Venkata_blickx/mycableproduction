package com.blickx.dao;

import com.blickx.domain.NFCEntity;

public interface NfcDao {

	int addNfc(NFCEntity nfcEntity);

}
