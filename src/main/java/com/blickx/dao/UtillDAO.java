package com.blickx.dao;

import java.util.List;

public interface UtillDAO {

	List<String> getSubscriptionsIDs(String clientId);

	Object getStatus(String clientId, String string);

	
}
