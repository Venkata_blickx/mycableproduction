package com.blickx.dao;

import java.util.List;

import com.blickx.to.PDAReportDto;

public interface PDAReprotDao {

	List<PDAReportDto> getPDAReqport(String client_id)throws Exception;

}
