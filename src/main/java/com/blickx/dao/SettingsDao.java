package com.blickx.dao;

import java.math.BigDecimal;
import java.util.List;

import com.blickx.domain.ClientInformationEntity;
import com.blickx.output.EditCustomerCommentTo;
import com.blickx.settings.to.GetBalanceTo;

public interface SettingsDao {

	public int updateBalance(String customer_id, BigDecimal balance, String reason);

	public GetBalanceTo getBalanceAmount(String customer_id);

	public BigDecimal getServiceCharge(String client_id);

	public List<String> getVc_numbers(String customer_id);

	public void addClientInfoForBlueToothPrint(
			ClientInformationEntity clientInfo);

	public ClientInformationEntity getClientInfoForBlueToothPrint(
			ClientInformationEntity clientInformationForPrint, String client_id);

	public String saveOrUpdateComments(List<EditCustomerCommentTo> comments,
			String client_id);

	public String sendWishestoAll(String msg, String client_id);

	public String getUpdateApkVersion();

	public String paymentReminder(String msg, String client_id);

}
