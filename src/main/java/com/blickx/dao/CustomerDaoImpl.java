package com.blickx.dao;

import java.math.BigDecimal;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.HashSet;
import java.util.Iterator;
import java.util.LinkedList;
import java.util.List;
import java.util.Set;

import org.apache.commons.lang.StringUtils;
import org.apache.log4j.Logger;
import org.hibernate.HibernateException;
import org.hibernate.Query;
import org.hibernate.SQLQuery;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.Transaction;
import org.hibernate.exception.ConstraintViolationException;
import org.hibernate.exception.DataException;
import org.hibernate.transform.Transformers;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;




















import com.blickx.domain.AddOnsEntity;
import com.blickx.domain.AreaCustomerMapEntity;
import com.blickx.domain.AreaEmployeeMapEntity;
import com.blickx.domain.Authentication;
import com.blickx.domain.BillGeneration;
import com.blickx.domain.Billing;
import com.blickx.domain.ContactNumberUpdateHistory;
import com.blickx.domain.Customer;
import com.blickx.domain.CustomerToMobile;
import com.blickx.domain.EditCustomerCommentEntity;
import com.blickx.domain.EmailUpdateHistoryEntity;
import com.blickx.domain.Employee;
import com.blickx.domain.PackageEntity;
import com.blickx.domain.PermenentDeActiveHistoryEntity;
import com.blickx.domain.SMSGateWayEntity;
import com.blickx.domain.SetupBox;
import com.blickx.domain.TempCustomerId;
import com.blickx.exceptions.BoxNumberDupliactedException;
import com.blickx.exceptions.CIdDupliactedException;
import com.blickx.exceptions.ContactNumberDupliactedException;
import com.blickx.exceptions.ErrorCodes;
import com.blickx.exceptions.VcNumberDuplicatedException;
import com.blickx.id.generator.BillIdGenerator;
import com.blickx.id.generator.CustomerIdGenerator;
import com.blickx.mycable.customerAddElements.Cart;
import com.blickx.mycable.customerAddElements.CustomerAddInputTo;
import com.blickx.mycable.customerAddElements.Items;
import com.blickx.mycable.customerAddElements.SelectedPackageForCustomerAdd;
import com.blickx.mycable.customerAddElements.SetupboxForAddCustomer;
import com.blickx.output.EditCustomerCommentTo;
import com.blickx.response.PackagesAndAddonsOfCustomer;
import com.blickx.searchEntities.PaymentFields;
import com.blickx.searchEntities.SearchCustomer;
import com.blickx.settings.to.AddonsEntityTo;
import com.blickx.sms.gateway.send.SendSMSRunnable;
import com.blickx.to.CustomerBillTo;
import com.blickx.to.CustomerTo;
import com.blickx.to.CustomerWrapperTo;
import com.blickx.to.MiniStatementForAndroidTo;
import com.blickx.to.PackageReportsForAndroidTo;
import com.blickx.to.SearchCustomerMoreDetails;
import com.blickx.to.SearchCustomerTo;
import com.blickx.to.SetUpBoxToo;
import com.blickx.to.SetupBoxTo;
import com.blickx.to.ViewBillsByMonthTo;
import com.blickx.to.input.ContactUpdateTo;
import com.blickx.to.input.CustomerCredentials;
import com.blickx.to.input.DetailsToSend;
import com.blickx.to.input.GetCustomer;
import com.blickx.to.input.InputIds;
import com.blickx.to.input.UidInsertInput;
import com.blickx.utill.BillGenType;
import com.blickx.utill.CommonUtils;
import com.blickx.utill.CurrentDate;
import com.blickx.utill.CusHistoryDto;
import com.blickx.utill.CustomerHistoryDto;
import com.blickx.utill.SendSMSThroughBSMS;

public class CustomerDaoImpl implements CustomerDao {

	private static final String CUS_ID = "cus_id";

	private static Logger logger = Logger.getLogger(CustomerDaoImpl.class);

	@Autowired
	SessionFactory sfactory;

	@Autowired
	CustomerIdGenerator idGenerator;
	
	@Autowired
	BillIdGenerator billIdGenerator;
	
	@Autowired
	EmployeeDao edao;

	@Override
	public String addCustomer(String client_id,GetCustomer customerDetails) {

		Session session = sfactory.openSession();
		Transaction tx = null;
		String customer_id = null;
		try {

			tx = session.beginTransaction();
			customer_id = idGenerator.getNextSid(client_id);
			logger.info(customer_id);
			
			Customer customer = new Customer();
			BeanUtils.copyProperties(customerDetails, customer);

			customer.setCustomer_id(customer_id);
			customer.setClient_id(client_id);
			customer.setStatus(true);
			
			isContactNumberExistsForAddCustomer(client_id, session, customer);
			isCustomerCIDExistedforAddCustomer(client_id, session, customer);
			
			Set<SetupBox> setupBoxes = new HashSet<SetupBox>();
			for(SetupBoxTo boxTo : customerDetails.getSetupBox())
		  	{ 
				isSetupBoxNumberExistedforAddSetupBox(session, boxTo);
				if(CommonUtils.exists(boxTo.getVc_number())){
					isVcNumberExistedforAddSetupBox(session, boxTo);
				}
		  		SetupBox setupBox = new SetupBox(); 
		  		BeanUtils.copyProperties(boxTo,setupBox);
		  		setupBox.setStatus(true);
		  		setupBox.setPackage_id(Integer.parseInt(boxTo.getPackage_id()));
		  		setupBox.setCustomer(customer);
		  		setupBoxes.add(setupBox);
		  	}
			customer.setSetupBoxes(setupBoxes);
			
			session.save(customer);
			
			if(CommonUtils.exists(customer.getAreacode())){
				AreaCustomerMapEntity areaCustomerMap = new AreaCustomerMapEntity();
				Date currentDate = new Date();
				areaCustomerMap.setAreaId(Integer.parseInt(customer.getAreacode()));
				areaCustomerMap.setStartDate(currentDate);
				areaCustomerMap.setCreatedTimeStamp(currentDate);
				areaCustomerMap.setLastUpdatedTimestamp(currentDate);
				areaCustomerMap.setCustomerId(customer.getCustomer_id());
				session.save(areaCustomerMap);
			}
			
			
			
			try{
			  	for(SetupBoxTo boxTo : customerDetails.getSetupBox())
			  	{ 
			  		SetupBox setupBox = new SetupBox(); 
			  		BeanUtils.copyProperties(boxTo,setupBox);
					//setupBox.setCustomer_id(customer_id);
					setupBox.setClient_id(client_id);
					setupBox.setStatus(true);
					
					addSetupBox(setupBox, session, tx, customer_id);
					//setUpBoxs.add(setupBox); 
				}
			  	
			  	session.createSQLQuery("CALL update_addon_amount_as_balance_in_billing(:cus_id)")
			  	.setString("cus_id", customer_id).executeUpdate();
			  	
			}catch(ConstraintViolationException e){
				if(tx != null)tx.rollback();
				logger.error(e);
				return "MCI1001 Internal Server Error: Please contact your site adminstrator!";
			}catch(HibernateException e){
				if(tx != null)tx.rollback();
				logger.error(e);
				return "MCI1000 Internal Server Error: Please contact your site adminstrator!";
			}catch(Exception e){
				if(tx != null)tx.rollback();
				logger.error(e);
				return "MCI100 Internal Server Error: Please contact your site adminstrator!";
			}
			// customer.setSetupBoxes(setUpBoxs);
			
			//SQLQuery query = session.createSQLQuery("call Get_addon_amount_based_on_expiry(:boxNumber)");
			//totalAmount = (BigDecimal)query.uniqueResult();
			//BigDecimal totalAmount = (BigDecimal)query.setParameter("customer_id", customer_id).uniqueResult();
			//logger.info("Total Addon Amount for the Customer"+totalAmount);
			
			tx.commit();
		} catch (CIdDupliactedException e) {
			if (tx != null)
				tx.rollback();
			logger.error(e);
			return e.toString();
		}catch (ContactNumberDupliactedException e) {
			if (tx != null)
				tx.rollback();
			logger.error(e);
			return e.toString();
		}catch (ConstraintViolationException e) {
			if (tx != null)
				tx.rollback();
			logger.error(e);
			return "MCI1001 Internal Server Error: Please contact your site adminstrator!";
		}catch(BoxNumberDupliactedException msg){
			if(tx != null)tx.rollback();
			logger.error("boxNumberDuplicatedException catch block"+msg.toString());
			return msg.toString();
		}catch(VcNumberDuplicatedException msg){
			if(tx != null)tx.rollback();
			logger.error("VCNumberDuplicatedException catch block"+msg.toString());
			return msg.toString();
		} catch (DataException e) {
			if (tx != null)
				tx.rollback();
			logger.error(e);
			return "MCI1002 Internal Server Error: Please contact your site adminstrator!";
		} catch (HibernateException e) {
			if (tx != null)
				tx.rollback();
			logger.error(e);
			return "MCI1000 Internal Server Error: Please contact your site adminstrator!";
		} catch (Exception e) {
			if (tx != null)
				tx.rollback();
			logger.error(e);
			return "MCI100 Internal Server Error: Please contact your site adminstrator!";
		} finally {
			session.close();
		}
		return "Customer added successfully!";
	}

	private void isCustomerCIDExistedforAddCustomer(String client_id, Session session, Customer customer)
			throws CIdDupliactedException {
		if(session.createSQLQuery("select * from customer where c_id = '"+customer.getC_id()+"' and client_id = '"+client_id+"'").list().size()>0)throw new CIdDupliactedException(ErrorCodes.getcIdDuplicateMsg(customer.getC_id()));
	}

	private void isContactNumberExistsForAddCustomer(String client_id, Session session, Customer customer)
			throws ContactNumberDupliactedException {
		if(session.createSQLQuery("select * from customer where contact_number = '"+customer.getContact_number()+"' and client_id = '"+client_id+"'").list().size()>0)throw new ContactNumberDupliactedException(ErrorCodes.getContactNumberDuplicateMsg(customer.getContact_number()));
	}

	private void isVcNumberExistedforAddSetupBox(Session session, SetupBoxTo boxTo) throws VcNumberDuplicatedException {
		if(session.createSQLQuery("select * from setupbox where vc_number = '"+boxTo.getVc_number()+"'").list().size()>0)throw new VcNumberDuplicatedException(ErrorCodes.getVcNumberDuplicateMsg(boxTo.getVc_number()));
	}

	private void isSetupBoxNumberExistedforAddSetupBox(Session session, SetupBoxTo boxTo) throws BoxNumberDupliactedException {
		if(session.createSQLQuery("select * from setupbox where box_number = '"+boxTo.getBox_number()+"'").list().size()>0)throw new BoxNumberDupliactedException(ErrorCodes.getBoxNumberDuplicateMsg(boxTo.getBox_number()));
	}

	@Override
	public int addExtraBox(SetupBoxTo setupBoxTo) {
		// TODO Auto-generated method stub
		
		
		Session session = sfactory.openSession();
		Transaction tx = null;
		try{
			tx = session.beginTransaction();
			SetupBox setupBox = new SetupBox();
			BeanUtils.copyProperties(setupBoxTo, setupBox);
			addSetupBox(setupBox, session, tx, setupBoxTo.getCustomer_id());
			tx.commit();
		}catch(BoxNumberDupliactedException msg){
			if(tx != null)tx.rollback();
			logger.error("boxNumberDuplicatedException catch block"+msg.toString());
			return Integer.parseInt(msg.toString());
		}catch(VcNumberDuplicatedException msg){
			if(tx != null)tx.rollback();
			logger.error("VCNumberDuplicatedException catch block"+msg.toString());
			return Integer.parseInt(msg.toString());
		}catch(ConstraintViolationException e){
			if(tx != null)tx.rollback();
			return 101;
		}catch(HibernateException e){
			if(tx != null)tx.rollback();
			return 100;
		}catch(Exception e){
			if(tx != null)tx.rollback();
			return 0;
		}finally{
			session.close();
		}
		return 1;
	}
	
	public static void addSetupBox(SetupBox setupBox, Session session, Transaction tx, String customer_id)
			throws HibernateException,ConstraintViolationException, BoxNumberDupliactedException, VcNumberDuplicatedException, ParseException {

		try {
			
			
				/*if(session.createSQLQuery("select * from setupbox where box_number = '"+setupBox.getBox_number()+"' and client_id ='"+setupBox.getClient_id()+"'").list().size()>0)throw new BoxNumberDupliactedException(ErrorCodes.BOX_NUMBER_DUPLICATE_CODE);
				if(session.createSQLQuery("select * from setupbox where vc_number = '"+setupBox.getVc_number()+"' and client_id ='"+setupBox.getClient_id()+"'").list().size()>0)throw new VcNumberDuplicatedException(ErrorCodes.VC_NUMBER_DUPLICATE_CODE);
			*/
			
			if (CommonUtils.exists(setupBox.getAddonNames())) {
				logger.info(setupBox.getAddonNames());
				String[] add_ons = setupBox.getAddonNames().split(",");
				StringBuffer addOnSearch = new StringBuffer();
				BigDecimal totalAmount;
				
				List<String> channels = new ArrayList<String>();
				List<String> expiry_dates = new ArrayList<String>();
				
				for(int x=0;x<add_ons.length;x++){
					if(x%2==0)
					channels.add(add_ons[x].trim());
				}
				for(int x=0;x<add_ons.length;x++){
					if(x%2==1)
					expiry_dates.add(add_ons[x].trim());
				}
				
				List<AddOnsEntity> addons = new ArrayList<AddOnsEntity>();
				SimpleDateFormat formatter = new SimpleDateFormat("yyyy-MM-dd");
				
				for(int x=0;x<channels.size();x++){
					AddOnsEntity addon = new AddOnsEntity();
					addon.setStart_date(setupBox.getStart_date());
					addon.setBox_number(setupBox.getBox_number());
					addon.setCustomer_id(customer_id);
					logger.info(expiry_dates.size()+"******hi******"+channels.size());
					addon.setAddon(channels.get(x));
					logger.info("************"+formatter.parse(expiry_dates.get(x)));
					
					addon.setExpiry_date(formatter.parse(expiry_dates.get(x)));
					addons.add(addon);
					
				}
				
				List<Integer> addedAddonIds = new ArrayList<Integer>();
				
				for(AddOnsEntity addon:addons){
					logger.info("Start of SEssion.save()************************************8");
					Integer recentId = (Integer) session.save(addon);	// Adding addonEntity to table
					addedAddonIds.add(recentId);
				}
				
				for(Integer ids:addedAddonIds){
					logger.info(ids);
				}
				
				session.flush();
				session.clear();
				
				StringBuffer addons_string = new StringBuffer("");
				for(String s: channels){
					addons_string.append(s);
					addons_string.append(",");
				}
				
				for (String s : channels) {
					addOnSearch.append("\'" + s.trim() + "\',");
				}
				
				logger.info(addOnSearch.toString().substring(0,
						addOnSearch.length() - 1));
				String add_on_string = addOnSearch.toString().substring(0, addOnSearch.length() - 1);
				logger.info(add_on_string.substring(1));
				
				StringBuffer  addOnnSearch = new StringBuffer("(");
				
				for(String s:channels){
					addOnnSearch.append("\'"+s+"\',");
				}
				logger.info(addOnnSearch);
				logger.info(addOnnSearch.toString().substring(0,addOnSearch.length()));
				@SuppressWarnings("unused")
				String addOnQuery = "SELECT SUM(channel_amount) from channels WHERE channel_name in " +addOnnSearch.toString().substring(0,addOnSearch.length())+");";
				
				StringBuffer addonIds = new StringBuffer("");
				for(Integer id:addedAddonIds){
					addonIds.append(id);
					addonIds.append(",");
				}
				
				StringBuffer queryToGetAddonSpotAmount = new StringBuffer();
				queryToGetAddonSpotAmount.append("SELECT sum((12 * (YEAR(expiry_date) - YEAR(start_date)) + (MONTH(expiry_date) - MONTH(start_date)))  *(select sum(c.channel_amount) from channels c where channel_name = addon and client_id = '"+setupBox.getClient_id()+"')) as addonAmount ");
				queryToGetAddonSpotAmount.append("FROM addons ");
				queryToGetAddonSpotAmount.append("where id in (");
				queryToGetAddonSpotAmount.append(addonIds.toString().substring(0, addonIds.length()-1)+");");

				
				BigDecimal addonAmount = (BigDecimal) session.createSQLQuery(queryToGetAddonSpotAmount.toString())
						.uniqueResult();
				
				
				
				
				
				SQLQuery query = session.createSQLQuery("call Get_addon_amount_based_on_expiry(:boxNumber)");
				//totalAmount = (BigDecimal)query.uniqueResult();
				totalAmount = (BigDecimal)query.setParameter("boxNumber", setupBox.getBox_number()).uniqueResult();
				logger.info("Addon Amount : " +totalAmount);
				logger.info(totalAmount);
				
				
				
				// String string = add_on_string.substring(1).toString();
				// logger.info(add_on_string);
				// String assigned_packageID=(Byte.toString((Byte)
				// session.createSQLQuery("select package_id from packages where package_name='"+setupBox.getAssigned_package()+"';").uniqueResult()));

				session.createSQLQuery(
						"call Add_Setupbox(:cus_id, :boxnumber, :start_date, :vc_number, :nds_number, :package, :addon_amount, :add_on_string, :addons_string, :discount, :clientid)")
						.setString("cus_id", customer_id)
						.setString("boxnumber", setupBox.getBox_number())
						.setDate("start_date", setupBox.getStart_date())
						.setString("vc_number", setupBox.getVc_number())
						.setString("nds_number", setupBox.getNds_number())
						.setString("package", setupBox.getAssigned_package())
						.setBigDecimal("addon_amount", addonAmount)
						.setString("add_on_string", add_on_string)
						.setString("addons_string", addons_string.toString().substring(0,addons_string.length()-1))
						.setBigDecimal("discount", new BigDecimal(setupBox.getDiscount()))
						.setString("clientid", setupBox.getClient_id())
						.executeUpdate();
				
				session.createSQLQuery(
						"call update_balance_for_addons(:cus_id, :balance) ")
						.setString("cus_id", customer_id)
						.setBigDecimal("balance", addonAmount).executeUpdate();
				
			}else{
				session.createSQLQuery(
						"call Add_Setupbox(:cus_id, :boxnumber, :start_date, :vc_number, :nds_number, :package, :addon_amount, :add_on_string, :addons_string, :discount, :clientid) ")
						.setString("cus_id", customer_id)
						.setString("boxnumber", setupBox.getBox_number())
						.setDate("start_date", setupBox.getStart_date())
						.setString("vc_number", setupBox.getVc_number())
						.setString("nds_number", setupBox.getNds_number())
						.setString("package", setupBox.getAssigned_package())
						.setBigDecimal("addon_amount", new BigDecimal(0))
						.setString("add_on_string", null)
						.setString("addons_string", null)
						.setBigDecimal("discount", new BigDecimal(setupBox.getDiscount()))
						.setString("clientid", setupBox.getClient_id())
						.executeUpdate();
			}
			
			
		} catch (ConstraintViolationException e) {
			logger.error(e);
			throw e;
		}catch (HibernateException e) {
			logger.error(e);
			throw e;
		}catch (Exception e) {
			// TODO: handle exception
			logger.error(e);
			throw e;
		}
	}

	
	
	@SuppressWarnings("unchecked")
	@Override
	public List<Customer> getCustById(String custId) {

		List<Customer> list = null;
		Session session = null;
		try {
			session = sfactory.openSession();
			list = session.createQuery("from Customer c where c.customer_id=?")
					.setParameter(0, custId).list();
			for(Customer cust:list){
				for(SetupBox box:cust.getSetupBoxes()){
					box.getAddonNames();
				}
			}
		} catch (Exception e) {
			logger.error(e);
			return list;
		} finally {
			if (session != null)
				session.close();
		}
		return list;
	}

	public String updateCustomer(Customer customer, List<EditCustomerCommentTo> listOfComments) {
		Session session = sfactory.openSession();
		Transaction tx = null;
		try {
			tx = session.beginTransaction();
			logger.info("update customer started");

			Customer loadedCustomer = (Customer) session.get(Customer.class, customer.getCustomer_id());
			
			isContactNumberExistsForUpdateCustomer(session, customer);
			isCustomerCIDExistsForUpdate(session, customer);
			
			
			if(listOfComments.size() > 0){
				for(EditCustomerCommentTo commentTo: listOfComments){
					logger.info("Comment is Adding/Updating");
					EditCustomerCommentEntity commentEntity = new EditCustomerCommentEntity();
					BeanUtils.copyProperties(commentTo, commentEntity);
					session.saveOrUpdate(commentEntity);
					
				}
			}
			if(CommonUtils.exists(customer.getAreacode())){
				@SuppressWarnings("unchecked")
				List<AreaCustomerMapEntity> oldAreaMaps = session.createQuery("from AreaCustomerMapEntity ae where ae.customerId = ? and ae.endDate is null")
						.setString(0, customer.getCustomer_id())
						.list();
				Date currentDate = new Date();
				if(oldAreaMaps.size() > 0){
					AreaCustomerMapEntity oldAreaMap = oldAreaMaps.get(0);
					
					if(!customer.getAreacode().equalsIgnoreCase(oldAreaMap.getAreaId()+"")){
					
					oldAreaMap.setEndDate(new Date());
					oldAreaMap.setLastUpdatedTimestamp(new Date());
					session.update(oldAreaMap);
					
					AreaCustomerMapEntity newAreaMap = new AreaCustomerMapEntity();
					newAreaMap.setAreaId(Integer.parseInt(customer.getAreacode()));
					newAreaMap.setStartDate(currentDate);
					newAreaMap.setCreatedTimeStamp(currentDate);
					newAreaMap.setLastUpdatedTimestamp(currentDate);
					newAreaMap.setCustomerId(customer.getCustomer_id());
					
					session.save(newAreaMap);
					}
				} else {
					System.out.println("*****");
					AreaCustomerMapEntity newAreaMap = new AreaCustomerMapEntity();
					newAreaMap.setAreaId(Integer.parseInt(customer.getAreacode()));
					newAreaMap.setStartDate(currentDate);
					newAreaMap.setCreatedTimeStamp(currentDate);
					newAreaMap.setLastUpdatedTimestamp(currentDate);
					newAreaMap.setCustomerId(customer.getCustomer_id());
					
					session.save(newAreaMap);
				}
				}else if(CommonUtils.exists(loadedCustomer.getAreacode())){
					session.createSQLQuery("update area_customer_map aem set aem.end_date = current_timestamp where"
							+ " aem.customer_id = '"+customer.getCustomer_id()+"' and aem.end_date is null").executeUpdate();
				}
			
			
			
			
			
			

			setUpdatedCustomerData(customer, loadedCustomer);

			for(SetupBox setupBox : customer.getSetupBoxes()){
				@SuppressWarnings("unchecked")
				List<SetupBox> existedSetupBoxes = (List<SetupBox>) session.createQuery(
						"from SetupBox s where s.row_id = ?")
				.setInteger(0, setupBox.getRow_id()).list();
				
				if (existedSetupBoxes.size() > 0) {
					SetupBox existedSetupBox = (SetupBox) session
							.createQuery(
									"from SetupBox s where s.row_id = ?")
							.setInteger(0, setupBox.getRow_id())
							.uniqueResult();
					logger.info("Existed SetupBox: " + existedSetupBox.getBox_number());
					if (existedSetupBox != null) {
						logger.info("##################existed setupbox################");
						isSetupBoxNumberExistedforUpdatSetupBox(session,
								setupBox, customer.getCustomer_id());
						
						if(CommonUtils.exists(existedSetupBox.getVc_number())){
						isVcNumberExistedforUpdateSetupBox(session,
								setupBox, customer.getCustomer_id());
						}
						
						if (existedSetupBox.getPackage_id() != setupBox
								.getPackage_id()) {
							logger.info("*********Package Was Changed**************");
							existedSetupBox.setUpdatepackage_date(setupBox
									.getUpdatepackage_date());
						}
						
						existedSetupBox.setBox_number(setupBox.getBox_number());
						existedSetupBox.setVc_number(setupBox.getVc_number());
						existedSetupBox.setNds_number(setupBox.getNds_number());
						existedSetupBox.setAssigned_package(setupBox
								.getAssigned_package());
						existedSetupBox.setPackage_id(setupBox.getPackage_id());
						existedSetupBox.setDiscount(setupBox.getDiscount());
						if(CommonUtils.exists(setupBox.getAddonNames())){
							
							logger.info("Customer Update- Existed Setupbox Update: addons existed" + setupBox.getAddonNames());

							String[] add_ons = setupBox.getAddonNames().split(",");
							//StringBuffer addOnSearch = new StringBuffer();
							//BigDecimal totalAmount;
							
							List<String> channels = new ArrayList<String>();
							List<String> expiry_dates = new ArrayList<String>();
							
							for(int x=0;x<add_ons.length;x++){
								if(x%2==0)
								channels.add(add_ons[x].trim());
							}
							for(int x=0;x<add_ons.length;x++){
								if(x%2==1)
								expiry_dates.add(add_ons[x].trim());
							}
							
							Set<AddonsEntityTo> addons = new HashSet<AddonsEntityTo>();
							SimpleDateFormat formatter = new SimpleDateFormat("yyyy-MM-dd");
							
							for(int x=0;x<channels.size();x++){
								AddonsEntityTo addon = new AddonsEntityTo();
								addon.setStart_date(setupBox.getStart_date());
								addon.setBox_number(setupBox.getBox_number());
								addon.setCustomer_id(customer.getCustomer_id());
								logger.info(expiry_dates.size()+"******<-expiry dates and channels size->******"+channels.size());
								addon.setAddon(channels.get(x));
								logger.info("************"+formatter.parse(expiry_dates.get(x)));
								
								addon.setExpiry_date(formatter.parse(expiry_dates.get(x)));
								
								AddOnsEntity addOnEntity = (AddOnsEntity) session.createQuery("from AddOnsEntity a where a.box_number = ? and a.addon = ?").setString(0, setupBox.getBox_number()).setString(1, channels.get(x)).uniqueResult();
								//logger.info(addOnEntity);
								if((addOnEntity == null)){
								//if(!CommonUtils.exists(addOnEntity.getAddon())){
									logger.info("Update Customer- Update Existed Setupbox : New ADDon " + addon);
									addons.add(addon);
								}else{
									//addon.setId(addOnEntity.getId());
									addOnEntity.setStart_date(addOnEntity.getExpiry_date());
									addOnEntity.setExpiry_date(formatter.parse(expiry_dates.get(x)));
									updateAddon(addOnEntity, customer.getClient_id(), session);
								}
							}
							
							addAddons(addons, customer.getClient_id(), session, customer.getCustomer_id());
							
							
						}
						

						session.update(existedSetupBox);
					}
				} else {
					logger.info("############################ New Setupbox################");
					SetupBoxTo boxTo = new SetupBoxTo();
					BeanUtils.copyProperties(setupBox, boxTo);
					isSetupBoxNumberExistedforAddSetupBox(session, boxTo);
					if(CommonUtils.exists(boxTo.getVc_number())){
						isVcNumberExistedforAddSetupBox(session, boxTo);
					}
					addSetupBox(setupBox, session, tx,
							customer.getCustomer_id());

					setupBox.setCustomer(loadedCustomer);

					session.save(setupBox);
				}
			}
			
			
			session.update(loadedCustomer);
			
			logger.info("cusotmer update End");
			tx.commit();
		} catch (ContactNumberDupliactedException e) {
			if (tx != null)
				tx.rollback();
			logger.error(e);
			return e.toString();
		} catch (CIdDupliactedException e) {
			if (tx != null)
				tx.rollback();
			logger.error(e);
			return e.toString();
		} catch (BoxNumberDupliactedException e) {
			if (tx != null)
				tx.rollback();
			logger.error(e);
			return e.toString();
		} catch (VcNumberDuplicatedException e) {
			if (tx != null)
				tx.rollback();
			logger.error(e);
			return e.toString();
		} catch (DataException e) {
			if (tx != null)
				tx.rollback();
			logger.error(e);
			return "MCI1002 Internal Server Error: Please contact your site adminstrator!";
		} catch (HibernateException e) {
			if (tx != null)
				tx.rollback();
			logger.error(e);
			return "MCI1000 Internal Server Error: Please contact your site adminstrator!";
		} catch (Exception e) {
			if (tx != null)
				tx.rollback();
			e.printStackTrace();
			logger.error(e);
			return "MCI100 Internal Server Error: Please contact your site adminstrator!";
		} finally {
			session.close();
		}
		return "1";
	  }

	private void setUpdatedCustomerData(Customer customerUpdateDto, Customer customer) {
		customer.setCustomer_name(customerUpdateDto.getCustomer_name());
		customer.setContact_number(customerUpdateDto.getContact_number());
		customer.setAreacode(customerUpdateDto.getAreacode());
		customer.setEmail(customerUpdateDto.getEmail());
		customer.setLandline_number(customerUpdateDto.getLandline_number());
		customer.setCustomer_doc(customerUpdateDto.getCustomer_doc());
		customer.setCustomer_photo(customerUpdateDto.getCustomer_photo());
		//customer.setEnd_date(customerUpdateDto.getEnd_date());
		//customer.setJoining_date(customerUpdateDto.getJoining_date());
		customer.setGender(customerUpdateDto.getGender());
		//customer.setStatus(customerUpdateDto.isStatus());
		customer.setCategory(customerUpdateDto.getCategory());
		customer.setLine1(customerUpdateDto.getLine1());
		customer.setLine2(customerUpdateDto.getLine2());
		customer.setCity(customerUpdateDto.getCity());
		customer.setState(customerUpdateDto.getState());
		customer.setPincode(customerUpdateDto.getPincode());
		customer.setDeposit(customerUpdateDto.getDeposit());
		/*customer.setDomain(customerUpdateDto.getDomain());
		customer.setExternalId(customerUpdateDto.getExternalId());*/
		
		
		customer.setC_id(customerUpdateDto.getC_id());
		customer.setPhoto_name(customerUpdateDto.getPhoto_name());
		customer.setDoc_name(customerUpdateDto.getDoc_name());
		customer.setPhoto_type(customerUpdateDto.getPhoto_type());
		customer.setDoc_type(customerUpdateDto.getDoc_type());
	}
	
	private void isVcNumberExistedforUpdateSetupBox(Session session,
			SetupBox boxTo, String customer_id) throws VcNumberDuplicatedException {
		logger.info("isSetupBoxNumberExistedforUpdatSetupBox" + boxTo.getVc_number());
		if (session
				.createSQLQuery(
						"select * from setupbox where vc_number = ('"
								+ boxTo.getVc_number() + "')  and customer_id not in ('" + customer_id + "')").list().size() > 0) {
			throw new VcNumberDuplicatedException(
					ErrorCodes.getVcNumberDuplicateMsg(boxTo.getVc_number()));
		}
	}

	private void isSetupBoxNumberExistedforUpdatSetupBox(Session session,
			SetupBox boxTo, String customer_id) throws BoxNumberDupliactedException {
		logger.info("isSetupBoxNumberExistedforUpdatSetupBox" + boxTo.getBox_number());
		if (session
				.createSQLQuery(
						"select * from setupbox where box_number = '" + boxTo.getBox_number() + "' and customer_id not in ('" + customer_id + "')").list().size() > 0){
			throw new BoxNumberDupliactedException(
					ErrorCodes.getBoxNumberDuplicateMsg(boxTo.getBox_number()));
		}
	}

	private void isCustomerCIDExistsForUpdate(Session session, Customer customer)
			throws CIdDupliactedException {
		if (session.createSQLQuery("select * from customer where c_id = '"
								+ customer.getC_id()
								+ "' and client_id = '"
								+ customer.getClient_id()
								+ "' and customer_id not in('"
								+ customer.getCustomer_id() + "')").list()
				.size() > 0)
			throw new CIdDupliactedException(ErrorCodes.getcIdDuplicateMsg(customer.getC_id()));
	}

	private void isContactNumberExistsForUpdateCustomer(Session session, Customer customer)
			throws ContactNumberDupliactedException {
		if (session.createSQLQuery("select * from customer where contact_number = '"
								+ customer.getContact_number()
								+ "' and client_id = '"
								+ customer.getClient_id()
								+ "' and customer_id not in('"
								+ customer.getCustomer_id() + "')").list()
				.size() > 0)
			throw new ContactNumberDupliactedException(ErrorCodes.getContactNumberDuplicateMsg(customer.getContact_number()));
	}
	 

	@Override
	public int deleteCustomer(String customer_id) {

		int res = 0;
		Session session = null;
		try {
			session = sfactory.openSession();
			// Session session=HibernateUtil.getSession();
			Transaction tx = session.beginTransaction();
			String hql = "from Customer c where c.customer_id=?";
			Query q = session.createQuery(hql);
			q = q.setParameter(0, customer_id);
			@SuppressWarnings("unchecked")
			List<Customer> list = q.list();
			Object obj = list.get(0);
			if (obj != null)
				res = 1;
			session.delete(obj);
			tx.commit();
			session.close();
		} catch (Exception e) {
			logger.error(e);
			res = 0;
		} finally {
			if (session != null)
				session.close();
		}
		return res;
	}

	@Override
	public CustomerTo getCustomerById(String customer_id) {

		CustomerTo customer = new CustomerTo();
		
		
		try {
			
			CustomerWrapperTo customerWrapperTo = new CustomerWrapperTo();
			SearchCustomer searchCustomer = new SearchCustomer();
			searchCustomer.setCustomer_id(customer_id);
			customerWrapperTo = searchCustomers(searchCustomer);
			customer = customerWrapperTo.getCustomerTo().get(0);
			
			Session session = sfactory.openSession();
			Transaction tx = null;
			try{
				tx = session.beginTransaction();
				logger.info("******************************************");
					Set<SetUpBoxToo> set = new HashSet<SetUpBoxToo>();
				    set.addAll(customer.getSetupBoxes());
		
				    for (Iterator<SetUpBoxToo> it = set.iterator(); it.hasNext(); ) {
				    	SetUpBoxToo setupBox = it.next();
				    	
				    	logger.info("customer_id\t:"+customer_id);
				    	logger.info("boxnumber\t:"+setupBox.getBox_number());
				    	
				        @SuppressWarnings("unchecked")
						List<AddOnsEntity> list = session.createQuery("from AddOnsEntity a where a.customer_id = ? and a.box_number = ?")
				        	.setString(0, customer_id)
				        	.setString(1, setupBox.getBox_number()).list();
				        
				        setupBox.setAddOns(list);
				        set.add(setupBox);
				    }
				    
				    CustomerTo cust = new CustomerTo();
				    BeanUtils.copyProperties(customer, cust);
				    cust.setSetupBoxes(set);
			
			tx.commit();
			}catch (Exception e) {
				if(tx != null)tx.rollback();
				logger.error(e);
				e.printStackTrace();
				return customer;
			} finally{
				session.close();
			}
		} catch (Exception e) {
			//if(tx != null)tx.rollback();
			logger.error(e);
			return customer;
		} 
		
		return customer;
	}

	@SuppressWarnings("unchecked")
	@Override
	public CustomerAddInputTo getCustomerProfileById(String customer_id) {
		Customer cust = new Customer();
		CustomerAddInputTo customerData = new CustomerAddInputTo();
		
		TempCustomerId tempCustomerId = new TempCustomerId();
		
		Session session = null;
		Transaction tx = null;
		
		try {
			logger.info("CustomerDaoImpl.... getCustomerProfileById() - starting ");
			session = sfactory.openSession();
			tx = session.beginTransaction();
			
			cust = (Customer) session.load(Customer.class, customer_id);
			BeanUtils.copyProperties(cust, customerData);
			System.out.println((cust.getJoining_date()+ "").substring(0, 10));
			customerData.setJoining_date((cust.getJoining_date()+ "").substring(0, 10));
			customerData.setDeposit(new Double(cust.getDeposit()).toString());
			logger.info(customerData);
			List<SetupboxForAddCustomer> setupboxes = new ArrayList<SetupboxForAddCustomer>();
			
			for(SetupBox s: cust.getSetupBoxes()){
				if(!s.isPda_status()) {
				SetupboxForAddCustomer setupBox = new SetupboxForAddCustomer();
				BeanUtils.copyProperties(s, setupBox);
				setupBox.setStart_date((s.getStart_date()+"").substring(0, 10));
				
				PackageEntity packageEntity = (PackageEntity) session.load(PackageEntity.class, s.getPackage_id());
				SelectedPackageForCustomerAdd selPackage = new SelectedPackageForCustomerAdd();
				BeanUtils.copyProperties(packageEntity, selPackage);
				setupBox.setDiscount(s.getDiscount());
				logger.info("selectPackageForSetupBox" + selPackage);
				setupBox.setSelectedPackage(selPackage);
				
				Cart cart = new Cart();
				List<Items> items = new ArrayList<Items>();
				/*
					List<AddOnsEntity> addons = session.createQuery("from AddOnsEntity a where a.box_number = ?").setString(0, s.getBox_number()).list();
					for(AddOnsEntity addon : addons){
						
						Items item = new Items();
						
						
						
						Channels channel = (Channels) session.createQuery("from Channels c where c.channel_name = ?").setString(0, addon.getAddon()).list().get(0);
						BigInteger qty = (BigInteger) session.createSQLQuery("SELECT 12 * (YEAR(?) - YEAR(?)) + (MONTH(?)  - MONTH(?)) as value")
								.setDate(0, addon.getExpiry_date())
								.setDate(1, addon.getStart_date())
								.setDate(2, addon.getExpiry_date())
								.setDate(3, addon.getStart_date()).uniqueResult();
								
						item.set_id(new Integer(channel.getChannel_id()).toString());
						item.set_name(addon.getAddon());
						item.set_price(channel.getChannel_amount());
						item.set_quantity(qty.intValue());
						
						items.add(item);
					}*/
					cart.setItems(items);
					logger.info(cart);
				setupBox.setCart(cart);
				logger.info(setupBox);
				
				setupboxes.add(setupBox);
			}
			}
			customerData.setSetupboxes(setupboxes);
			List<AreaCustomerMapEntity> areaResult = session.createQuery("from AreaCustomerMapEntity ae where ae.endDate is null and ae.customerId = ?")
					.setString(0, customer_id).list();
			if(areaResult.size() > 0){
			customerData.setAreaCustomerMap(areaResult.get(0));
			}
			List<EditCustomerCommentEntity> comments = session.createQuery("from EditCustomerCommentEntity ecc where ecc.customer_id = ?").setString(0, customer_id).list();
			if(comments.size() > 0)
			customerData.setComments(CommonUtils.covertTransferObject(comments));
			else
				customerData.setComments(new ArrayList<EditCustomerCommentTo>());
			logger.info(customerData);
			tx.commit();
			
		}  catch (HibernateException e) {
			e.printStackTrace();
			logger.error("CustomerDaoImpl - getCustomerProfileById" + e);
			return customerData;
		} catch (Exception e) {
			logger.error("CustomerDaoImpl - getCustomerProfileById" + e);
			return customerData;
		} finally {
			if (session != null)
				session.close();
		}
		logger.debug("CustomerDaoImpl - getCustomerProfileById" + tempCustomerId);
		return customerData;
	}

	@Override
	public String activateStatus(InputIds input) {

		String res = "Done";
		Session session = null;
		Transaction tx = null;
		try {
			session = sfactory.openSession();
			// Session session=HibernateUtil.getSession();
			tx = session.beginTransaction();
			Customer customer = (Customer) session.load(Customer.class,
					input.getCustomer_id());
			
			//if(!customer.isPermenent_status()){
			for(SetupBox setupBox:customer.getSetupBoxes()){
				SetupBox setup = (SetupBox) session.createQuery("from SetupBox s where s.box_number = ?").setString(0, setupBox.getBox_number()).list().get(0);
				setup.setStatus(true);
				setup.setEmployee_id(input.getEmployee_id());
			}
			
			customer.setStatus(true);
			
			session.update(customer);

			tx.commit();
		} catch (Exception e) {
			logger.error(e);
			return "No row Found with the id";
		} finally {
			if (session != null)
				session.close();
		}
		return res;

	}

	@Override
	public String deActivateStatus(InputIds input) {

		String res = "Done";
		Session session = null;
		try {
			session = sfactory.openSession();
			// Session session=HibernateUtil.getSession();
			Transaction tx = session.beginTransaction();
			Customer customer = (Customer) session.load(Customer.class,
					input.getCustomer_id());
			//System.out.println(customer.getSetupBoxes());
			for(SetupBox setupBox:customer.getSetupBoxes()){
				SetupBox setup = (SetupBox) session.createQuery("from SetupBox s where s.box_number = ?").setString(0, setupBox.getBox_number()).list().get(0);
				setup.setStatus(false);
				setup.setDiscount(0.0);
				setup.setEmployee_id(input.getEmployee_id());
			}
			customer.setStatus(false);
			session.update(customer);
			tx.commit();
		} catch (Exception e) {
			logger.error(e);
			return "No row Found with the id";
		} finally {
			if (session != null)
				session.close();
		}
		return res;

	}

	@SuppressWarnings("unchecked")
	@Override
	public CustomerWrapperTo searchCustomers(SearchCustomer searchCustomer) {

		CustomerWrapperTo customerWrapperTo = null;
		
		StringBuffer attachQuery = new StringBuffer("from Customer c ");
		boolean isSearchParametersExist = false;
		if (CommonUtils.exists(searchCustomer.getCustomer_id())
				|| CommonUtils.exists(searchCustomer.getCustomer_name())
				|| CommonUtils.exists(searchCustomer.getContact_number())
				|| CommonUtils.exists(searchCustomer.getAreacode())) {
			attachQuery.append("where ");
			isSearchParametersExist = true;
		}

		if (CommonUtils.exists(searchCustomer.getCustomer_id())) {

			attachQuery.append("c.customer_id like \'%");
			attachQuery.append(searchCustomer.getCustomer_id());
			attachQuery.append("%\'");
			attachQuery.append(" AND ");

		}
		if (CommonUtils.exists(searchCustomer.getCustomer_name())) {

			attachQuery.append("c.customer_name like  \'%");
			attachQuery.append(searchCustomer.getCustomer_name());
			attachQuery.append("%\'");
			attachQuery.append(" AND ");

		}
		if (CommonUtils.exists(searchCustomer.getContact_number())) {

			attachQuery.append("c.contact_number like \'%");
			attachQuery.append(searchCustomer.getContact_number());
			attachQuery.append("%\'");
			attachQuery.append(" AND ");

		}
		if (CommonUtils.exists(searchCustomer.getAreacode())) {

			attachQuery.append("c.areacode like \'%");
			attachQuery.append(searchCustomer.getAreacode());
			attachQuery.append("%\'");
			attachQuery.append(" AND");
		}

		String searchQuery = attachQuery.toString();
		if (isSearchParametersExist) {
			searchQuery = attachQuery.toString().substring(0,
					attachQuery.length() - 4);
			searchQuery = searchQuery + "";
		}

		logger.info(searchQuery);
		Session session = sfactory.openSession();
		Transaction tx = null;
		Query query = null;
		List<Customer> list = null;
		try {
			
			tx = session.beginTransaction();
			query = session.createQuery(searchQuery);
			list = query.list();
			for(Customer cust :list){
				for(SetupBox box:cust.getSetupBoxes()){
					box.getAddon_ids();
					/*for(AddOnsEntity addonEtity:box.getAdd_onchannels()){
						addonEtity.getAddon();
					}*/
				}
			}
			List<CustomerTo> custs =  new ArrayList<CustomerTo>();
			
			for(Customer cust:list){
				CustomerTo custTo = new CustomerTo();
				BeanUtils.copyProperties(cust, custTo);
				Set<SetUpBoxToo> setupBoxList = new HashSet<SetUpBoxToo>();
				for(SetupBox setupBox:cust.getSetupBoxes()){
					SetUpBoxToo setupboxTo = new SetUpBoxToo();
					BeanUtils.copyProperties(setupBox, setupboxTo);
					//List<AddOnsEntity> addOnEntity = new ArrayList<AddOnsEntity>();
					/*for(AddOnsEntity addOn:setupBox.getAdd_onchannels()){
						addOnEntity.add(addOn);
					}*/
					//setupboxTo.setAddOns(addOnEntity);
					setupBoxList.add(setupboxTo);
					
				}
				custTo.setSetupBoxes(setupBoxList);
				custs.add(custTo);
				//customerWrapperTo.setCustomerTo(custs.add(custTo));
			}
			
			//BeanUtils.copyProperties(list, custs);
			customerWrapperTo = new CustomerWrapperTo(custs);
			//BeanUtils.cop
			//BeanUtils.copyProperties(custs, customerWrapperTo);
			//System.out.println("An object: " + ToStringBuilder.reflectionToString(list));
			System.out.println(customerWrapperTo);
			for(CustomerTo cust:customerWrapperTo.getCustomerTo()){
				
				double totalRent = 0;
				for(SetUpBoxToo setupBox:cust.getSetupBoxes()){
					
					//BigDecimal packAmount = new BigDecimal(0);
					//BigDecimal addonAmount = new BigDecimal(0);
					double packAmount = 0;
					BigDecimal addonAmount = new BigDecimal(0);
					
					if(CommonUtils.exists(setupBox.getAssigned_package())){
						String pack = setupBox.getAssigned_package();
						if(!setupBox.getAssigned_package().equalsIgnoreCase("A-la-Carte")){
						packAmount = (double) session.createQuery("Select package_amount from PackageEntity where package_name=? and client_id =?").setString(0, pack).setString(1, setupBox.getClient_id()).uniqueResult();
						//packAmount.add(new BigDecimal(amt));
						setupBox.setPackage_amount(packAmount);
						}else{
							setupBox.setPackage_amount(0);
						}
					}
					if(CommonUtils.exists(setupBox.getAddonNames())){
						String[] add_ons = setupBox.getAddonNames().split(",");
						
						StringBuffer  addOnnSearch = new StringBuffer("(");
						for(String s:add_ons){
							addOnnSearch.append("\'"+s+"\',");
						}
						logger.info(addOnnSearch);
						
						//String add_on_string = addOnnSearch.toString().substring(0, addOnnSearch.length() - 1);
						String addOnQuery = "SELECT SUM(channel_amount) from channels WHERE channel_name in "+addOnnSearch.toString().substring(0,addOnnSearch.length()-1)+") and client_id =?;";
						
						SQLQuery query1 = session.createSQLQuery(addOnQuery);
						addonAmount = (BigDecimal) query1.setString(0, setupBox.getClient_id()).uniqueResult();
						if(addonAmount == null || addonAmount.equals(0)){addonAmount = new BigDecimal(0.0);
						}
						setupBox.setAddon_amount(addonAmount);
						
					}else{
						setupBox.setAddon_amount(new BigDecimal(0.0));
					}
					double monthlyRent;
					
					monthlyRent = addonAmount.doubleValue() + packAmount - setupBox.getDiscount();
					setupBox.setTotal_amount(monthlyRent); 
					totalRent = totalRent + monthlyRent;
				}
				cust.setTotalRent(totalRent);
			}
			
			tx.commit();
		} catch (Exception e) {
			logger.error(e);
			e.printStackTrace();
		} finally {
				session.close();
		}
		return customerWrapperTo;

	}

	public String billPayment(String customer_id, double paid, double balance,
			double total, String employee_id, String client_id) {

		Session session = sfactory.openSession();
		Transaction tx = null;
		
		try {
			tx = session.beginTransaction();
			StringBuffer spQuery = new StringBuffer("");
			
			CustomerTo customerTo = new CustomerTo();
			customerTo.setCustomer_id(customer_id);
			
			CustomerBillTo billTo = new CustomerBillTo();
			BeanUtils.copyProperties(getCustomerWithBillingDetails(customerTo), billTo);
			
			total = billTo.getTotal_amount().doubleValue() ;
			
			balance = total - paid;
			String bill_id = billIdGenerator.getNextSid(session, client_id);
			spQuery.append("CALL MakePayment  (:bill_id, :cus_id, :paid, :balance, :total, :emp_id, :client_id)");
			logger.info(employee_id + "\t" + customer_id + "\t" + paid + "\t"
					+ balance + "\t" + total+ "\t" + bill_id);
			SQLQuery callStoredProcedure_MSSQL = session.createSQLQuery(
					spQuery.toString()).addEntity(Billing.class);
			callStoredProcedure_MSSQL.setString("bill_id", bill_id);
			callStoredProcedure_MSSQL.setString(CUS_ID, customer_id);
			callStoredProcedure_MSSQL.setDouble("paid", paid);
			callStoredProcedure_MSSQL.setDouble("balance", balance);
			callStoredProcedure_MSSQL.setDouble("total", total);
			callStoredProcedure_MSSQL.setString("emp_id", employee_id);
			callStoredProcedure_MSSQL.setString("client_id", client_id);

			logger.info(callStoredProcedure_MSSQL.getQueryString());
			System.out.println(callStoredProcedure_MSSQL.executeUpdate());
			
			Authentication clientCredetnials = (Authentication) session.createQuery("from Authentication a where a.client_id = ?")
					.setParameter(0, client_id).list().get(0);
			
			if(clientCredetnials.isMsg_alert()){
				// 1) templateID, 2) paidAmount, 3) Cable Tv, 4) bill_recipt_no, 5) Regards(client name)
				String clientContactNumber = clientCredetnials.getClientContactNumber();
				if(!CommonUtils.exists(clientContactNumber))clientContactNumber = "";
				DetailsToSend details = new DetailsToSend("43217", paid + "", "", bill_id, clientCredetnials.getClient_name(), clientContactNumber);
				
				
				//DetailsToSend details = new DetailsToSend(customer_id, client_id, MsgFormats.getPayedSMS(paid, bill_id, clientCredetnials.getClient_name()));
				details.setContact_number((String) session.createQuery("select c.contact_number from Customer c where c.customer_id = ?").setString(0, customer_id).uniqueResult());
				SMSGateWayEntity smsGateWayEntity = (SMSGateWayEntity) session.load(SMSGateWayEntity.class, 2);
				
				SMSGateWayEntity smsGateWay = new SMSGateWayEntity();
				BeanUtils.copyProperties(smsGateWayEntity, smsGateWay);
				
				/*SendSMSRunnable sendSMSThread = new SendSMSRunnable(session, details, smsGateWay);
				Thread t = new Thread(sendSMSThread);
				t.start();*/
				
				SendSMSThroughBSMS sendSms = new SendSMSThroughBSMS(clientCredetnials.getClient_id(),"102",details.getContact_number(),
		                  BigDecimal.valueOf(paid).setScale(0, BigDecimal.ROUND_HALF_UP).toString(),
		                  " MYCABLE ",bill_id,clientCredetnials.getSmsRegardsMsg(),clientContactNumber);

				Thread sendSmsThread = new Thread(sendSms);
				sendSmsThread.start();
				
				
				logger.info("if(clientCredetials.isMsg_alert() is ended.........."); 
				
				//System.out.println(SendSMS.sendSMSToCustomer(session, details));
					session.createSQLQuery("update authentication set msg_count = msg_count+1 where client_id = ?").setParameter(0, client_id).executeUpdate();
				
				
				
			}
			
			
			tx.commit();
		}catch (HibernateException e) {
			if (tx != null)
				tx.rollback();

			logger.error("Exception! " + e);
			return "payment failed";
		} catch (Exception e) {
			if (tx != null)
				tx.rollback();
			logger.error(e);
			return "payment failed";
		} finally {
			session.close();
		}
		logger.info("Payment Successful\nCustomer_id:" +customer_id + "\nPaidAmount : " + paid + "\nEmployeeId : " + employee_id + "\nclientId : " + client_id);
		return "Payment Successful";

	}

	@SuppressWarnings("unchecked")
	@Override
	public CustomerBillTo getCustomerWithBillingDetails(
			CustomerTo customerCredentials) {
		CustomerBillTo resultset = new CustomerBillTo();
		boolean paid_status = true;
		Session session = sfactory.openSession();
		Transaction tx = null;
		try {
			tx = session.beginTransaction();
			if(CommonUtils.exists(customerCredentials.getC_id())){
				customerCredentials.setCustomer_id((String) session.createQuery("select c.customer_id from Customer c where c_id=? and client_id = ?")
						.setString(0, customerCredentials.getC_id()).setString(1, customerCredentials.getClient_id()).uniqueResult());
			}if(CommonUtils.exists(customerCredentials.getContact_number())){
				customerCredentials.setCustomer_id((String) session.createQuery("select c.customer_id from Customer c where contact_number=?")
						.setString(0, customerCredentials.getContact_number()).uniqueResult());
			}if(CommonUtils.exists(customerCredentials.getVc_number())){
				customerCredentials.setCustomer_id((String) session.createSQLQuery("select s.customer_id from setupbox s where s.vc_number=? or s.box_number = ?")
						.setString(0, customerCredentials.getVc_number())
						.setString(1, customerCredentials.getVc_number()).uniqueResult());
			}
			SQLQuery query = session
					.createSQLQuery("select * from bill_generation where customer_id=\'"
							+ customerCredentials.getCustomer_id() + "\' and paid_status = 0;");
			@SuppressWarnings("rawtypes")
			List res = (List) query.list();
			if(res.size() > 0)paid_status = false;
			logger.info(paid_status);
			if (paid_status) {

				if (!CommonUtils.exists(customerCredentials.getCustomer_id())) {
					customerCredentials.setCustomer_id(null);
				}
				if (!CommonUtils.exists(customerCredentials.getCustomer_name())) {
					customerCredentials.setCustomer_name(null);
				}
				if (CommonUtils.exists(customerCredentials.getCustomer_id())
						|| CommonUtils.exists(customerCredentials
								.getCustomer_name())) {
				Query query2 = session.createSQLQuery("call GetCustomerBillDetails_Windows_afterpaid(:cus_id, :cus_name);");

				List<CustomerBillTo> result = query2
						.setString("cus_id", customerCredentials.getCustomer_id())
						.setString("cus_name", customerCredentials.getCustomer_name())
						.setResultTransformer(
						Transformers.aliasToBean(CustomerBillTo.class)).list();
				for (int i = 0; i < result.size(); i++) {
					resultset = (CustomerBillTo) result.get(i);
				}
				}
			} else {
			
			
			if (!CommonUtils.exists(customerCredentials.getCustomer_id())) {
				customerCredentials.setCustomer_id(null);
			}
			if (!CommonUtils.exists(customerCredentials.getCustomer_name())) {
				customerCredentials.setCustomer_name(null);
			}
			if (CommonUtils.exists(customerCredentials.getCustomer_id())
					|| CommonUtils.exists(customerCredentials
							.getCustomer_name())) {
				logger.info("Calling GetCustomerBillDetails_Windows");

				List<CustomerBillTo> resultWithAliasedBean = session
						.createSQLQuery(
								"CALL GetCustomerBillDetails_Windows(:cus_id, :cus_name)")
						.setString(CUS_ID, customerCredentials.getCustomer_id())
						.setString("cus_name",
								customerCredentials.getCustomer_name())
						.setResultTransformer(
								Transformers.aliasToBean(CustomerBillTo.class))
						.list();
				resultset = (CustomerBillTo) resultWithAliasedBean.get(0);
			}
			}
			tx.commit();
		} catch (HibernateException e) {
			if (tx != null)
				tx.rollback();
			e.printStackTrace();
			logger.error("Exception! " + e);
		} catch (Exception e) {
			if (tx != null)
				tx.rollback();
			e.printStackTrace();
			logger.error("Exception! " + e);
		} finally {
			session.close();
		}
		return resultset;
	}

	@Override
	public List<MiniStatementForAndroidTo> miniStatement(String customer_id) {
		List<MiniStatementForAndroidTo> resultset = new ArrayList<MiniStatementForAndroidTo>();

		Session session = sfactory.openSession();
		Transaction tx = null;
		try {
			tx = session.beginTransaction();

			logger.info("Calling ministatement SP");

			@SuppressWarnings({ "rawtypes", "unchecked" })
			List<MiniStatementForAndroidTo> resultWithAliasedBean = (List) session
					.createSQLQuery("CALL GetMiniStatement(:cus_id)")
					.addEntity(MiniStatementForAndroidTo.class)
					.setString(CUS_ID, customer_id).list();

			List<MiniStatementForAndroidTo> result = new ArrayList<MiniStatementForAndroidTo>();

			for (MiniStatementForAndroidTo res : resultWithAliasedBean) {

				if (res.getBalance() == null)
					res.setBalance(new BigDecimal(0));
				result.add(res);

			}

			resultset = result;

			tx.commit();
		} catch (HibernateException e) {
			if (tx != null)
				tx.rollback();
			e.printStackTrace();
			logger.error("Exception! " + e);
		} finally {
			session.close();
		}
		return resultset;
	}

	@Override
	public List<PackageReportsForAndroidTo> getPackageReports() {
		List<PackageReportsForAndroidTo> resultset = new ArrayList<PackageReportsForAndroidTo>();

		Session session = null;
		Transaction tx = null;
		try {
			session = sfactory.openSession();
			tx = session.beginTransaction();
			logger.info("Calling reportstest SP");

			@SuppressWarnings({ "rawtypes", "unchecked" })
			List<PackageReportsForAndroidTo> resultWithAliasedBean = (List) session
					.createSQLQuery("CALL reportstest()")
					.setResultTransformer(
							Transformers
									.aliasToBean(PackageReportsForAndroidTo.class))
					.list();

			resultset = resultWithAliasedBean;

			tx.commit();
		} catch (HibernateException e) {
			if (tx != null)
				tx.rollback();
			e.printStackTrace();
			logger.error("Exception! " + e);
		} finally {
			if (session != null)
				session.close();
		}
		return resultset;
	}

	@SuppressWarnings("unchecked")
	@Override
	public List<SearchCustomerTo> getDeactivatedCustomers(String client_id) {

		List<SearchCustomerTo> list = null;
		Session session = sfactory.openSession();
		Transaction tx = null;
		try {
			tx = session.beginTransaction();
			list = session
					.createSQLQuery(CommonUtils.searchCustoemrQueryInActive+" where bill.status = 0 and bill.client_id='"+client_id+"'")
					.setResultTransformer(Transformers.aliasToBean(SearchCustomerTo.class))
					.list();
			
			
			tx.commit();
		} catch (Exception e) {
			if (tx != null)
				tx.rollback();
			logger.error("Exception! " + e);
		} finally {
			session.close();
		}

		return list;
	}

	@SuppressWarnings("unchecked")
	@Override
	public List<SearchCustomerTo> getUnPaidCustomers(String client_id) {
		List<SearchCustomerTo> list = null;
		Session session = sfactory.openSession();
		Transaction tx = null;
		try {
			tx = session.beginTransaction();
			StringBuffer searchQuery = new StringBuffer(CommonUtils.searchCustoemrQuery);
			searchQuery.append(" join bill_generation b on bill.customer_id = b.customer_id AND b.paid_status = 0 AND bill.client_id='"+client_id+"'");
			list = session
					.createSQLQuery(searchQuery.toString()).setResultTransformer(Transformers.aliasToBean(SearchCustomerTo.class))
					.list();
			tx.commit();
		} catch (Exception e) {
			if (tx != null)
				tx.rollback();
			logger.error("Exception! " + e);
		} finally {
			session.close();
		}

		return list;
	}

	@SuppressWarnings("unchecked")
	@Override
	public String makePaymentFromDesktop(PaymentFields paymentfields) {

		Session session = sfactory.openSession();
		Transaction tx = null;
		List<Employee> employeeList = null;
		if(CommonUtils.exists(paymentfields.getEmployee_id()) && (!paymentfields.getEmployee_id().equals("null"))){
		 employeeList = session.createQuery("from Employee where client_id = ? and  employee_id = ?")
		.setString(0, paymentfields.getClient_id())
		.setString(1, paymentfields.getEmployee_id())
		.list(); 
		}else{
			employeeList = session.createQuery("from Employee where client_id = ? and  employee_name = \'Admin\'")
					.setString(0, paymentfields.getClient_id())
					.setString(1, paymentfields.getEmployee_id())
					.list();
		}
		
		if(employeeList.size() > 0){
			paymentfields.setEmployee_id(employeeList.get(0).getEmployee_id());
		} else {
			Employee employee = new Employee();
			
			employee.setEmployee_name("Admin");
			employee.setCity("Bangalore");
			employee.setClient_id(paymentfields.getClient_id());
			edao.addEmployee(employee, new ArrayList<AreaEmployeeMapEntity>());
			@SuppressWarnings("unchecked")
			List<Employee> emp = session.createQuery("from Employee where client_id = ? and employee_name = \'Admin\'").setString(0, paymentfields.getClient_id()).list(); 
			paymentfields.setEmployee_id(emp.get(0).getEmployee_id());
		}
		double balance = paymentfields.getBalance();
		
		CustomerTo customerTo = new CustomerTo();
		customerTo.setCustomer_id(paymentfields.getCustomer_id());
		
		CustomerBillTo billTo = new CustomerBillTo();
		BeanUtils.copyProperties(getCustomerWithBillingDetails(customerTo), billTo);
		
		double total_amount = billTo.getTotal_amount().doubleValue() ;
		
		
		double paid_amount = paymentfields.getPaid_amount();
		double discount = paymentfields.getDiscount();
		String customer_id = paymentfields.getCustomer_id();
		String employee_id = paymentfields.getEmployee_id();
		String bill_id = billIdGenerator.getNextSid(session, paymentfields.getClient_id());
		try {
			tx = session.beginTransaction();
			StringBuffer spQuery = new StringBuffer("");

			balance = total_amount - (paid_amount + discount);

			spQuery.append("CALL MakePaymentFromDesktop  (:bill_id, :cus_id, :paid, :balance, :total, :emp_id, :payment_type, :cheque_No, :discount, :client_id, :tx_id )");
			logger.info(employee_id + "\t" + customer_id + "\t" + paid_amount
					+ "\t" + balance + "\t" + total_amount);
			SQLQuery callStoredProcedure_MSSQL = session.createSQLQuery(
					spQuery.toString());
			callStoredProcedure_MSSQL.setString("bill_id", bill_id);
			callStoredProcedure_MSSQL.setString("tx_id", paymentfields.getTransaction_id());
			callStoredProcedure_MSSQL.setString(CUS_ID, customer_id);
			callStoredProcedure_MSSQL.setDouble("paid", paid_amount);
			callStoredProcedure_MSSQL.setDouble("balance", balance);
			callStoredProcedure_MSSQL.setDouble("total", total_amount);
			callStoredProcedure_MSSQL.setString("emp_id", employee_id);
			callStoredProcedure_MSSQL.setString("cheque_No", paymentfields.getCheque_No());
			callStoredProcedure_MSSQL.setString("payment_type", paymentfields.getPayment_type());
			callStoredProcedure_MSSQL.setDouble("discount", discount);
			callStoredProcedure_MSSQL.setString("client_id", paymentfields.getClient_id());

			logger.info(callStoredProcedure_MSSQL.getQueryString());
			String recipt_no = (String) (callStoredProcedure_MSSQL.uniqueResult()).toString();

			Authentication clientCredetnials = (Authentication) session.createQuery("from Authentication a where a.client_id = ?")
					.setParameter(0, paymentfields.getClient_id()).list().get(0);
			
			if(clientCredetnials.isMsg_alert()){
				//DetailsToSend details = new DetailsToSend(customer_id, paymentfields.getClient_id(), MsgFormats.getPayedSMS(paid_amount, recipt_no, clientCredetnials.getClient_name()));
				
				// 1) templateID, 2) paidAmount, 3) Cable Tv, 4) bill_recipt_no, 5) Regards(client name)
				String clientContactNumber = clientCredetnials.getClientContactNumber();
				if(!CommonUtils.exists(clientContactNumber))clientContactNumber = "";
				DetailsToSend details = new DetailsToSend("43217", paid_amount + "", "", recipt_no, clientCredetnials.getClient_name(), clientContactNumber);
				
				
				//DetailsToSend details = new DetailsToSend(customer_id, client_id, MsgFormats.getPayedSMS(paid, bill_id, clientCredetnials.getClient_name()));
				String contact = (String) session.createQuery("select c.contact_number from Customer c where c.customer_id = ?").setString(0, paymentfields.getCustomer_id()).uniqueResult();
				logger.info("Contact No # " + contact);
				details.setContact_number(contact);
				SMSGateWayEntity smsGateWayEntity = (SMSGateWayEntity) session.load(SMSGateWayEntity.class, 2);
				logger.info("Details to Send SMS: " + details);
				SMSGateWayEntity smsGateWay = new SMSGateWayEntity();
				BeanUtils.copyProperties(smsGateWayEntity, smsGateWay);
				
				/*SendSMSRunnable sendSMSThread = new SendSMSRunnable(session, details, smsGateWay);
				Thread t = new Thread(sendSMSThread);
				t.start();*/
				
				SendSMSThroughBSMS sendSms = new SendSMSThroughBSMS(clientCredetnials.getClient_id(),"102",contact,
		                  BigDecimal.valueOf(paid_amount).setScale(0, BigDecimal.ROUND_HALF_UP).toString(),
		                  " MYCABLE ",bill_id,clientCredetnials.getSmsRegardsMsg(),clientContactNumber);

				Thread sendSmsThread = new Thread(sendSms);
				sendSmsThread.start();
				
				
				logger.info("if(clientCredetials.isMsg_alert() is ended.........."); 
				
				//System.out.println(SendSMS.sendSMSToCustomer(session, details));
				
				
			}
			
			logger.info(recipt_no);
			tx.commit();
			return recipt_no;
		} catch (Exception e) {
			if (tx != null)
				tx.rollback();
			logger.error(e);
			
			return "Payment not Completed";
			
		} finally {
			session.close();
		}
	}

	@Override
	public boolean billPayments(String customer_id, String employee_id,
			double paid, double balance, double total, String client_id, String paymentTimeStamp, String transactionId) {

		logger.info("###################################################################### Sync Started DAO IMPL ##############################################################");

		Session session = sfactory.openSession();
		Transaction tx = null;
		try {
			tx = session.beginTransaction();

			logger.info("Calling billPayments");

			StringBuffer spQuery = new StringBuffer("");
			
			CustomerTo customerTo = new CustomerTo();
			customerTo.setCustomer_id(customer_id);
			
			CustomerBillTo billTo = new CustomerBillTo();
			BeanUtils.copyProperties(getCustomerWithBillingDetails(customerTo), billTo);
			
			total = billTo.getTotal_amount().doubleValue() ;
			
			balance = total - paid;
			if(session.createSQLQuery("select * from billing where transaction_id = ? and client_id = ?").setString(0, transactionId).setString(1, client_id).list().size() == 0){
				spQuery.append("CALL MakePaymentForSynch  (:bill_id, :cus_id, :paid, :balance, :total, :emp_id, :client_id, :transaction_id, :payment_time_stamp)");
				String bill_id = billIdGenerator.getNextSid(session, client_id);
	
				/*logger.info(employee_id + "\t" + customer_id + "\t" + paid + "\t" + balance + "\t" + total);
				SQLQuery callStoredProcedure_MSSQL = session.createSQLQuery(
						spQuery.toString()).addEntity(CustomerBillPayment.class);
				callStoredProcedure_MSSQL.setString("bill_id", bill_id);
				callStoredProcedure_MSSQL.setString("cus_id", customer_id);
				callStoredProcedure_MSSQL.setDouble("paid", paid);
				callStoredProcedure_MSSQL.setDouble("balance", balance);
				callStoredProcedure_MSSQL.setDouble("total", total);
				callStoredProcedure_MSSQL.setString("emp_id", employee_id);
				callStoredProcedure_MSSQL.setString("client_id", client_id);
				callStoredProcedure_MSSQL.setString("transaction_id", transactionId);
				callStoredProcedure_MSSQL.setDate("payment_time_stamp", new SimpleDateFormat("yyyy-MM-dd HH:mm:ss").parse(paymentTimeStamp));
	
				logger.info(callStoredProcedure_MSSQL.getQueryString());
				//int res = callStoredProcedure_MSSQL.executeUpdate();
*/				
					Billing billingTx = new Billing();
					billingTx.setCustomer_id(customer_id);
					billingTx.setDate(new SimpleDateFormat("yyyy-MM-dd HH:mm:ss").parse(paymentTimeStamp));
					billingTx.setBill_recipt_no(bill_id);
					billingTx.setBalance_amount(balance);
					billingTx.setPaid_amount(paid);
					billingTx.setTotal_amount(total);
					billingTx.setEmployee_id(employee_id);
					if(employee_id.equalsIgnoreCase("Online"))billingTx.setPayment_type("Online");
					else billingTx.setPayment_type("Cash");
					billingTx.setClient_id(client_id);
					billingTx.setTransaction_id(transactionId);
					
					logger.info("Billing Entity" + billingTx);
					session.evict(billingTx);
					int res = (int) session.save(billingTx);
					
					
						logger.info("billing inserted result is : " + res);
						session.createQuery("update BillGeneration set paid_status = ? where customer_id = ? and paid_status = ?")
						.setBoolean(0, true)
						.setString(1, customer_id)
						.setBoolean(2, false).executeUpdate();
						
				logger.info("res : " + res);
				
				Authentication clientCredetnials = (Authentication) session.createQuery("from Authentication a where a.client_id = ?")
						.setParameter(0, client_id).list().get(0);
				
				
				if(clientCredetnials.isMsg_alert()){
					// 1) templateID, 2) paidAmount, 3) Cable Tv, 4) bill_recipt_no, 5) Regards(client name)
					String clientContactNumber = clientCredetnials.getClientContactNumber();
					if(!CommonUtils.exists(clientContactNumber))clientContactNumber = "";
					DetailsToSend details = new DetailsToSend("43217", paid + "", "", bill_id, clientCredetnials.getClient_name(), clientContactNumber);
					
					
					//DetailsToSend details = new DetailsToSend(customer_id, client_id, MsgFormats.getPayedSMS(paid, bill_id, clientCredetnials.getClient_name()));
					details.setContact_number((String) session.createQuery("select c.contact_number from Customer c where c.customer_id = ?").setString(0, customer_id).uniqueResult());
					SMSGateWayEntity smsGateWayEntity = (SMSGateWayEntity) session.load(SMSGateWayEntity.class, 2);
					
					SMSGateWayEntity smsGateWay = new SMSGateWayEntity();
					BeanUtils.copyProperties(smsGateWayEntity, smsGateWay);
					
					/*SendSMSRunnable sendSMSThread = new SendSMSRunnable(session, details, smsGateWay);
					Thread t = new Thread(sendSMSThread);
					t.start();*/
					
					SendSMSThroughBSMS sendSms = new SendSMSThroughBSMS(clientCredetnials.getClient_id(),"102",details.getContact_number(),
			                  BigDecimal.valueOf(paid).setScale(0, BigDecimal.ROUND_HALF_UP).toString(),
			                  " MYCABLE ",bill_id,clientCredetnials.getSmsRegardsMsg(),clientContactNumber);
					Thread sendSmsThread = new Thread(sendSms);
					sendSmsThread.start();
					
					logger.info("if(clientCredetials.isMsg_alert() is ended.........."); 
					//System.out.println(SendSMS.sendSMSToCustomer(session, details));
					session.createSQLQuery("update authentication set msg_count = msg_count+1 where client_id = ?").setParameter(0, client_id).executeUpdate();
					
				}
				logger.info("Transaction is done.."); 
			}else if(!CommonUtils.exists(transactionId)){
				logger.info(".................................. Transaction id is null or empty .....................................................");
				spQuery.append("CALL MakePaymentForSynch  (:bill_id, :cus_id, :paid, :balance, :total, :emp_id, :client_id, :transaction_id, :payment_time_stamp)");
				String bill_id = billIdGenerator.getNextSid(session, client_id);

				logger.info(employee_id + "\t" + customer_id + "\t" + paid + "\t" + balance + "\t" + total);
				/*SQLQuery callStoredProcedure_MSSQL = session.createSQLQuery(
						spQuery.toString()).addEntity(CustomerBillPayment.class);
				callStoredProcedure_MSSQL.setString("bill_id", bill_id);
				callStoredProcedure_MSSQL.setString("cus_id", customer_id);
				callStoredProcedure_MSSQL.setDouble("paid", paid);
				callStoredProcedure_MSSQL.setDouble("balance", balance);
				callStoredProcedure_MSSQL.setDouble("total", total);
				callStoredProcedure_MSSQL.setString("emp_id", employee_id);
				callStoredProcedure_MSSQL.setString("client_id", client_id);
				callStoredProcedure_MSSQL.setString("transaction_id", transactionId);
				callStoredProcedure_MSSQL.setDate("payment_time_stamp", new SimpleDateFormat("yyyy-MM-dd HH:mm:ss").parse(paymentTimeStamp));

				logger.info(callStoredProcedure_MSSQL.getQueryString());
				int res = callStoredProcedure_MSSQL.executeUpdate();*/

				Billing billingTx = new Billing();
				billingTx.setCustomer_id(customer_id);
				billingTx.setDate(new SimpleDateFormat("yyyy-MM-dd HH:mm:ss").parse(paymentTimeStamp));
				billingTx.setBill_recipt_no(bill_id);
				billingTx.setBalance_amount(balance);
				billingTx.setPaid_amount(paid);
				billingTx.setTotal_amount(total);
				billingTx.setEmployee_id(employee_id);
				billingTx.setClient_id(client_id);
				billingTx.setTransaction_id(transactionId);
				logger.info("Billing Entity" + billingTx);
				int res = (int) session.save(billingTx);
				
				
					logger.info("billing inserted result is : " + res);
					session.createQuery("update BillGeneration set paid_status = ? where customer_id = ? and paid_status = ?")
					.setBoolean(0, true)
					.setString(1, customer_id)
					.setBoolean(2, false).executeUpdate();
					
					logger.info("res : " + res);
				
				
				Authentication clientCredetnials = (Authentication) session.createQuery("from Authentication a where a.client_id = ?")
						.setParameter(0, client_id).list().get(0);
				
				
				if(clientCredetnials.isMsg_alert()){
					String clientContactNumber = clientCredetnials.getClientContactNumber();
					if(!CommonUtils.exists(clientContactNumber))clientContactNumber = "";
					DetailsToSend details = new DetailsToSend("43217", paid + "", "", bill_id, clientCredetnials.getClient_name(), clientContactNumber);
					
					details.setContact_number((String) session.createQuery("select c.contact_number from Customer c where c.customer_id = ?").setString(0, details.getCustomer_id()).uniqueResult());
					SMSGateWayEntity smsGateWayEntity = (SMSGateWayEntity) session.load(SMSGateWayEntity.class, 1);
					
					SMSGateWayEntity smsGateWay = new SMSGateWayEntity();
					BeanUtils.copyProperties(smsGateWayEntity, smsGateWay);
					
					/*SendSMSRunnable sendSMSThread = new SendSMSRunnable(session, details, smsGateWay);
					Thread t = new Thread(sendSMSThread);
					t.start();*/
					
					SendSMSThroughBSMS sendSms = new SendSMSThroughBSMS(clientCredetnials.getClient_id(),"102",details.getContact_number(),
			                  BigDecimal.valueOf(paid).setScale(0, BigDecimal.ROUND_HALF_UP).toString(),
			                  " MYCABLE ",bill_id,clientCredetnials.getSmsRegardsMsg(),clientContactNumber);
					Thread sendSmsThread = new Thread(sendSms);
					sendSmsThread.start();
					
					
					logger.info("if(clientCredetials.isMsg_alert() is ended.........."); 
					//System.out.println(SendSMS.sendSMSToCustomer(session, details));
					session.createSQLQuery("update authentication set msg_count = msg_count+1 where client_id = ?").setParameter(0, client_id).executeUpdate();
					
				}
				logger.info("Transaction is done.."); 
				
			}
			else{
				logger.info("Transaction with same timestamp is existed already ......................................");
			}
			tx.commit();
			logger.info("Transaction is Committed ...................."); 
			logger.info("###################################################################### Sync Ended DAO IMPL ##############################################################");

			return true;
		} catch (HibernateException e) {
			if (tx != null)
				tx.rollback();

			logger.error("Exception! " + e);
			return false;

		}catch (Exception e) {
			if (tx != null)
				tx.rollback();

			logger.error("Exception! " + e);
			return false;
		} finally {
			logger.info("###################################################################### Sync Started DAO IMPL Finally Block ##############################################################");
			session.close();
		}

	}

	@SuppressWarnings("unchecked")
	@Override
	public CustomerToMobile getCustomerWithBillingDetails(String customer_id) {

		CustomerToMobile customerToMobile = new CustomerToMobile();
		boolean status = true;
		Session session = null;
		Transaction tx = null;
		try {
			session = sfactory.openSession();
			tx = session.beginTransaction();
			SQLQuery query = session
					.createSQLQuery("select * from bill_generation where customer_id=\'"
							+ customer_id + "\' and paid_status = 0;");
			@SuppressWarnings("rawtypes")
			List res = (List) query.list();
			if(res.size() > 0)status = false;
			//logger.info(status);
			if (status) {

				/*StringBuffer queryToGetCustomerToMobile = new StringBuffer();
				queryToGetCustomerToMobile.append("select c.customer_id,c.customer_name,c.contact_number,b.balance_amount,bill.package_monthly_amount");
				queryToGetCustomerToMobile.append(" AS 'package_amount',bill.addon_monthly_amount AS 'addon_amount',"
								+ "(bill.addon_monthly_amount+bill.package_monthly_amount) as monthly_rent, (b.balance_amount) as 'total_amount'");
				queryToGetCustomerToMobile.append(" from mycable.customer c join mycable.billing b ");
				queryToGetCustomerToMobile.append(" on c.customer_id = b. customer_id");
				queryToGetCustomerToMobile.append(" join bill_generation bill");
				queryToGetCustomerToMobile.append(" on bill.customer_id=c.customer_id");
				queryToGetCustomerToMobile.append("	where c.customer_id =\'"+ customer_id
								+ "\'  order by b.date desc LIMIT 1");*/

				Query query2 = session.createSQLQuery("call GetCustomerBillDetails_afterpaid(:cus_id);")
						.setString("cus_id", customer_id)
						.setResultTransformer(
								Transformers.aliasToBean(CustomerToMobile.class));

				List<CustomerToMobile> result = query2.list();
				for (int i = 0; i < result.size(); i++) {
					customerToMobile = (CustomerToMobile) result.get(i);
					//System.out.println(customerToMobile.getAddon_amount());
				}
			} else {

				Query query1 = session
						.createSQLQuery("CALL GetCustomerBillDetails(:cus_id)")
						.setParameter(CUS_ID, customer_id)
						.setResultTransformer(
								Transformers.aliasToBean(CustomerToMobile.class));

				List<CustomerToMobile> result = query1.list();
				for (int i = 0; i < result.size(); i++) {
					customerToMobile = (CustomerToMobile) result.get(i);
					//System.out.println(customerToMobile.getAddon_amount());
				}
			}
			tx.commit();
		} catch (HibernateException e) {
			logger.error(e);
		} finally {
			if (session != null)
				session.close();
		}
		return customerToMobile;
	}

	@Override
	public ViewBillsByMonthTo viewBillsByMonth(CustomerCredentials customerCredentials) {
		ViewBillsByMonthTo resultset = null;

		Session session = sfactory.openSession();
		Transaction tx = null;
		try {
			tx = session.beginTransaction();
			
			//Retrieving customer_id by using c_id.
			if(CommonUtils.exists(customerCredentials.getCustomer_id())){
				customerCredentials.setCustomer_id((String) session.createQuery("select c.customer_id from Customer c where c_id=?").setString(0, customerCredentials.getCustomer_id()).uniqueResult());
			}
			
			if (!CommonUtils.exists(customerCredentials.getCustomer_id())) {
				customerCredentials.setCustomer_id(null);
			}
			if (!CommonUtils.exists(customerCredentials.getCustomer_name())) {
				customerCredentials.setCustomer_name(null);
			}
			if (CommonUtils.exists(customerCredentials.getCustomer_id())
					|| CommonUtils.exists(customerCredentials
							.getCustomer_name())) {
				logger.info("Calling GetCustomerBillDetails_Windows");

				@SuppressWarnings("unchecked")
				List<ViewBillsByMonthTo> resultWithAliasedBean = session
						.createSQLQuery("CALL ViewCustomerBillDetails(:cus_id, :cus_name, :_month)")
						.setString(CUS_ID, customerCredentials.getCustomer_id())
						.setString("cus_name", customerCredentials.getCustomer_name())
						.setInteger("_month", customerCredentials.getMonth())
						.setResultTransformer(Transformers.aliasToBean(ViewBillsByMonthTo.class))
						.list();

				resultset = (ViewBillsByMonthTo) resultWithAliasedBean.get(0);
			}
			tx.commit();
		} catch (HibernateException e) {
			if (tx != null)
				tx.rollback();
			e.printStackTrace();
			logger.error("Exception! " + e);
		} catch (Exception e) {
			if (tx != null)
				tx.rollback();
			e.printStackTrace();
			logger.error("Exception! " + e);
		} finally {
			session.close();
		}
		return resultset;
	}

	@Override
	public String updateBox(SetupBoxTo setupBoxto) {
		// TODO Auto-generated method stub
		Session session = sfactory.openSession();
		Transaction tx = null;
		try{
			tx = session.beginTransaction();
			
			String client_id = setupBoxto.getCustomer_id().substring(0,8);
			logger.info("***********"+client_id);
			//BeanUtils.copyProperties(setupBoxto, setupBox);
			//session.update(setupBox);
			if(setupBoxto.isIspackage_flag()){
				if(CommonUtils.exists(setupBoxto.getAssigned_package())){
				@SuppressWarnings("unused")
				int res = session.createSQLQuery("call UpdatePackage(:client_id, :cus_id, :package, :boxnumber, :update_date)")
						.setString("client_id", client_id)
						.setString("cus_id", setupBoxto.getCustomer_id())
						 .setString("package", setupBoxto.getAssigned_package())
						 .setString("boxnumber", setupBoxto.getBox_number())
						 .setDate("update_date", setupBoxto.getStart_date())
						 .executeUpdate();
				logger.info("updatePackage Ending&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&");
				}
			}if(setupBoxto.isIsaddon_flag()){
				if(CommonUtils.exists(setupBoxto.getAddonNames())){
					String[] add_ons = setupBoxto.getAddonNames().split(","); 
					StringBuffer addOnString = new  StringBuffer("");
	  
						for(String s:add_ons){ addOnString.append("\'"+s+"\',"); }
		  
						  logger.info(addOnString);
		  
						  @SuppressWarnings("unused")
						int res = session.createSQLQuery("call UpdateAddon(:cus_id, :add_on_string, :addon_string, :boxnumber, :update_date)")
							  .setString("cus_id", setupBoxto.getCustomer_id())
							  .setString("add_on_string", addOnString.toString().substring(0,addOnString.length()-1))
							  .setString("addon_string", setupBoxto.getAddonNames())
							  .setString("boxnumber", setupBoxto.getBox_number())
							  .setDate("update_date", setupBoxto.getStart_date())
							  .executeUpdate();
						  logger.info("updateAddon Ending&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&");
					} 
			}
			
			//if(session.createSQLQuery("select * from setupbox where box_number = '"+setupBoxto.getBox_number()+"' and client_id ='"+client_id+"'").list().size()>0)throw new BoxNumberDupliactedException(ErrorCodes.BOX_NUMBER_DUPLICATE_CODE);
			//if(session.createSQLQuery("select * from setupbox where vc_number = '"+setupBoxto.getVc_number()+"' and client_id ='"+client_id+"'").list().size()>0)throw new VcNumberDuplicatedException(ErrorCodes.VC_NUMBER_DUPLICATE_CODE);
		
			
			SetupBox box = (SetupBox) session.createQuery(" from SetupBox where row_id="+setupBoxto.getRow_id()).list().get(0);
			box.setBox_number(setupBoxto.getBox_number());
			box.setDiscount(setupBoxto.getDiscount());
			box.setNds_number(setupBoxto.getNds_number());
			box.setVc_number(setupBoxto.getVc_number());
			//box.setStart_date(setupBoxto.getStart_date());
			
			session.update(box);
		tx.commit();
		}/*catch(BoxNumberDupliactedException msg){
			if(tx != null)tx.rollback();
			logger.error("boxNumberDuplicatedException catch block"+msg.toString());
			return msg.toString();
		}catch(VcNumberDuplicatedException msg){
			if(tx != null)tx.rollback();
			logger.error("VCNumberDuplicatedException catch block"+msg.toString());
			return msg.toString();
		}*/catch(ConstraintViolationException e){
			if(tx != null)tx.rollback();
			logger.error(e);
			return e.toString();
		}catch(HibernateException e){
			if(tx != null)tx.rollback();
			logger.error(e);
			return e.toString();
		}finally{
			session.close();
		}
		
		
		return "Updated Successfully";
	}

	@SuppressWarnings("unchecked")
	@Override
	public List<String> getCustomerDocs(String customer_id) {
		List <String> result = new ArrayList<String>();
		Session session = sfactory.openSession();
		Transaction tx = null;
		try{
			tx = session.beginTransaction();
			String hql = "select customer_photo,customer_doc from Customer c where c.customer_id=?";
			Query q = session.createQuery(hql);
			q.setString(0, customer_id);
			result = q.list();
		}catch(HibernateException e){
			if(tx != null)tx.rollback();
			return result;
		}catch(Exception e){
			if(tx != null)tx.rollback();
			return result;
		}finally{
			session.close();
		}
		return result;
	}

	public CustomerDaoImpl(){
		
	}

	@Override
	public int activateBox(InputIds input) {
		
		Session session = sfactory.openSession();
		Transaction tx = null;
		try{
			tx = session.beginTransaction();
			SetupBox setupBox = (SetupBox) session.createQuery("from SetupBox where box_number= ?").setString(0, input.getBox_number()).uniqueResult();
			//logger.info(setupBox);
			
			setupBox.setStatus(true);
			setupBox.setEmployee_id(input.getEmployee_id());
			session.update(setupBox);
			
			tx.commit();
		}catch(HibernateException e){
			if(tx != null)tx.rollback();
			logger.error(e);
			return 100;
		}catch(Exception e){
			if(tx != null)tx.rollback();
			logger.error(e);
			return 100;
		}finally{
			session.close();
		}
		return 1;
	}

	
	@Override
	public int deActivateBox(InputIds input) {
		
		
		Session session = sfactory.openSession();
		Transaction tx = null;
		try{
			tx = session.beginTransaction();
			
			SetupBox setupBox = (SetupBox) session.createQuery("from SetupBox where box_number= ?").setString(0, input.getBox_number()).uniqueResult();
			//logger.info(setupBox);
			
			setupBox.setStatus(false);
			setupBox.setDiscount(0.0);
			setupBox.setEmployee_id(input.getEmployee_id());
			session.update(setupBox);
			
			tx.commit();
		}catch(HibernateException e){
			if(tx != null)tx.rollback();
			logger.error(e);
			return 100;
		}catch(Exception e){
			if(tx != null)tx.rollback();
			logger.error(e);
			return 100;
		}finally{
			session.close();
		}
		return 1;
	}
	
	
	@SuppressWarnings("unchecked")
	public List<SearchCustomerTo> searchCustomerTo(String client_id, SearchCustomer searchCustomer){
		
		List<SearchCustomerTo> result = new ArrayList<SearchCustomerTo>();
		
		Session session = sfactory.openSession();
		Transaction tx = null;
		Query query = null;
		
		String searchQuery = "call searchCustomer(?,?,?,?,?);";
		
		
		try {
			
			tx = session.beginTransaction();
			query = session.createSQLQuery(searchQuery)
					.setParameter(0, client_id)
					.setParameter(1, searchCustomer.getC_id())
					.setParameter(3, searchCustomer.getContact_number())
					.setParameter(4, searchCustomer.getBoxOrVcNumber())
					.setParameter(2, searchCustomer.getCustomer_name())
					.setResultTransformer(Transformers.aliasToBean(SearchCustomerTo.class));
			result = query.list();
			/*for(SearchCustomerTo cus:result){
				if(cus.getAddon_amount() == null)cus.setAddon_amount(new BigDecimal(0));
				if(cus.getPackage_amount() == null)cus.setPackage_amount(new BigDecimal(0));
				finalresult.add(cus);
			}*/
			tx.commit();
		}catch(HibernateException e){
			if(tx != null)tx.rollback();
			return result;
		}finally{
			session.close();
		}
		logger.info(new Date());
		return result;
	}
	
	@Override
	@SuppressWarnings("unchecked")
	public List<SearchCustomerMoreDetails> getCustomerMoreDetails(String customerId){
		
		List<SearchCustomerMoreDetails> result = new ArrayList<SearchCustomerMoreDetails>();
		Session session = sfactory.openSession();
		Transaction tx = null;
		Query query = null;
		String searchQuery = "call GetCustomerMoreDetails(?);";
		try {
			tx = session.beginTransaction();
			query = session.createSQLQuery(searchQuery)
					.setParameter(0, customerId)
					.setResultTransformer(Transformers.aliasToBean(SearchCustomerMoreDetails.class));
			result = query.list();
			tx.commit();
		}catch(HibernateException e){
			if(tx != null)tx.rollback();
			return result;
		}finally{
			session.close();
		}
		logger.info(new Date());
		return result;
	}

	@Override
	public int uidUpdate(UidInsertInput uidInsertInput) {
		
		Session session = sfactory.openSession();
		Transaction tx = null;
		try{
			
			tx = session.beginTransaction();
			
			Customer customer = (Customer) session.load(Customer.class, uidInsertInput.getCustomer_id());
			customer.setCustomer_uid(uidInsertInput.getCustomer_uid());
			session.update(customer);
			
			tx.commit();
			
		}catch(HibernateException e){
			if(tx != null)tx.rollback();
			logger.error(e);
			return 0;
		}catch (Exception e) {
			if(tx != null)tx.rollback();
			logger.error(e);
			return 0;
		}finally{
			session.close();
		}
		
		return 1;
	}

	@Override
	public int deleteSetupBox(String box_number) {
		
		Session session = sfactory.openSession();
		Transaction tx = null;
		try{
			
			tx = session.beginTransaction();
			
			SetupBox setupBox = (SetupBox) session.createQuery("from SetupBox where box_number= ?").setString(0, box_number).uniqueResult();
			session.delete(setupBox);
			
			tx.commit();
		
		}catch(HibernateException e){
			if(tx != null)tx.rollback();
			logger.error(e);
			return 0;
		}finally{
			session.close();
		}
		return 1;
	}

	@Override
	public String getCustomer_id(String uid_number, String client_id) {
		// TODO Auto-generated method stub
		Session session = sfactory.openSession();
		Transaction tx = null;
		String customer_id = "";
		
		try{
			tx = session.beginTransaction();
			
			customer_id = (String) session.createQuery("select customer_id from Customer c where c.customer_uid = ? and client_id = ?")
					.setString(0, uid_number)
					.setString(1, client_id).uniqueResult();
			
			tx.commit();
		}catch(HibernateException e){
			if(tx != null)tx.rollback();
			logger.error(e);
			return customer_id;
		}finally{
			session.close();
		}
		return customer_id;
	}

	@Override
	public int permenentDeActive(InputIds input) {
		Session session = sfactory.openSession();
		Transaction tx = null;
		try{
			
			tx = session.beginTransaction();
			
			Customer customer = (Customer) session.load(Customer.class, input.getCustomer_id());
			
			for(SetupBox setupBox:customer.getSetupBoxes()){
				SetupBox setup = (SetupBox) session.createQuery("from SetupBox s where s.box_number = ?").setString(0, setupBox.getBox_number()).list().get(0);
				setup.setStatus(false);
				setup.setEmployee_id(input.getEmployee_id());
				
				
				
				session.createSQLQuery("update bill_generation set box_number = \'" + setup.getBox_number() + "PDA" + Integer.parseInt(input.getCustomer_id().substring(8)) + "\' where box_number = \'" + setup.getBox_number() + "\'").executeUpdate();
				session.createSQLQuery("update addons set box_number = \'" + setup.getBox_number() + "PDA" + Integer.parseInt(input.getCustomer_id().substring(8)) + "\' where box_number = \'" + setup.getBox_number() + "\'").executeUpdate();
				
				setup.setBox_number(setup.getBox_number() + "PDA" + Integer.parseInt(input.getCustomer_id().substring(8)));
				setup.setVc_number(setup.getVc_number() + "PDA" + Integer.parseInt(input.getCustomer_id().substring(8)));
				setup.setPda_status(true);
				
				session.update(setup);
			}
			
			PermenentDeActiveHistoryEntity pd = new PermenentDeActiveHistoryEntity();
			pd.setClient_id(input.getCustomer_id().substring(0,8));
			pd.setCustomer_id(input.getCustomer_id());
			pd.setRemoved_cid(customer.getC_id());
			pd.setEmployee_id(input.getEmployee_id());
			pd.setDeactivated_on((Date) session.createSQLQuery("select current_timestamp").uniqueResult());
			session.save(pd);
			
			customer.setC_id(customer.getC_id() + "PDA" + Integer.parseInt(input.getCustomer_id().substring(8)));
			customer.setContact_number(customer.getContact_number() + "PDA" + Integer.parseInt(input.getCustomer_id().substring(8)));
			//customer.setC_id("");
			customer.setStatus(false);
			customer.setPermenent_status(true);
			customer.getSetupBoxes();
			session.update(customer);
			
			tx.commit();
		
		}catch(HibernateException e){
			if(tx != null)tx.rollback();
			logger.error(e);
			return 0;
		}finally{
			session.close();
		}
		return 1;
	}

	public String updateAddon(AddOnsEntity addOnEntity, String client_id, Session session) {

		//Session session = sfactory.openSession();
		//Transaction tx = null;
		try{
			//tx = session.beginTransaction();
			//AddOnsEntity addonEntity = new AddOnsEntity();
			//BeanUtils.copyProperties(addon, addonEntity);
			
			/*Date oldExpiryDate = (Date) session.createQuery("select expiry_date from AddOnsEntity where id=?").setInteger(0, addOnEntity.getId()).uniqueResult();
			
			addOnEntity.setStart_date(oldExpiryDate);
			*/
			logger.info(addOnEntity);
			session.update(addOnEntity);
			
			StringBuffer queryToGetAddonSpotAmount = new StringBuffer();
			queryToGetAddonSpotAmount.append("SELECT sum((12 * (YEAR(expiry_date) - YEAR(start_date)) + (MONTH(expiry_date) - MONTH(start_date)))  *(select c.channel_amount from channels c where channel_name = ('"+addOnEntity.getAddon()+"') and client_id = '"+client_id+"')) as amount ");
			queryToGetAddonSpotAmount.append("FROM addons ");
			queryToGetAddonSpotAmount.append("where id in (");
			queryToGetAddonSpotAmount.append(addOnEntity.getId()+");");

			
			BigDecimal addonAmount = (BigDecimal) session.createSQLQuery(queryToGetAddonSpotAmount.toString())
					.uniqueResult();
			
			logger.info("Addon Amount :"+addonAmount);
			
			session.createSQLQuery("update bill_generation set  addon_monthly_amount = ifnull(addon_monthly_amount,0)+? where box_number = ? and month = month(current_date)")
					.setBigDecimal(0, addonAmount)
					.setString(1, addOnEntity.getBox_number()).executeUpdate();
			
			session.createSQLQuery("call update_balance_for_addons(:cus_id, :balance)")
					.setString("cus_id", addOnEntity.getCustomer_id())
					.setBigDecimal("balance", addonAmount).executeUpdate();
			
			//tx.commit();
		}catch(Exception e){
			//if(tx != null)tx.rollback();
			logger.error(e.toString());
			return e.toString();
		}
		return "Updated Successfully";
	}

	@Override
	public String deleteAddon(int addonId, String client_id) {

		Session session = sfactory.openSession();
		Transaction tx = null;
		try{
			tx = session.beginTransaction();
			AddOnsEntity addonEntity = (AddOnsEntity) session.load(AddOnsEntity.class, addonId);
			//BeanUtils.copyProperties(addon, addonEntity);
			logger.info(addonEntity);
			String queryToGetUnlikeAddons = "select group_concat(addon) from addons where box_number = '"+addonEntity.getBox_number()+"' and addon not like '"+addonEntity.getAddon()+"'";
			String remainingAddons = (String) session.createSQLQuery(queryToGetUnlikeAddons).uniqueResult();
			
			String queryToUpdateAddonColumn = "update setupbox set addons = '"+remainingAddons+"' where box_number = '"+addonEntity.getBox_number()+"';";
			session.createSQLQuery(queryToUpdateAddonColumn).executeUpdate();
			session.delete(addonEntity);
			
			tx.commit();
		}catch(Exception e){
			if(tx != null)tx.rollback();
			logger.error(e);
			return e.toString();
		}
		return "Deleted Successfully";
	}

	public String addAddons(Set<AddonsEntityTo> addOnEntityToList, String client_id, Session session, String customer_id) {
		//Map<String, String> response = new HashMap<String, String>();
		
		//Session session = sfactory.openSession();
		//Transaction tx = null;
		try{
			//tx = session.beginTransaction();
			List<Integer> addedIds = new ArrayList<Integer>();
			
			if(addOnEntityToList.size()>0){
				
				Iterator<AddonsEntityTo> iterator = addOnEntityToList.iterator();
				String box_number = "";
				
					while(iterator.hasNext()){
						AddonsEntityTo addonEntityTo = iterator.next();
						AddOnsEntity addonEntity = new AddOnsEntity();
						
						BeanUtils.copyProperties(addonEntityTo, addonEntity);
						
						box_number = addonEntity.getBox_number();
						
						//customer_id = (String) session.createQuery("select customer_id from SetupBox s where s.box_number = ?").setString(0, box_number).uniqueResult();
						
						addonEntity.setCustomer_id(customer_id);
						logger.info(addonEntity);
						Integer latestId = (Integer) session.save(addonEntity);// *********** Saving AddonEntity ************
						addedIds.add(latestId);
						
						//response.put(new Integer(addonEntityTo.getId()).toString(), "1");
					}
					
					StringBuffer addonIds = new StringBuffer("");
					StringBuffer addons = new StringBuffer("");
						for(int i=0;i<addOnEntityToList.size();i++){
							addonIds.append(addedIds.get(i));
							addonIds.append(",");
						}
					
					Iterator<AddonsEntityTo> iterator2 = addOnEntityToList.iterator();
					
						while(iterator2.hasNext()){
							addons.append(iterator2.next().getAddon());
							addons.append(",");
						}
					
					String existingAddons = (String) session.createSQLQuery("select addons from setupbox where box_number = ?")
							.setString(0, box_number).uniqueResult();
					
					StringBuffer newAddons = new StringBuffer(existingAddons!=null?existingAddons:"");
					newAddons.append(","+addons.substring(0, addons.length()-1).toString());
					
					session.createSQLQuery("update setupbox set addons = ? where box_number=?")
						.setString(0, newAddons.toString())
						.setString(1, box_number).executeUpdate();
					
					String addonArray[] = addons.toString().split(",");
					StringBuffer addOnSearchFormat = new StringBuffer();
					
					for(String s:addonArray){
						addOnSearchFormat.append("\'"+s+"\',");
					}
					
					StringBuffer queryToGetAddonSpotAmount = new StringBuffer();
					queryToGetAddonSpotAmount.append("SELECT sum((12 * (YEAR(expiry_date) - YEAR(start_date)) + (MONTH(expiry_date) - MONTH(start_date)))  *(select sum(c.channel_amount) from channels c where channel_name = addon and client_id = '"+client_id+"')) as addonAmount ");
					queryToGetAddonSpotAmount.append("FROM addons ");
					queryToGetAddonSpotAmount.append("where id in (");
					queryToGetAddonSpotAmount.append(addonIds.toString().substring(0, addonIds.length()-1)+");");
	
					
					BigDecimal addonAmount = (BigDecimal) session.createSQLQuery(queryToGetAddonSpotAmount.toString())
							.uniqueResult();
					
					session.createSQLQuery("update bill_generation set addon_monthly_amount = ifnull(addon_monthly_amount,0)+? where box_number = ? and month = month(current_date)")
							.setBigDecimal(0, addonAmount)
							.setString(1, box_number).executeUpdate();
					
					session.createSQLQuery("call update_balance_for_addons(:cus_id, :balance)")
							.setString("cus_id", customer_id)
							.setBigDecimal("balance", addonAmount).executeUpdate();
				
			}
			
			
			//tx.commit();
		}catch(Exception e){
			//if(tx != null)tx.rollback();
			logger.error(e);
			return e.toString();
		}finally{
			//session.close();
		}
		return "Added Successfully";	
	}

	@SuppressWarnings("unchecked")
	@Override
	public List<AddonsEntityTo> getTheSetupboxAddons(String boxNumber) {
		List<AddonsEntityTo> response = new ArrayList<AddonsEntityTo>();
		
		Session session = sfactory.openSession();
		Transaction tx = null;
		try{
			tx = session.beginTransaction();
			
			response = session.createQuery("from AddOnsEntity where box_number=?").setParameter(0, boxNumber).list();
			
			tx.commit();
		}catch(Exception e){
			if(tx != null)tx.rollback();
			logger.error(e);
			return response;
		}
		return response;
	}
	
	@SuppressWarnings("unchecked")
	@Override
	public List<PackagesAndAddonsOfCustomer> getTheCustomerAddons(String c_id, String client_id) {
		
		List<PackagesAndAddonsOfCustomer> reponseTo = new ArrayList<PackagesAndAddonsOfCustomer>();
		
		Session session = sfactory.openSession();
		Transaction tx = null;
		try{
			tx = session.beginTransaction();
			logger.info("Gettting Addons Basedon C_id DaoImpl: " + c_id);
			String customer_id = "";
			if(CommonUtils.exists(c_id)){
				customer_id = (String) session.createQuery("select c.customer_id from Customer c where c.c_id = ? and client_id = ?").setString(0, c_id).setString(1, client_id).uniqueResult();
				List<String> boxNumbers = session.createSQLQuery("select box_number from setupbox where customer_id = ?").setString(0, customer_id).list();
				logger.info("Box Numbers for the Customer: " + boxNumbers);
				for(String boxNumber:boxNumbers){
					PackagesAndAddonsOfCustomer setupboxPackageDetails = (PackagesAndAddonsOfCustomer) session.createSQLQuery("select s.box_number, s.assigned_package, p.package_amount, s.discount, (p.package_amount - s.discount) as package_payable, group_concat(a.addon,' - ', a.expiry_date) AS addons from setupbox s "
																			+ " join packages p on p.package_id = s.package_id"
																			+ " left join addons a on  a.box_number = s.box_number "
																			+ " where s.box_number = ? and s.pda_status = 0").setString(0, boxNumber).setResultTransformer(Transformers.aliasToBean(PackagesAndAddonsOfCustomer.class)).uniqueResult();
					List<AddOnsEntity> addonList = new ArrayList<AddOnsEntity>();
					addonList = session.createQuery("from AddOnsEntity where box_number=?").setParameter(0, boxNumber).list();
					setupboxPackageDetails.setAddonList(addonList);
					reponseTo.add(setupboxPackageDetails);
				}
				
			}
			tx.commit();
		}catch(Exception e){
			if(tx != null)tx.rollback();
			logger.error(e);
			return reponseTo;
		}
		return reponseTo;
	}
	
	@Override
	public SetUpBoxToo getSettopBoxByBoxNumber(String boxNumber) {
		SetUpBoxToo setupBoxResponse = new SetUpBoxToo();
		
		Session session = sfactory.openSession();
		Transaction tx = null;
		try{
			tx = session.beginTransaction();
			SetupBox setupbox = (SetupBox) session.load(SetupBox.class, boxNumber);
			
			BeanUtils.copyProperties(setupbox, setupBoxResponse);
			
			@SuppressWarnings("unchecked")
			List<AddOnsEntity> addons = session.createQuery("from AddOnsEntity a where a.box_number = ?").setString(0, boxNumber)
													.list();
			//BeanUtils.copyProperties(addons, setupBoxResponse.getAddOns());
			setupBoxResponse.setAddOns(addons);
			
			tx.commit();
		}catch(Exception e){
			if(tx != null)tx.rollback();
			logger.error(e);
			return setupBoxResponse;
		}finally{
			session.close();
		}
		return setupBoxResponse;
	}

	@Override
	@SuppressWarnings("unchecked")
	public List<SetUpBoxToo> getTheSetupboxesByCustomerId(String customer_id) {
		List<SetUpBoxToo> setupBoxResponse = new ArrayList<SetUpBoxToo>();
		
		Session session = sfactory.openSession();
		Transaction tx = null;
		try{
			tx = session.beginTransaction();
			List<SetupBox> setupBoxes = (List<SetupBox>) session.createQuery("from SetupBox s where s.customer_id = ?").setString(0, customer_id).list();
			
			for(SetupBox setupBox:setupBoxes){
				SetUpBoxToo setupBoxTo = new SetUpBoxToo();
				BeanUtils.copyProperties(setupBox, setupBoxTo);
				
				double packAmount = 0;
				BigDecimal addonAmount = new BigDecimal(0);
				
				if(CommonUtils.exists(setupBoxTo.getAssigned_package())){
					String pack = setupBoxTo.getAssigned_package();
					if(!setupBoxTo.getAssigned_package().equalsIgnoreCase("A-la-Carte")){
					packAmount = (double) session.createQuery("Select package_amount from PackageEntity where package_name=? and client_id =?").setString(0, pack).setString(1, setupBox.getClient_id()).uniqueResult();
					//packAmount.add(new BigDecimal(amt));
					setupBoxTo.setPackage_amount(packAmount);
					}else{
						setupBoxTo.setPackage_amount(0);
					}
				}
				if(CommonUtils.exists(setupBox.getAddonNames())){
					String[] add_ons = setupBox.getAddonNames().split(",");
					
					StringBuffer  addOnnSearch = new StringBuffer("(");
					for(String s:add_ons){
						addOnnSearch.append("\'"+s+"\',");
					}
					logger.info(addOnnSearch);
					
					//String add_on_string = addOnnSearch.toString().substring(0, addOnnSearch.length() - 1);
					String addOnQuery = "SELECT SUM(channel_amount) from channels WHERE channel_name in "+addOnnSearch.toString().substring(0,addOnnSearch.length()-1)+") and client_id =?;";
					
					SQLQuery query1 = session.createSQLQuery(addOnQuery);
					addonAmount = (BigDecimal) query1.setString(0, setupBox.getClient_id()).uniqueResult();
					if(addonAmount == null || addonAmount.equals(0)){addonAmount = new BigDecimal(0.0);
					}
					setupBoxTo.setAddon_amount(addonAmount);
					
				}else{
					setupBoxTo.setAddon_amount(new BigDecimal(0.0));
				}
				double monthlyRent;
				
				monthlyRent = addonAmount.doubleValue() + packAmount - setupBoxTo.getDiscount();
				setupBoxTo.setTotal_amount(monthlyRent); 
				
				
				List<AddOnsEntity> addons = session.createQuery("from AddOnsEntity a where a.box_number = ?").setString(0, setupBoxTo.getBox_number())
														.list();
				//BeanUtils.copyProperties(addons, setupBoxResponse.getAddOns());
				setupBoxTo.setAddOns(addons);
				
				setupBoxResponse.add(setupBoxTo);
			}
			
			tx.commit();
		}catch(Exception e){
			if(tx != null)tx.rollback();
			logger.error(e);
			return setupBoxResponse;
		}finally{
			session.close();
		}
		return setupBoxResponse;
	}

	@Override
	public String getCustomerIDUsingCid(String client_id,
			SearchCustomer searchCustomer) {
		String customer_id = "";
		Session session = sfactory.openSession();
		Transaction tx = null;
		try{
			tx = session.beginTransaction();
			
			
			StringBuffer queryToGetCustomerId = new StringBuffer("");
			
			
			if(CommonUtils.exists(searchCustomer.getC_id())){
				queryToGetCustomerId.append("select customer_id from Customer c where c.client_id = ? and c.c_id = ?");
				customer_id = (String) session.createQuery(queryToGetCustomerId.toString())
						.setString(0, client_id)
						.setString(1, searchCustomer.getC_id())
						.uniqueResult();
			}if(CommonUtils.exists(searchCustomer.getCustomer_uid())){
				queryToGetCustomerId.append("select customer_id from Customer c where c.client_id = ? and c.customer_uid = ?");
				customer_id = (String) session.createQuery(queryToGetCustomerId.toString())
						.setString(0, client_id)
						.setString(1, searchCustomer.getCustomer_uid())
						.uniqueResult();
			}if(CommonUtils.exists(searchCustomer.getContact_number())){
				queryToGetCustomerId.append("select customer_id from Customer c where c.client_id = ? and c.contact_number = ?");
				customer_id = (String) session.createQuery(queryToGetCustomerId.toString())
						.setString(0, client_id)
						.setString(1, searchCustomer.getContact_number())
						.uniqueResult();
			}
			
			
			
			tx.commit();
		}catch(Exception e){
			if(tx != null)tx.rollback();
			logger.error(e);
			return customer_id;
		}finally{
			session.close();
		}
		return customer_id;
	}

	@Override
	public int updateContactNumber(ContactUpdateTo data) {
		Session session = sfactory.openSession(); 
		Transaction tx= null;
		try{ 
			tx = session.beginTransaction();
			logger.info("contactNumber update started");
			
	  Customer customer = (Customer) session.createQuery("from Customer c where c.customer_id='"
			  + data.getCustomerId() +"'").list().get(0);
		  
		 customer.setContact_number(data.getNewMobileNumber());
		  
	  session.update(customer); 
	  ContactNumberUpdateHistory conHistory = new ContactNumberUpdateHistory();
	  conHistory.setCustomerId(data.getCustomerId());
	  conHistory.setNewContactNumber(data.getNewMobileNumber());
	  conHistory.setOldContactNumber(data.getOldMobileNumber());
	  
	  SimpleDateFormat formatter = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
		String dateInString = data.getUpdateTimeStamp();	
	  
	  conHistory.setUpdateOn(formatter.parse(dateInString));
	  session.save(conHistory);
	  
	  logger.info("contactNumber update End");
	  tx.commit(); 
	  }catch(HibernateException e){
		  if(tx != null)tx.rollback();
		  logger.error("contactNumber update Hibernate Exception " + e); return 0; 
	  }catch(Exception e){
		  if(tx != null)tx.rollback();
		  logger.error("contactNumber update Exception " + e); return 0; 
	  } finally{
	  session.close(); 
	  }
	  
	  return 1;
	}

	public CustomerTo getUniqueCustomerProfile(String client_id){
		CustomerTo customerTo = new CustomerTo();
		
		Session session = null;
		Transaction tx = null;
		try{
			session = sfactory.openSession();
			tx = session.beginTransaction();
			
			TempCustomerId tempId = (TempCustomerId) session.createQuery("from TempCustomerId t where t.client_id = ?").setString(0, client_id).list().get(0);
			String customer_id = tempId.getCustomer_id();
			session.delete(tempId);
			
			Customer customer = (Customer) session.load(Customer.class, customer_id);
			logger.info(customer);
			
			tx.commit();
			
		}catch(HibernateException e){
			if(tx != null)tx.rollback();
			logger.error("CustomerDaoImpl getUniqueCustomerProfile() - HibernateException : " + e);
			return customerTo;
		}catch(Exception e){
			if(tx != null)tx.rollback();
			logger.error("CustomerDaoImpl getUniqueCustomerProfile() - Exception : " + e);
			return customerTo;
		}finally{
			if(session != null)session.close();
		}
		return customerTo;
	}

	@Override
	public String addAddons(Set<AddonsEntityTo> addOnEntityTo, String client_id) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public String updateAddon(AddonsEntityTo addon, String client_id) {
		// TODO Auto-generated method stub
		return null;
	}

public AddonsEntityTo getAddonEntity(String box_nuumber, String addon) {
		
	AddOnsEntity addonEntity = new AddOnsEntity();
	AddonsEntityTo addonEntityTo = new AddonsEntityTo();
		Session session = sfactory.openSession();
		Transaction tx = null;
		try{
			tx = session.beginTransaction();
			addonEntity = (AddOnsEntity) session.createQuery("from AddOnsEntity a where a.box_number= ? and a.addon = ?").setString(0, box_nuumber)
					.setString(1, addon)
					.uniqueResult();
			//logger.info(setupBox);
			if(addonEntity != null)
			BeanUtils.copyProperties(addonEntity, addonEntityTo);
			
			tx.commit();
		}catch(HibernateException e){
			if(tx != null)tx.rollback();
			logger.error(e);
			return addonEntityTo;
		}catch(Exception e){
			if(tx != null)tx.rollback();
			logger.error(e);
			return addonEntityTo;
		}finally{
			session.close();
		}
		return addonEntityTo;
	}

	@Override
	public int updateEmail(ContactUpdateTo data) {
		Session session = sfactory.openSession();
		Transaction tx = null;
		try {
			tx = session.beginTransaction();
			logger.info("Email update started");

			Customer customer = (Customer) session
					.createQuery(
							"from Customer c where c.customer_id='"
									+ data.getCustomerId() + "'").list().get(0);

			customer.setEmail(data.getNewMobileNumber());

			session.update(customer);
			EmailUpdateHistoryEntity emailHistory = new EmailUpdateHistoryEntity();
			emailHistory.setCustomerId(data.getCustomerId());
			emailHistory.setNewEmail(data.getNewMobileNumber());
			emailHistory.setOldEmail(data.getOldMobileNumber());

			SimpleDateFormat formatter = new SimpleDateFormat(
					"yyyy-MM-dd HH:mm:ss");
			String dateInString = data.getUpdateTimeStamp();

			emailHistory.setUpdateOn(formatter.parse(dateInString));
			session.save(emailHistory);

			logger.info("Email update End");
			tx.commit();
		} catch (HibernateException e) {
			if (tx != null)
				tx.rollback();
			logger.error("Email update Hibernate Exception " + e);
			return 0;
		} catch (Exception e) {
			if (tx != null)
				tx.rollback();
			logger.error("Email update Exception " + e);
			return 0;
		} finally {
			session.close();
		}

		return 1;
	}

	@Override
	public int setupboxPermenantDeActivate(InputIds input) {
		Session session = sfactory.openSession();
		Transaction tx = null;
		try{
			tx = session.beginTransaction();
			
			
			
			String customer_id = (String) session.createSQLQuery("select distinct s.customer_id from setupbox s where box_number = ?").setString(0, input.getBox_number()).uniqueResult();
			
			
				SetupBox setup = (SetupBox) session.createQuery("from SetupBox s where s.box_number = ?").setString(0, input.getBox_number()).list().get(0);
				setup.setStatus(false);
				
				
				
				session.createSQLQuery("update bill_generation set box_number = \'" + setup.getBox_number() + "PDA" + Integer.parseInt(customer_id.substring(8)) + "\' where box_number = \'" + setup.getBox_number() + "\'").executeUpdate();
				session.createSQLQuery("update addons set box_number = \'" + setup.getBox_number() + "PDA" + Integer.parseInt(customer_id.substring(8)) + "\' where box_number = \'" + setup.getBox_number() + "\'").executeUpdate();
				
				setup.setBox_number(setup.getBox_number() + "PDA" + Integer.parseInt(customer_id.substring(8)));
				setup.setVc_number(setup.getVc_number() + "PDA" + Integer.parseInt(customer_id.substring(8)));
				setup.setEmployee_id(input.getEmployee_id());
				setup.setPda_status(true);
				
				session.update(setup);
				
			Customer customer = (Customer) session.load(Customer.class, customer_id);
			
			if(customer.getSetupBoxes().size() == 1) {
			PermenentDeActiveHistoryEntity pd = new PermenentDeActiveHistoryEntity();
			pd.setClient_id(customer_id.substring(0,8));
			pd.setCustomer_id(customer_id);
			pd.setRemoved_cid(customer.getC_id());
			pd.setDeactivated_on((Date) session.createSQLQuery("select current_timestamp").uniqueResult());
			session.save(pd);
			
			/*customer.setC_id(customer.getC_id() + "PDA" + Integer.parseInt(customer_id.substring(8)));
			customer.setContact_number(customer.getContact_number() + "PDA" + Integer.parseInt(customer_id.substring(8)));
			//customer.setC_id("");
			customer.setStatus(false);
			customer.setPermenent_status(true);
			customer.getSetupBoxes();
			session.update(customer);*/
			}
			
			tx.commit();
		}catch(HibernateException e){
			if(tx != null)tx.rollback();
			logger.error(e);
			return 0;
		}catch(Exception e){
			if(tx != null)tx.rollback();
			logger.error(e);
			return 0;
		}finally{
			session.close();
		}
		return 1;
	}

	@Override
	public List<SearchCustomerTo> permenentDeactiveList(String client_id) {

		
		List<SearchCustomerTo> result = new ArrayList<SearchCustomerTo>();
		
		StringBuffer attachQuery = new StringBuffer(CommonUtils.permanentDeactiveListQuery);
		attachQuery.append(" where bill.client_id = '"+client_id+"' and bill.permenent_status = 1 ");
		

		//List<SearchCustomerTo> finalresult = new ArrayList<SearchCustomerTo>();
		Session session = sfactory.openSession();
		Transaction tx = null;
		Query query = null;
		
		try {
			
			tx = session.beginTransaction();
			query = session.createSQLQuery(attachQuery.toString()).setResultTransformer(Transformers.aliasToBean(SearchCustomerTo.class));
			result = query.list();
			
			tx.commit();
		}catch(HibernateException e){
			if(tx != null)tx.rollback();
			return result;
		}finally{
			session.close();
		}
		logger.info(new Date());
		return result;
	
	}

	@SuppressWarnings("unchecked")
	@Override
	public List<SetupBox> getActiveSetupBoxList(String customerId,
			String clientId) {
		List<SetupBox> list = new ArrayList<SetupBox>();
		Session session = sfactory.openSession();
		try{												
		 list = (List<SetupBox>) session.createQuery("from SetupBox s where  s.customer_id = ? and s.status = 1 and s.pda_status = 0 ").setString(0, customerId).list();
		}catch (HibernateException e) {
			e.printStackTrace();
		}finally{
			if(session!=null)session.close();
		}
		return list;
	}

	@SuppressWarnings("unchecked")
	@Override
	public CustomerHistoryDto getCustomerHistory(BillGenType cusDetails,
			String clientId) {
		
		
		CustomerHistoryDto ch = new CustomerHistoryDto();
		
		List<CusHistoryDto> cusHistory = new ArrayList<CusHistoryDto>();
		
		Session session = sfactory.openSession();
		try{
			
			cusHistory = (List<CusHistoryDto>)session.createSQLQuery(" select cs.customer_id as customerId,cs.c_id as cId " +
					" ,cs.customer_name as customerName,sh.box_number as boxNumber,sh.action as cusStatus,sh.time as statusDate, " +
					" CASE " + 
					" when sh.employee_id IS NULL then 'Admin' " +
					" else " +
					"	( select employee_name from employee where employee_id = sh.employee_id ) " + 
					" end as empName " +
					" from customer cs,setupbox_status_update_history sh " +
					" where cs.customer_id = ? " +
					" and cs.customer_id = sh.customer_id " +
					" order by time desc; ")
				.setString(0, cusDetails.getCustomerId())
				.setResultTransformer(
						Transformers.aliasToBean(CusHistoryDto.class))
				.list();
			ch.setCusHistory(cusHistory);
		}catch(HibernateException e){
			e.printStackTrace();
		}finally{
			if(session!=null)session.close();
		}
		return ch;
	}

	@Override
	public PackageEntity getSetupBoxPackageDetails(SetupBox setup) {
		PackageEntity pa = new PackageEntity();
		Session session = null;
		try{
			session = sfactory.openSession();
		pa = (PackageEntity) session.createQuery("from PackageEntity where package_id=:pkgId and  client_id=:cliId")
				.setInteger("pkgId", setup.getPackage_id())
				.setString("cliId", setup.getClient_id())
				.uniqueResult();
			
		}catch(HibernateException e)
		{
				e.printStackTrace();
		}finally{
			if(session!=null)session.close();
		}
		
		return pa;
	}

	@Override
	public BillGeneration getBillGenerationDetails(BillGenType billGenType,
			SetupBox setup,PackageEntity pa,int month, int year) {
		BillGeneration bill = new BillGeneration();
		Session session = null;
		Transaction  tx = null;
		try{
			session = sfactory.openSession();
			tx = session.beginTransaction();
			bill = (BillGeneration) session.createQuery("from BillGeneration where customer_id=:cusId and box_number=:box and month=:mon and year(date)=:ya")
					.setString("cusId", billGenType.getCustomerId())
					.setString("box", setup.getBox_number())
					.setInteger("mon", month)
					.setInteger("ya", year)
					.uniqueResult();
			session.flush();
			
			if(bill!=null)
			{
			    String presentMonthDate=year+"-"+month+"-"+"01"+" 01:00:00";
			    SimpleDateFormat simpleDateFormat = new SimpleDateFormat("yyyy-MM-dd hh:mm:ss");
			    Date currentDate = simpleDateFormat.parse(presentMonthDate);
			    System.out.println(currentDate);  
			    CurrentDate curr = new CurrentDate();
			    curr.setCurDate(currentDate);
			    System.out.println(curr.getCurDate());  
				double package_daily_amount = pa.getPackage_amount()/30;
				double package_monthly_amount = pa.getPackage_amount();
				bill.setPackage_daily_amount(package_daily_amount);
				bill.setPackage_monthly_amount(package_monthly_amount);
				bill.setPaid_status(null);
				bill.setDiscount(setup.getDiscount());
				bill.setDate(curr.getCurDate());
				session.saveOrUpdate(bill);
				tx.commit();
				
			}
			
		}catch(HibernateException | ParseException e)
		{
				e.printStackTrace();
		}finally{
			if(session!=null)
				session.close();
		}
		return bill;
	}

	@Override
	public CurrentDate getCurrentDate() {
		Session session = null;
		CurrentDate curDate= new CurrentDate();
		try{
			session = sfactory.openSession();
			curDate = (CurrentDate) session.createSQLQuery("select now() as curDate;")
						.setResultTransformer(Transformers.aliasToBean(CurrentDate.class))
						.uniqueResult();
			
		}catch(HibernateException e)
		{
			e.printStackTrace();
		}finally{
			if(session!=null)
				session.close();
		}
		return curDate;
	}

	@Override
	public BillGeneration createBill(BillGenType billGen, SetupBox setup,
			PackageEntity pa, int month, int year) {
		BillGeneration bill = new BillGeneration();
		Transaction tx =  null;
		Session session =  null;
		try{
			session = sfactory.openSession();
			tx = session.beginTransaction();
			 String presentMonthDate=year+"-"+month+"-"+"01"+" 01:00:00";
			    SimpleDateFormat simpleDateFormat = new SimpleDateFormat("yyyy-MM-dd hh:mm:ss");
			    Date currentDate = simpleDateFormat.parse(presentMonthDate);
		    System.out.println(currentDate);  
		    CurrentDate curr = new CurrentDate();
		    curr.setCurDate(currentDate);
		    System.out.println(curr.getCurDate());
			double package_daily_amount = pa.getPackage_amount()/30;
			double package_monthly_amount = pa.getPackage_amount();
			
			bill.setMonth(String.valueOf(month));
			bill.setCustomer_id(billGen.getCustomerId());
			bill.setPackage_daily_amount(package_daily_amount);
			bill.setAddon_daily_amount(0.0);
			bill.setPackage_monthly_amount(package_monthly_amount);
			bill.setAddon_monthly_amount(0.00);
			bill.setPaid_status(null);
			bill.setDate(curr.getCurDate());
			bill.setBox_number(setup.getBox_number());
			bill.setClient_id(setup.getClient_id());
			bill.setDiscount(setup.getDiscount());
			session.saveOrUpdate(bill);
			tx.commit();
		}catch(HibernateException | ParseException e)
		{
				e.printStackTrace();
		}finally{
			if(session!=null)
				session.close();
		}
		return bill;

	}

	@Override
	public boolean generateBillCurrentMonth(
			BillGenType billGen, SetupBox setup, PackageEntity pa) {
		boolean status = false;
		int month = 0,year =0;
		Calendar cal = Calendar.getInstance();
		if(cal.get(Calendar.MONTH) == 0){
			month = 12;
			year  = cal.get(Calendar.YEAR) - 1;} 
		else{
			month = cal.get(Calendar.MONTH);
			year = cal.get(Calendar.YEAR);}
	    
		BillGeneration bill=new BillGeneration();
		Session session =  null;
		Transaction tx = null;
		
		try{
			
			String prevMonthDate=year+"-"+month+"-"+"01"+" 01:00:00";
		    SimpleDateFormat simpleDateFormat = new SimpleDateFormat("yyyy-MM-dd hh:mm:ss");
		    Date prvMonthDate = simpleDateFormat.parse(prevMonthDate);
			
				session = sfactory.openSession();
				tx = session.beginTransaction();
			bill = (BillGeneration) session.createQuery("from BillGeneration where customer_id=:cusId and box_number=:box and month=:mon and year(date)=:ya")
					.setString("cusId", billGen.getCustomerId())
					.setString("box", setup.getBox_number())
					.setInteger("mon", month)
					.setInteger("ya", year)
					.uniqueResult();
					session.flush();
			
			if(bill!=null)
			{
				System.out.println(prvMonthDate);  
			    CurrentDate curr = new CurrentDate();
			    curr.setCurDate(prvMonthDate);
			    
			    System.out.println(curr.getCurDate());  
				double package_daily_amount = pa.getPackage_amount()/30;
				double package_monthly_amount = pa.getPackage_amount();
				bill.setPackage_daily_amount(package_daily_amount);
				bill.setPackage_monthly_amount(package_monthly_amount);
				bill.setPaid_status(false);
				bill.setDiscount(setup.getDiscount());
				bill.setDate(curr.getCurDate());
				bill.setMonth(String.valueOf(month));
				
				session.saveOrUpdate(bill);
				tx.commit();
				status = true;
			}
			
			if(bill == null)
			{
				
				@SuppressWarnings("unchecked")
				List<BillGeneration> billList = (List<BillGeneration>) session.createQuery("from BillGeneration where customer_id=:cusId and box_number=:box  and paid_status=:paidStatus"
						+ " and month!=:mon ")
						.setString("cusId", billGen.getCustomerId())
						.setString("box", setup.getBox_number())
						.setBoolean("paidStatus", false)
						.setString("mon", String.valueOf(month))
						.list();
				if(billList!=null){
					for(BillGeneration bi:billList){
						if(bi.getPackage_monthly_amount() > 0 && (!bi.getPaid_status())){
							session.createSQLQuery("update billing set balance_amount = balance_amount + ? where customer_id = ? order by row_id desc limit 1")
							.setDouble(0, bi.getPackage_monthly_amount()).setString(1, billGen.getCustomerId())
							.executeUpdate();
							//BillGeneration prevMonthBillEntity = (BillGeneration) session.load(BillGeneration.class, bill.getRow_id());
							if(bi.getPaid_status() != null && !bi.getPaid_status()) {
							bi.setPaid_status(true);
							session.update(bi);
							}
						}
					}
				}
				session.flush();
			    CurrentDate curr = new CurrentDate();
			    curr.setCurDate(prvMonthDate);
			    System.out.println(curr.getCurDate());  
				double package_daily_amount = pa.getPackage_amount()/30;
				double package_monthly_amount = pa.getPackage_amount();
				
				BillGeneration Prevbill=new BillGeneration();
				Prevbill.setMonth(String.valueOf(month));
				Prevbill.setCustomer_id(billGen.getCustomerId());
				Prevbill.setPackage_daily_amount(package_daily_amount);
				Prevbill.setAddon_daily_amount(0.00);
				Prevbill.setPackage_monthly_amount(package_monthly_amount);
				Prevbill.setAddon_monthly_amount(0.00);
				Prevbill.setPaid_status(false);
				Prevbill.setDate(curr.getCurDate());
				Prevbill.setBox_number(setup.getBox_number());
				Prevbill.setClient_id(setup.getClient_id());
				Prevbill.setDiscount(setup.getDiscount());
				session.saveOrUpdate(Prevbill);
				tx.commit();
				status = true;
			}
			
		}catch(HibernateException | ParseException e)
		{
			e.printStackTrace();
		}finally{
			if(session!=null)
				session.close();
		}
		return status;
	}

	@Override
	public void updateArppCustomerPreviousBilling(BillGenType billGen,
			SetupBox setup, PackageEntity pa, String clientId) {
		int month = 0,year =0;
		Calendar cal = Calendar.getInstance();
		if(cal.get(Calendar.MONTH) == 0){
			month = 12;
			year  = cal.get(Calendar.YEAR) - 1;} 
		else{
			month = cal.get(Calendar.MONTH);
			year = cal.get(Calendar.YEAR);}
		Session session = null;
		Transaction tx = null;
		try{
			session = sfactory.openSession();
			tx = session.beginTransaction();
		@SuppressWarnings("unchecked")
		List<BillGeneration> billList = (List<BillGeneration>) session.createQuery("from BillGeneration where customer_id=:cusId and box_number=:box  and paid_status=:paidStatus"
				+ " and month!=:mon ")
				.setString("cusId", billGen.getCustomerId())
				.setString("box", setup.getBox_number())
				.setBoolean("paidStatus", false)
				.setString("mon", String.valueOf(month))
				.list();
		if(billList!=null){
			for(BillGeneration bi:billList){
				if(bi.getPackage_monthly_amount() > 0 && (!bi.getPaid_status())){
					session.createSQLQuery("update billing set balance_amount = balance_amount + ? where customer_id = ? order by row_id desc limit 1")
					.setDouble(0, bi.getPackage_monthly_amount()).setString(1, billGen.getCustomerId())
					.executeUpdate();
					//BillGeneration prevMonthBillEntity = (BillGeneration) session.load(BillGeneration.class, bill.getRow_id());
					if(bi.getPaid_status() != null && !bi.getPaid_status()) {
					bi.setPaid_status(true);
					session.update(bi);
					}
				}
			}
		}
		tx.commit();
		}catch(HibernateException e)
		{
			e.printStackTrace();
		}finally{
			if(session!=null)session.close();
		}
	}

	@Override
	public boolean checkCustomerPaidStatus(String customerId, String clientId) {
		boolean status = false;
		int month = 0,year =0;
		Calendar cal = Calendar.getInstance();
		if(cal.get(Calendar.MONTH) == 0){
			month = 12;
			year  = cal.get(Calendar.YEAR) - 1;} 
		else{
			month = cal.get(Calendar.MONTH);
			year = cal.get(Calendar.YEAR);}
		BillGeneration bill=new BillGeneration();
		
		Session session = null;
		try{
			session = sfactory.openSession();
			bill = (BillGeneration) session.createQuery("from BillGeneration where customer_id=:cusId and month=:mon and year(date)=:ya and paid_status=:paid")
					.setString("cusId", customerId)
					.setInteger("mon", month)
					.setInteger("ya", year)
					.setBoolean("paid", true)
					.setMaxResults(1)
					.uniqueResult();
		if(bill!=null)status = true;
		else status = false;
		}catch(HibernateException e)
		{
			e.printStackTrace();
		}finally{
			if(session!=null)session.close();
		}
		return status;
	}

	
}
