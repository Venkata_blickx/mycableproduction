package com.blickx.dao;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;

import org.apache.log4j.Logger;
import org.hibernate.HibernateException;
import org.hibernate.Query;
import org.hibernate.SQLQuery;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.Transaction;
import org.hibernate.exception.ConstraintViolationException;
import org.hibernate.exception.DataException;
import org.hibernate.transform.Transformers;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;

import com.blickx.domain.DeviceEntity;
import com.blickx.domain.DeviceLocation;
import com.blickx.domain.Employee;
import com.blickx.domain.SelectedDevice;
import com.blickx.domain.SelectedEmployee;
import com.blickx.exceptions.ContactNumberDupliactedException;
import com.blickx.exceptions.DeviceNameDuplicatedException;
import com.blickx.exceptions.ErrorCodes;
import com.blickx.exceptions.ImeiNumberDuplicatedException;
import com.blickx.exceptions.SimNumberDuplicatedException;
import com.blickx.id.generator.DeviceIdGenerator;
import com.blickx.output.DeviceLacationRespons;
import com.blickx.output.DeviceLocationTo;
import com.blickx.searchEntities.SearchDevice;
import com.blickx.searchEntities.SearchDeviceEntity;
import com.blickx.to.DashBoardCollectionDetailsTo;
import com.blickx.to.DeviceHistoryTo;
import com.blickx.to.DeviceNamesTo;
import com.blickx.to.DeviceTo;
import com.blickx.utill.CommonUtils;
import com.blickx.utill.SimpleUtill;

public class DeviceDaoImpl implements DeviceDao{

	private Logger logger = Logger.getLogger(DeviceDaoImpl.class);
	
	@Autowired
	SessionFactory sfactory;
	
	
	@Override
	public String addDevice(DeviceEntity device) {
		
		Session session=sfactory.openSession();
		Transaction tx = null;
		int result=0;
		try{
			tx = session.beginTransaction();
			
			isDeviceNameExistsForAddDevice(device.getClient_id(), session, device);
			isSimNumberExistsForAddDevice(device.getClient_id(), session, device);
			isImeiNumberExistsForAddDevice(device.getClient_id(), session, device);
			
			//Session session=HibernateUtil.getSession();
			/*if(CommonUtils.exists(device.getEmployee_name())){
				String employee_id=(String) session.createSQLQuery("select employee_id from employee d where d.employee_name=\'"+device.getEmployee_name()+"\'").uniqueResult();
				device.setEmployee_id(employee_id);
				}
			else{
				device.setEmployee_id("0");
			}*/
			device.setStatus(true);
			  
			Object obj=session.save(device);
			if(obj!=null)result=1;
			tx.commit();
		} catch (DeviceNameDuplicatedException e) {
			if (tx != null)tx.rollback();
			logger.error(e);
			return e.toString();
		}   catch (SimNumberDuplicatedException e) {
			if (tx != null)tx.rollback();
			logger.error(e);
			return e.toString();
		}   catch (ImeiNumberDuplicatedException e) {
			if (tx != null)tx.rollback();
			logger.error(e);
			return e.toString();
		}  catch (ConstraintViolationException e) {
			if (tx != null)tx.rollback();
			logger.error(e);
			return "MCI1001 Internal Server Error: Please contact your site adminstrator!";
		} catch (DataException e) {
			if (tx != null)tx.rollback();
			logger.error(e);
			return "MCI1002 Internal Server Error: Please contact your site adminstrator!";
		} catch (HibernateException e) {
			if (tx != null)tx.rollback();
			logger.error(e);
			return "MCI1000 Internal Server Error: Please contact your site adminstrator!";
		} catch (Exception e) {
			if (tx != null)tx.rollback();
			logger.error(e);
			return "MCI100 Internal Server Error: Please contact your site adminstrator!";
		} finally{
			if(session != null)session.close();
		}
		return "1";
	}
	
	private void isSimNumberExistsForAddDevice(String client_id,
			Session session, DeviceEntity device)
			throws SimNumberDuplicatedException {
		if (session
				.createSQLQuery(
						"select * from device where sim_number = '"
								+ device.getSim_number()
								+ "' and client_id = '" + client_id + "'")
				.list().size() > 0)
			throw new SimNumberDuplicatedException(
					ErrorCodes.getSimNumberDuplicateMsg(device.getSim_number()));
	}
	
	private void isSimNumberExistsForUpdateDevice(String client_id, Session session, DeviceEntity device)
			throws SimNumberDuplicatedException {
		if (session.createSQLQuery("select * from device where sim_number = '"
								+ device.getSim_number()
								+ "' and client_id = '"
								+ client_id
								+ "' and device_id not in("
								+ device.getDevice_id() + ")").list()
				.size() > 0)
			throw new SimNumberDuplicatedException(ErrorCodes.getSimNumberDuplicateMsg(device.getSim_number()));
	}
	
	private void isDeviceNameExistsForAddDevice(String client_id,
			Session session, DeviceEntity device)
			throws DeviceNameDuplicatedException {
		if (session
				.createSQLQuery(
						"select * from device where device_name = '"
								+ device.getDevice_name()
								+ "' and client_id = '" + client_id + "'")
				.list().size() > 0)
			throw new DeviceNameDuplicatedException(
					ErrorCodes.getDeviceNameDuplicateMsg(device.getDevice_name()));
	}
	
	private void isDeviceNameExistsForUpdateDevice(String client_id, Session session, DeviceEntity device)
			throws DeviceNameDuplicatedException {
		if (session.createSQLQuery("select * from device where device_name = '"
								+ device.getDevice_name()
								+ "' and client_id = '"
								+ client_id
								+ "' and device_id not in("
								+ device.getDevice_id() + ")").list()
				.size() > 0)
			throw new DeviceNameDuplicatedException(ErrorCodes.getDeviceNameDuplicateMsg(device.getDevice_name()));
	}
	
	private void isImeiNumberExistsForAddDevice(String client_id,
			Session session, DeviceEntity device)
			throws ImeiNumberDuplicatedException {
		if (session
				.createSQLQuery(
						"select * from device where ime_number = '"
								+ device.getIme_number()
								+ "' and client_id = '" + client_id + "'")
				.list().size() > 0)
			throw new ImeiNumberDuplicatedException (
					ErrorCodes.getImeiNumberDuplicateMsg(device.getIme_number()));
	}
	
	private void isImeiNumberExistsForUpdateDevice(String client_id, Session session, DeviceEntity device)
			throws ImeiNumberDuplicatedException {
		if (session.createSQLQuery("select * from device where ime_number = '"
								+ device.getIme_number()
								+ "' and client_id = '"
								+ client_id
								+ "' and device_id not in("
								+ device.getDevice_id() + ")").list()
				.size() > 0)
			throw new ImeiNumberDuplicatedException(ErrorCodes.getImeiNumberDuplicateMsg(device.getIme_number()));
	}
	
	@Override
	public String updateDevice(DeviceEntity deviceTo) {
		
		Session session = sfactory.openSession();
		Transaction tx = null;
		int res;
		try{
			tx = session.beginTransaction();
			//Session session=HibernateUtil.getSession();
			/*if(CommonUtils.exists(device.getEmployee_name())){
				String employee_id=(String) session.createSQLQuery("select employee_id from employee d where d.employee_name=\'"+device.getEmployee_name()+"\'").uniqueResult();
				device.setEmployee_id(employee_id);;
			}else{
				device.setEmployee_id("0");
			}*/
			DeviceEntity device = (DeviceEntity) session.load(DeviceEntity.class, deviceTo.getDevice_id());
			
			isDeviceNameExistsForUpdateDevice(device.getClient_id(), session, device);
			isSimNumberExistsForUpdateDevice(device.getClient_id(), session, device);
			isImeiNumberExistsForUpdateDevice(device.getClient_id(), session, device);
			
			
			device.setData_plan(deviceTo.getData_plan());
			device.setDate_of_purchase(deviceTo.getDate_of_purchase());
			device.setDevice_name(deviceTo.getDevice_name());
			device.setSim_number(deviceTo.getSim_number());
			device.setIme_number(deviceTo.getIme_number());
			device.setService_provider(deviceTo.getService_provider());
			device.setStatus(deviceTo.isStatus());
			
			if(CommonUtils.exists(device.getEmployee_id()) && CommonUtils.exists(device.getEmployee_name())){
				Employee emp = (Employee) session.load(Employee.class, device.getEmployee_id());
				
				if(!emp.getAssign_device().equalsIgnoreCase(device.getDevice_name())){
					emp.setAssign_device(deviceTo.getDevice_name());
					session.update(emp);
				}
			}
			
			session.update(device);
			res=1;
			tx.commit();
		} catch (DeviceNameDuplicatedException e) {
			if (tx != null)tx.rollback();
			logger.error(e);
			return e.toString();
		}  catch (SimNumberDuplicatedException e) {
			if (tx != null)tx.rollback();
			logger.error(e);
			return e.toString();
		}  catch (ImeiNumberDuplicatedException e) {
			if (tx != null)tx.rollback();
			logger.error(e);
			return e.toString();
		}  catch (ConstraintViolationException e) {
			if (tx != null)tx.rollback();
			logger.error(e);
			return "MCI1001 Internal Server Error: Please contact your site adminstrator!";
		} catch (DataException e) {
			if (tx != null)tx.rollback();
			logger.error(e);
			return "MCI1002 Internal Server Error: Please contact your site adminstrator!";
		} catch (HibernateException e) {
			if (tx != null)tx.rollback();
			logger.error(e);
			return "MCI1000 Internal Server Error: Please contact your site adminstrator!";
		} catch (Exception e) {
			if (tx != null)tx.rollback();
			logger.error(e);
			return "MCI100 Internal Server Error: Please contact your site adminstrator!";
		} finally{
			if(session != null)session.close();
		}
		return "1";
		
	}

	

	@SuppressWarnings({ "unused", "unchecked" })
	@Override
	public List<DeviceEntity> getDeviceByIdAndName(String device_id,
			String device_name) {
			List<DeviceEntity> list=null;
		
		try{
			Session session=sfactory.openSession();
			Transaction tx = session.beginTransaction();
			
			String hql="from DeviceEntity e where e.device_id=?";
			Query q=session.createQuery(hql);
			q=q.setParameter(0, device_id);
			list=q.list();
			
			DeviceEntity device=(DeviceEntity) list.get(0);
			
			if(device.getDevice_name().equalsIgnoreCase(device_name)){
				return list;
			}
			else {
				list=null;
				return list;
			}
			
		}catch(Exception e){
			logger.error(e);
			list=null;
			return list;
		}
	}

	@SuppressWarnings({ "unused", "unchecked" })
	@Override
	public List<DeviceEntity> getDeviceByName(String device_name) {
		DeviceEntity device=null;
		List<DeviceEntity> list=new ArrayList<DeviceEntity>();
		try{
			Session session=sfactory.openSession();
			//Session session=HibernateUtil.getSession();
			String hql="from DeviceEntity d where d.device_name=?";
			Query q=session.createQuery(hql);
			q=q.setParameter(0, device_name);
			list=q.list();
			device=(DeviceEntity) list.get(0);
			session.close();
			
		}catch(Exception e){
			logger.error(e);
			return null;
		}
		return list;
	}

	
	@SuppressWarnings("unchecked")
	@Override
	public List<DeviceTo> searchDevices(SearchDevice searchDevice) {
	
		
		StringBuffer attachQuery= new StringBuffer("from DeviceEntity d ");
		attachQuery.append("where client_id = '"+searchDevice.getClient_id()+"' AND ");
		boolean isSearchParametersExist = true;
		if(CommonUtils.exists(searchDevice.getDevice_id()) || CommonUtils.exists(searchDevice.getDevice_name()) 
				||CommonUtils.exists(searchDevice.getSim_number()) ){
			
			isSearchParametersExist =true;
			
		}
		
		if(CommonUtils.exists(searchDevice.getDevice_id())){
			
			attachQuery.append("d.device_id like \'%");
			attachQuery.append(searchDevice.getDevice_id());
			attachQuery.append("%\'");
			attachQuery.append(" AND ");
		}
		if(CommonUtils.exists(searchDevice.getDevice_name())){
			
			
			attachQuery.append("d.device_name like \'%");
			attachQuery.append(searchDevice.getDevice_name());
			attachQuery.append("%\'");
			attachQuery.append(" AND ");
		}
		
		if(CommonUtils.exists(searchDevice.getSim_number())){
			
			attachQuery.append("d.sim_number like \'%");
			attachQuery.append(searchDevice.getSim_number());
			attachQuery.append("%\'");
			attachQuery.append(" AND ");
		}

		
		String searchQuery = attachQuery.toString();
		
		if(isSearchParametersExist){
			searchQuery = attachQuery.toString().substring(0,attachQuery.length()-4);
		}
		 
		logger.info(searchQuery);
		Session session = null;
		Query query = null;
		Transaction tx = null;
		List<DeviceTo> deviceList = new ArrayList<DeviceTo>();
		List<DeviceTo> list = null;
		try{
		session = sfactory.openSession();
		tx = session.beginTransaction();
		query = session.createQuery(searchQuery);
		list = query.list();
		BeanUtils.copyProperties(list, deviceList);
		tx.commit();
		}catch(HibernateException e){
			logger.error(e);
		}finally{
			if(session != null)session.close();
		}
		
		return list;
		
	/*	StringBuffer attachQuery= new StringBuffer();
		attachQuery.append("select e.employee_name,d.device_id,d.device_name,d.date_of_purchase,d.ime_number,d.service_provider,d.data_plan,e.contact_number,d.status ");
		attachQuery.append("from mycable.employee e RIGHT JOIN mycable.device d ");
		attachQuery.append("on e.device_id=d.device_id ");
		attachQuery.append(" UNION ");
		attachQuery.append("select e.employee_name,d.device_id,d.device_name,d.date_of_purchase,d.ime_number,d.service_provider,d.data_plan,e.contact_number ");
		attachQuery.append("from mycable.employee e RIGHT JOIN mycable.device d ");
		attachQuery.append("on e.device_id=d.device_id ");
		boolean isSearchParametersExist = false;
		if(CommonUtils.exists(searchDevice.getEmployee_name()) || CommonUtils.exists(searchDevice.getContact_number())
				||CommonUtils.exitsInt(searchDevice.getDevice_id()) || CommonUtils.exists(searchDevice.getDevice_name())){
			attachQuery.append("where ");
			isSearchParametersExist =true;
			
		}
		
		if(CommonUtils.exists(searchDevice.getEmployee_name())){
			
			attachQuery.append("e.employee_name like \'%");
			attachQuery.append(searchDevice.getEmployee_name());
			attachQuery.append("%\'");
			attachQuery.append(" AND ");
		}
		if(CommonUtils.exists(searchDevice.getContact_number())){
			
			
			attachQuery.append("e.contact_number like \'%");
			attachQuery.append(searchDevice.getContact_number());
			attachQuery.append("%\'");
			attachQuery.append(" AND ");
		}
		
		if(CommonUtils.exists(searchDevice.getDevice_name())){
			
			attachQuery.append("d.device_name like \'%");
			attachQuery.append(searchDevice.getDevice_name());
			attachQuery.append("%\'");
			attachQuery.append(" AND ");
		}

		
		String searchQuery = attachQuery.toString();
		
		if(isSearchParametersExist){
			searchQuery = attachQuery.toString().substring(0,attachQuery.length()-4);
		}
		 
		 logger.info(searchQuery);
		 Session session=sfactory.openSession();
		
		Query query = session.createSQLQuery(searchQuery).setResultTransformer(Transformers.aliasToBean(SearchDeviceEntity.class));
		return (List<SearchDeviceEntity>)query.list();
*/		
	}

	@SuppressWarnings("rawtypes")
	@Override
	public String deleteDevice(String device_id) {

		try{
			Session session=sfactory.openSession();
			//Session session=HibernateUtil.getSession();
		Transaction tx=session.beginTransaction();
		String hql="from DeviceEntity e where e.device_id=?";
		Query q=session.createQuery(hql);
		q=q.setParameter(0, device_id);
		List list=q.list();
		
		DeviceEntity device=(DeviceEntity) list.get(0);
		
		session.delete("DeviceEntity", device);
		tx.commit();
		session.close();
		}catch(Exception e){
			logger.error(e);
			return "Not Done";
		}
		return "Done";
	}
	
	@Override
	public String deActivateStatus(int device_id) {
		String res="Done";
		
		try{
			Session session=sfactory.openSession();
			//Session session=HibernateUtil.getSession();
			Transaction tx = session.beginTransaction(); 
			DeviceEntity device=(DeviceEntity) session.load(DeviceEntity.class, device_id);
			device.setStatus(false);
			session.update(device);
			
			tx.commit();
			session.close();
			}catch(Exception e){
				logger.error(e);
				return "No row Found with the id";
			}
		
		
		return res;
	}
	
	@Override
	public String activateStatus(int device_id) {
		String res="Done";
		
		try{
			Session session=sfactory.openSession();
			//Session session=HibernateUtil.getSession();
			Transaction tx = session.beginTransaction(); 
			DeviceEntity device=(DeviceEntity) session.load(DeviceEntity.class, device_id);
			device.setStatus(true);
			session.update(device);
			
			tx.commit();
			session.close();
			}catch(Exception e){
				logger.error(e);
				return "No row Found with the id";
			}
		
		
		return res;
	}

	@SuppressWarnings({ "rawtypes", "unchecked" })
	@Override
	public List<DeviceNamesTo> getDeviceNames(String client_id) {
		List<DeviceNamesTo> deviceNames = new ArrayList<DeviceNamesTo>();
		Session session=sfactory.openSession();
		//String hql = "select device_id,device_name from device where device_id not in (select device_id from employee) and client_id = '"+client_id+"'";
		
		/*select device_id,device_name,
		case when employee_id is null && employee_name is null then 0 when employee_id is not null  && employee_name is not null  then 1 end as assigned_status 
		from device where client_id = "MYA01000"*/
		
		String hql = "select device_id, device_name,"
				+ " case when employee_id is null && employee_name is null then false when employee_id is not null  && employee_name is not null  then true end as assigned_status"
				+ " from device where"
				+ " client_id = '"+client_id+"'";
		SQLQuery q=session.createSQLQuery(hql);
		List<DeviceNamesTo> list = q.setResultTransformer(Transformers.aliasToBean(DeviceNamesTo.class)).list();
		logger.info(list);
		deviceNames.addAll(list);
		return list;
	}

	@SuppressWarnings("unchecked")
	@Override
	public List<DeviceTo> getInactiveDevices(String client_id) {
		List<DeviceTo> result = new ArrayList<DeviceTo>();
		Session session = sfactory.openSession();
		Transaction tx = null;
		try {
			tx = session.beginTransaction();
			
			List<DeviceEntity> resultset = session.createQuery("from DeviceEntity e where e.status = ? AND e.client_id = ?")
					.setBoolean(0, false)
					.setString(1, client_id)
					.list();
			
			for(DeviceEntity device:resultset){
				DeviceTo dev = new DeviceTo();
				BeanUtils.copyProperties(device, dev);
				result.add(dev);
			}
			
			tx.commit();
		}catch (HibernateException e) {
			if (tx != null)
				tx.rollback();
			logger.error("Exception! " + e);
		}finally {
			session.close();
		}
		return result;
	}

	@Override
	public List<DeviceHistoryTo> getDeviceHistory(int device_id) {
		List<DeviceHistoryTo> result = new ArrayList<DeviceHistoryTo>();
		Session session = sfactory.openSession();
		Transaction tx = null;
		try {
			tx = session.beginTransaction();
			
			@SuppressWarnings("unchecked")
			List<DeviceHistoryTo> resultset = session.createSQLQuery(CommonUtils.deviceHistoryQuery)
					.setResultTransformer(Transformers.aliasToBean(DeviceHistoryTo.class))
					.setInteger(0, device_id)
					.list();
			
			result = resultset;
			
			tx.commit();
		}catch (HibernateException e) {
			if (tx != null)
				tx.rollback();
			logger.error("Exception! " + e);
		}finally {
			session.close();
		}
		return result;
	}

	@Override
	public DeviceEntity getDeviceDetails(String client_id) {
		Session session = null;
		DeviceEntity device = new DeviceEntity();
		Transaction tx = null;
		try{
			session = sfactory.openSession();
			tx = session.beginTransaction();
			//Session session=HibernateUtil.getSession();
			SelectedDevice selDevice = null;
			
			Query q = session.createQuery("from SelectedDevice e where e.client_id=?");
			q.setParameter(0, client_id);
			List<SelectedDevice> list=q.list();
			selDevice =  list.get(0);
			
			logger.info(selDevice);
			
			
			BeanUtils.copyProperties(selDevice, device);
			logger.info("selectedDevice"+selDevice);
			session.delete(selDevice);
			
			tx.commit();
		}catch(HibernateException e){
			logger.error(e);
			return device;
		}catch(Exception e){
			logger.error(e);
			return device;
		}finally{
			session.close();
		}
		return device;
	}

	@Override
	public DeviceEntity getDeviceById(String device_id) {
			
		DeviceEntity device = null;
		Session session = null;
		Transaction tx = null;
		try{
			session = sfactory.openSession();
			tx = session.beginTransaction();
			//Session session=HibernateUtil.getSession();
			device = (DeviceEntity) session.createSQLQuery("select * from device where device_id="+device_id+";")
					.addEntity(DeviceEntity.class)
					.list().get(0);
			logger.info(device);
			
			tx.commit();
			
		}catch(Exception e){
			logger.error(e);
			return null;
		}finally{
			session.close();
		}
		logger.info("***************end************");
		return device;
	}

	@Override
	public void updateDeviceLoaction(String imeNumber, String longitude,
			String lattitude) {

		DeviceLocation deviceLocation = new DeviceLocation();
		Session session = null;
		Transaction tx = null;
		try {
			session = sfactory.openSession();
			tx = session.beginTransaction();
			logger.info("******** ime_number : " + imeNumber);
			DeviceEntity device = (DeviceEntity) session.createQuery("from DeviceEntity de where de.ime_number = ?").setString(0, imeNumber).list()
					.get(0);
			logger.info("########update location details############" + device);
			device.setLogin_status(true);
			session.update(device);
			deviceLocation.setDeviceId(device.getDevice_id());
			deviceLocation.setLocationLangitude(longitude);
			deviceLocation.setLocationLattitude(lattitude);
			
			logger.info(device);
			logger.info(deviceLocation);
			session.saveOrUpdate(deviceLocation);
			
			logger.info("########update location details End############");
			tx.commit();

		} catch (Exception e) {
			logger.error(e);
		} finally {
			session.close();
		}
		logger.info("***************end************");

	}

	@Override
	public List<DeviceLocationTo> getDeviceLocation(String client_id) {

		List<DeviceLocationTo> deviceLocationToList = new ArrayList<DeviceLocationTo>();
		Session session = null;
		Transaction tx = null;
		try{
			session = sfactory.openSession();
			tx = session.beginTransaction();
			//Session session=HibernateUtil.getSession();
			
			
			String sqlQuery = "select d.device_name as deviceName, d.login_status as deviceStatus, e.employee_id as deviceOwnerID, e.employee_name as deviceOwner, dl.location_longitude, dl.location_lattitude, "
					+ "TIMESTAMPDIFF(SECOND, dl.updated_timestamp, current_timestamp()) as timeDiff"
					+ " from device d inner join employee e on d.employee_id = e.employee_id" 
					+ " inner join device_locations dl on dl.device_id = d.device_id"
					+ " where d.client_id = ?";
			
			List<DeviceLacationRespons> resultset = session.createSQLQuery(sqlQuery)
					.setResultTransformer(Transformers.aliasToBean(DeviceLacationRespons.class))
					.setString(0, client_id)
					.list();
			
			
			for(DeviceLacationRespons response : resultset){
				DeviceLocationTo deviceLoactionTo = new DeviceLocationTo();
				
				BeanUtils.copyProperties(response, deviceLoactionTo);
				deviceLoactionTo.setLastTransactionMadeBefore(SimpleUtill.timeConversion(response.getTimeDiff().intValue()));
				logger.info(response.getLocation_lattitude());
				logger.info(response.getLocation_longitude());
				logger.info("*************************" + (CommonUtils.exists(response.getLocation_lattitude()) ? "0" :response.getLocation_lattitude()));
				List<Double> locationvars = new ArrayList<Double>();
				if(CommonUtils.exists(response.getLocation_lattitude())&&CommonUtils.exists(response.getLocation_longitude())){
					locationvars.add(new Double((!response.getLocation_lattitude().equals("null")) ? response.getLocation_lattitude() : "0.0"));
					locationvars.add(new Double((!response.getLocation_longitude().equals("null"))  ? response.getLocation_longitude() : "0.0"));
				}else{
					locationvars.add(new Double(0));
					locationvars.add(new Double(0));
				}
				deviceLoactionTo.setDeviceLocation(locationvars);
				DashBoardCollectionDetailsTo employeeCollectionDetails = getEmployeeCollectionDetails(session, deviceLoactionTo.getDeviceOwnerID());
				//deviceLoactionTo.setCurrentDayCollection(getEmployeeCollectionDetails(session, deviceLoactionTo.getDeviceOwnerID(), client_id)); 
				deviceLoactionTo.setCurrentDayCollection(employeeCollectionDetails.getDay_collection());
				deviceLoactionTo.setCurrentWeekCollection(employeeCollectionDetails.getWeek_collection());
				deviceLoactionTo.setCurrentMonthCollection(employeeCollectionDetails.getMonth_collection());
				deviceLocationToList.add(deviceLoactionTo);
			}
			
			logger.info(resultset);
			
			tx.commit();
			
		}catch(Exception e){
			logger.error(e);
			return null;
		}finally{
			session.close();
		}
		logger.info("***************end************");
		return deviceLocationToList;
	}

	private DashBoardCollectionDetailsTo getEmployeeCollectionDetails(Session session, String employee_id) {
		DashBoardCollectionDetailsTo response = new DashBoardCollectionDetailsTo();
		try{
			response =  (DashBoardCollectionDetailsTo) session.createSQLQuery(
					"CALL IndividualEmployeCollectionDetails(:iemployee_id)")
					.setString("iemployee_id", employee_id)
					.setResultTransformer(Transformers.aliasToBean(DashBoardCollectionDetailsTo.class)).uniqueResult();
					logger.info(response);
		}catch(HibernateException exception){
			logger.error(exception);
		}catch (Exception e) {
			logger.error(e);// TODO: handle exception
		}
		return response;
	}
}
