package com.blickx.dao;

import java.util.List;

import com.blickx.domain.AreaEmployeeMapEntity;
import com.blickx.domain.Employee;
import com.blickx.searchEntities.SearchEmployee;
import com.blickx.to.EmployeAddOrUpdateTo;
import com.blickx.to.GetEmployeeCollectionTo;
import com.blickx.to.GetEmployeeNamesWithIds;

public interface EmployeeDao {

	
	public String addEmployee(Employee emp, List<AreaEmployeeMapEntity> areaEMployeeMapEntities);

	public EmployeAddOrUpdateTo getEmployeeById(String employee_id);

	public String updateEmployee(Employee employee, EmployeAddOrUpdateTo forAreaCodes);

	public List<Employee> getEmployeeByIdAndName(String employee_id, String employee_name);

	public List<Employee> getEmpByName(String employee_name);

	public String activateStatus(String employee_id);
	
	public String deactivateStatus(String employee_id);

	List<Employee> searchEmployee(SearchEmployee searchEmployee);

	public List<GetEmployeeCollectionTo> getEmployeeCollection(
			String employee_id);

	public List<String> getEmployeeNames();
	
	public List<String> getEmployeeNamesToAssignComplaints(String client_id);

	public List<Employee> getInactiveEmployees(String client_id);

	public String changePassword(String employee_id, String password);

	public List<GetEmployeeNamesWithIds> getEmployeeNamesWithIds(String client_id);

	public int uidUpdate(String employee_id, String uid_number);

	public String getEmployee_id(String uid_number);

	public Employee Unique_Employee_Details(String client_id);
}
