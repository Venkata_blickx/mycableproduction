package com.blickx.dao;

import java.util.List;
import java.util.Set;

import com.blickx.domain.BillGeneration;
import com.blickx.domain.Customer;
import com.blickx.domain.CustomerToMobile;
import com.blickx.domain.PackageEntity;
import com.blickx.domain.SetupBox;
import com.blickx.mycable.customerAddElements.CustomerAddInputTo;
import com.blickx.output.EditCustomerCommentTo;
import com.blickx.response.PackagesAndAddonsOfCustomer;
import com.blickx.searchEntities.PaymentFields;
import com.blickx.searchEntities.SearchCustomer;
import com.blickx.settings.to.AddonsEntityTo;
import com.blickx.to.CustomerBillTo;
import com.blickx.to.CustomerTo;
import com.blickx.to.CustomerWrapperTo;
import com.blickx.to.MiniStatementForAndroidTo;
import com.blickx.to.PackageReportsForAndroidTo;
import com.blickx.to.PaymentTxEntityDTO;
import com.blickx.to.SearchCustomerMoreDetails;
import com.blickx.to.SearchCustomerTo;
import com.blickx.to.SetUpBoxToo;
import com.blickx.to.SetupBoxTo;
import com.blickx.to.ViewBillsByMonthTo;
import com.blickx.to.input.ContactUpdateTo;
import com.blickx.to.input.CustomerCredentials;
import com.blickx.to.input.GetCustomer;
import com.blickx.to.input.InputIds;
import com.blickx.to.input.UidInsertInput;
import com.blickx.utill.BillGenType;
import com.blickx.utill.CurrentDate;
import com.blickx.utill.CusHistoryDto;
import com.blickx.utill.CustomerHistoryDto;

public interface CustomerDao {
	
	//public String updateCustomerData(GetCustomer customerData);
	
	public CustomerTo getUniqueCustomerProfile(String client_id);
	
	public String addCustomer(String client_id, GetCustomer customer);
	
	public List<Customer> getCustById(String custId);
	
	public String updateCustomer(Customer customer, List<EditCustomerCommentTo> list);
	
	public int deleteCustomer(String customer_id);
	
	public CustomerTo getCustomerById(String customer_id);
	
	public CustomerAddInputTo getCustomerProfileById(String customer_id);
	
	public String activateStatus(InputIds input);
	
	public String deActivateStatus(InputIds input);

	public CustomerWrapperTo searchCustomers(SearchCustomer searchCustomer);

	public CustomerToMobile getCustomerWithBillingDetails(String customer_id);
	
	public String billPayment(String customer_id, double paid, double balance,
			double total, String employee_id, String client_id);

	public CustomerBillTo getCustomerWithBillingDetails(
			CustomerTo customerCredentials);

	public List<MiniStatementForAndroidTo> miniStatement(String customer_id);

	public List<PackageReportsForAndroidTo> getPackageReports();

	public List<SearchCustomerTo> getDeactivatedCustomers(String client_id);

	public List<SearchCustomerTo> getUnPaidCustomers(String client_id);

	public String makePaymentFromDesktop(PaymentFields paymentfields);
	
	public boolean billPayments(String customer_id, String employee_id, double paid, double balance, double total, String client_id, String paymentTimeStamp, String transactionId);

	public ViewBillsByMonthTo viewBillsByMonth(CustomerCredentials customerCredentials);

	public String updateBox(SetupBoxTo setupBoxto);

	public int addExtraBox(SetupBoxTo setupBoxTo);

	public List<String> getCustomerDocs(String customer_id);

	public int deActivateBox(InputIds input);

	public List<SearchCustomerTo> searchCustomerTo(String client_id, SearchCustomer searchCustomer);

	public int uidUpdate(UidInsertInput uidInsertInput);

	public int deleteSetupBox(String box_number);

	public String getCustomer_id(String uid_number, String client_id);

	public int permenentDeActive(InputIds input);

	public int activateBox(InputIds input);

	public String updateAddon(AddonsEntityTo addon, String client_id);

	public String deleteAddon(int addonId, String client_id);

	public SetUpBoxToo getSettopBoxByBoxNumber(String boxNumber);

	public String addAddons(Set<AddonsEntityTo> addOnEntityTo, String client_id);

	public List<AddonsEntityTo> getTheSetupboxAddons(String boxNumber);

	public List<PackagesAndAddonsOfCustomer> getTheCustomerAddons(String customer_id, String client_id);

	
	public List<SetUpBoxToo> getTheSetupboxesByCustomerId(String customer_id);

	public String getCustomerIDUsingCid(String client_id,
			SearchCustomer searchCustomer);

	public int updateContactNumber(ContactUpdateTo data);
	
	public int updateEmail(ContactUpdateTo data);

	public AddonsEntityTo getAddonEntity(String box_number, String get_name);

	public int setupboxPermenantDeActivate(InputIds inputIds);

	public List<SearchCustomerTo> permenentDeactiveList(String client_id);

	public List<SearchCustomerMoreDetails> getCustomerMoreDetails(String customerId);

	public List<SetupBox> getActiveSetupBoxList(String customerId,
			String clientId);

	public CustomerHistoryDto getCustomerHistory(BillGenType cusDetails,
			String clientId);

	public PackageEntity getSetupBoxPackageDetails(SetupBox setup);

	public BillGeneration getBillGenerationDetails(BillGenType billGenType,
			SetupBox setupBox, PackageEntity pa, int month, int year);

	public CurrentDate getCurrentDate();

	public BillGeneration createBill(BillGenType billGen, SetupBox setup,
			PackageEntity pa, int month, int year);

	public boolean generateBillCurrentMonth(
			BillGenType billGen, SetupBox setup, PackageEntity pa);

	public void updateArppCustomerPreviousBilling(BillGenType billGen,
			SetupBox setup, PackageEntity pa, String clientId);

	public boolean checkCustomerPaidStatus(String customerId, String clientId);

	

	

	/*public void saveOnlineTxDetails(PaymentTxEntityDTO payEntity);

	public void updateTxAfterEBSResponse(PaymentTxEntityDTO paymentTxEntity);
*/
}
