package com.blickx.searchEntities;

public class SearchCustomer {

	private String customer_id;
	private String customer_name;
	private String contact_number;
	private String areacode;
	private String c_id;
	private String card_number;
	private String customer_uid;
	private String boxOrVcNumber;
	
	public String getBoxOrVcNumber() {
		return boxOrVcNumber;
	}

	public void setBoxOrVcNumber(String boxOrVcNumber) {
		this.boxOrVcNumber = boxOrVcNumber;
	}

	public String getCustomer_uid() {
		return customer_uid;
	}

	public void setCustomer_uid(String customer_uid) {
		this.customer_uid = customer_uid;
	}

	@Override
	public String toString() {
		return "SearchCustomer [customer_id=" + customer_id
				+ ", customer_name=" + customer_name + ", contact_number="
				+ contact_number + ", areacode=" + areacode + ", c_id=" + c_id
				+ ", card_number=" + card_number + ", customer_uid="
				+ customer_uid + ", boxOrVcNumber=" + boxOrVcNumber + "]";
	}

	public String getC_id() {
		return c_id;
	}

	public void setC_id(String c_id) {
		this.c_id = c_id;
	}

	public String getCard_number() {
		return card_number;
	}

	public void setCard_number(String card_number) {
		this.card_number = card_number;
	}

	public String getCustomer_id() {
		return customer_id;
	}

	public void setCustomer_id(String customer_id) {
		this.customer_id = customer_id;
	}

	public String getCustomer_name() {
		return customer_name;
	}

	public void setCustomer_name(String customer_name) {
		this.customer_name = customer_name;
	}

	public String getContact_number() {
		return contact_number;
	}

	public void setContact_number(String contact_number) {
		this.contact_number = contact_number;
	}

	public String getAreacode() {
		return areacode;
	}

	public void setAreacode(String areacode) {
		this.areacode = areacode;
	}

	public SearchCustomer(String customer_id, String customer_name,
			String contact_number, String areacode) {
		super();
		this.customer_id = customer_id;
		this.customer_name = customer_name;
		this.contact_number = contact_number;
		this.areacode = areacode;
	}
	
	public SearchCustomer(){}
}
