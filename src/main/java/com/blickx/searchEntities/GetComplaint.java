package com.blickx.searchEntities;

public class GetComplaint {

	private String cutomer_id;
	private String customer_name;
	private String c_id;
	private String complaint_no;
	private String client_id;
	
	public String getCutomer_id() {
		return cutomer_id;
	}
	public void setCutomer_id(String cutomer_id) {
		this.cutomer_id = cutomer_id;
	}
	public String getClient_id() {
		return client_id;
	}
	public void setClient_id(String client_id) {
		this.client_id = client_id;
	}
	public String getCustomer_name() {
		return customer_name;
	}
	public void setCustomer_name(String customer_name) {
		this.customer_name = customer_name;
	}
	public String getComplaint_no() {
		return complaint_no;
	}
	public void setComplaint_no(String complaint_no) {
		this.complaint_no = complaint_no;
	}
	
	public String getC_id() {
		return c_id;
	}
	public void setC_id(String c_id) {
		this.c_id = c_id;
	}
	public GetComplaint(){
		
	}
}
