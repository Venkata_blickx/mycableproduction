package com.blickx.searchEntities;

import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;

import org.springframework.format.annotation.DateTimeFormat;

@Entity
public class SearchByDateEntity {

	@Column
	@DateTimeFormat(pattern="yyyy-MM-dd")
	private Date from;
	
	@Column
	@DateTimeFormat(pattern="yyyy-MM-dd")
	private Date to;
	
	private String client_id;

	public String getClient_id() {
		return client_id;
	}

	public void setClient_id(String client_id) {
		this.client_id = client_id;
	}

	public Date getFrom() {
		return from;
	}

	public void setFrom(Date from) {
		this.from = from;
	}

	public Date getTo() {
		return to;
	}

	public void setTo(Date to) {
		this.to = to;
	}
	
	public SearchByDateEntity(){
		
	}
}
