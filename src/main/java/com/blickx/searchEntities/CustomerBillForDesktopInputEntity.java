package com.blickx.searchEntities;

public class CustomerBillForDesktopInputEntity {

	private String customer_id;
	private String customer_name;
	private int type;
	
	public String getCustomer_id() {
		return customer_id;
	}
	public void setCustomer_id(String customer_id) {
		this.customer_id = customer_id;
	}
	public String getCustomer_name() {
		return customer_name;
	}
	public void setCustomer_name(String customer_name) {
		this.customer_name = customer_name;
	}
	public int getType() {
		return type;
	}
	public void setType(int type) {
		this.type = type;
	}
	
	public CustomerBillForDesktopInputEntity(){
		
	}
}
