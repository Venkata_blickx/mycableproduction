package com.blickx.searchEntities;

import java.util.Date;

import org.springframework.format.annotation.DateTimeFormat;


public class CloseComplaint {

	private String complaint_id;
	
	private String comment;
	
	private String employee_name;
	
	private String closed_by;
	
	@DateTimeFormat(pattern="yyyy-MM-dd")
	private Date closed_date;

	public Date getClosed_date() {
		return closed_date;
	}

	public void setClosed_date(Date closed_date) {
		this.closed_date = closed_date;
	}

	@Override
	public String toString() {
		return "CloseComplaint [complaint_id=" + complaint_id + ", comment="
				+ comment + ", employee_name=" + employee_name + ", closed_by="
				+ closed_by + ", closed_date=" + closed_date + "]";
	}

	public String getClosed_by() {
		return closed_by;
	}

	public void setClosed_by(String closed_by) {
		this.closed_by = closed_by;
	}

	public String getEmployee_name() {
		return employee_name;
	}

	public void setEmployee_name(String employee_name) {
		this.employee_name = employee_name;
	}

	public String getComplaint_id() {
		return complaint_id;
	}

	public void setComplaint_id(String complaint_id) {
		this.complaint_id = complaint_id;
	}

	public String getComment() {
		return comment;
	}

	public void setComment(String comment) {
		this.comment = comment;
	}
	
	/*@Column
	@DateTimeFormat(pattern="yyyy-MM-dd")
	private Date close_date;*/
	
	public CloseComplaint(){
		
	}
}
