package com.blickx.searchEntities;

import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;

import org.springframework.format.annotation.DateTimeFormat;

@Entity
public class AssignComplaintSearch {

	@Override
	public String toString() {
		return "AssignComplaintSearch [complaint_id=" + complaint_id
				+ ", employee_name=" + employee_name + ", assigned_date="
				+ assigned_date + "]";
	}

	@Column
	private String complaint_id;
	
	@Column
	private String employee_name;
	
	@Column
	@DateTimeFormat(pattern="yyyy-MM-dd")
	private Date assigned_date;

	
	public String getComplaint_id() {
		return complaint_id;
	}

	public void setComplaint_id(String complaint_id) {
		this.complaint_id = complaint_id;
	}

	public String getEmployee_name() {
		return employee_name;
	}

	public void setEmployee_name(String employee_name) {
		this.employee_name = employee_name;
	}

	public Date getAssigned_date() {
		return assigned_date;
	}

	public void setAssigned_date(Date assigned_date) {
		this.assigned_date = assigned_date;
	}
	
	public AssignComplaintSearch(){
		
	}
}
