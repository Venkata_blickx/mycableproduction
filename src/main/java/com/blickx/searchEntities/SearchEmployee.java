package com.blickx.searchEntities;

public class SearchEmployee {

	private String employee_id;
	private String employee_name;
	private String contact_number;
	private String areacode;
	private String client_id;
	

	public String getClient_id() {
		return client_id;
	}

	public void setClient_id(String client_id) {
		this.client_id = client_id;
	}

	public String getEmployee_id() {
		return employee_id;
	}

	public void setEmployee_id(String employee_id) {
		this.employee_id = employee_id;
	}

	public String getEmployee_name() {
		return employee_name;
	}

	public void setEmployee_name(String employee_name) {
		this.employee_name = employee_name;
	}

	public String getContact_number() {
		return contact_number;
	}

	public void setContact_number(String contact_number) {
		this.contact_number = contact_number;
	}

	public String getAreacode() {
		return areacode;
	}

	public void setAreacode(String areacode) {
		this.areacode = areacode;
	}
	
	public SearchEmployee(String employee_id, String employee_name,
			String contact_number, String areacode) {
		super();
		this.employee_id = employee_id;
		this.employee_name = employee_name;
		this.contact_number = contact_number;
		this.areacode = areacode;
	}

	public SearchEmployee(){}
}
