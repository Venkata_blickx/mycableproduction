package com.blickx.searchEntities;

import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;

import org.springframework.format.annotation.DateTimeFormat;

@Entity
public class GetEmployeeReportsSearchEntity {

	@Override
	public String toString() {
		return "GetEmployeeReportsSearchEntity [employee_id=" + employee_id
				+ ", employee_name=" + employee_name + ", dateRange="
				+ dateRange + ", from=" + from + ", to=" + to + ", client_id="
				+ client_id + "]";
	}

	public String getDateRange() {
		return dateRange;
	}

	public void setDateRange(String dateRange) {
		this.dateRange = dateRange;
	}

	private String employee_id;

	private String employee_name;
	
	private String dateRange;

	private String from;

	private String to;

	private String client_id;
	
	
	public String getClient_id() {
		return client_id;
	}

	public void setClient_id(String client_id) {
		this.client_id = client_id;
	}

	public String getEmployee_id() {
		return employee_id;
	}

	public void setEmployee_id(String employee_id) {
		this.employee_id = employee_id;
	}

	public String getEmployee_name() {
		return employee_name;
	}

	public void setEmployee_name(String employee_name) {
		this.employee_name = employee_name;
	}

	public String getFrom() {
		return from;
	}

	public void setFrom(String from) {
		this.from = from;
	}

	public String getTo() {
		return to;
	}

	public void setTo(String to) {
		this.to = to;
	}

	public GetEmployeeReportsSearchEntity(){
		
	}
}
