package com.blickx.searchEntities;

public class SearchDevice {

	private String sim_number;
	private String device_id;
	private String device_name;
	private String client_id;
	
	
	public String getClient_id() {
		return client_id;
	}


	public void setClient_id(String client_id) {
		this.client_id = client_id;
	}


	public String getDevice_name() {
		return device_name;
	}


	public void setDevice_name(String device_name) {
		this.device_name = device_name;
	}



	public String getSim_number() {
		return sim_number;
	}


	public void setSim_number(String sim_number) {
		this.sim_number = sim_number;
	}


	public String getDevice_id() {
		return device_id;
	}


	public void setDevice_id(String device_id) {
		this.device_id = device_id;
	}

	public SearchDevice(){}
}
