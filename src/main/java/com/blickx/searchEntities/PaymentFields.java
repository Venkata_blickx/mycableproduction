package com.blickx.searchEntities;


public class PaymentFields {

	private String customer_id;
	private String employee_id;
	private double balance;
	private double total_amount;
	private double paid_amount;
	private String cheque_No;
	private String payment_type;
	private double discount;
	private String client_id;
	private String transaction_id;
	
	
	@Override
	public String toString() {
		return "PaymentFields [customer_id=" + customer_id + ", employee_id="
				+ employee_id + ", balance=" + balance + ", total_amount="
				+ total_amount + ", paid_amount=" + paid_amount
				+ ", cheque_No=" + cheque_No + ", payment_type=" + payment_type
				+ ", discount=" + discount + ", client_id=" + client_id
				+ ", transaction_id=" + transaction_id + "]";
	}
	public String getTransaction_id() {
		return transaction_id;
	}
	public void setTransaction_id(String transaction_id) {
		this.transaction_id = transaction_id;
	}
	public String getClient_id() {
		return client_id;
	}
	public void setClient_id(String client_id) {
		this.client_id = client_id;
	}
	public double getDiscount() {
		return discount;
	}
	public void setDiscount(double discount) {
		this.discount = discount;
	}
	public String getCheque_No() {
		return cheque_No;
	}
	public void setCheque_No(String cheque_No) {
		this.cheque_No = cheque_No;
	}
	public String getPayment_type() {
		return payment_type;
	}
	public void setPayment_type(String payment_type) {
		this.payment_type = payment_type;
	}
	public String getCustomer_id() {
		return customer_id;
	}
	public void setCustomer_id(String customer_id) {
		this.customer_id = customer_id;
	}
	public String getEmployee_id() {
		return employee_id;
	}
	public void setEmployee_id(String employee_id) {
		this.employee_id = employee_id;
	}
	public double getBalance() {
		return balance;
	}
	public void setBalance(double balance) {
		this.balance = balance;
	}
	public double getTotal_amount() {
		return total_amount;
	}
	public void setTotal_amount(double total_amount) {
		this.total_amount = total_amount;
	}
	public double getPaid_amount() {
		return paid_amount;
	}
	public void setPaid_amount(double paid_amount) {
		this.paid_amount = paid_amount;
	}
	
	public PaymentFields(){
		
	}
}
