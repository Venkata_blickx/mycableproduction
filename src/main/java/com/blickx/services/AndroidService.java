package com.blickx.services;

import java.util.List;

import com.blickx.domain.ClientInformationEntity;
import com.blickx.domain.CustomerToMobile;
import com.blickx.output.PaidCustomerTo;
import com.blickx.to.CustomerToMobileTo;
import com.blickx.to.EmployeeCredentialsTo;
import com.blickx.to.input.InputIds;

public interface AndroidService {

	public List<CustomerToMobile> getAllCustomerBills(String client_id);

	public List<EmployeeCredentialsTo> getAllEmployeCredetials(String client_id);

	public List<CustomerToMobileTo> getAllCustomersBills(String client_id, String ime_number);
	
	public List<ClientInformationEntity> getClientInfo(String client_id);

	public List<CustomerToMobileTo> getAllUpdatedCustomersBills(
			String client_id, InputIds input);

	public List<PaidCustomerTo> getPaidTxs(String client_id, InputIds input);


}
