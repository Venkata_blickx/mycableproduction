package com.blickx.services;

import java.util.List;

import com.blickx.domain.AreaEntity;
import com.blickx.settings.to.AreaEntityTo;

public interface AreaService {

	public String addAreaDetails(AreaEntity area);

	public AreaEntity getAreaDetailsById(AreaEntity area);

	public String updateAreaDetails(AreaEntity area);

	public String deleteAreaDetails(AreaEntityTo area);

	public List<AreaEntityTo> getAllAreaDetails(String client_id);

}
