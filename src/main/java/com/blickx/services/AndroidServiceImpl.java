package com.blickx.services;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;

import com.blickx.dao.AndroidDao;
import com.blickx.domain.ClientInformationEntity;
import com.blickx.domain.CustomerToMobile;
import com.blickx.output.PaidCustomerTo;
import com.blickx.to.CustomerToMobileTo;
import com.blickx.to.EmployeeCredentialsTo;
import com.blickx.to.input.InputIds;

public class AndroidServiceImpl implements AndroidService{

	
	@Autowired
	AndroidDao androidDao;
	
	@Override
	public List<CustomerToMobile> getAllCustomerBills(String client_id) {
		// TODO Auto-generated method stub
		return androidDao.getAllCustomerBills(client_id);
	}

	@Override
	public List<EmployeeCredentialsTo> getAllEmployeCredetials(String client_id) {
		// TODO Auto-generated method stub
		return androidDao.getAllEmployeCredetials(client_id);
	}

	@Override
	public List<CustomerToMobileTo> getAllCustomersBills(String client_id, String ime_number) {
		// TODO Auto-generated method stub
		return androidDao.getAllCustomersBills(client_id, ime_number);
	}
	
	@Override
	public List<ClientInformationEntity> getClientInfo(String client_id) {
		// TODO Auto-generated method stub
		return androidDao.getClientInfo(client_id);
	}

	@Override
	public List<CustomerToMobileTo> getAllUpdatedCustomersBills(
			String client_id, InputIds input) {
		// TODO Auto-generated method stub
		return androidDao.getAllUpdatedCustomersBills(client_id, input);
	}

	@Override
	public List<PaidCustomerTo> getPaidTxs(String client_id, InputIds input) {
		// TODO Auto-generated method stub
		return androidDao.getPaidTxs(client_id, input);
	}

	

}
