package com.blickx.services;

import java.util.List;
import java.util.Map;

import com.blickx.domain.ComplaintEntity;
import com.blickx.mycable.beme.dto.ComplaintBemeInputDTO;
import com.blickx.searchEntities.AssignComplaintSearch;
import com.blickx.searchEntities.CloseComplaint;
import com.blickx.searchEntities.GetComplaint;
import com.blickx.searchEntities.SearchByDateEntity;
import com.blickx.to.ComplaintRatingObject;
import com.blickx.to.ComplaintRatingTo;
import com.blickx.to.ComplaintStatusTo;
import com.blickx.to.ComplaintSummaryTo;
import com.blickx.to.GetComplaintTo;
import com.blickx.to.GetOpenClosedComplaintTo;
import com.blickx.to.input.ComplaintInputTo;

public interface ComplaintService {

	public String addComplaint(ComplaintEntity complaint);

	public ComplaintEntity getComplaint(GetComplaint getComplaint);

	public List<GetOpenClosedComplaintTo> getOpenOrClosedComplaints(String client_id, String type);

	public List<GetOpenClosedComplaintTo> getOpenOrClosedComplaintsByDate(
			SearchByDateEntity dates);

	public Map<String, String> updateComplaint(ComplaintEntity complaint);

	public ComplaintStatusTo getComplaintStatus(String client_id);

	public List<ComplaintSummaryTo> getComplaintSummaryOpen(String client_id);

	public int makeAssign(AssignComplaintSearch assign);

	public int closeComplaint(CloseComplaint closeComplaint);

	public ComplaintRatingObject getComplaintRating(String client_id);
	
	public String addComplaintDesktop(ComplaintInputTo complaintTo);

	public List<String> getC_ids(String client_id);

	public List<com.blickx.output.GetComplaintList> GetComplaintList(
			String client_id, Map<String, String> input);

	public Integer resolveComplaint(CloseComplaint closeComplaint);

	public List<com.blickx.output.GetComplaintList> GetUpdatedComplaintList(
			String client_id, Map<String, String> input);

	//public String addComplaintBeeMe(ComplaintBemeInputDTO complaintTo);

}
