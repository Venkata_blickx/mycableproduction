package com.blickx.services;

import java.util.List;

import com.blickx.settings.to.GetDeActiveAndActiveCustomersAndSetupBoxesCount;
import com.blickx.to.BillCollectorsGraphTO;
import com.blickx.to.CollectionStatistics;
import com.blickx.to.ConsolidatePaymentStatus;
import com.blickx.to.DailyCollectionReport;
import com.blickx.to.DashBoardCollectionDetailsTo;
import com.blickx.to.NoOfPayedCustomerTo;
import com.blickx.to.PackageDetailsForDashBoardTo;
import com.blickx.to.SmartCardIssuedCustomerTo;

public interface DashBoardService {

	public List<DailyCollectionReport> getDailyCollection(String client_id);

	public List<BillCollectorsGraphTO> getBillCollectorsGraph(String client_id);

	public List<PackageDetailsForDashBoardTo> getPackageDetails(String client_id);

	public NoOfPayedCustomerTo getNoOfCustomersPayed();

	public DashBoardCollectionDetailsTo getCollectionsForDashBoard(String client_id);

	public SmartCardIssuedCustomerTo getSmartCardIssuedCustomers(String client_id);

	public GetDeActiveAndActiveCustomersAndSetupBoxesCount getDeActivateAndActiveCustomersSetupBoxCount(String client_id);
	
	public ConsolidatePaymentStatus getConsolidatedPaymentStatus(String client_id);

	public List<CollectionStatistics> getCollectionStatistics(String client_id);

}
