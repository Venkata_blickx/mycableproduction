package com.blickx.services;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;

import com.blickx.dao.DeviceDao;
import com.blickx.domain.DeviceEntity;
import com.blickx.output.DeviceLocationTo;
import com.blickx.searchEntities.SearchDevice;
import com.blickx.to.DeviceHistoryTo;
import com.blickx.to.DeviceNamesTo;
import com.blickx.to.DeviceTo;

public class DeviceServiceImpl implements DeviceService{

	@Autowired
	DeviceDao deviceDao;
	
	@Override
	public String addDevice(DeviceEntity device) {
		// TODO Auto-generated method stub
		return deviceDao.addDevice(device);
	}
	
	public String updateDevice(DeviceEntity device) {
		
		// TODO Auto-generated method stub
		return deviceDao.updateDevice(device);
	}

	@Override
	public DeviceEntity getDeviceById(String device_id) {
		// TODO Auto-generated method stub
		return deviceDao.getDeviceById(device_id);
	}

	@Override
	public List<DeviceEntity> getDeviceByIdAndName(String device_id,
			String device_name) {
		// TODO Auto-generated method stub
		return deviceDao.getDeviceByIdAndName(device_id,device_name);
	}

	@Override
	public List<DeviceEntity> getDeviceByName(String device_name) {
		// TODO Auto-generated method stub
		return deviceDao.getDeviceByName(device_name);
	}

	@Override
	public List<DeviceTo> searchDevices(SearchDevice searchDevice) {
		// TODO Auto-generated method stub
		return deviceDao.searchDevices(searchDevice);
	}

	@Override
	public String deleteDevice(String device_id) {
		// TODO Auto-generated method stub
		return deviceDao.deleteDevice(device_id);
	}

	@Override
	public String activateStatus(int device_id) {
		// TODO Auto-generated method stub
		return deviceDao.activateStatus(device_id);
	}
	
	@Override
	public String deActivateStatus(int device_id) {
		// TODO Auto-generated method stub
		return deviceDao.deActivateStatus(device_id);
	}

	@Override
	public List<DeviceNamesTo> getDeviceNames(String client_id) {
		// TODO Auto-generated method stub
		return deviceDao.getDeviceNames(client_id);
	}

	@Override
	public List<DeviceTo> getInactiveDevices(String client_id) {
		// TODO Auto-generated method stub
		return deviceDao.getInactiveDevices(client_id);
	}

	@Override
	public List<DeviceHistoryTo> getDeviceHistory(int device_id) {
		// TODO Auto-generated method stub
		return deviceDao.getDeviceHistory(device_id);
	}

	@Override
	public DeviceEntity getDeviceDetails(String client_id) {
		// TODO Auto-generated method stub
		return deviceDao.getDeviceDetails(client_id);
	}

	@Override
	public void updateDeviceLocation(String imeNumber, String longitude,
			String lattitude) {
		// TODO Auto-generated method stub
		deviceDao.updateDeviceLoaction(imeNumber, longitude, lattitude);
		
	}

	@Override
	public List<DeviceLocationTo> getDeviceLocation(String client_id) {
		// TODO Auto-generated method stub
		return deviceDao.getDeviceLocation(client_id);
	}

}
