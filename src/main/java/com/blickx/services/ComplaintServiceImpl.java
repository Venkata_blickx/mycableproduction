package com.blickx.services;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.apache.log4j.Logger;
import org.hibernate.HibernateException;
import org.springframework.beans.factory.annotation.Autowired;

import com.blickx.controller.HomeController;
import com.blickx.dao.ComplaintsDao;
import com.blickx.domain.ComplaintEntity;
import com.blickx.mycable.beme.dto.ComplaintBemeInputDTO;
import com.blickx.searchEntities.AssignComplaintSearch;
import com.blickx.searchEntities.CloseComplaint;
import com.blickx.searchEntities.GetComplaint;
import com.blickx.searchEntities.SearchByDateEntity;
import com.blickx.to.ComplaintRatingObject;
import com.blickx.to.ComplaintRatingTo;
import com.blickx.to.ComplaintStatusTo;
import com.blickx.to.ComplaintSummaryTo;
import com.blickx.to.GetComplaintTo;
import com.blickx.to.GetOpenClosedComplaintTo;
import com.blickx.to.input.ComplaintInputTo;

public class ComplaintServiceImpl implements ComplaintService {

	private Logger logger = Logger.getLogger(HomeController.class);
	
	@Autowired
	ComplaintsDao complaintDao;
	
	@Override
	public String addComplaint(ComplaintEntity complaint) {
		// TODO Auto-generated method stub
		return complaintDao.addComplaint(complaint);
	}

	@Override
	public ComplaintEntity getComplaint(GetComplaint getComplaint) {
		// TODO Auto-generated method stub
		return complaintDao.getComplaint(getComplaint);
	}

	@Override
	public List<GetOpenClosedComplaintTo> getOpenOrClosedComplaints(String client_id, String type) {
		// TODO Auto-generated method stub
		return complaintDao.getOpenOrClosedComplaints(client_id, type);
	}

	@Override
	public List<GetOpenClosedComplaintTo> getOpenOrClosedComplaintsByDate(SearchByDateEntity dates) {
		// TODO Auto-generated method stub
		return complaintDao.getOpenOrClosedComplaintsByDate(dates);
	}

	@Override
	public Map<String, String> updateComplaint(ComplaintEntity complaint) {
		// TODO Auto-generated method stub
		Map<String, String> response = new HashMap<String, String>();
		try{
			complaintDao.updateComplaint(complaint);
			response.put("MSG", "Updated Successfully..");
		}catch(HibernateException e){
			logger.error(e);
			response.put("MSG", e.toString());
		}
		return response;
	}

	@Override
	public ComplaintStatusTo getComplaintStatus(String client_id) {
		// TODO Auto-generated method stub
		return complaintDao.getComplaintStatus(client_id);
	}

	@Override
	public List<ComplaintSummaryTo> getComplaintSummaryOpen(String client_id) {
		// TODO Auto-generated method stub
		return complaintDao.getComplaintSummaryOpen(client_id);
	}

	@Override
	public int makeAssign(AssignComplaintSearch assign) {
		// TODO Auto-generated method stub
		return complaintDao.makeAssign(assign);
	}

	@Override
	public int closeComplaint(CloseComplaint closeComplaint) {
		// TODO Auto-generated method stub
		return complaintDao.closeComplaint(closeComplaint);
	}

	@Override
	public ComplaintRatingObject getComplaintRating(String client_id) {
		// TODO Auto-generated method stub
		return complaintDao.getComplaintRating(client_id);
	}
	
	@Override
	public String addComplaintDesktop(ComplaintInputTo complaint) {
		return complaintDao.addComplaintDesktop(complaint);
		
	}

	@Override
	public List<String> getC_ids(String client_id) {
		// TODO Auto-generated method stub
		return complaintDao.getC_ids(client_id);
	}

	@Override
	public List<com.blickx.output.GetComplaintList> GetComplaintList(
			String client_id, Map<String, String> input) {
		// TODO Auto-generated method stub
		return complaintDao.GetComplaintList(client_id, input);
	}

	@Override
	public List<com.blickx.output.GetComplaintList> GetUpdatedComplaintList(
			String client_id, Map<String, String> input) {
		// TODO Auto-generated method stub
		return complaintDao.GetUpdatedComplaintList(client_id, input);
	}

	@Override
	public Integer resolveComplaint(CloseComplaint closeComplaint) {
		// TODO Auto-generated method stub
		return complaintDao.resolveComplaint(closeComplaint);
	}

	/*@Override
	public String addComplaintBeeMe(ComplaintBemeInputDTO complaintTo) {
		// TODO Auto-generated method stub
		return complaintDao.addComplaintBeeMe(complaintTo);
	}*/

	
}
