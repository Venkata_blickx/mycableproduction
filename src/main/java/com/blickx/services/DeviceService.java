package com.blickx.services;

import java.util.List;

import com.blickx.domain.DeviceEntity;
import com.blickx.domain.SelectedDevice;
import com.blickx.output.DeviceLocationTo;
import com.blickx.searchEntities.SearchDevice;
import com.blickx.to.DeviceHistoryTo;
import com.blickx.to.DeviceNamesTo;
import com.blickx.to.DeviceTo;

public interface DeviceService {

	public String addDevice(DeviceEntity device);
	
	public String updateDevice(DeviceEntity device);

	public DeviceEntity getDeviceById(String device_id);
	
	public DeviceEntity getDeviceDetails(String client_id);

	public List<DeviceEntity> getDeviceByIdAndName(String device_id, String device_name);

	public List<DeviceEntity> getDeviceByName(String device_name);

	public List<DeviceTo> searchDevices(SearchDevice searchDevice);

	public String deleteDevice(String device_id);

	public String activateStatus(int device_id);

	public String deActivateStatus(int device_id);

	public List<DeviceNamesTo> getDeviceNames(String client_id);

	public List<DeviceTo> getInactiveDevices(String client_id);

	public List<DeviceHistoryTo> getDeviceHistory(int device_id);
	
	public void updateDeviceLocation(String imeNumber, String longitude, String lattitude);

	public List<DeviceLocationTo> getDeviceLocation(String client_id);

}
