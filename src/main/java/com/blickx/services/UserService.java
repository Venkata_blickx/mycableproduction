package com.blickx.services;

public interface UserService {

	public String checkUser(String username,String password, String ime_number);

	public String logout(String username, String ime_number);

	public String loginWithImeAndPassword(String ime_number, String password);
}
