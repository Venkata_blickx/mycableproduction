package com.blickx.services;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;

import com.blickx.dao.EmployeeDao;
import com.blickx.domain.AreaEmployeeMapEntity;
import com.blickx.domain.Employee;
import com.blickx.searchEntities.SearchEmployee;
import com.blickx.to.EmployeAddOrUpdateTo;
import com.blickx.to.GetEmployeeCollectionTo;
import com.blickx.to.GetEmployeeNamesWithIds;

public class EmployeeServiceImpl implements EmployeeService {

	@Autowired
	EmployeeDao empdao;
	
	@Override
	public String addEmployee(EmployeAddOrUpdateTo employee) {
		// TODO Auto-generated method stub
		Employee empEntity = new Employee();
		BeanUtils.copyProperties(employee, empEntity);
		Date date =  new Date();
		List<AreaEmployeeMapEntity> areaEmployeeMapList = new ArrayList<AreaEmployeeMapEntity>();
		
		for(Integer areaCodeId: employee.getAreaCodes()){
			AreaEmployeeMapEntity areaEmployeeMap = new AreaEmployeeMapEntity();
			
			areaEmployeeMap.setAreaId(areaCodeId);
			areaEmployeeMap.setStartDate(date);
			areaEmployeeMap.setCreatedTimeStamp(date);
			areaEmployeeMap.setLastUpdatedTimestamp(date);
			
			areaEmployeeMapList.add(areaEmployeeMap);
		}
			
		return empdao.addEmployee(empEntity, areaEmployeeMapList);
	}

	@Override
	public EmployeAddOrUpdateTo getEmployeeById(String employee_id) {
		// TODO Auto-generated method stub
		return empdao.getEmployeeById(employee_id);
	}

	@Override
	public List<Employee> getEmpByIdAndName(String employee_id,
			String employee_name) {
		// TODO Auto-generated method stub
		return empdao.getEmployeeByIdAndName(employee_id,employee_name);
	}

	@Override
	public List<Employee> getEmpByName(String employee_name) {
		// TODO Auto-generated method stub
		return empdao.getEmpByName(employee_name);
	}

	@Override
	public String updateEmployee(EmployeAddOrUpdateTo employeeInput) {
		// TODO Auto-generated method stub
		
		Employee  employee = new Employee();
		BeanUtils.copyProperties(employeeInput, employee);
		
		return empdao.updateEmployee(employee, employeeInput);
	}

	@Override
	public String activateStatus(String employee_id) {
		// TODO Auto-generated method stub
		return empdao.activateStatus(employee_id);
	}
	
	@Override
	public String deactivateStatus(String employee_id) {
		// TODO Auto-generated method stub
		return empdao.deactivateStatus(employee_id);
	}

	@Override
	public List<Employee> searchEmployees(SearchEmployee searchEmployee) {
		// TODO Auto-generated method stub
		return empdao.searchEmployee(searchEmployee);
	}

	@Override
	public List<GetEmployeeCollectionTo> getEmployeeCollection(
			String employee_id) {
		// TODO Auto-generated method stub
		return empdao.getEmployeeCollection(employee_id);
	}

	@Override
	public List<String> getEmployeeCollection() {
		// TODO Auto-generated method stub
		return empdao.getEmployeeNames();
	}

	@Override
	public List<String> getEmployeeNamesToAssignComplaints(String client_id) {
		// TODO Auto-generated method stub
		return empdao.getEmployeeNamesToAssignComplaints(client_id);
	}

	@Override
	public List<Employee> getInactiveEmployees(String client_id) {
		// TODO Auto-generated method stub
		return empdao.getInactiveEmployees(client_id);
	}

	@Override
	public String changePassword(String employee_id, String password) {
		// TODO Auto-generated method stub
		return empdao.changePassword(employee_id, password);
	}

	@Override
	public List<GetEmployeeNamesWithIds> getEmployeeNamesWithIds(String client_id) {
		// TODO Auto-generated method stub
		return empdao.getEmployeeNamesWithIds(client_id);
	}

	@Override
	public int uidUpdate(String employee_id, String uid_number) {
		// TODO Auto-generated method stub
		return empdao.uidUpdate(employee_id, uid_number);
	}
	
	@Override
	public String getEmployee_id(String uid_number) {
		// TODO Auto-generated method stub
		return empdao.getEmployee_id(uid_number);
	}

	@Override
	public Employee Unique_Employee_Details(String client_id) {
		// TODO Auto-generated method stub
		return empdao.Unique_Employee_Details(client_id);
	}

}
