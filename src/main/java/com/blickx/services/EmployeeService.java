package com.blickx.services;

import java.util.List;

import com.blickx.domain.Employee;
import com.blickx.searchEntities.SearchEmployee;
import com.blickx.to.EmployeAddOrUpdateTo;
import com.blickx.to.GetEmployeeCollectionTo;
import com.blickx.to.GetEmployeeNamesWithIds;

public interface EmployeeService {

	public String addEmployee(EmployeAddOrUpdateTo employee);

	public EmployeAddOrUpdateTo getEmployeeById(String employee_id);

	public List<Employee> getEmpByIdAndName(String employee_id, String employee_name);

	public List<Employee> getEmpByName(String employee_name);

	public String updateEmployee(EmployeAddOrUpdateTo employee);

	public String activateStatus(String employee_id);
	
	public String deactivateStatus(String employee_id);

	public List<Employee> searchEmployees(SearchEmployee searchEmployee);

	public List<GetEmployeeCollectionTo> getEmployeeCollection(
			String employee_id);

	public List<String> getEmployeeCollection();

	public List<String> getEmployeeNamesToAssignComplaints(String client_id);

	public List<Employee> getInactiveEmployees(String client_id);

	public String changePassword(String employee_id, String password);

	public List<GetEmployeeNamesWithIds> getEmployeeNamesWithIds(String client_id);

	public int uidUpdate(String employee_id, String uid_number);

	String getEmployee_id(String uid_number);

	public Employee Unique_Employee_Details(String client_id);
	

}
