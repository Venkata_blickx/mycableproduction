package com.blickx.services;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;
import java.util.Map;
import java.util.Set;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;

import com.blickx.copyitems.to.objects.GetPaymentTxDTO;
import com.blickx.dao.CustomerDao;
import com.blickx.dao.SettingsDao;
import com.blickx.daoImpl.TransactionDao;
import com.blickx.domain.BillGeneration;
import com.blickx.domain.Customer;
import com.blickx.domain.CustomerToMobile;
import com.blickx.domain.PackageEntity;
import com.blickx.domain.SetupBox;
import com.blickx.exceptions.CustomerPaidException;
import com.blickx.exceptions.PackageEmptyException;
import com.blickx.exceptions.SetupBoxEmpty;
import com.blickx.mycable.customerAddElements.CustomerAddInputTo;
import com.blickx.mycable.customerAddElements.ManipulateCustomerDataforUpdateCustomer;
import com.blickx.output.wrapper.UpdateContactResponse;
import com.blickx.output.wrapper.UpdateContactResponseTO;
import com.blickx.response.PackagesAndAddonsOfCustomer;
import com.blickx.searchEntities.PaymentFields;
import com.blickx.searchEntities.SearchCustomer;
import com.blickx.settings.to.AddonsEntityTo;
import com.blickx.to.CustomerBillTo;
import com.blickx.to.CustomerTo;
import com.blickx.to.CustomerWrapperTo;
import com.blickx.to.MiniStatementForAndroidTo;
import com.blickx.to.PackageReportsForAndroidTo;
import com.blickx.to.PaymentTxEntityDTO;
import com.blickx.to.SearchCustomerMoreDetails;
import com.blickx.to.SearchCustomerTo;
import com.blickx.to.SetUpBoxToo;
import com.blickx.to.SetupBoxTo;
import com.blickx.to.ViewBillsByMonthTo;
import com.blickx.to.input.ContactUpdateTo;
import com.blickx.to.input.CustomerCredentials;
import com.blickx.to.input.GetCustomer;
import com.blickx.to.input.InputIds;
import com.blickx.to.input.UidInsertInput;
import com.blickx.tx.mailing.DetailsToSend;
import com.blickx.tx.mailing.SendMailRunnable;
import com.blickx.utill.BillGenMessage;
import com.blickx.utill.BillGenType;
import com.blickx.utill.CurrentDate;
import com.blickx.utill.CusHistoryDto;
import com.blickx.utill.CustomerHistoryDto;
import com.blickx.wrapper.UpdateContactNumberWrapper;

public class CustomerServiceImpl implements CustomerService {
	
	@Autowired
	CustomerDao cdao;
	
	@Autowired
	SettingsDao settingsDao;
	
	@Autowired
	TransactionDao tdao;
	
	private Logger logger = Logger.getLogger(CustomerServiceImpl.class);
	@Override
	public String updateCustomer(CustomerAddInputTo input) {
		String res = null;
		try{
			
			// Customer Profile Update
			
			//BeanUtils.copyProperties(ManipulateCustomerData.prepareCustomerData(customerData, input), customerUpdate);
			res = cdao.updateCustomer(new ManipulateCustomerDataforUpdateCustomer().getCustomerTo(input, cdao), input.getComments());
			//settingsDao.saveOrUpdateComments(input.getComments(), input.getClient_id());

			// SetTopBox Update
			
		}catch(Exception e){
			logger.error(e);
			return res;
		}
		 return res;
	}
	
	@Override
	public CustomerAddInputTo getCustomerProfileById(String customer_id) {
		
		return cdao.getCustomerProfileById(customer_id);
		
	}
	
	public CustomerTo getUniqueCustomerProfile(String client_id){
		
		return cdao.getUniqueCustomerProfile(client_id);
	}
	
	public String addCustomer(String client_id, GetCustomer customer) {
		
		return cdao.addCustomer(client_id, customer);
	}

	@Override
	public List<Customer> getCustById(String custId) {
		
		return cdao.getCustById(custId);
		
	}

	@Override
	public int deleteCustomer(String customer_id) {
		
		return cdao.deleteCustomer(customer_id);
	}

	@Override
	public CustomerTo getCustomerById(String customer_id) {
		
		return cdao.getCustomerById(customer_id);
	}

	@Override
	public String activateStatus(InputIds input) {
		
		return cdao.activateStatus(input);
	}

	public String deActivateStatus(InputIds input) {
		
		return cdao.deActivateStatus(input);
	}

	@Override
	public CustomerWrapperTo searchCustomers(SearchCustomer searchCustomer) {
		// TODO Auto-generated method stub
		return cdao.searchCustomers(searchCustomer);
	}
	
	@Override
	public CustomerToMobile getCustomerWithBillingDetails(String customer_id){
		return cdao.getCustomerWithBillingDetails(customer_id);
	}

	@Override
	public String billPayment(String customer_id, double paid, double balance,
			double total, String employee_id, String client_id) {
		return cdao.billPayment(customer_id, paid, balance, total, employee_id, client_id);
	}

	@Override
	public CustomerBillTo getCustomerWithBillingDetails(
			CustomerTo customerCredentials) {
		// TODO Auto-generated method stub
		return cdao.getCustomerWithBillingDetails(customerCredentials);
	}

	@Override
	public List<MiniStatementForAndroidTo> miniStatement(String customer_id) {
		// TODO Auto-generated method stub
		return cdao.miniStatement(customer_id);
	}

	@Override
	public List<PackageReportsForAndroidTo> getPackageReports() {
		// TODO Auto-generated method stub
		return cdao.getPackageReports();
	}

	@Override
	public List<SearchCustomerTo> getDeactivatedCustomers(String client_id) {
		// TODO Auto-generated method stub
		return cdao.getDeactivatedCustomers(client_id);
	}

	@Override
	public List<SearchCustomerTo> getUnPaidCustomers(String client_id) {
		// TODO Auto-generated method stub
		return cdao.getUnPaidCustomers(client_id);
	}

	@Override
	public String paymentFromDesktop(PaymentFields paymentfields) {
		
			return cdao.makePaymentFromDesktop(paymentfields);
	}
	
	public boolean billPayments(String customer_id, String employee_id, double paid, double balance, double total, String client_id, String paymentTimeStamp, String transactionId) {
		return cdao.billPayments(customer_id, employee_id, paid, balance, total, client_id, paymentTimeStamp, transactionId);
	}

	@Override
	public ViewBillsByMonthTo viewBillsByMonth(CustomerCredentials customerCredentials) {
		// TODO Auto-generated method stub
		return cdao.viewBillsByMonth(customerCredentials);
	}

	@Override
	public String updateBox(SetupBoxTo setupBoxto) {
		// TODO Auto-generated method stub
		return cdao.updateBox(setupBoxto);
	}

	@Override
	public int addExtraSetupBox(SetupBoxTo setupBoxTo) {
		// TODO Auto-generated method stub
		return cdao.addExtraBox(setupBoxTo);
	}

	@Override
	public List<String> getCustomerDocs(String customer_id) {
		// TODO Auto-generated method stub
		return cdao.getCustomerDocs(customer_id);
	}

	@Override
	public int deActivateBox(InputIds input) {
		// TODO Auto-generated method stub
		return cdao.deActivateBox(input);
	}
	
	@Override
	public int activateBox(InputIds input) {
		// TODO Auto-generated method stub
		return cdao.activateBox(input);
	}

	@Override
	public List<SearchCustomerTo> searchCustomerTo(String client_id, SearchCustomer searchCustomer) {
		// TODO Auto-generated method stub
		return cdao.searchCustomerTo(client_id, searchCustomer);
	}

	@Override
	public int uidUpdate(UidInsertInput uidInsertInput) {
		// TODO Auto-generated method stub
		return cdao.uidUpdate(uidInsertInput);
	}

	@Override
	public int deleteSetupBox(String box_number) {
		// TODO Auto-generated method stub
		return cdao.deleteSetupBox(box_number);
	}

	@Override
	public String getCustomer_id(String uid_number, String client_id) {
		// TODO Auto-generated method stub
		return cdao.getCustomer_id(uid_number, client_id);
	}

	@Override
	public int permenentDeActive(InputIds input) {
		// TODO Auto-generated method stub
		return cdao.permenentDeActive(input);
	}

	@Override
	public String updateAddon(AddonsEntityTo addon, String client_id) {
		// TODO Auto-generated method stub
		return cdao.updateAddon(addon, client_id);
	}

	@Override
	public String deleteAddon(int addonId, String client_id) {
		// TODO Auto-generated method stub
		return cdao.deleteAddon(addonId, client_id);
	}

	@Override
	public SetUpBoxToo getSettopBoxByBoxNumber(String boxNumber) {
		// TODO Auto-generated method stub
		return cdao.getSettopBoxByBoxNumber(boxNumber);
	}

	@Override
	public String addAddon(Set<AddonsEntityTo> addOnEntityTo, String client_id) {
		// TODO Auto-generated method stub
		return cdao.addAddons(addOnEntityTo, client_id);
	}

	@Override
	public List<AddonsEntityTo> getTheSetupboxAddons(String boxNumber) {
		// TODO Auto-generated method stub
		return cdao.getTheSetupboxAddons(boxNumber);
	}
	
	@Override
	public List<PackagesAndAddonsOfCustomer> getTheCustomerAddons(String customer_id, String client_id) {
		// TODO Auto-generated method stub
		return cdao.getTheCustomerAddons(customer_id, client_id);
	}

	@Override
	public List<SetUpBoxToo> getTheSetupboxesByCustomerId(String customer_id) {
		// TODO Auto-generated method stub
		return cdao.getTheSetupboxesByCustomerId(customer_id);
	}

	@Override
	public String getCustomerIDUsingCid(String client_id,
			SearchCustomer searchCustomer) {
		// TODO Auto-generated method stub
		return cdao.getCustomerIDUsingCid(client_id, searchCustomer);
	}

	@Override
	public UpdateContactResponseTO updateContactNumber(
			UpdateContactNumberWrapper input) {
		// TODO Auto-generated method stub
		UpdateContactResponseTO response = new UpdateContactResponseTO();
		
		if(input.getUpdateContactTOs().size() > 0){
			List<UpdateContactResponse> updateList = new ArrayList<UpdateContactResponse>();
			for(ContactUpdateTo data:input.getUpdateContactTOs()){
				UpdateContactResponse individualResponse = new UpdateContactResponse();
				individualResponse.setNewContactNumber(data.getNewMobileNumber());
				individualResponse.setCustomerId(data.getCustomerId());
				individualResponse.setStatus(cdao.updateContactNumber(data));
				updateList.add(individualResponse);
			}
			response.setUpdateContactResponse(updateList);
			logger.info("#######################"+response);
			return response;
		}else{
			return response;
		}
	}

	@Override
	public UpdateContactResponseTO updateEmail(
			UpdateContactNumberWrapper input) {
		// TODO Auto-generated method stub
		UpdateContactResponseTO response = new UpdateContactResponseTO();
		
		if(input.getUpdateContactTOs().size() > 0){
			List<UpdateContactResponse> updateList = new ArrayList<UpdateContactResponse>();
			for(ContactUpdateTo data:input.getUpdateContactTOs()){
				UpdateContactResponse individualResponse = new UpdateContactResponse();
				individualResponse.setNewContactNumber(data.getNewMobileNumber());
				individualResponse.setCustomerId(data.getCustomerId());
				individualResponse.setStatus(cdao.updateEmail(data));
				updateList.add(individualResponse);
			}
			response.setUpdateContactResponse(updateList);
			logger.info("Email Update Wrapper updated Response: "+response);
			return response;
		}else{
			return response;
		}
		
	}

	@Override
	public int setupboxPermenantDeActivate(InputIds inputIds) {
		// TODO Auto-generated method stub
		return cdao.setupboxPermenantDeActivate(inputIds);
	}

	@Override
	public void saveOnlineTxDetails(Map<String, String> input) throws ParseException {
		// TODO Auto-generated method stub
		PaymentTxEntityDTO pDTO = new PaymentTxEntityDTO();
		pDTO.setResponseCode(5);  // responseCode : 5 is our status code to represent the tx is not updated with the EBS response code
									// 0 - is for success tx &&  1 - Error in EBS && 2 - Cancelled by user
		pDTO.setMerchantRefNo(input.get("MerchantRefNo"));
		pDTO.setCustomer_id(input.get("Customer_Id"));
		pDTO.setAmount(input.get("Amount"));
		pDTO.setCreatedDate(new Date());
		pDTO.setClient_id(input.get("Customer_Id").substring(0, 8));
		tdao.saveOnlineTxDetails(pDTO);
	}

	@Override
	public void updateTxAfterEBSResponse(Map<String, String> input) throws ParseException {
		
		System.out.println("DEBUGGING **************** INPUT IS ******************************"+input);
		
		try {
		PaymentTxEntityDTO payDto = GetPaymentTxDTO.getPaymentTxDTO(input);
		
		System.out.println("DEBUGGING **************** PAY DTO IS ******************************"+payDto);
		
		if(tdao.checkTxEligible(payDto)) { 
		
		// MyCable System Payment 	
			tdao.updateTxAfterEBSResponse(payDto);
			tdao.updateEmailIfNotExists(payDto);
			//payDto.setResponseCode(0);
			if(payDto.getResponseCode() == 0) {
				
				DetailsToSend details = tdao.getDetailsToSendEmail(payDto);
				CustomerTo custIdDetails = new CustomerTo();
				custIdDetails.setCustomer_id(payDto.getCustomer_id());
				details.setTotalNeedtoPay(cdao.getCustomerWithBillingDetails(custIdDetails).getTotal_amount().toString());
				
				String payTimeStamp = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss").format(new Date());
				
				if(payDto.getCreatedDate() != null){
					payTimeStamp = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss").format(payDto.getCreatedDate());
				}
				
				
		    System.out.println("DEBUGGING **************** PAYMENT TIMESTAMP IS ******************************"+payTimeStamp);
				
			cdao.billPayments(payDto.getCustomer_id(), "Online", Double.parseDouble(payDto.getAmount()), 0.0, 0.0, 
					payDto.getCustomer_id().substring(0, 8), payTimeStamp , payDto.getMerchantRefNo());
			
			
			
			SendMailRunnable sendMailThread = new SendMailRunnable(details);
			Thread t = new Thread(sendMailThread);
			t.start();
			} else {
				logger.info(" Payment Request Code is not successfull, ResponseCode: " + payDto.getResponseCode() + "\t" + payDto.getResponseMsg());
			}
		}
		} catch(Exception e){
			logger.error("CustomerServiceIMpl- update Tx entyei Catch block : " + e);
		}
		/*DetailsToSend details = tdao.getDetailsToSendEmail(payDto);
		CustomerTo custIdDetails = new CustomerTo();
		custIdDetails.setCustomer_id(payDto.getCustomer_id());
		details.setTotalNeedtoPay(cdao.getCustomerWithBillingDetails(custIdDetails).getTotal_amount().toString());
		
		SendMailRunnable sendMailThread = new SendMailRunnable(details);
		Thread t = new Thread(sendMailThread);
		t.start();*/
		
		//.out.println(new SendAnPayment	EmailToCustomer().sendAnEmail(details).getStatus());
		
	}

	@Override
	public List<SearchCustomerTo> permenentDeactiveList(String client_id) {
		// TODO Auto-generated method stub
		return cdao.permenentDeactiveList(client_id);
	}

	@Override
	public List<SearchCustomerMoreDetails> getCustomerMoreDetails(
			String customerId) {
		return cdao.getCustomerMoreDetails(customerId);
	}

	@Override
	public BillGenMessage customBillGen(BillGenType billGen, String clientId) {
			BillGenMessage billStatus = new BillGenMessage();
		try{
		List<SetupBox> setupList = cdao.getActiveSetupBoxList(billGen.getCustomerId(),clientId);
		if(setupList.isEmpty())isSetupBoxEmpty();
		
		if(billGen.getBillType() == 1){
		//PreviousMonthBill --> PP
			 if(cdao.checkCustomerPaidStatus(billGen.getCustomerId(),clientId))isCustomerPaid();
			for(SetupBox setup:setupList){
			PackageEntity pa = cdao.getSetupBoxPackageDetails(setup);
			if(pa == null)isPackageEmpty();
			logger.info(pa.getPackage_id());
			if(cdao.generateBillCurrentMonth(billGen,setup,pa)) billStatus.setBillGenStatus("BillGenration Successfully Completed:");
			else billStatus.setBillGenStatus("BillGeneration Not Successfully Completed:");
			}
			for(SetupBox setup:setupList){
				PackageEntity pa = cdao.getSetupBoxPackageDetails(setup);
				if(pa == null)isPackageEmpty();
				logger.info(pa.getPackage_id());
				BillGeneration bg = (BillGeneration)isBillAlreadyGenerated(billGen,setup,pa);
				if(bg!=null)		billStatus.setBillGenStatus("BillGenration Successfully Completed:");
				else	billStatus.setBillGenStatus("BillGeneration Not Successfully Completed:");
			}
		}
		
		if(billGen.getBillType() == 0){
		//CurrentMonthBill  --> PostPaid
		for(SetupBox setup:setupList){
			PackageEntity pa = cdao.getSetupBoxPackageDetails(setup);
			if(pa == null)isPackageEmpty();
			logger.info(pa.getPackage_id());
			BillGeneration bg = (BillGeneration)isBillAlreadyGenerated(billGen,setup,pa);
			if(bg!=null){billStatus.setBillGenStatus("BillGenration Successfully Completed:");
			cdao.updateArppCustomerPreviousBilling(billGen,setup,pa,clientId);
			}
			else{billStatus.setBillGenStatus("BillGeneration Not Successfully Completed:");}
			}
		}
		
		}catch(CustomerPaidException pe){
			billStatus.setBillGenStatus("Customer Already Amount Paid");
		}
		catch(SetupBoxEmpty is){
			billStatus.setBillGenStatus("SetupBox is Empty");
		}catch (PackageEmptyException pe) {
			billStatus.setBillGenStatus("PackageId is Empty");
		}
		catch(Exception e)
		{
			e.printStackTrace();
			billStatus.setBillGenStatus("BillGen Error");
		}
		return billStatus;
	}

	

	private void isCustomerPaid()throws CustomerPaidException {
		throw new CustomerPaidException();
	}

	private BillGeneration isBillAlreadyGenerated(BillGenType billGen, SetupBox setup,
			PackageEntity pa) {
		BillGeneration status = null;
		Date curr = new Date();
		Calendar cal = Calendar.getInstance();
		cal.setTime(curr);
		int month = cal.get(Calendar.MONTH);
		int year = cal.get(Calendar.YEAR);
		//CurrentDate currDate = cdao.getCurrentDate();
		 status = cdao.getBillGenerationDetails(billGen,setup,pa,(month+1),year);
		if(status == null)
			 status = (BillGeneration)cdao.createBill(billGen,setup,pa,(month+1),year);
		
		return status;
	}

	private void isPackageEmpty() throws PackageEmptyException {
		throw new PackageEmptyException();
	}

	private void isSetupBoxEmpty()throws SetupBoxEmpty {
		
		throw new SetupBoxEmpty();
		
	}

	@Override
	public CustomerHistoryDto getCustomerHistory(BillGenType cusDetails,
			String clientId) {
		return cdao.getCustomerHistory(cusDetails,clientId);
	}

	
	
}
