package com.blickx.services;

import java.util.List;

import com.blickx.to.PDAReportDto;

public interface PDAReportService {

	List<PDAReportDto> getPDAReqport(String client_id)throws Exception;

}
