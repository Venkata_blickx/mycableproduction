package com.blickx.services;

import org.springframework.beans.factory.annotation.Autowired;

import com.blickx.dao.UserDao;

public class UserServiceImpl implements UserService {

	@Autowired
	UserDao udao;
	
	@Override
	public String checkUser(String username,String password, String ime_number) {
		
		return udao.checkUser(username,password, ime_number);
	}

	@Override
	public String logout(String username, String ime_number) {
		// TODO Auto-generated method stub
		return udao.logout(username, ime_number);
	}

	@Override
	public String loginWithImeAndPassword(String ime_number, String password) {
		// TODO Auto-generated method stub
		return udao.loginWithImeAndPassword(ime_number, password);
	}

}
