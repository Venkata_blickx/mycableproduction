package com.blickx.services;

import java.io.IOException;
import java.util.StringTokenizer;

import org.apache.commons.codec.binary.Base64;
import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.blickx.dao.UserDaoImpl;

@Service
public class AuthenticationService {
	
	@Autowired
	public SessionFactory sessionFactory;
	
	public boolean authenticate(String authCredentials) {

		if (null == authCredentials)
			return false;
		// header value format will be "Basic encodedstring" for Basic
		// authentication. Example "Basic YWRtaW46YWRtaW4="
		final String encodedUserPassword = authCredentials.replaceFirst("Basic"
				+ " ", "");
		String usernameAndPassword = null;
		try {
			byte[] decodedBytes = Base64.decodeBase64(encodedUserPassword);
			usernameAndPassword = new String(decodedBytes, "UTF-8");
			System.out.println("Auth DAta: " + usernameAndPassword);
		} catch (IOException e) {
			e.printStackTrace();
		}
		final StringTokenizer tokenizer = new StringTokenizer(
				usernameAndPassword, ":");
		final String username = tokenizer.nextToken();
		final String password = tokenizer.nextToken();

		// we have fixed the userid and password as admin
		// call some UserService/LDAP here
		/*boolean authenticationStatus = "admin".equals(username)
				&& "admin".equals(password);*/
		
		boolean authenticationStatus = new UserDaoImpl(sessionFactory).basicAuthentication(username, password);
		
		return authenticationStatus;
	}
	
	public boolean isAuthorizedUser(String username, String client_id) {

		
		boolean authenticationStatus = new UserDaoImpl(sessionFactory).basicAuthenticationAuthorisedOrNot(username, client_id);
		
		return authenticationStatus;
	}
	
	
	
	public AuthenticationService(){
		
	}
}
