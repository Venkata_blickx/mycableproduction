package com.blickx.services;

import java.util.List;

import com.blickx.channels.categories.ChannelSeraliser;
import com.blickx.domain.Channels;
import com.blickx.to.ChannelsTOO;
import com.blickx.to.ChannelsTo;
import com.blickx.to.PackageTO;
import com.blickx.to.SearchPackageTO;

public interface PackageService {
	
	public List<String> getPackageList();
	
	public int addPackage(PackageTO packageTO);
	
	public PackageTO getPackageListById(int package_id);
	
	public int updatePackage(PackageTO packages);
	
	public String deletePackage(int package_id);
	
	public String deactivatePackage(int package_id);
	
	public String activatePackage(int package_id);
	
	public List<SearchPackageTO> searchPackages(String client_id);
	
	public int addChannel(ChannelsTo channel);
	
	public List<ChannelSeraliser> getChannelNames();
	
	public ChannelsTo getChannelById(int channel_id);
	
	public int updateChannel(Channels channel);
	
	public List<ChannelsTOO> getAllChannels(String client_id);
}
