package com.blickx.services;

import com.blickx.domain.PackageEntity;
import com.blickx.mycable.customerAddElements.Items;
import com.blickx.to.AutoCId;
import com.blickx.to.DeActiveEnquiryDto;
import com.blickx.to.DeActiveEnquiryResponse;
import com.blickx.to.input.GetCustomer;

public interface EnqueryService {

	String enqueryAddCustomer(String client_id, GetCustomer customer);

	PackageEntity getPackageDetails(String client_id);

	Items getIteamDetails(String client_id);

	DeActiveEnquiryResponse addDeActiveEnquiry(String clientId,
			DeActiveEnquiryDto deactiveEnquiry);

	AutoCId getAutoCIdNumber(String clientId);

}
