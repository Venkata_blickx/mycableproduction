package com.blickx.services;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;

import com.blickx.channels.categories.ChannelSeraliser;
import com.blickx.dao.PackagDaoImpl;
import com.blickx.domain.Channels;
import com.blickx.to.ChannelDisplay;
import com.blickx.to.ChannelNamesWithCost;
import com.blickx.to.ChannelsTOO;
import com.blickx.to.ChannelsTo;
import com.blickx.to.GetExpireAddonAlertTo;
import com.blickx.to.GetTheAddonsToUpdate;
import com.blickx.to.PackageTO;
import com.blickx.to.SearchPackageTO;
import com.blickx.to.input.CustomerFetchCredetials;

public class PackageServiceImpl implements PackageService {

	@Autowired
	PackagDaoImpl pDao;
	
	public List<ChannelSeraliser> getChannelNames() {
		
		/*//getCatefories()
		//foreach(category in getCategoies){
		
		 * 
		 * 
		 * 
		 
		//
		
		channelSeralisers.add(new ChannelSeraliser("", null));
		
		for(ChannelsTOO channels:pDao.getAllChannels()){
			
			if(channels.getCategory() == "Sports"){
				
			}
		*/	
			
			
		return pDao.getChannelNames();
	}
	
	
	
	@Override
	public List<String> getPackageList() {
		// TODO Auto-generated method stub
		return pDao.getPackages();
	}
	
	public int addPackage(PackageTO packageTO) {
		// TODO Auto-generated method stub
		return pDao.addPackage(packageTO);
	}
	
	public PackageTO getPackageListById(int package_id) {
		// TODO Auto-generated method stub
		return pDao.getPackageById(package_id);
	}
	public int updatePackage(PackageTO packages) {
		// TODO Auto-generated method stub
		return pDao.updatePackage(packages);
	}
	public String deletePackage(int package_id) {
		// TODO Auto-generated method stub
		return pDao.deletePackage(package_id);
	}
	public int addChannel(ChannelsTo channel) {
		// TODO Auto-generated method stub
		return pDao.addChannel(channel);
	}
	public ChannelsTo getChannelById(int channel_id) {
		// TODO Auto-generated method stub
		return pDao.getChannelById(channel_id);
	}
	public int updateChannel(Channels channel) {
		// TODO Auto-generated method stub
		return pDao.updateChannel(channel);
	}
	public String deleteChannel(int channel_id) {
		// TODO Auto-generated method stub
		return pDao.deleteChannel(channel_id);
	}
	public String activateChannel(int channel_id) {
		// TODO Auto-generated method stub
		return pDao.activateChannel(channel_id);
	}

	public String deactivateChannel(int channel_id) {
		// TODO Auto-generated method stub
		return pDao.deactivateChannel(channel_id);
	}
	public String deactivatePackage(int package_id) {
		// TODO Auto-generated method stub
		return pDao.deactivatePackage(package_id);
	}
	public String activatePackage(int package_id) {
		// TODO Auto-generated method stub
		return pDao.activatePackage(package_id);
	}
	public List<SearchPackageTO> searchPackages(String client_id) {
		// TODO Auto-generated method stub
		return pDao.searchPackages(client_id);
	}
	public List<ChannelsTOO> getAllChannels(String client_id) {
		// TODO Auto-generated method stub
		return pDao.getAllChannels(client_id);
	}
	public List<String> getChannelsListFromPackage(int package_id) {
		// TODO Auto-generated method stub
		return pDao.getChannelsListFromPackage(package_id);
	}
	public double getPackageAmount(String package_name) {
		// TODO Auto-generated method stub
		return pDao.getPackageAmount(package_name);
	}
	public double getAddOnAmount(String addons) {
		// TODO Auto-generated method stub
		return pDao.getAddOnAmount(addons);
	}



	public List<ChannelNamesWithCost> getAllChannelNamesWithAmount(String client_id) {
		// TODO Auto-generated method stub
		return pDao.getAllChannelNamesWithAmount(client_id);
	}



	public List<String> displayChannelsOfSetupBox(String customer_id,
			String box_number) {
		// TODO Auto-generated method stub
		return pDao.displayChannelsOfSetupBox(customer_id, box_number);
	}



	public List<String> getChannelsCategories(String client_id) {
		// TODO Auto-generated method stub
		return pDao.getChannelsCategories(client_id);
	}



	public List<GetExpireAddonAlertTo> getAlertsForAddonExpiry(String client_id) {
		// TODO Auto-generated method stub
		return pDao.getAlertsForAddonExpiry(client_id);
	}



	public List<GetTheAddonsToUpdate> getTheAddonsToUpdate(
			CustomerFetchCredetials fetchCredetials) {
		// TODO Auto-generated method stub
		return pDao.getTheAddonsToUpdate(fetchCredetials);
	}
}
